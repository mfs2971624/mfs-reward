import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IDailyCommissions, DailyCommissions } from 'app/shared/model/daily-commissions.model';
import { DailyCommissionsService } from './daily-commissions.service';
import { DailyCommissionsComponent } from './daily-commissions.component';
import { DailyCommissionsDetailComponent } from './daily-commissions-detail.component';
import { DailyCommissionsUpdateComponent } from './daily-commissions-update.component';

@Injectable({ providedIn: 'root' })
export class DailyCommissionsResolve implements Resolve<IDailyCommissions> {
  constructor(private service: DailyCommissionsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDailyCommissions> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((dailyCommissions: HttpResponse<DailyCommissions>) => {
          if (dailyCommissions.body) {
            return of(dailyCommissions.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DailyCommissions());
  }
}

export const dailyCommissionsRoute: Routes = [
  {
    path: '',
    component: DailyCommissionsComponent,
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'DailyCommissions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DailyCommissionsDetailComponent,
    resolve: {
      dailyCommissions: DailyCommissionsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DailyCommissions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DailyCommissionsUpdateComponent,
    resolve: {
      dailyCommissions: DailyCommissionsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DailyCommissions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DailyCommissionsUpdateComponent,
    resolve: {
      dailyCommissions: DailyCommissionsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DailyCommissions'
    },
    canActivate: [UserRouteAccessService]
  }
];
