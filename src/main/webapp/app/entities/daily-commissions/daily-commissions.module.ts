import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsrewardSharedModule } from 'app/shared/shared.module';
import { DailyCommissionsComponent } from './daily-commissions.component';
import { DailyCommissionsDetailComponent } from './daily-commissions-detail.component';
import { DailyCommissionsUpdateComponent } from './daily-commissions-update.component';
import { DailyCommissionsDeleteDialogComponent } from './daily-commissions-delete-dialog.component';
import { dailyCommissionsRoute } from './daily-commissions.route';

@NgModule({
  imports: [MfsrewardSharedModule, RouterModule.forChild(dailyCommissionsRoute)],
  declarations: [
    DailyCommissionsComponent,
    DailyCommissionsDetailComponent,
    DailyCommissionsUpdateComponent,
    DailyCommissionsDeleteDialogComponent
  ],
  entryComponents: [DailyCommissionsDeleteDialogComponent]
})
export class MfsrewardDailyCommissionsModule {}
