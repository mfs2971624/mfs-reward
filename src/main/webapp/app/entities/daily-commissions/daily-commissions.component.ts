import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDailyCommissions } from 'app/shared/model/daily-commissions.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { DailyCommissionsService } from './daily-commissions.service';
import { DailyCommissionsDeleteDialogComponent } from './daily-commissions-delete-dialog.component';
import { TransactionService } from 'app/entities/transaction';
import { ITransaction } from 'app/shared/model/transaction.model';
import * as moment from 'moment';

@Component({
  selector: 'jhi-daily-commissions',
  templateUrl: './daily-commissions.component.html'
})
export class DailyCommissionsComponent implements OnInit, OnDestroy {
  dailyCommissions?: IDailyCommissions[];
  transactions: ITransaction[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  today = moment().format('YYYY-MM-DD');
  hideAll = true;
  hideByDate = false;

  constructor(
    protected dailyCommissionsService: DailyCommissionsService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected transactionService: TransactionService
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page || this.page || 1;

    this.dailyCommissionsService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IDailyCommissions[]>) => this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate),
        () => this.onError()
      );
    this.hideAll = true;
    this.hideByDate = false;
  }

  loadPageByToday(page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page || this.page || 1;

    this.dailyCommissionsService
      .queryByToday(this.today, {
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IDailyCommissions[]>) => this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate),
        () => this.onError()
      );
    this.hideAll = false;
    this.hideByDate = true;
  }

  ngOnInit(): void {
    console.log(this.today);
    this.handleNavigation();
    this.registerChangeInDailyCommissions();
  }

  protected handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPageByToday(pageNumber, true);
      }
    }).subscribe();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IDailyCommissions): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInDailyCommissions(): void {
    this.eventSubscriber = this.eventManager.subscribe('dailyCommissionsListModification', () => this.loadPageByToday());
  }

  delete(dailyCommissions: IDailyCommissions): void {
    const modalRef = this.modalService.open(DailyCommissionsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.dailyCommissions = dailyCommissions;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IDailyCommissions[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/daily-commissions'], {
        queryParams: {
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
    }
    this.dailyCommissions = data || [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ? 1 : 1;
  }
}
