import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDailyCommissions } from 'app/shared/model/daily-commissions.model';

type EntityResponseType = HttpResponse<IDailyCommissions>;
type EntityArrayResponseType = HttpResponse<IDailyCommissions[]>;

@Injectable({ providedIn: 'root' })
export class DailyCommissionsService {
  public resourceUrl = SERVER_API_URL + 'api/daily-commissions';

  constructor(protected http: HttpClient) {}

  create(dailyCommissions: IDailyCommissions): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dailyCommissions);
    return this.http
      .post<IDailyCommissions>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(dailyCommissions: IDailyCommissions): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dailyCommissions);
    return this.http
      .put<IDailyCommissions>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDailyCommissions>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDailyCommissions[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(dailyCommissions: IDailyCommissions): IDailyCommissions {
    const copy: IDailyCommissions = Object.assign({}, dailyCommissions, {
      lastModifiedDate:
        dailyCommissions.lastModifiedDate && dailyCommissions.lastModifiedDate.isValid()
          ? dailyCommissions.lastModifiedDate.toJSON()
          : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastModifiedDate = res.body.lastModifiedDate ? moment(res.body.lastModifiedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((dailyCommissions: IDailyCommissions) => {
        dailyCommissions.lastModifiedDate = dailyCommissions.lastModifiedDate ? moment(dailyCommissions.lastModifiedDate) : undefined;
      });
    }
    return res;
  }

  queryByToday(today: string, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDailyCommissions[]>(this.resourceUrl + '/today/' + today, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }
}
