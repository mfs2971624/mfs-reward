import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDailyCommissions } from 'app/shared/model/daily-commissions.model';
import { DailyCommissionsService } from './daily-commissions.service';

@Component({
  templateUrl: './daily-commissions-delete-dialog.component.html'
})
export class DailyCommissionsDeleteDialogComponent {
  dailyCommissions?: IDailyCommissions;

  constructor(
    protected dailyCommissionsService: DailyCommissionsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.dailyCommissionsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('dailyCommissionsListModification');
      this.activeModal.close();
    });
  }
}
