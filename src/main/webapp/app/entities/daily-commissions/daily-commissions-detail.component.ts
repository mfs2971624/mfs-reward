import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDailyCommissions } from 'app/shared/model/daily-commissions.model';

@Component({
  selector: 'jhi-daily-commissions-detail',
  templateUrl: './daily-commissions-detail.component.html'
})
export class DailyCommissionsDetailComponent implements OnInit {
  dailyCommissions: IDailyCommissions | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dailyCommissions }) => (this.dailyCommissions = dailyCommissions));
  }

  previousState(): void {
    window.history.back();
  }
}
