import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IDailyCommissions, DailyCommissions } from 'app/shared/model/daily-commissions.model';
import { DailyCommissionsService } from './daily-commissions.service';

@Component({
  selector: 'jhi-daily-commissions-update',
  templateUrl: './daily-commissions-update.component.html'
})
export class DailyCommissionsUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    agentMsisdn: [],
    lastModifiedDate: [],
    lastTransactionId: [],
    totalCashinTransactions: [],
    totalCashoutTransactions: [],
    totalIncentiveCommissions: [],
    spare1: [],
    spare2: [],
    spare3: [],
    spare4: [],
    spare5: []
  });

  constructor(
    protected dailyCommissionsService: DailyCommissionsService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dailyCommissions }) => {
      if (!dailyCommissions.id) {
        const today = moment().startOf('day');
        dailyCommissions.lastModifiedDate = today;
      }

      this.updateForm(dailyCommissions);
    });
  }

  updateForm(dailyCommissions: IDailyCommissions): void {
    this.editForm.patchValue({
      id: dailyCommissions.id,
      agentMsisdn: dailyCommissions.agentMsisdn,
      lastModifiedDate: dailyCommissions.lastModifiedDate ? dailyCommissions.lastModifiedDate.format(DATE_TIME_FORMAT) : null,
      lastTransactionId: dailyCommissions.lastTransactionId,
      totalCashinTransactions: dailyCommissions.totalCashinTransactions,
      totalCashoutTransactions: dailyCommissions.totalCashoutTransactions,
      totalIncentiveCommissions: dailyCommissions.totalIncentiveCommissions,
      spare1: dailyCommissions.spare1,
      spare2: dailyCommissions.spare2,
      spare3: dailyCommissions.spare3,
      spare4: dailyCommissions.spare4,
      spare5: dailyCommissions.spare5
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dailyCommissions = this.createFromForm();
    if (dailyCommissions.id !== undefined) {
      this.subscribeToSaveResponse(this.dailyCommissionsService.update(dailyCommissions));
    } else {
      this.subscribeToSaveResponse(this.dailyCommissionsService.create(dailyCommissions));
    }
  }

  private createFromForm(): IDailyCommissions {
    return {
      ...new DailyCommissions(),
      id: this.editForm.get(['id'])!.value,
      agentMsisdn: this.editForm.get(['agentMsisdn'])!.value,
      lastModifiedDate: this.editForm.get(['lastModifiedDate'])!.value
        ? moment(this.editForm.get(['lastModifiedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastTransactionId: this.editForm.get(['lastTransactionId'])!.value,
      totalCashinTransactions: this.editForm.get(['totalCashinTransactions'])!.value,
      totalCashoutTransactions: this.editForm.get(['totalCashoutTransactions'])!.value,
      totalIncentiveCommissions: this.editForm.get(['totalIncentiveCommissions'])!.value,
      spare1: this.editForm.get(['spare1'])!.value,
      spare2: this.editForm.get(['spare2'])!.value,
      spare3: this.editForm.get(['spare3'])!.value,
      spare4: this.editForm.get(['spare4'])!.value,
      spare5: this.editForm.get(['spare5'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDailyCommissions>>): void {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
