/* eslint-disable */
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

// @ts-ignore
@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'campagne',
        loadChildren: './campagne/campagne.module#MfsrewardCampagneModule'
      },
      {
        path: 'promotion',
        loadChildren: './promotion/promotion.module#MfsrewardPromotionModule'
      },
      {
        path: 'reward',
        loadChildren: './reward/reward.module#MfsrewardRewardModule'
      },
      {
        path: 'promo-execution-plan',
        loadChildren: './promo-execution-plan/promo-execution-plan.module#MfsrewardPromoExecutionPlanModule'
      },
      {
        path: 'transaction',
        loadChildren: './transaction/transaction.module#MfsrewardTransactionModule'
      },
      {
        path: 'transaction-reward',
        loadChildren: './transaction-reward/transaction-reward.module#MfsrewardTransactionRewardModule'
      },
      {
        path: 'partners',
        loadChildren: './partners/partner.module#MfsrewardPartnersModule'
      },
      {
        path: 'blacklisted-agents',
        loadChildren: () => import('./blacklisted-agents/blacklisted-agents.module').then(m => m.MfsrewardBlacklistedAgentsModule)
      },
      {
        path: 'daily-commissions',
        loadChildren: () => import('./daily-commissions/daily-commissions.module').then(m => m.MfsrewardDailyCommissionsModule)
      },
      {
        path: 'commission-disbursement',
        loadChildren: () =>
          import('./commission-disbursement/commission-disbursement.module').then(m => m.MfsrewardCommissionDisbursementModule)
      },
      {
        path: 'incentive-transfers',
        loadChildren: () => import('./incentive-transfers/incentive-transfers.module').then(m => m.MfsrewardIncentiveTransfersModule)
      },

      {
        path: 'subscribers-uncomes',
        loadChildren: () => import('./subscribers-uncomes/subscribers-uncomes.module').then(m => m.MfsrewardSubscribersUncomesModule)
      },
      {
        path: 'daily-commission-transactions',
        loadChildren: () =>
          import('./daily-commission-transactions/daily-commission-transactions.module').then(
            m => m.MfsrewardDailyCommissionTransactionsModule
          )
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MfsrewardEntityModule {}
