import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITransactionReward } from 'app/shared/model/transaction-reward.model';
import { TransactionRewardService } from './transaction-reward.service';

@Component({
  selector: 'jhi-transaction-reward-delete-dialog',
  templateUrl: './transaction-reward-delete-dialog.component.html'
})
export class TransactionRewardDeleteDialogComponent {
  transactionReward: ITransactionReward;

  constructor(
    protected transactionRewardService: TransactionRewardService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.transactionRewardService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'transactionRewardListModification',
        content: 'Deleted an transactionReward'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-transaction-reward-delete-popup',
  template: ''
})
export class TransactionRewardDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ transactionReward }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TransactionRewardDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.transactionReward = transactionReward;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/transaction-reward', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/transaction-reward', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
