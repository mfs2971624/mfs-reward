import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITransactionReward } from 'app/shared/model/transaction-reward.model';

@Component({
  selector: 'jhi-transaction-reward-detail',
  templateUrl: './transaction-reward-detail.component.html'
})
export class TransactionRewardDetailComponent implements OnInit {
  transactionReward: ITransactionReward;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ transactionReward }) => {
      this.transactionReward = transactionReward;
    });
  }

  previousState() {
    window.history.back();
  }
}
