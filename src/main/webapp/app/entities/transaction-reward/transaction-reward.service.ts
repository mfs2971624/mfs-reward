import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITransactionReward } from 'app/shared/model/transaction-reward.model';

type EntityResponseType = HttpResponse<ITransactionReward>;
type EntityArrayResponseType = HttpResponse<ITransactionReward[]>;

@Injectable({ providedIn: 'root' })
export class TransactionRewardService {
  public resourceUrl = SERVER_API_URL + 'api/transaction-rewards';
  public resourceUrlbyid = SERVER_API_URL + 'api/transaction-rewardsById';

  constructor(protected http: HttpClient) {}

  create(transactionReward: ITransactionReward): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(transactionReward);
    return this.http
      .post<ITransactionReward>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(transactionReward: ITransactionReward): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(transactionReward);
    return this.http
      .put<ITransactionReward>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITransactionReward>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }
  findAllbyid(id: number): Observable<EntityArrayResponseType> {
    return this.http
      .get<ITransactionReward[]>(`${this.resourceUrlbyid}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITransactionReward[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(transactionReward: ITransactionReward): ITransactionReward {
    const copy: ITransactionReward = Object.assign({}, transactionReward, {
      rewardDateTime:
        transactionReward.rewardDateTime != null && transactionReward.rewardDateTime.isValid()
          ? transactionReward.rewardDateTime.toJSON()
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.rewardDateTime = res.body.rewardDateTime != null ? moment(res.body.rewardDateTime) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((transactionReward: ITransactionReward) => {
        transactionReward.rewardDateTime = transactionReward.rewardDateTime != null ? moment(transactionReward.rewardDateTime) : null;
      });
    }
    return res;
  }
}
