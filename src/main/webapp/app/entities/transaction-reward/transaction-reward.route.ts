import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TransactionReward } from 'app/shared/model/transaction-reward.model';
import { TransactionRewardService } from './transaction-reward.service';
import { TransactionRewardComponent } from './transaction-reward.component';
import { TransactionRewardDetailComponent } from './transaction-reward-detail.component';
import { TransactionRewardUpdateComponent } from './transaction-reward-update.component';
import { TransactionRewardDeletePopupComponent } from './transaction-reward-delete-dialog.component';
import { ITransactionReward } from 'app/shared/model/transaction-reward.model';

@Injectable({ providedIn: 'root' })
export class TransactionRewardResolve implements Resolve<ITransactionReward> {
  constructor(private service: TransactionRewardService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITransactionReward> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TransactionReward>) => response.ok),
        map((transactionReward: HttpResponse<TransactionReward>) => transactionReward.body)
      );
    }
    return of(new TransactionReward());
  }
}

export const transactionRewardRoute: Routes = [
  {
    path: '',
    component: TransactionRewardComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'TransactionRewards'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TransactionRewardDetailComponent,
    resolve: {
      transactionReward: TransactionRewardResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TransactionRewards'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TransactionRewardUpdateComponent,
    resolve: {
      transactionReward: TransactionRewardResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TransactionRewards'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TransactionRewardUpdateComponent,
    resolve: {
      transactionReward: TransactionRewardResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TransactionRewards'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const transactionRewardPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TransactionRewardDeletePopupComponent,
    resolve: {
      transactionReward: TransactionRewardResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TransactionRewards'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
