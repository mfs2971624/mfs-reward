import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ITransactionReward, TransactionReward } from 'app/shared/model/transaction-reward.model';
import { TransactionRewardService } from './transaction-reward.service';
import { IReward } from 'app/shared/model/reward.model';
import { ITransaction } from 'app/shared/model/transaction.model';
import { TransactionService } from 'app/entities/transaction';
import { RewardService } from '../reward/reward.service';

@Component({
  selector: 'jhi-transaction-reward-update',
  templateUrl: './transaction-reward-update.component.html'
})
export class TransactionRewardUpdateComponent implements OnInit {
  isSaving: boolean;

  rewards: IReward[];

  transactions: ITransaction[];

  editForm = this.fb.group({
    id: [],
    rewardStatus: [],
    rewardMessage: [],
    rewardValue: [],
    rewardDateTime: [],
    rewardId: [],
    transactionId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected transactionRewardService: TransactionRewardService,
    protected rewardService: RewardService,
    protected transactionService: TransactionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ transactionReward }) => {
      this.updateForm(transactionReward);
    });
    this.rewardService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IReward[]>) => mayBeOk.ok),
        map((response: HttpResponse<IReward[]>) => response.body)
      )
      .subscribe((res: IReward[]) => (this.rewards = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.transactionService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ITransaction[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITransaction[]>) => response.body)
      )
      .subscribe((res: ITransaction[]) => (this.transactions = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(transactionReward: ITransactionReward) {
    this.editForm.patchValue({
      id: transactionReward.id,
      rewardStatus: transactionReward.rewardStatus,
      rewardMessage: transactionReward.rewardMessage,
      rewardValue: transactionReward.rewardValue,
      rewardDateTime: transactionReward.rewardDateTime != null ? transactionReward.rewardDateTime.format(DATE_TIME_FORMAT) : null,
      rewardId: transactionReward.rewardId,
      transactionId: transactionReward.transactionId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const transactionReward = this.createFromForm();
    if (transactionReward.id !== undefined) {
      this.subscribeToSaveResponse(this.transactionRewardService.update(transactionReward));
    } else {
      this.subscribeToSaveResponse(this.transactionRewardService.create(transactionReward));
    }
  }

  private createFromForm(): ITransactionReward {
    return {
      ...new TransactionReward(),
      id: this.editForm.get(['id']).value,
      rewardStatus: this.editForm.get(['rewardStatus']).value,
      rewardMessage: this.editForm.get(['rewardMessage']).value,
      rewardValue: this.editForm.get(['rewardValue']).value,
      rewardDateTime:
        this.editForm.get(['rewardDateTime']).value != null
          ? moment(this.editForm.get(['rewardDateTime']).value, DATE_TIME_FORMAT)
          : undefined,
      rewardId: this.editForm.get(['rewardId']).value,
      transactionId: this.editForm.get(['transactionId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITransactionReward>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackRewardById(index: number, item: IReward) {
    return item.id;
  }

  trackTransactionById(index: number, item: ITransaction) {
    return item.id;
  }
}
