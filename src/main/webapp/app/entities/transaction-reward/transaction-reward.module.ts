import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsrewardSharedModule } from 'app/shared';
import {
  TransactionRewardComponent,
  TransactionRewardDetailComponent,
  TransactionRewardUpdateComponent,
  TransactionRewardDeletePopupComponent,
  TransactionRewardDeleteDialogComponent,
  transactionRewardRoute,
  transactionRewardPopupRoute
} from './';

const ENTITY_STATES = [...transactionRewardRoute, ...transactionRewardPopupRoute];

@NgModule({
  imports: [MfsrewardSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TransactionRewardComponent,
    TransactionRewardDetailComponent,
    TransactionRewardUpdateComponent,
    TransactionRewardDeleteDialogComponent,
    TransactionRewardDeletePopupComponent
  ],
  entryComponents: [
    TransactionRewardComponent,
    TransactionRewardUpdateComponent,
    TransactionRewardDeleteDialogComponent,
    TransactionRewardDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MfsrewardTransactionRewardModule {}
