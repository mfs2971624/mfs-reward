import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IIncentiveTransfers, IncentiveTransfers } from 'app/shared/model/incentive-transfers.model';
import { IncentiveTransfersService } from './incentive-transfers.service';
import { IncentiveTransfersComponent } from './incentive-transfers.component';
import { IncentiveTransfersDetailComponent } from './incentive-transfers-detail.component';
import { IncentiveTransfersUpdateComponent } from './incentive-transfers-update.component';

@Injectable({ providedIn: 'root' })
export class IncentiveTransfersResolve implements Resolve<IIncentiveTransfers> {
  constructor(private service: IncentiveTransfersService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IIncentiveTransfers> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((incentiveTransfers: HttpResponse<IncentiveTransfers>) => {
          if (incentiveTransfers.body) {
            return of(incentiveTransfers.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new IncentiveTransfers());
  }
}

export const incentiveTransfersRoute: Routes = [
  {
    path: '',
    component: IncentiveTransfersComponent,
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'IncentiveTransfers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: IncentiveTransfersDetailComponent,
    resolve: {
      incentiveTransfers: IncentiveTransfersResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'IncentiveTransfers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: IncentiveTransfersUpdateComponent,
    resolve: {
      incentiveTransfers: IncentiveTransfersResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'IncentiveTransfers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: IncentiveTransfersUpdateComponent,
    resolve: {
      incentiveTransfers: IncentiveTransfersResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'IncentiveTransfers'
    },
    canActivate: [UserRouteAccessService]
  }
];
