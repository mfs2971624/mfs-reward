import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IIncentiveTransfers, IncentiveTransfers } from 'app/shared/model/incentive-transfers.model';
import { IncentiveTransfersService } from './incentive-transfers.service';
import { ICommissionDisbursement } from 'app/shared/model/commission-disbursement.model';
import { CommissionDisbursementService } from 'app/entities/commission-disbursement/commission-disbursement.service';

@Component({
  selector: 'jhi-incentive-transfers-update',
  templateUrl: './incentive-transfers-update.component.html'
})
export class IncentiveTransfersUpdateComponent implements OnInit {
  isSaving = false;
  commissiondisbursements: ICommissionDisbursement[] = [];

  editForm = this.fb.group({
    id: [],
    agentMsisdn: [],
    amount: [],
    paymentDate: [],
    paymentStatus: [],
    mobiquityResponse: [],
    spare1: [],
    spare2: [],
    spare3: [],
    spare4: [],
    spare5: [],
    commissionDisbursementId: []
  });

  constructor(
    protected incentiveTransfersService: IncentiveTransfersService,
    protected commissionDisbursementService: CommissionDisbursementService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ incentiveTransfers }) => {
      if (!incentiveTransfers.id) {
        const today = moment().startOf('day');
        incentiveTransfers.paymentDate = today;
      }

      this.updateForm(incentiveTransfers);

      this.commissionDisbursementService
        .query()
        .subscribe((res: HttpResponse<ICommissionDisbursement[]>) => (this.commissiondisbursements = res.body || []));
    });
  }

  updateForm(incentiveTransfers: IIncentiveTransfers): void {
    this.editForm.patchValue({
      id: incentiveTransfers.id,
      agentMsisdn: incentiveTransfers.agentMsisdn,
      amount: incentiveTransfers.amount,
      paymentDate: incentiveTransfers.paymentDate ? incentiveTransfers.paymentDate.format(DATE_TIME_FORMAT) : null,
      paymentStatus: incentiveTransfers.paymentStatus,
      mobiquityResponse: incentiveTransfers.mobiquityResponse,
      spare1: incentiveTransfers.spare1,
      spare2: incentiveTransfers.spare2,
      spare3: incentiveTransfers.spare3,
      spare4: incentiveTransfers.spare4,
      spare5: incentiveTransfers.spare5,
      commissionDisbursementId: incentiveTransfers.commissionDisbursementId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const incentiveTransfers = this.createFromForm();
    if (incentiveTransfers.id !== undefined) {
      this.subscribeToSaveResponse(this.incentiveTransfersService.update(incentiveTransfers));
    } else {
      this.subscribeToSaveResponse(this.incentiveTransfersService.create(incentiveTransfers));
    }
  }

  private createFromForm(): IIncentiveTransfers {
    return {
      ...new IncentiveTransfers(),
      id: this.editForm.get(['id'])!.value,
      agentMsisdn: this.editForm.get(['agentMsisdn'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      paymentDate: this.editForm.get(['paymentDate'])!.value
        ? moment(this.editForm.get(['paymentDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      paymentStatus: this.editForm.get(['paymentStatus'])!.value,
      mobiquityResponse: this.editForm.get(['mobiquityResponse'])!.value,
      spare1: this.editForm.get(['spare1'])!.value,
      spare2: this.editForm.get(['spare2'])!.value,
      spare3: this.editForm.get(['spare3'])!.value,
      spare4: this.editForm.get(['spare4'])!.value,
      spare5: this.editForm.get(['spare5'])!.value,
      commissionDisbursementId: this.editForm.get(['commissionDisbursementId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIncentiveTransfers>>): void {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICommissionDisbursement): any {
    return item.id;
  }
}
