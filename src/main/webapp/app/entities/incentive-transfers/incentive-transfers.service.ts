import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IIncentiveTransfers } from 'app/shared/model/incentive-transfers.model';

type EntityResponseType = HttpResponse<IIncentiveTransfers>;
type EntityArrayResponseType = HttpResponse<IIncentiveTransfers[]>;

@Injectable({ providedIn: 'root' })
export class IncentiveTransfersService {
  public resourceUrl = SERVER_API_URL + 'api/incentive-transfers';

  constructor(protected http: HttpClient) {}

  create(incentiveTransfers: IIncentiveTransfers): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(incentiveTransfers);
    return this.http
      .post<IIncentiveTransfers>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(incentiveTransfers: IIncentiveTransfers): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(incentiveTransfers);
    return this.http
      .put<IIncentiveTransfers>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IIncentiveTransfers>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IIncentiveTransfers[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(incentiveTransfers: IIncentiveTransfers): IIncentiveTransfers {
    const copy: IIncentiveTransfers = Object.assign({}, incentiveTransfers, {
      paymentDate:
        incentiveTransfers.paymentDate && incentiveTransfers.paymentDate.isValid() ? incentiveTransfers.paymentDate.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.paymentDate = res.body.paymentDate ? moment(res.body.paymentDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((incentiveTransfers: IIncentiveTransfers) => {
        incentiveTransfers.paymentDate = incentiveTransfers.paymentDate ? moment(incentiveTransfers.paymentDate) : undefined;
      });
    }
    return res;
  }
}
