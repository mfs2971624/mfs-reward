import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IIncentiveTransfers } from 'app/shared/model/incentive-transfers.model';

@Component({
  selector: 'jhi-incentive-transfers-detail',
  templateUrl: './incentive-transfers-detail.component.html'
})
export class IncentiveTransfersDetailComponent implements OnInit {
  incentiveTransfers: IIncentiveTransfers | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ incentiveTransfers }) => (this.incentiveTransfers = incentiveTransfers));
  }

  previousState(): void {
    window.history.back();
  }
}
