import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IIncentiveTransfers } from 'app/shared/model/incentive-transfers.model';
import { IncentiveTransfersService } from './incentive-transfers.service';

@Component({
  templateUrl: './incentive-transfers-delete-dialog.component.html'
})
export class IncentiveTransfersDeleteDialogComponent {
  incentiveTransfers?: IIncentiveTransfers;

  constructor(
    protected incentiveTransfersService: IncentiveTransfersService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.incentiveTransfersService.delete(id).subscribe(() => {
      this.eventManager.broadcast('incentiveTransfersListModification');
      this.activeModal.close();
    });
  }
}
