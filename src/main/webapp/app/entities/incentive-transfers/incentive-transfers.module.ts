import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsrewardSharedModule } from 'app/shared/shared.module';
import { IncentiveTransfersComponent } from './incentive-transfers.component';
import { IncentiveTransfersDetailComponent } from './incentive-transfers-detail.component';
import { IncentiveTransfersUpdateComponent } from './incentive-transfers-update.component';
import { IncentiveTransfersDeleteDialogComponent } from './incentive-transfers-delete-dialog.component';
import { incentiveTransfersRoute } from './incentive-transfers.route';

@NgModule({
  imports: [MfsrewardSharedModule, RouterModule.forChild(incentiveTransfersRoute)],
  declarations: [
    IncentiveTransfersComponent,
    IncentiveTransfersDetailComponent,
    IncentiveTransfersUpdateComponent,
    IncentiveTransfersDeleteDialogComponent
  ],
  entryComponents: [IncentiveTransfersDeleteDialogComponent]
})
export class MfsrewardIncentiveTransfersModule {}
