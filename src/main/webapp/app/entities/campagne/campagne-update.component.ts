import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { ICampagne, Campagne } from 'app/shared/model/campagne.model';
import { CampagneService } from './campagne.service';

@Component({
  selector: 'jhi-campagne-update',
  templateUrl: './campagne-update.component.html'
})
export class CampagneUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    nom: [null, [Validators.required]],
    dateDebut: [null, [Validators.required]],
    dateFin: [null, [Validators.required]],
    statut: []
  });

  constructor(protected campagneService: CampagneService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ campagne }) => {
      this.updateForm(campagne);
    });
  }

  updateForm(campagne: ICampagne) {
    this.editForm.patchValue({
      id: campagne.id,
      nom: campagne.nom,
      dateDebut: campagne.dateDebut != null ? campagne.dateDebut.format(DATE_TIME_FORMAT) : null,
      dateFin: campagne.dateFin != null ? campagne.dateFin.format(DATE_TIME_FORMAT) : null,
      statut: campagne.statut
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const campagne = this.createFromForm();
    if (campagne.id !== undefined) {
      this.subscribeToSaveResponse(this.campagneService.update(campagne));
    } else {
      this.subscribeToSaveResponse(this.campagneService.create(campagne));
    }
  }

  private createFromForm(): ICampagne {
    return {
      ...new Campagne(),
      id: this.editForm.get(['id']).value,
      nom: this.editForm.get(['nom']).value,
      dateDebut:
        this.editForm.get(['dateDebut']).value != null ? moment(this.editForm.get(['dateDebut']).value, DATE_TIME_FORMAT) : undefined,
      dateFin: this.editForm.get(['dateFin']).value != null ? moment(this.editForm.get(['dateFin']).value, DATE_TIME_FORMAT) : undefined,
      statut: this.editForm.get(['statut']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICampagne>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
