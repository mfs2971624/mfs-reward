import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsrewardSharedModule } from 'app/shared';
import {
  CampagneComponent,
  CampagneDetailComponent,
  CampagneUpdateComponent,
  CampagneDeletePopupComponent,
  CampagneDeleteDialogComponent,
  campagneRoute,
  campagnePopupRoute
} from './';

const ENTITY_STATES = [...campagneRoute, ...campagnePopupRoute];

@NgModule({
  imports: [MfsrewardSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CampagneComponent,
    CampagneDetailComponent,
    CampagneUpdateComponent,
    CampagneDeleteDialogComponent,
    CampagneDeletePopupComponent
  ],
  entryComponents: [CampagneComponent, CampagneUpdateComponent, CampagneDeleteDialogComponent, CampagneDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MfsrewardCampagneModule {}
