import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICampagne } from 'app/shared/model/campagne.model';

@Component({
  selector: 'jhi-campagne-detail',
  templateUrl: './campagne-detail.component.html'
})
export class CampagneDetailComponent implements OnInit {
  campagne: ICampagne;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ campagne }) => {
      this.campagne = campagne;
    });
  }

  previousState() {
    window.history.back();
  }
}
