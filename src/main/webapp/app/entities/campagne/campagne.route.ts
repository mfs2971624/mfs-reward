import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Campagne } from 'app/shared/model/campagne.model';
import { CampagneService } from './campagne.service';
import { CampagneComponent } from './campagne.component';
import { CampagneDetailComponent } from './campagne-detail.component';
import { CampagneUpdateComponent } from './campagne-update.component';
import { CampagneDeletePopupComponent } from './campagne-delete-dialog.component';
import { ICampagne } from 'app/shared/model/campagne.model';

@Injectable({ providedIn: 'root' })
export class CampagneResolve implements Resolve<ICampagne> {
  constructor(private service: CampagneService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICampagne> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Campagne>) => response.ok),
        map((campagne: HttpResponse<Campagne>) => campagne.body)
      );
    }
    return of(new Campagne());
  }
}

export const campagneRoute: Routes = [
  {
    path: '',
    component: CampagneComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Campaigns'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CampagneDetailComponent,
    resolve: {
      campagne: CampagneResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Campaigns'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CampagneUpdateComponent,
    resolve: {
      campagne: CampagneResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Campaigns'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CampagneUpdateComponent,
    resolve: {
      campagne: CampagneResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Campaigns'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const campagnePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CampagneDeletePopupComponent,
    resolve: {
      campagne: CampagneResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Campaigns'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
