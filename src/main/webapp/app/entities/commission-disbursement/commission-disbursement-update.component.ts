import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICommissionDisbursement, CommissionDisbursement } from 'app/shared/model/commission-disbursement.model';
import { CommissionDisbursementService } from './commission-disbursement.service';

@Component({
  selector: 'jhi-commission-disbursement-update',
  templateUrl: './commission-disbursement-update.component.html'
})
export class CommissionDisbursementUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    startDate: [],
    endDate: [],
    validated: [],
    validationDate: [],
    processingStatus: [],
    spare1: [],
    spare2: [],
    spare3: [],
    spare4: [],
    spare5: []
  });

  constructor(
    protected commissionDisbursementService: CommissionDisbursementService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commissionDisbursement }) => {
      if (!commissionDisbursement.id) {
        const today = moment().startOf('day');
        commissionDisbursement.startDate = today;
        commissionDisbursement.endDate = today;
        commissionDisbursement.validationDate = today;
      }

      this.updateForm(commissionDisbursement);
    });
  }

  updateForm(commissionDisbursement: ICommissionDisbursement): void {
    this.editForm.patchValue({
      id: commissionDisbursement.id,
      name: commissionDisbursement.name,
      startDate: commissionDisbursement.startDate ? commissionDisbursement.startDate.format(DATE_TIME_FORMAT) : null,
      endDate: commissionDisbursement.endDate ? commissionDisbursement.endDate.format(DATE_TIME_FORMAT) : null,
      validated: commissionDisbursement.validated,
      validationDate: commissionDisbursement.validationDate ? commissionDisbursement.validationDate.format(DATE_TIME_FORMAT) : null,
      processingStatus: commissionDisbursement.processingStatus,
      spare1: commissionDisbursement.spare1,
      spare2: commissionDisbursement.spare2,
      spare3: commissionDisbursement.spare3,
      spare4: commissionDisbursement.spare4,
      spare5: commissionDisbursement.spare5
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const commissionDisbursement = this.createFromForm();
    if (commissionDisbursement.id !== undefined) {
      this.subscribeToSaveResponse(this.commissionDisbursementService.update(commissionDisbursement));
    } else {
      this.subscribeToSaveResponse(this.commissionDisbursementService.create(commissionDisbursement));
    }
  }

  private createFromForm(): ICommissionDisbursement {
    return {
      ...new CommissionDisbursement(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      startDate: this.editForm.get(['startDate'])!.value ? moment(this.editForm.get(['startDate'])!.value, DATE_TIME_FORMAT) : undefined,
      endDate: this.editForm.get(['endDate'])!.value ? moment(this.editForm.get(['endDate'])!.value, DATE_TIME_FORMAT) : undefined,
      validated: this.editForm.get(['validated'])!.value,
      validationDate: this.editForm.get(['validationDate'])!.value
        ? moment(this.editForm.get(['validationDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      processingStatus: this.editForm.get(['processingStatus'])!.value,
      spare1: this.editForm.get(['spare1'])!.value,
      spare2: this.editForm.get(['spare2'])!.value,
      spare3: this.editForm.get(['spare3'])!.value,
      spare4: this.editForm.get(['spare4'])!.value,
      spare5: this.editForm.get(['spare5'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommissionDisbursement>>): void {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
