import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICommissionDisbursement } from 'app/shared/model/commission-disbursement.model';
import { CommissionDisbursementService } from './commission-disbursement.service';

@Component({
  templateUrl: './commission-disbursement-delete-dialog.component.html'
})
export class CommissionDisbursementDeleteDialogComponent {
  commissionDisbursement?: ICommissionDisbursement;

  constructor(
    protected commissionDisbursementService: CommissionDisbursementService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.commissionDisbursementService.delete(id).subscribe(() => {
      this.eventManager.broadcast('commissionDisbursementListModification');
      this.activeModal.close();
    });
  }
}
