import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICommissionDisbursement } from 'app/shared/model/commission-disbursement.model';

type EntityResponseType = HttpResponse<ICommissionDisbursement>;
type EntityArrayResponseType = HttpResponse<ICommissionDisbursement[]>;

@Injectable({ providedIn: 'root' })
export class CommissionDisbursementService {
  public resourceUrl = SERVER_API_URL + 'api/commission-disbursements';

  constructor(protected http: HttpClient) {}

  create(commissionDisbursement: ICommissionDisbursement): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(commissionDisbursement);
    return this.http
      .post<ICommissionDisbursement>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(commissionDisbursement: ICommissionDisbursement): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(commissionDisbursement);
    return this.http
      .put<ICommissionDisbursement>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICommissionDisbursement>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICommissionDisbursement[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(commissionDisbursement: ICommissionDisbursement): ICommissionDisbursement {
    const copy: ICommissionDisbursement = Object.assign({}, commissionDisbursement, {
      startDate:
        commissionDisbursement.startDate && commissionDisbursement.startDate.isValid()
          ? commissionDisbursement.startDate.toJSON()
          : undefined,
      endDate:
        commissionDisbursement.endDate && commissionDisbursement.endDate.isValid() ? commissionDisbursement.endDate.toJSON() : undefined,
      validationDate:
        commissionDisbursement.validationDate && commissionDisbursement.validationDate.isValid()
          ? commissionDisbursement.validationDate.toJSON()
          : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate ? moment(res.body.startDate) : undefined;
      res.body.endDate = res.body.endDate ? moment(res.body.endDate) : undefined;
      res.body.validationDate = res.body.validationDate ? moment(res.body.validationDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((commissionDisbursement: ICommissionDisbursement) => {
        commissionDisbursement.startDate = commissionDisbursement.startDate ? moment(commissionDisbursement.startDate) : undefined;
        commissionDisbursement.endDate = commissionDisbursement.endDate ? moment(commissionDisbursement.endDate) : undefined;
        commissionDisbursement.validationDate = commissionDisbursement.validationDate
          ? moment(commissionDisbursement.validationDate)
          : undefined;
      });
    }
    return res;
  }
}
