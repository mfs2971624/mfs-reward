import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsrewardSharedModule } from 'app/shared/shared.module';
import { CommissionDisbursementComponent } from './commission-disbursement.component';
import { CommissionDisbursementDetailComponent } from './commission-disbursement-detail.component';
import { CommissionDisbursementUpdateComponent } from './commission-disbursement-update.component';
import { CommissionDisbursementDeleteDialogComponent } from './commission-disbursement-delete-dialog.component';
import { commissionDisbursementRoute } from './commission-disbursement.route';

@NgModule({
  imports: [MfsrewardSharedModule, RouterModule.forChild(commissionDisbursementRoute)],
  declarations: [
    CommissionDisbursementComponent,
    CommissionDisbursementDetailComponent,
    CommissionDisbursementUpdateComponent,
    CommissionDisbursementDeleteDialogComponent
  ],
  entryComponents: [CommissionDisbursementDeleteDialogComponent]
})
export class MfsrewardCommissionDisbursementModule {}
