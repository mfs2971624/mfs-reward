import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICommissionDisbursement } from 'app/shared/model/commission-disbursement.model';

@Component({
  selector: 'jhi-commission-disbursement-detail',
  templateUrl: './commission-disbursement-detail.component.html'
})
export class CommissionDisbursementDetailComponent implements OnInit {
  commissionDisbursement: ICommissionDisbursement | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commissionDisbursement }) => (this.commissionDisbursement = commissionDisbursement));
  }

  previousState(): void {
    window.history.back();
  }
}
