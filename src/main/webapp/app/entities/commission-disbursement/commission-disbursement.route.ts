import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICommissionDisbursement, CommissionDisbursement } from 'app/shared/model/commission-disbursement.model';
import { CommissionDisbursementService } from './commission-disbursement.service';
import { CommissionDisbursementComponent } from './commission-disbursement.component';
import { CommissionDisbursementDetailComponent } from './commission-disbursement-detail.component';
import { CommissionDisbursementUpdateComponent } from './commission-disbursement-update.component';

@Injectable({ providedIn: 'root' })
export class CommissionDisbursementResolve implements Resolve<ICommissionDisbursement> {
  constructor(private service: CommissionDisbursementService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICommissionDisbursement> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((commissionDisbursement: HttpResponse<CommissionDisbursement>) => {
          if (commissionDisbursement.body) {
            return of(commissionDisbursement.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CommissionDisbursement());
  }
}

export const commissionDisbursementRoute: Routes = [
  {
    path: '',
    component: CommissionDisbursementComponent,
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'CommissionDisbursements'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CommissionDisbursementDetailComponent,
    resolve: {
      commissionDisbursement: CommissionDisbursementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'CommissionDisbursements'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CommissionDisbursementUpdateComponent,
    resolve: {
      commissionDisbursement: CommissionDisbursementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'CommissionDisbursements'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CommissionDisbursementUpdateComponent,
    resolve: {
      commissionDisbursement: CommissionDisbursementResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'CommissionDisbursements'
    },
    canActivate: [UserRouteAccessService]
  }
];
