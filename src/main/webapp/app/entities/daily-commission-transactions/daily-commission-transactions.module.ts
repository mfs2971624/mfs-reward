/* eslint-disable */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsrewardSharedModule } from 'app/shared/shared.module';
import { DailyCommissionTransactionsComponent } from './daily-commission-transactions.component';
import { DailyCommissionTransactionsDetailComponent } from './daily-commission-transactions-detail.component';
import { DailyCommissionTransactionsUpdateComponent } from './daily-commission-transactions-update.component';
import { DailyCommissionTransactionsDeleteDialogComponent } from './daily-commission-transactions-delete-dialog.component';
import { dailyCommissionTransactionsRoute } from './daily-commission-transactions.route';

@NgModule({
  imports: [MfsrewardSharedModule, RouterModule.forChild(dailyCommissionTransactionsRoute)],
  declarations: [
    DailyCommissionTransactionsComponent,
    DailyCommissionTransactionsDetailComponent,
    DailyCommissionTransactionsUpdateComponent,
    DailyCommissionTransactionsDeleteDialogComponent
  ],
  entryComponents: [DailyCommissionTransactionsDeleteDialogComponent]
})
export class MfsrewardDailyCommissionTransactionsModule {}
