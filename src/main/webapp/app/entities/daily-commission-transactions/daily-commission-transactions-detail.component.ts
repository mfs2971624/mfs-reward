/* eslint-disable */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDailyCommissionTransactions } from 'app/shared/model/daily-commission-transactions.model';

@Component({
  selector: 'jhi-daily-commission-transactions-detail',
  templateUrl: './daily-commission-transactions-detail.component.html'
})
export class DailyCommissionTransactionsDetailComponent implements OnInit {
  dailyCommissionTransactions: IDailyCommissionTransactions | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(
      ({ dailyCommissionTransactions }) => (this.dailyCommissionTransactions = dailyCommissionTransactions)
    );
  }

  previousState(): void {
    window.history.back();
  }
}
