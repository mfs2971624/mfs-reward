/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDailyCommissionTransactions } from 'app/shared/model/daily-commission-transactions.model';

type EntityResponseType = HttpResponse<IDailyCommissionTransactions>;
type EntityArrayResponseType = HttpResponse<IDailyCommissionTransactions[]>;

@Injectable({ providedIn: 'root' })
export class DailyCommissionTransactionsService {
  public resourceUrl = SERVER_API_URL + 'api/daily-commission-transactions';

  constructor(protected http: HttpClient) {}

  create(dailyCommissionTransactions: IDailyCommissionTransactions): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dailyCommissionTransactions);
    return this.http
      .post<IDailyCommissionTransactions>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(dailyCommissionTransactions: IDailyCommissionTransactions): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dailyCommissionTransactions);
    return this.http
      .put<IDailyCommissionTransactions>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDailyCommissionTransactions>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDailyCommissionTransactions[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(dailyCommissionTransactions: IDailyCommissionTransactions): IDailyCommissionTransactions {
    const copy: IDailyCommissionTransactions = Object.assign({}, dailyCommissionTransactions, {
      processDate:
        dailyCommissionTransactions.processDate && dailyCommissionTransactions.processDate.isValid()
          ? dailyCommissionTransactions.processDate.toJSON()
          : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.processDate = res.body.processDate ? moment(res.body.processDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((dailyCommissionTransactions: IDailyCommissionTransactions) => {
        dailyCommissionTransactions.processDate = dailyCommissionTransactions.processDate
          ? moment(dailyCommissionTransactions.processDate)
          : undefined;
      });
    }
    return res;
  }
}
