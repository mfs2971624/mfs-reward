/* eslint-disable */
import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IDailyCommissionTransactions, DailyCommissionTransactions } from 'app/shared/model/daily-commission-transactions.model';
import { DailyCommissionTransactionsService } from './daily-commission-transactions.service';

@Component({
  selector: 'jhi-daily-commission-transactions-update',
  templateUrl: './daily-commission-transactions-update.component.html'
})
export class DailyCommissionTransactionsUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    agentMsisdn: [],
    processDate: [],
    totalTransactionsVolume: [],
    totalCommissions: [],
    spare1: [],
    spare2: [],
    spare3: [],
    spare4: [],
    spare5: []
  });

  constructor(
    protected dailyCommissionTransactionsService: DailyCommissionTransactionsService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dailyCommissionTransactions }) => {
      if (!dailyCommissionTransactions.id) {
        const today = moment().startOf('day');
        dailyCommissionTransactions.processDate = today;
      }

      this.updateForm(dailyCommissionTransactions);
    });
  }

  updateForm(dailyCommissionTransactions: IDailyCommissionTransactions): void {
    this.editForm.patchValue({
      id: dailyCommissionTransactions.id,
      agentMsisdn: dailyCommissionTransactions.agentMsisdn,
      processDate: dailyCommissionTransactions.processDate ? dailyCommissionTransactions.processDate.format(DATE_TIME_FORMAT) : null,
      totalTransactionsVolume: dailyCommissionTransactions.totalTransactionsVolume,
      totalCommissions: dailyCommissionTransactions.totalCommissions,
      spare1: dailyCommissionTransactions.spare1,
      spare2: dailyCommissionTransactions.spare2,
      spare3: dailyCommissionTransactions.spare3,
      spare4: dailyCommissionTransactions.spare4,
      spare5: dailyCommissionTransactions.spare5
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dailyCommissionTransactions = this.createFromForm();
    if (dailyCommissionTransactions.id !== undefined) {
      this.subscribeToSaveResponse(this.dailyCommissionTransactionsService.update(dailyCommissionTransactions));
    } else {
      this.subscribeToSaveResponse(this.dailyCommissionTransactionsService.create(dailyCommissionTransactions));
    }
  }

  private createFromForm(): IDailyCommissionTransactions {
    return {
      ...new DailyCommissionTransactions(),
      id: this.editForm.get(['id'])!.value,
      agentMsisdn: this.editForm.get(['agentMsisdn'])!.value,
      processDate: this.editForm.get(['processDate'])!.value
        ? moment(this.editForm.get(['processDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      totalTransactionsVolume: this.editForm.get(['totalTransactionsVolume'])!.value,
      totalCommissions: this.editForm.get(['totalCommissions'])!.value,
      spare1: this.editForm.get(['spare1'])!.value,
      spare2: this.editForm.get(['spare2'])!.value,
      spare3: this.editForm.get(['spare3'])!.value,
      spare4: this.editForm.get(['spare4'])!.value,
      spare5: this.editForm.get(['spare5'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDailyCommissionTransactions>>): void {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
