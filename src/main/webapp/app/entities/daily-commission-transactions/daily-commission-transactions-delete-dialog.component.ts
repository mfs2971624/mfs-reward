/* eslint-disable */
import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDailyCommissionTransactions } from 'app/shared/model/daily-commission-transactions.model';
import { DailyCommissionTransactionsService } from './daily-commission-transactions.service';

@Component({
  templateUrl: './daily-commission-transactions-delete-dialog.component.html'
})
export class DailyCommissionTransactionsDeleteDialogComponent {
  dailyCommissionTransactions?: IDailyCommissionTransactions;

  constructor(
    protected dailyCommissionTransactionsService: DailyCommissionTransactionsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.dailyCommissionTransactionsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('dailyCommissionTransactionsListModification');
      this.activeModal.close();
    });
  }
}
