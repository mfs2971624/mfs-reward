/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IDailyCommissionTransactions, DailyCommissionTransactions } from 'app/shared/model/daily-commission-transactions.model';
import { DailyCommissionTransactionsService } from './daily-commission-transactions.service';
import { DailyCommissionTransactionsComponent } from './daily-commission-transactions.component';
import { DailyCommissionTransactionsDetailComponent } from './daily-commission-transactions-detail.component';
import { DailyCommissionTransactionsUpdateComponent } from './daily-commission-transactions-update.component';

@Injectable({ providedIn: 'root' })
export class DailyCommissionTransactionsResolve implements Resolve<IDailyCommissionTransactions> {
  constructor(private service: DailyCommissionTransactionsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDailyCommissionTransactions> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((dailyCommissionTransactions: HttpResponse<DailyCommissionTransactions>) => {
          if (dailyCommissionTransactions.body) {
            return of(dailyCommissionTransactions.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DailyCommissionTransactions());
  }
}

export const dailyCommissionTransactionsRoute: Routes = [
  {
    path: '',
    component: DailyCommissionTransactionsComponent,
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'DailyCommissionTransactions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DailyCommissionTransactionsDetailComponent,
    resolve: {
      dailyCommissionTransactions: DailyCommissionTransactionsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DailyCommissionTransactions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DailyCommissionTransactionsUpdateComponent,
    resolve: {
      dailyCommissionTransactions: DailyCommissionTransactionsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DailyCommissionTransactions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DailyCommissionTransactionsUpdateComponent,
    resolve: {
      dailyCommissionTransactions: DailyCommissionTransactionsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DailyCommissionTransactions'
    },
    canActivate: [UserRouteAccessService]
  }
];
