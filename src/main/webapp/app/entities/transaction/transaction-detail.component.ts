import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITransaction } from 'app/shared/model/transaction.model';
import { TransactionRewardService } from '../transaction-reward/transaction-reward.service';
import { ITransactionReward } from 'app/shared/model/transaction-reward.model';

@Component({
  selector: 'jhi-transaction-detail',
  templateUrl: './transaction-detail.component.html'
})
export class TransactionDetailComponent implements OnInit {
  transaction: ITransaction;
  transactionRewards: ITransactionReward[];

  constructor(protected activatedRoute: ActivatedRoute, protected transactionRewardService: TransactionRewardService) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ transaction }) => {
      this.transaction = transaction;
    });

    this.transactionRewardService.findAllbyid(this.transaction.id).subscribe(res => {
      this.transactionRewards = res.body;
      console.log(this.transactionRewards);
      console.log(res);
    });
  }

  previousState() {
    window.history.back();
  }
}
