import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ITransaction, Transaction } from 'app/shared/model/transaction.model';
import { TransactionService } from './transaction.service';

@Component({
  selector: 'jhi-transaction-update',
  templateUrl: './transaction-update.component.html'
})
export class TransactionUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    requestId: [],
    trxIdMobiquity: [],
    trxType: [],
    trxPartnerType: [],
    trxPartnerId: [],
    trxCustomerMSISDN: [],
    trxCustomerAccountNumber: [],
    trxAmount: [],
    trxChannel: [],
    trxDate: [],
    eligiblePromos: [],
    traitementStatus: [],
    spare1: [],
    spare2: []
  });

  constructor(protected transactionService: TransactionService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ transaction }) => {
      this.updateForm(transaction);
    });
  }

  updateForm(transaction: ITransaction) {
    this.editForm.patchValue({
      id: transaction.id,
      requestId: transaction.requestId,
      trxIdMobiquity: transaction.trxIdMobiquity,
      trxType: transaction.trxType,
      trxPartnerType: transaction.trxPartnerType,
      trxPartnerId: transaction.trxPartnerId,
      trxCustomerMSISDN: transaction.trxCustomerMSISDN,
      trxCustomerAccountNumber: transaction.trxCustomerAccountNumber,
      trxAmount: transaction.trxAmount,
      trxChannel: transaction.trxChannel,
      trxDate: transaction.trxDate,
      eligiblePromos: transaction.eligiblePromos,
      traitementStatus: transaction.traitementStatus,
      spare1: transaction.spare1,
      spare2: transaction.spare2
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const transaction = this.createFromForm();
    if (transaction.id !== undefined) {
      this.subscribeToSaveResponse(this.transactionService.update(transaction));
    } else {
      this.subscribeToSaveResponse(this.transactionService.create(transaction));
    }
  }

  private createFromForm(): ITransaction {
    return {
      ...new Transaction(),
      id: this.editForm.get(['id']).value,
      requestId: this.editForm.get(['requestId']).value,
      trxIdMobiquity: this.editForm.get(['trxIdMobiquity']).value,
      trxType: this.editForm.get(['trxType']).value,
      trxPartnerType: this.editForm.get(['trxPartnerType']).value,
      trxPartnerId: this.editForm.get(['trxPartnerId']).value,
      trxCustomerMSISDN: this.editForm.get(['trxCustomerMSISDN']).value,
      trxCustomerAccountNumber: this.editForm.get(['trxCustomerAccountNumber']).value,
      trxAmount: this.editForm.get(['trxAmount']).value,
      trxChannel: this.editForm.get(['trxChannel']).value,
      trxDate: this.editForm.get(['trxDate']).value,
      eligiblePromos: this.editForm.get(['eligiblePromos']).value,
      traitementStatus: this.editForm.get(['traitementStatus']).value,
      spare1: this.editForm.get(['spare1']).value,
      spare2: this.editForm.get(['spare2']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITransaction>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
