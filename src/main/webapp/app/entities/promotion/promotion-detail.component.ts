import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IPromotion } from 'app/shared/model/promotion.model';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { IPromoExecutionPlan } from 'app/shared/model/promo-execution-plan.model';
import { PromoExecutionPlanService } from 'app/entities/promo-execution-plan';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { ITEMS_PER_PAGE } from 'app/shared';

@Component({
  selector: 'jhi-promotion-detail',
  templateUrl: './promotion-detail.component.html'
})
export class PromotionDetailComponent implements OnInit {
  promotion: IPromotion;
  promoExecutionPlans: IPromoExecutionPlan[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected PromoExecutionPlanService: PromoExecutionPlanService,
    protected jhiAlertService: JhiAlertService,
    protected parseLinks: JhiParseLinks,
    protected router: Router
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 1;
    this.previousPage = 0;
    this.reverse = 0;
    this.predicate = 0;
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ promotion }) => {
      this.promotion = promotion;
    });
    this.loadAll(1);
  }

  previousState() {
    window.history.back();
  }

  loadAll(page) {
    this.PromoExecutionPlanService.findbyPromotionId(this.promotion.id, {
      page: page - 1,
      size: this.itemsPerPage
    }).subscribe(
      (res: HttpResponse<IPromoExecutionPlan[]>) => this.paginatePromoExecutionPlans(res.body, res.headers),
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  protected paginatePromoExecutionPlans(data: IPromoExecutionPlan[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.promoExecutionPlans = data;
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      // this.transition();
      this.loadAll(page);
    }
  }

  transition() {}
}
