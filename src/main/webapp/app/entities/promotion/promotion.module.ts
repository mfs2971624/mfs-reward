import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsrewardSharedModule } from 'app/shared';
import {
  PromotionComponent,
  PromotionDetailComponent,
  PromotionUpdateComponent,
  PromotionDeletePopupComponent,
  PromotionDeleteDialogComponent,
  promotionRoute,
  promotionPopupRoute
} from './';

const ENTITY_STATES = [...promotionRoute, ...promotionPopupRoute];

@NgModule({
  imports: [MfsrewardSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PromotionComponent,
    PromotionDetailComponent,
    PromotionUpdateComponent,
    PromotionDeleteDialogComponent,
    PromotionDeletePopupComponent
  ],
  entryComponents: [PromotionComponent, PromotionUpdateComponent, PromotionDeleteDialogComponent, PromotionDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MfsrewardPromotionModule {}
