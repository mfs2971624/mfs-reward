import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IPromotion, Promotion } from 'app/shared/model/promotion.model';
import { PromotionService } from './promotion.service';
import { ICampagne } from 'app/shared/model/campagne.model';
import { CampagneService } from 'app/entities/campagne';

@Component({
  selector: 'jhi-promotion-update',
  templateUrl: './promotion-update.component.html'
})
export class PromotionUpdateComponent implements OnInit {
  isSaving: boolean;

  campagnes: ICampagne[];

  editForm = this.fb.group({
    id: [],
    nom: [null, [Validators.required]],
    critereEligilibite: [null, [Validators.required]],
    frequence: [null, [Validators.required]],
    frequenceDetails: [],
    statut: [],
    campagneId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected promotionService: PromotionService,
    protected campagneService: CampagneService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ promotion }) => {
      this.updateForm(promotion);
    });
    this.campagneService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICampagne[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICampagne[]>) => response.body)
      )
      .subscribe((res: ICampagne[]) => (this.campagnes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(promotion: IPromotion) {
    console.log(promotion);
    this.editForm.patchValue({
      id: promotion.id,
      nom: promotion.nom,
      critereEligilibite: promotion.critereEligilibite,
      frequence: promotion.frequence,
      frequenceDetails: promotion.frequenceDetails,
      statut: promotion.statut,
      campagneId: promotion.campagneId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const promotion = this.createFromForm();
    if (promotion.id !== undefined) {
      this.subscribeToSaveResponse(this.promotionService.update(promotion));
    } else {
      this.subscribeToSaveResponse(this.promotionService.create(promotion));
    }
  }

  private createFromForm(): IPromotion {
    return {
      ...new Promotion(),
      id: this.editForm.get(['id']).value,
      nom: this.editForm.get(['nom']).value,
      critereEligilibite: this.editForm.get(['critereEligilibite']).value,
      frequence: this.editForm.get(['frequence']).value,
      frequenceDetails: this.editForm.get(['frequenceDetails']).value,
      statut: this.editForm.get(['statut']).value,
      campagneId: this.editForm.get(['campagneId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPromotion>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCampagneById(index: number, item: ICampagne) {
    return item.id;
  }
}
