import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBlacklistedAgents } from 'app/shared/model/blacklisted-agents.model';
import { BlacklistedAgentsService } from './blacklisted-agents.service';

@Component({
  templateUrl: './blacklisted-agents-delete-dialog.component.html'
})
export class BlacklistedAgentsDeleteDialogComponent {
  blacklistedAgents?: IBlacklistedAgents;

  constructor(
    protected blacklistedAgentsService: BlacklistedAgentsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.blacklistedAgentsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('blacklistedAgentsListModification');
      this.activeModal.close();
    });
  }
}
