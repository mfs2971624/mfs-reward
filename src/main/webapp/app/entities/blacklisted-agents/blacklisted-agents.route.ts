import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBlacklistedAgents, BlacklistedAgents } from 'app/shared/model/blacklisted-agents.model';
import { BlacklistedAgentsService } from './blacklisted-agents.service';
import { BlacklistedAgentsComponent } from './blacklisted-agents.component';
import { BlacklistedAgentsDetailComponent } from './blacklisted-agents-detail.component';
import { BlacklistedAgentsUpdateComponent } from './blacklisted-agents-update.component';

@Injectable({ providedIn: 'root' })
export class BlacklistedAgentsResolve implements Resolve<IBlacklistedAgents> {
  constructor(private service: BlacklistedAgentsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBlacklistedAgents> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((blacklistedAgents: HttpResponse<BlacklistedAgents>) => {
          if (blacklistedAgents.body) {
            return of(blacklistedAgents.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BlacklistedAgents());
  }
}

export const blacklistedAgentsRoute: Routes = [
  {
    path: '',
    component: BlacklistedAgentsComponent,
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'BlacklistedAgents'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BlacklistedAgentsDetailComponent,
    resolve: {
      blacklistedAgents: BlacklistedAgentsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'BlacklistedAgents'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BlacklistedAgentsUpdateComponent,
    resolve: {
      blacklistedAgents: BlacklistedAgentsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'BlacklistedAgents'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BlacklistedAgentsUpdateComponent,
    resolve: {
      blacklistedAgents: BlacklistedAgentsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'BlacklistedAgents'
    },
    canActivate: [UserRouteAccessService]
  }
];
