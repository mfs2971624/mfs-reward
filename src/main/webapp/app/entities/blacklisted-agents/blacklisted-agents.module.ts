import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsrewardSharedModule } from 'app/shared/shared.module';
import { BlacklistedAgentsComponent } from './blacklisted-agents.component';
import { BlacklistedAgentsDetailComponent } from './blacklisted-agents-detail.component';
import { BlacklistedAgentsUpdateComponent } from './blacklisted-agents-update.component';
import { BlacklistedAgentsDeleteDialogComponent } from './blacklisted-agents-delete-dialog.component';
import { blacklistedAgentsRoute } from './blacklisted-agents.route';

@NgModule({
  imports: [MfsrewardSharedModule, RouterModule.forChild(blacklistedAgentsRoute)],
  declarations: [
    BlacklistedAgentsComponent,
    BlacklistedAgentsDetailComponent,
    BlacklistedAgentsUpdateComponent,
    BlacklistedAgentsDeleteDialogComponent
  ],
  entryComponents: [BlacklistedAgentsDeleteDialogComponent]
})
export class MfsrewardBlacklistedAgentsModule {}
