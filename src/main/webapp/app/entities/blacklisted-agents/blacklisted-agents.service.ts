import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBlacklistedAgents } from 'app/shared/model/blacklisted-agents.model';

type EntityResponseType = HttpResponse<IBlacklistedAgents>;
type EntityArrayResponseType = HttpResponse<IBlacklistedAgents[]>;

@Injectable({ providedIn: 'root' })
export class BlacklistedAgentsService {
  public resourceUrl = SERVER_API_URL + 'api/blacklisted-agents';

  constructor(protected http: HttpClient) {}

  create(blacklistedAgents: IBlacklistedAgents): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(blacklistedAgents);
    return this.http
      .post<IBlacklistedAgents>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(blacklistedAgents: IBlacklistedAgents): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(blacklistedAgents);
    return this.http
      .put<IBlacklistedAgents>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBlacklistedAgents>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBlacklistedAgents[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(blacklistedAgents: IBlacklistedAgents): IBlacklistedAgents {
    const copy: IBlacklistedAgents = Object.assign({}, blacklistedAgents, {
      blackListedDate:
        blacklistedAgents.blackListedDate && blacklistedAgents.blackListedDate.isValid()
          ? blacklistedAgents.blackListedDate.toJSON()
          : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.blackListedDate = res.body.blackListedDate ? moment(res.body.blackListedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((blacklistedAgents: IBlacklistedAgents) => {
        blacklistedAgents.blackListedDate = blacklistedAgents.blackListedDate ? moment(blacklistedAgents.blackListedDate) : undefined;
      });
    }
    return res;
  }
}
