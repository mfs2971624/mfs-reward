import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBlacklistedAgents, BlacklistedAgents } from 'app/shared/model/blacklisted-agents.model';
import { BlacklistedAgentsService } from './blacklisted-agents.service';

@Component({
  selector: 'jhi-blacklisted-agents-update',
  templateUrl: './blacklisted-agents-update.component.html'
})
export class BlacklistedAgentsUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    agentMsisdn: [],
    blackListedDate: [],
    blackListedBy: [],
    agentType: [],
    spare1: [],
    spare2: [],
    spare3: [],
    spare4: [],
    spare5: []
  });

  constructor(
    protected blacklistedAgentsService: BlacklistedAgentsService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ blacklistedAgents }) => {
      if (!blacklistedAgents.id) {
        const today = moment().startOf('day');
        blacklistedAgents.blackListedDate = today;
      }

      this.updateForm(blacklistedAgents);
    });
  }

  updateForm(blacklistedAgents: IBlacklistedAgents): void {
    this.editForm.patchValue({
      id: blacklistedAgents.id,
      agentMsisdn: blacklistedAgents.agentMsisdn,
      blackListedDate: blacklistedAgents.blackListedDate ? blacklistedAgents.blackListedDate.format(DATE_TIME_FORMAT) : null,
      blackListedBy: blacklistedAgents.blackListedBy,
      agentType: blacklistedAgents.agentType,
      spare1: blacklistedAgents.spare1,
      spare2: blacklistedAgents.spare2,
      spare3: blacklistedAgents.spare3,
      spare4: blacklistedAgents.spare4,
      spare5: blacklistedAgents.spare5
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const blacklistedAgents = this.createFromForm();
    if (blacklistedAgents.id !== undefined) {
      this.subscribeToSaveResponse(this.blacklistedAgentsService.update(blacklistedAgents));
    } else {
      this.subscribeToSaveResponse(this.blacklistedAgentsService.create(blacklistedAgents));
    }
  }

  private createFromForm(): IBlacklistedAgents {
    return {
      ...new BlacklistedAgents(),
      id: this.editForm.get(['id'])!.value,
      agentMsisdn: this.editForm.get(['agentMsisdn'])!.value,
      blackListedDate: this.editForm.get(['blackListedDate'])!.value
        ? moment(this.editForm.get(['blackListedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      blackListedBy: this.editForm.get(['blackListedBy'])!.value,
      agentType: this.editForm.get(['agentType'])!.value,
      spare1: this.editForm.get(['spare1'])!.value,
      spare2: this.editForm.get(['spare2'])!.value,
      spare3: this.editForm.get(['spare3'])!.value,
      spare4: this.editForm.get(['spare4'])!.value,
      spare5: this.editForm.get(['spare5'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBlacklistedAgents>>): void {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
