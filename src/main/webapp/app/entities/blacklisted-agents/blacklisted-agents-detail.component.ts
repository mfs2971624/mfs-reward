import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBlacklistedAgents } from 'app/shared/model/blacklisted-agents.model';

@Component({
  selector: 'jhi-blacklisted-agents-detail',
  templateUrl: './blacklisted-agents-detail.component.html'
})
export class BlacklistedAgentsDetailComponent implements OnInit {
  blacklistedAgents: IBlacklistedAgents | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ blacklistedAgents }) => (this.blacklistedAgents = blacklistedAgents));
  }

  previousState(): void {
    window.history.back();
  }
}
