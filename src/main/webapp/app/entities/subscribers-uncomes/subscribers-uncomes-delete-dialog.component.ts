import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISubscribersUncomes } from 'app/shared/model/subscribers-uncomes.model';
import { SubscribersUncomesService } from './subscribers-uncomes.service';

@Component({
  templateUrl: './subscribers-uncomes-delete-dialog.component.html'
})
export class SubscribersUncomesDeleteDialogComponent {
  subscribersUncomes?: ISubscribersUncomes;

  constructor(
    protected subscribersUncomesService: SubscribersUncomesService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.subscribersUncomesService.delete(id).subscribe(() => {
      this.eventManager.broadcast('subscribersUncomesListModification');
      this.activeModal.close();
    });
  }
}
