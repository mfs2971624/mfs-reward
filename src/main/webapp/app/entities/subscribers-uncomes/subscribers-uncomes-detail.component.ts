import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISubscribersUncomes } from 'app/shared/model/subscribers-uncomes.model';

@Component({
  selector: 'jhi-subscribers-uncomes-detail',
  templateUrl: './subscribers-uncomes-detail.component.html'
})
export class SubscribersUncomesDetailComponent implements OnInit {
  subscribersUncomes: ISubscribersUncomes | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ subscribersUncomes }) => (this.subscribersUncomes = subscribersUncomes));
  }

  previousState(): void {
    window.history.back();
  }
}
