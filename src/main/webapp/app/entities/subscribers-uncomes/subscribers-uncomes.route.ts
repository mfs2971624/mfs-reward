import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ISubscribersUncomes, SubscribersUncomes } from 'app/shared/model/subscribers-uncomes.model';
import { SubscribersUncomesService } from './subscribers-uncomes.service';
import { SubscribersUncomesComponent } from './subscribers-uncomes.component';
import { SubscribersUncomesDetailComponent } from './subscribers-uncomes-detail.component';
import { SubscribersUncomesUpdateComponent } from './subscribers-uncomes-update.component';

@Injectable({ providedIn: 'root' })
export class SubscribersUncomesResolve implements Resolve<ISubscribersUncomes> {
  constructor(private service: SubscribersUncomesService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISubscribersUncomes> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((subscribersUncomes: HttpResponse<SubscribersUncomes>) => {
          if (subscribersUncomes.body) {
            return of(subscribersUncomes.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new SubscribersUncomes());
  }
}

export const subscribersUncomesRoute: Routes = [
  {
    path: '',
    component: SubscribersUncomesComponent,
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'SubscribersUncomes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SubscribersUncomesDetailComponent,
    resolve: {
      subscribersUncomes: SubscribersUncomesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SubscribersUncomes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SubscribersUncomesUpdateComponent,
    resolve: {
      subscribersUncomes: SubscribersUncomesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SubscribersUncomes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SubscribersUncomesUpdateComponent,
    resolve: {
      subscribersUncomes: SubscribersUncomesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SubscribersUncomes'
    },
    canActivate: [UserRouteAccessService]
  }
];
