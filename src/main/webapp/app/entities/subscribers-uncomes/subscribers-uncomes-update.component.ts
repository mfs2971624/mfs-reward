import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ISubscribersUncomes, SubscribersUncomes } from 'app/shared/model/subscribers-uncomes.model';
import { SubscribersUncomesService } from './subscribers-uncomes.service';

@Component({
  selector: 'jhi-subscribers-uncomes-update',
  templateUrl: './subscribers-uncomes-update.component.html'
})
export class SubscribersUncomesUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    subscriberMsisdn: [],
    processDate: [],
    uncome: [],
    spare1: [],
    spare2: [],
    spare3: [],
    spare4: [],
    spare5: []
  });

  constructor(
    protected subscribersUncomesService: SubscribersUncomesService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ subscribersUncomes }) => {
      if (!subscribersUncomes.id) {
        const today = moment().startOf('day');
        subscribersUncomes.processDate = today;
      }

      this.updateForm(subscribersUncomes);
    });
  }

  updateForm(subscribersUncomes: ISubscribersUncomes): void {
    this.editForm.patchValue({
      id: subscribersUncomes.id,
      subscriberMsisdn: subscribersUncomes.subscriberMsisdn,
      processDate: subscribersUncomes.processDate ? subscribersUncomes.processDate.format(DATE_TIME_FORMAT) : null,
      uncome: subscribersUncomes.uncome,
      spare1: subscribersUncomes.spare1,
      spare2: subscribersUncomes.spare2,
      spare3: subscribersUncomes.spare3,
      spare4: subscribersUncomes.spare4,
      spare5: subscribersUncomes.spare5
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const subscribersUncomes = this.createFromForm();
    if (subscribersUncomes.id !== undefined) {
      this.subscribeToSaveResponse(this.subscribersUncomesService.update(subscribersUncomes));
    } else {
      this.subscribeToSaveResponse(this.subscribersUncomesService.create(subscribersUncomes));
    }
  }

  private createFromForm(): ISubscribersUncomes {
    return {
      ...new SubscribersUncomes(),
      id: this.editForm.get(['id'])!.value,
      subscriberMsisdn: this.editForm.get(['subscriberMsisdn'])!.value,
      processDate: this.editForm.get(['processDate'])!.value
        ? moment(this.editForm.get(['processDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      uncome: this.editForm.get(['uncome'])!.value,
      spare1: this.editForm.get(['spare1'])!.value,
      spare2: this.editForm.get(['spare2'])!.value,
      spare3: this.editForm.get(['spare3'])!.value,
      spare4: this.editForm.get(['spare4'])!.value,
      spare5: this.editForm.get(['spare5'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISubscribersUncomes>>): void {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
