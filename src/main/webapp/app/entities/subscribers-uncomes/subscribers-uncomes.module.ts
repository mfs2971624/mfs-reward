import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsrewardSharedModule } from 'app/shared/shared.module';
import { SubscribersUncomesComponent } from './subscribers-uncomes.component';
import { SubscribersUncomesDetailComponent } from './subscribers-uncomes-detail.component';
import { SubscribersUncomesUpdateComponent } from './subscribers-uncomes-update.component';
import { SubscribersUncomesDeleteDialogComponent } from './subscribers-uncomes-delete-dialog.component';
import { subscribersUncomesRoute } from './subscribers-uncomes.route';

@NgModule({
  imports: [MfsrewardSharedModule, RouterModule.forChild(subscribersUncomesRoute)],
  declarations: [
    SubscribersUncomesComponent,
    SubscribersUncomesDetailComponent,
    SubscribersUncomesUpdateComponent,
    SubscribersUncomesDeleteDialogComponent
  ],
  entryComponents: [SubscribersUncomesDeleteDialogComponent]
})
export class MfsrewardSubscribersUncomesModule {}
