import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISubscribersUncomes } from 'app/shared/model/subscribers-uncomes.model';

type EntityResponseType = HttpResponse<ISubscribersUncomes>;
type EntityArrayResponseType = HttpResponse<ISubscribersUncomes[]>;

@Injectable({ providedIn: 'root' })
export class SubscribersUncomesService {
  public resourceUrl = SERVER_API_URL + 'api/subscribers-uncomes';

  constructor(protected http: HttpClient) {}

  create(subscribersUncomes: ISubscribersUncomes): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(subscribersUncomes);
    return this.http
      .post<ISubscribersUncomes>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(subscribersUncomes: ISubscribersUncomes): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(subscribersUncomes);
    return this.http
      .put<ISubscribersUncomes>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISubscribersUncomes>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISubscribersUncomes[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(subscribersUncomes: ISubscribersUncomes): ISubscribersUncomes {
    const copy: ISubscribersUncomes = Object.assign({}, subscribersUncomes, {
      processDate:
        subscribersUncomes.processDate && subscribersUncomes.processDate.isValid() ? subscribersUncomes.processDate.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.processDate = res.body.processDate ? moment(res.body.processDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((subscribersUncomes: ISubscribersUncomes) => {
        subscribersUncomes.processDate = subscribersUncomes.processDate ? moment(subscribersUncomes.processDate) : undefined;
      });
    }
    return res;
  }
}
