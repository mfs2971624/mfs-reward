import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IReward, Reward } from 'app/shared/model/reward.model';
import { RewardService } from './reward.service';
import { IPromotion } from 'app/shared/model/promotion.model';
import { PromotionService } from 'app/entities/promotion/promotion.service';

@Component({
  selector: 'jhi-reward-update',
  templateUrl: './reward-update.component.html'
})
export class RewardUpdateComponent implements OnInit {
  isSaving: boolean;

  promotions: IPromotion[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    rewardType: [null, [Validators.required]],
    taux: [null, [Validators.required]],
    rewardNotifMessage: [null, [Validators.required]],
    countTreshold: [],
    valueTreshold: [],
    promotionId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected rewardService: RewardService,
    protected promotionService: PromotionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ reward }) => {
      this.updateForm(reward);
    });
    this.promotionService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IPromotion[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPromotion[]>) => response.body)
      )
      .subscribe((res: IPromotion[]) => (this.promotions = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(reward: IReward): void {
    this.editForm.patchValue({
      id: reward.id,
      name: reward.name,
      rewardType: reward.rewardType,
      taux: reward.taux,
      rewardNotifMessage: reward.rewardNotifMessage,
      countTreshold: reward.countTreshold,
      valueTreshold: reward.valueTreshold,
      promotionId: reward.promotionId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const reward = this.createFromForm();
    if (reward.id !== undefined) {
      this.subscribeToSaveResponse(this.rewardService.update(reward));
    } else {
      this.subscribeToSaveResponse(this.rewardService.create(reward));
    }
  }

  private createFromForm(): IReward {
    console.log(this.editForm.get(['promotionId'])!.value);
    return {
      ...new Reward(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      rewardType: this.editForm.get(['rewardType'])!.value,
      taux: this.editForm.get(['taux'])!.value,
      rewardNotifMessage: this.editForm.get(['rewardNotifMessage'])!.value,
      countTreshold: this.editForm.get(['countTreshold'])!.value,
      valueTreshold: this.editForm.get(['valueTreshold'])!.value,
      promotionId: this.editForm.get(['promotionId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReward>>): void {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackById(index: number, item: IPromotion): any {
    return item.id;
  }
}
