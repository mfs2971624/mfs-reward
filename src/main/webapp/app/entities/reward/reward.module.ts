import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsrewardSharedModule } from 'app/shared';
import { rewardPopupRoute, rewardRoute } from './reward.route';
import { RewardComponent } from './reward.component';
import { RewardDetailComponent } from './reward-detail.component';
import { RewardUpdateComponent } from './reward-update.component';
import { RewardDeleteDialogComponent, RewardDeletePopupComponent } from './reward-delete-dialog.component';

const ENTITY_STATES = [...rewardRoute, ...rewardPopupRoute];

@NgModule({
  imports: [MfsrewardSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [RewardComponent, RewardDetailComponent, RewardUpdateComponent, RewardDeleteDialogComponent, RewardDeletePopupComponent],
  entryComponents: [RewardComponent, RewardUpdateComponent, RewardDeleteDialogComponent, RewardDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MfsrewardRewardModule {}
