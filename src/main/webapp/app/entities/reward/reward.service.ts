import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IReward } from 'app/shared/model/reward.model';

type EntityResponseType = HttpResponse<IReward>;
type EntityArrayResponseType = HttpResponse<IReward[]>;

@Injectable({ providedIn: 'root' })
export class RewardService {
  public resourceUrl = SERVER_API_URL + 'api/rewards';

  constructor(protected http: HttpClient) {}

  create(reward: IReward): Observable<EntityResponseType> {
    return this.http.post<IReward>(this.resourceUrl, reward, { observe: 'response' });
  }

  update(reward: IReward): Observable<EntityResponseType> {
    return this.http.put<IReward>(this.resourceUrl, reward, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IReward>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IReward[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
