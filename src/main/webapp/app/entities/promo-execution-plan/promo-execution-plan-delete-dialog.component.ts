import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPromoExecutionPlan } from 'app/shared/model/promo-execution-plan.model';
import { PromoExecutionPlanService } from './promo-execution-plan.service';

@Component({
  selector: 'jhi-promo-execution-plan-delete-dialog',
  templateUrl: './promo-execution-plan-delete-dialog.component.html'
})
export class PromoExecutionPlanDeleteDialogComponent {
  promoExecutionPlan: IPromoExecutionPlan;

  constructor(
    protected promoExecutionPlanService: PromoExecutionPlanService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.promoExecutionPlanService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'promoExecutionPlanListModification',
        content: 'Deleted an promoExecutionPlan'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-promo-execution-plan-delete-popup',
  template: ''
})
export class PromoExecutionPlanDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ promoExecutionPlan }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(PromoExecutionPlanDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.promoExecutionPlan = promoExecutionPlan;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/promo-execution-plan', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/promo-execution-plan', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
