import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPromoExecutionPlan } from 'app/shared/model/promo-execution-plan.model';

@Component({
  selector: 'jhi-promo-execution-plan-detail',
  templateUrl: './promo-execution-plan-detail.component.html'
})
export class PromoExecutionPlanDetailComponent implements OnInit {
  promoExecutionPlan: IPromoExecutionPlan;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ promoExecutionPlan }) => {
      this.promoExecutionPlan = promoExecutionPlan;
    });
  }

  previousState() {
    window.history.back();
  }
}
