import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IPromoExecutionPlan, PromoExecutionPlan } from 'app/shared/model/promo-execution-plan.model';
import { PromoExecutionPlanService } from './promo-execution-plan.service';
import { IPromotion } from 'app/shared/model/promotion.model';
import { PromotionService } from 'app/entities/promotion';

@Component({
  selector: 'jhi-promo-execution-plan-update',
  templateUrl: './promo-execution-plan-update.component.html'
})
export class PromoExecutionPlanUpdateComponent implements OnInit {
  isSaving: boolean;

  promotions: IPromotion[];

  editForm = this.fb.group({
    id: [],
    dateDebut: [],
    dateFin: [],
    promotionId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected promoExecutionPlanService: PromoExecutionPlanService,
    protected promotionService: PromotionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ promoExecutionPlan }) => {
      this.updateForm(promoExecutionPlan);
    });
    this.promotionService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IPromotion[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPromotion[]>) => response.body)
      )
      .subscribe((res: IPromotion[]) => (this.promotions = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(promoExecutionPlan: IPromoExecutionPlan) {
    this.editForm.patchValue({
      id: promoExecutionPlan.id,
      dateDebut: promoExecutionPlan.dateDebut != null ? promoExecutionPlan.dateDebut.format(DATE_TIME_FORMAT) : null,
      dateFin: promoExecutionPlan.dateFin != null ? promoExecutionPlan.dateFin.format(DATE_TIME_FORMAT) : null,
      promotionId: promoExecutionPlan.promotionId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const promoExecutionPlan = this.createFromForm();
    if (promoExecutionPlan.id !== undefined) {
      this.subscribeToSaveResponse(this.promoExecutionPlanService.update(promoExecutionPlan));
    } else {
      this.subscribeToSaveResponse(this.promoExecutionPlanService.create(promoExecutionPlan));
    }
  }

  private createFromForm(): IPromoExecutionPlan {
    return {
      ...new PromoExecutionPlan(),
      id: this.editForm.get(['id']).value,
      dateDebut:
        this.editForm.get(['dateDebut']).value != null ? moment(this.editForm.get(['dateDebut']).value, DATE_TIME_FORMAT) : undefined,
      dateFin: this.editForm.get(['dateFin']).value != null ? moment(this.editForm.get(['dateFin']).value, DATE_TIME_FORMAT) : undefined,
      promotionId: this.editForm.get(['promotionId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPromoExecutionPlan>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackPromotionById(index: number, item: IPromotion) {
    return item.id;
  }
}
