import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPromoExecutionPlan } from 'app/shared/model/promo-execution-plan.model';
import { IPromotion } from 'app/shared/model/promotion.model';

type EntityResponseType = HttpResponse<IPromoExecutionPlan>;
type EntityArrayResponseType = HttpResponse<IPromoExecutionPlan[]>;

@Injectable({ providedIn: 'root' })
export class PromoExecutionPlanService {
  public resourceUrl = SERVER_API_URL + 'api/promo-execution-plans';

  constructor(protected http: HttpClient) {}

  create(promoExecutionPlan: IPromoExecutionPlan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(promoExecutionPlan);
    return this.http
      .post<IPromoExecutionPlan>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(promoExecutionPlan: IPromoExecutionPlan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(promoExecutionPlan);
    return this.http
      .put<IPromoExecutionPlan>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPromoExecutionPlan>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPromoExecutionPlan[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findbyPromotionId(id: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPromoExecutionPlan[]>(`${this.resourceUrl}/byPromoId/${id}`, { params: options, observe: 'response' });
  }

  protected convertDateFromClient(promoExecutionPlan: IPromoExecutionPlan): IPromoExecutionPlan {
    const copy: IPromoExecutionPlan = Object.assign({}, promoExecutionPlan, {
      dateDebut:
        promoExecutionPlan.dateDebut != null && promoExecutionPlan.dateDebut.isValid() ? promoExecutionPlan.dateDebut.toJSON() : null,
      dateFin: promoExecutionPlan.dateFin != null && promoExecutionPlan.dateFin.isValid() ? promoExecutionPlan.dateFin.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateDebut = res.body.dateDebut != null ? moment(res.body.dateDebut) : null;
      res.body.dateFin = res.body.dateFin != null ? moment(res.body.dateFin) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((promoExecutionPlan: IPromoExecutionPlan) => {
        promoExecutionPlan.dateDebut = promoExecutionPlan.dateDebut != null ? moment(promoExecutionPlan.dateDebut) : null;
        promoExecutionPlan.dateFin = promoExecutionPlan.dateFin != null ? moment(promoExecutionPlan.dateFin) : null;
      });
    }
    return res;
  }
}
