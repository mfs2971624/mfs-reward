import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsrewardSharedModule } from 'app/shared';
import {
  PromoExecutionPlanComponent,
  PromoExecutionPlanDetailComponent,
  PromoExecutionPlanUpdateComponent,
  PromoExecutionPlanDeletePopupComponent,
  PromoExecutionPlanDeleteDialogComponent,
  promoExecutionPlanRoute,
  promoExecutionPlanPopupRoute
} from './';

const ENTITY_STATES = [...promoExecutionPlanRoute, ...promoExecutionPlanPopupRoute];

@NgModule({
  imports: [MfsrewardSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PromoExecutionPlanComponent,
    PromoExecutionPlanDetailComponent,
    PromoExecutionPlanUpdateComponent,
    PromoExecutionPlanDeleteDialogComponent,
    PromoExecutionPlanDeletePopupComponent
  ],
  entryComponents: [
    PromoExecutionPlanComponent,
    PromoExecutionPlanUpdateComponent,
    PromoExecutionPlanDeleteDialogComponent,
    PromoExecutionPlanDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MfsrewardPromoExecutionPlanModule {}
