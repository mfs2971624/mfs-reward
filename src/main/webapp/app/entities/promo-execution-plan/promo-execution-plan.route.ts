import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { PromoExecutionPlan } from 'app/shared/model/promo-execution-plan.model';
import { PromoExecutionPlanService } from './promo-execution-plan.service';
import { PromoExecutionPlanComponent } from './promo-execution-plan.component';
import { PromoExecutionPlanDetailComponent } from './promo-execution-plan-detail.component';
import { PromoExecutionPlanUpdateComponent } from './promo-execution-plan-update.component';
import { PromoExecutionPlanDeletePopupComponent } from './promo-execution-plan-delete-dialog.component';
import { IPromoExecutionPlan } from 'app/shared/model/promo-execution-plan.model';

@Injectable({ providedIn: 'root' })
export class PromoExecutionPlanResolve implements Resolve<IPromoExecutionPlan> {
  constructor(private service: PromoExecutionPlanService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPromoExecutionPlan> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<PromoExecutionPlan>) => response.ok),
        map((promoExecutionPlan: HttpResponse<PromoExecutionPlan>) => promoExecutionPlan.body)
      );
    }
    return of(new PromoExecutionPlan());
  }
}

export const promoExecutionPlanRoute: Routes = [
  {
    path: '',
    component: PromoExecutionPlanComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'PromoExecutionPlans'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PromoExecutionPlanDetailComponent,
    resolve: {
      promoExecutionPlan: PromoExecutionPlanResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PromoExecutionPlans'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PromoExecutionPlanUpdateComponent,
    resolve: {
      promoExecutionPlan: PromoExecutionPlanResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PromoExecutionPlans'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PromoExecutionPlanUpdateComponent,
    resolve: {
      promoExecutionPlan: PromoExecutionPlanResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PromoExecutionPlans'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const promoExecutionPlanPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PromoExecutionPlanDeletePopupComponent,
    resolve: {
      promoExecutionPlan: PromoExecutionPlanResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PromoExecutionPlans'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
