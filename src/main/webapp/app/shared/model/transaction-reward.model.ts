import { Moment } from 'moment';

export const enum StatutTransactionReward {
  GRANTED = 'GRANTED',
  THREESHOLD = 'THREESHOLD',
  FAILED = 'FAILED'
}

export interface ITransactionReward {
  id?: number;
  rewardStatus?: StatutTransactionReward;
  rewardMessage?: string;
  rewardValue?: number;
  rewardDateTime?: Moment;
  rewardId?: number;
  transactionId?: number;
}

export class TransactionReward implements ITransactionReward {
  constructor(
    public id?: number,
    public rewardStatus?: StatutTransactionReward,
    public rewardMessage?: string,
    public rewardValue?: number,
    public rewardDateTime?: Moment,
    public rewardId?: number,
    public transactionId?: number
  ) {}
}
