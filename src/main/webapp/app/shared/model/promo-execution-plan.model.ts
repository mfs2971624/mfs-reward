import { Moment } from 'moment';

export interface IPromoExecutionPlan {
  id?: number;
  dateDebut?: Moment;
  dateFin?: Moment;
  promotionId?: number;
}

export class PromoExecutionPlan implements IPromoExecutionPlan {
  constructor(public id?: number, public dateDebut?: Moment, public dateFin?: Moment, public promotionId?: number) {}
}
