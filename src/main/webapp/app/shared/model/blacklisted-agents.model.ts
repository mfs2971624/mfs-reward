import { Moment } from 'moment';

export interface IBlacklistedAgents {
  id?: number;
  agentMsisdn?: string;
  blackListedDate?: Moment;
  blackListedBy?: string;
  agentType?: string;
  spare1?: string;
  spare2?: string;
  spare3?: string;
  spare4?: string;
  spare5?: string;
}

export class BlacklistedAgents implements IBlacklistedAgents {
  constructor(
    public id?: number,
    public agentMsisdn?: string,
    public blackListedDate?: Moment,
    public blackListedBy?: string,
    public agentType?: string,
    public spare1?: string,
    public spare2?: string,
    public spare3?: string,
    public spare4?: string,
    public spare5?: string
  ) {}
}
