import { Moment } from 'moment';
import { PaymentStatus } from 'app/shared/model/enumerations/payment-status.model';

export interface IIncentiveTransfers {
  id?: number;
  agentMsisdn?: string;
  amount?: number;
  paymentDate?: Moment;
  paymentStatus?: PaymentStatus;
  mobiquityResponse?: string;
  spare1?: string;
  spare2?: string;
  spare3?: string;
  spare4?: string;
  spare5?: string;
  commissionDisbursementId?: number;
}

export class IncentiveTransfers implements IIncentiveTransfers {
  constructor(
    public id?: number,
    public agentMsisdn?: string,
    public amount?: number,
    public paymentDate?: Moment,
    public paymentStatus?: PaymentStatus,
    public mobiquityResponse?: string,
    public spare1?: string,
    public spare2?: string,
    public spare3?: string,
    public spare4?: string,
    public spare5?: string,
    public commissionDisbursementId?: number
  ) {}
}
