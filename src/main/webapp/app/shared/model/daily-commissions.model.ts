import { Moment } from 'moment';

export interface IDailyCommissions {
  id?: number;
  agentMsisdn?: string;
  lastModifiedDate?: Moment;
  lastTransactionId?: string;
  totalCashinTransactions?: number;
  totalCashoutTransactions?: number;
  totalIncentiveCommissions?: number;
  spare1?: string;
  spare2?: string;
  spare3?: string;
  spare4?: string;
  spare5?: string;
}

export class DailyCommissions implements IDailyCommissions {
  constructor(
    public id?: number,
    public agentMsisdn?: string,
    public lastModifiedDate?: Moment,
    public lastTransactionId?: string,
    public totalCashinTransactions?: number,
    public totalCashoutTransactions?: number,
    public totalIncentiveCommissions?: number,
    public spare1?: string,
    public spare2?: string,
    public spare3?: string,
    public spare4?: string,
    public spare5?: string
  ) {}
}
