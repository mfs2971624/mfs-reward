import { Moment } from 'moment';

export interface ISubscribersUncomes {
  id?: number;
  subscriberMsisdn?: string;
  processDate?: Moment;
  uncome?: number;
  spare1?: string;
  spare2?: string;
  spare3?: string;
  spare4?: string;
  spare5?: string;
}

export class SubscribersUncomes implements ISubscribersUncomes {
  constructor(
    public id?: number,
    public subscriberMsisdn?: string,
    public processDate?: Moment,
    public uncome?: number,
    public spare1?: string,
    public spare2?: string,
    public spare3?: string,
    public spare4?: string,
    public spare5?: string
  ) {}
}
