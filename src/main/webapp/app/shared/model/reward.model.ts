import { ITransactionReward } from 'app/shared/model/transaction-reward.model';

export interface IReward {
  id?: number;
  name?: string;
  rewardType?: string;
  taux?: number;
  rewardNotifMessage?: string;
  countTreshold?: number;
  valueTreshold?: number;
  transactionRewards?: ITransactionReward[];
  promotionId?: number;
}

export class Reward implements IReward {
  constructor(
    public id?: number,
    public name?: string,
    public rewardType?: string,
    public taux?: number,
    public rewardNotifMessage?: string,
    public countTreshold?: number,
    public valueTreshold?: number,
    public transactionRewards?: ITransactionReward[],
    public promotionId?: number
  ) {}
}
