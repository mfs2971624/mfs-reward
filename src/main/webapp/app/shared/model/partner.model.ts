import { Moment } from 'moment';

export const enum Statut {
  ACTIVE = 'ACTIVE',
  DISABLED = 'DISABLED'
}

export interface IPartner {
  id?: number;
  name?: string;
  callbackUrl?: string;
  status?: Statut;
}

export class Partner implements IPartner {
  constructor(public id?: number, public name?: string, public callbackUrl?: string, public status?: Statut) {}
}
