import { Moment } from 'moment';
import { IPromotion } from 'app/shared/model/promotion.model';

export const enum Statut {
  ACTIVE = 'ACTIVE',
  DISABLED = 'DISABLED'
}

export interface ICampagne {
  id?: number;
  nom?: string;
  dateDebut?: Moment;
  dateFin?: Moment;
  statut?: Statut;
  promotions?: IPromotion[];
}

export class Campagne implements ICampagne {
  constructor(
    public id?: number,
    public nom?: string,
    public dateDebut?: Moment,
    public dateFin?: Moment,
    public statut?: Statut,
    public promotions?: IPromotion[]
  ) {}
}
