/* eslint-disable */
import { Moment } from 'moment';

export interface IDailyCommissionTransactions {
  id?: number;
  agentMsisdn?: string;
  processDate?: Moment;
  totalTransactionsVolume?: number;
  totalCommissions?: number;
  spare1?: string;
  spare2?: string;
  spare3?: string;
  spare4?: string;
  spare5?: string;
}

export class DailyCommissionTransactions implements IDailyCommissionTransactions {
  constructor(
    public id?: number,
    public agentMsisdn?: string,
    public processDate?: Moment,
    public totalTransactionsVolume?: number,
    public totalCommissions?: number,
    public spare1?: string,
    public spare2?: string,
    public spare3?: string,
    public spare4?: string,
    public spare5?: string
  ) {}
}
