import { IReward } from 'app/shared/model/reward.model';
import { IPromoExecutionPlan } from 'app/shared/model/promo-execution-plan.model';

export const enum TypeFrequence {
  Daily = 'Daily',
  Weekly = 'Weekly',
  Monthly = 'Monthly'
}

export const enum Statut {
  ACTIVE = 'ACTIVE',
  DISABLED = 'DISABLED'
}

export interface IPromotion {
  id?: number;
  nom?: string;
  critereEligilibite?: string;
  frequence?: TypeFrequence;
  frequenceDetails?: number;
  statut?: Statut;
  rewards?: IReward[];
  promoExecutionPlans?: IPromoExecutionPlan[];
  campagneId?: number;
}

export class Promotion implements IPromotion {
  constructor(
    public id?: number,
    public nom?: string,
    public critereEligilibite?: string,
    public frequence?: TypeFrequence,
    public frequenceDetails?: number,
    public statut?: Statut,
    public rewards?: IReward[],
    public promoExecutionPlans?: IPromoExecutionPlan[],
    public campagneId?: number
  ) {}
}
