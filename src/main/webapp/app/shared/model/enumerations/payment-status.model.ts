export const enum PaymentStatus {
  PENDING = 'PENDING',

  FAILED = 'FAILED',

  SUCCESS = 'SUCCESS'
}
