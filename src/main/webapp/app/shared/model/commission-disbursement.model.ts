import { Moment } from 'moment';
import { IIncentiveTransfers } from 'app/shared/model/incentive-transfers.model';
import { ProcessingStatus } from 'app/shared/model/enumerations/processing-status.model';

export interface ICommissionDisbursement {
  id?: number;
  name?: string;
  startDate?: Moment;
  endDate?: Moment;
  validated?: boolean;
  validationDate?: Moment;
  processingStatus?: ProcessingStatus;
  spare1?: string;
  spare2?: string;
  spare3?: string;
  spare4?: string;
  spare5?: string;
  incentiveTransfers?: IIncentiveTransfers[];
}

export class CommissionDisbursement implements ICommissionDisbursement {
  constructor(
    public id?: number,
    public name?: string,
    public startDate?: Moment,
    public endDate?: Moment,
    public validated?: boolean,
    public validationDate?: Moment,
    public processingStatus?: ProcessingStatus,
    public spare1?: string,
    public spare2?: string,
    public spare3?: string,
    public spare4?: string,
    public spare5?: string,
    public incentiveTransfers?: IIncentiveTransfers[]
  ) {
    this.validated = this.validated || false;
  }
}
