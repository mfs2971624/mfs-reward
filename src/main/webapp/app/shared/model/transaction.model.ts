import { ITransactionReward } from 'app/shared/model/transaction-reward.model';

export const enum StatutTraitementTransaction {
  INITIATED = 'INITIATED',
  PROCESSED = 'PROCESSED',
  REWARDED = 'REWARDED',
  FAILED = 'FAILED'
}

export interface ITransaction {
  id?: number;
  requestId?: string;
  trxIdMobiquity?: string;
  trxType?: string;
  trxPartnerType?: string;
  trxPartnerId?: string;
  trxCustomerMSISDN?: string;
  trxCustomerAccountNumber?: string;
  trxAmount?: string;
  trxChannel?: string;
  trxDate?: string;
  eligiblePromos?: string;
  traitementStatus?: StatutTraitementTransaction;
  spare1?: string;
  spare2?: string;
  transactionRewards?: ITransactionReward[];
}

export class Transaction implements ITransaction {
  constructor(
    public id?: number,
    public requestId?: string,
    public trxIdMobiquity?: string,
    public trxType?: string,
    public trxPartnerType?: string,
    public trxPartnerId?: string,
    public trxCustomerMSISDN?: string,
    public trxCustomerAccountNumber?: string,
    public trxAmount?: string,
    public trxChannel?: string,
    public trxDate?: string,
    public eligiblePromos?: string,
    public traitementStatus?: StatutTraitementTransaction,
    public spare1?: string,
    public spare2?: string,
    public transactionRewards?: ITransactionReward[]
  ) {}
}
