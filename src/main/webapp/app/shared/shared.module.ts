import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MfsrewardSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [MfsrewardSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [MfsrewardSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MfsrewardSharedModule {
  static forRoot() {
    return {
      ngModule: MfsrewardSharedModule
    };
  }
}
