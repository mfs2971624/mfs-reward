import { NgModule } from '@angular/core';

import { MfsrewardSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [MfsrewardSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [MfsrewardSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class MfsrewardSharedCommonModule {}
