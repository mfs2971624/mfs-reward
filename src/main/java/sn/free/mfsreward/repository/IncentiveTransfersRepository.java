package sn.free.mfsreward.repository;

import sn.free.mfsreward.domain.IncentiveTransfers;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the IncentiveTransfers entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IncentiveTransfersRepository extends JpaRepository<IncentiveTransfers, Long> {
}
