package sn.free.mfsreward.repository;

import sn.free.mfsreward.domain.DailyCommissionTransactions;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the DailyCommissionTransactions entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DailyCommissionTransactionsRepository extends JpaRepository<DailyCommissionTransactions, Long> {
}
