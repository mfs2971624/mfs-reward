package sn.free.mfsreward.repository;

import sn.free.mfsreward.domain.SubscribersUncomes;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the SubscribersUncomes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubscribersUncomesRepository extends JpaRepository<SubscribersUncomes, Long> {

    Optional<SubscribersUncomes> findFirstBySubscriberMsisdn(String subscriberMsisdn);

}
