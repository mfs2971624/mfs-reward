package sn.free.mfsreward.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.free.mfsreward.domain.Partner;
import sn.free.mfsreward.domain.enumeration.Statut;

import java.util.List;

@Repository
public interface PartnerRepository extends JpaRepository<Partner, Long> {

    List<Partner> findAllByStatusIs(Statut statut);
}
