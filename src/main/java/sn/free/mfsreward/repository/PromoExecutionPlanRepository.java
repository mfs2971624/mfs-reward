package sn.free.mfsreward.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.mfsreward.domain.PromoExecutionPlan;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Collection;


/**
 * Spring Data  repository for the PromoExecutionPlan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PromoExecutionPlanRepository extends JpaRepository<PromoExecutionPlan, Long> {
    Collection<PromoExecutionPlan> findAllByDateDebutIsLessThanEqualAndDateFinIsGreaterThanEqual(Instant debut, Instant fin);

    Page<PromoExecutionPlan> findByPromotionId(Pageable pageable, Long id);
}
