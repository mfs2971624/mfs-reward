package sn.free.mfsreward.repository;

import sn.free.mfsreward.domain.CommissionDisbursement;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CommissionDisbursement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommissionDisbursementRepository extends JpaRepository<CommissionDisbursement, Long> {
}
