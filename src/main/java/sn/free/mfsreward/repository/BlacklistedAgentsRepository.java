package sn.free.mfsreward.repository;

import sn.free.mfsreward.domain.BlacklistedAgents;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the BlacklistedAgents entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BlacklistedAgentsRepository extends JpaRepository<BlacklistedAgents, Long> {

    Optional<BlacklistedAgents> findFirstByAgentMsisdn(String agentMsisdn);

}
