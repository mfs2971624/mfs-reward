package sn.free.mfsreward.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.mfsreward.domain.DailyCommissions;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;
import sn.free.mfsreward.service.dto.DailyCommissionsDTO;

/**
 * Spring Data  repository for the DailyCommissions entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DailyCommissionsRepository extends JpaRepository<DailyCommissions, Long> {

    Optional<DailyCommissions> findFirstByAgentMsisdnAndLastModifiedDateAfter(String agentMsisdn, Instant startDate);

    Page<DailyCommissionsDTO> findByLastModifiedDate(Pageable pageable, Instant today);
}

