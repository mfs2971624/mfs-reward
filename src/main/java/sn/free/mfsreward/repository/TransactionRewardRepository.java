package sn.free.mfsreward.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.mfsreward.domain.TransactionReward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.free.mfsreward.domain.enumeration.StatutTransactionReward;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data  repository for the TransactionReward entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionRewardRepository extends JpaRepository<TransactionReward, Long> {

    List<TransactionReward> findByTransactionId(Long id);

    Page<TransactionReward> findAllByOrderByRewardDateTimeDesc(Pageable pageable);


    List<TransactionReward> findByTransaction_TrxCustomerMSISDNAndTransactionTrxPartnerIdAndRewardStatusAndRewardDateTimeIsBetween(String trxCustomerMSISDN, String trxPartnerId, StatutTransactionReward rewardStatus, Instant rewardDateTimeStart, Instant rewardDateTimeEndend);

}
