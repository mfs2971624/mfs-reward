package sn.free.mfsreward.repository;

import sn.free.mfsreward.domain.Promotion;
import sn.free.mfsreward.domain.Reward;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Reward entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RewardRepository extends JpaRepository<Reward, Long> {

    List<Reward> findAllByPromotionIn(List<Promotion> promotionList);

    Reward getById(Long id);

}
