package sn.free.mfsreward.repository;

import sn.free.mfsreward.domain.Promotion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.mfsreward.domain.enumeration.Statut;
import sn.free.mfsreward.service.dto.PromotionDTO;

import java.util.Collection;
import java.util.List;


/**
 * Spring Data  repository for the Promotion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PromotionRepository extends JpaRepository<Promotion, Long> {

    List<PromotionDTO> findAllByStatutIsAndIdIn(Statut statut, Collection<Long> id);
}
