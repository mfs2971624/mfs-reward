package sn.free.mfsreward.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import sn.free.mfsreward.domain.Transaction;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.mfsreward.service.dto.TransactionDTO;
import java.util.Date;
import java.util.Collection;
import java.util.List;


/**
 * Spring Data  repository for the Transaction entity.
 */
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    Page<Transaction> findByTrxCustomerMSISDN(Pageable pageable, String msisdn);

    Page<Transaction> findAllByOrderByTrxDateDesc(Pageable pageable);

    List<Transaction> findByTrxTypeAndTrxAmountGreaterThanEqual(String type, String amount);

    @Query(value = "SELECT SUM(CAST(t.trxAmount AS float)) FROM Transaction t WHERE (t.trxType = 'CASHOUT' OR t.trxType = 'CASHIN') AND t.trxPartnerId = :agentMsisdn AND t.trxDate >= :startDate")
    double getSumTransactionsTypeDayAgent(@Param("agentMsisdn") String agentMsisdn, @Param("startDate") String startDate);

    @Query("select t from Transaction t  where t.trxCustomerMSISDN = ?1 and t.trxDate like ?2 and t.traitementStatus = 'REWARDED'")
    Collection<Transaction> trouverTransactionParNumParJour(String msisdn, String date);

    default Collection<Transaction> trouverTransactionParNumParJourEnv(String msisdn, String date){
        return trouverTransactionParNumParJour(msisdn, date.substring(0,8)+"%");
    }
}
