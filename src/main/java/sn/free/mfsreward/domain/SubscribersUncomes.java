package sn.free.mfsreward.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A SubscribersUncomes.
 */
@Entity
@Table(name = "subscribers_uncomes")
public class SubscribersUncomes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "subscriber_msisdn")
    private String subscriberMsisdn;

    @Column(name = "process_date")
    private Instant processDate;

    @Column(name = "uncome")
    private Double uncome;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @Column(name = "spare_4")
    private String spare4;

    @Column(name = "spare_5")
    private String spare5;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubscriberMsisdn() {
        return subscriberMsisdn;
    }

    public SubscribersUncomes subscriberMsisdn(String subscriberMsisdn) {
        this.subscriberMsisdn = subscriberMsisdn;
        return this;
    }

    public void setSubscriberMsisdn(String subscriberMsisdn) {
        this.subscriberMsisdn = subscriberMsisdn;
    }

    public Instant getProcessDate() {
        return processDate;
    }

    public SubscribersUncomes processDate(Instant processDate) {
        this.processDate = processDate;
        return this;
    }

    public void setProcessDate(Instant processDate) {
        this.processDate = processDate;
    }

    public Double getUncome() {
        return uncome;
    }

    public SubscribersUncomes uncome(Double uncome) {
        this.uncome = uncome;
        return this;
    }

    public void setUncome(Double uncome) {
        this.uncome = uncome;
    }

    public String getSpare1() {
        return spare1;
    }

    public SubscribersUncomes spare1(String spare1) {
        this.spare1 = spare1;
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public SubscribersUncomes spare2(String spare2) {
        this.spare2 = spare2;
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public SubscribersUncomes spare3(String spare3) {
        this.spare3 = spare3;
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public SubscribersUncomes spare4(String spare4) {
        this.spare4 = spare4;
        return this;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

    public String getSpare5() {
        return spare5;
    }

    public SubscribersUncomes spare5(String spare5) {
        this.spare5 = spare5;
        return this;
    }

    public void setSpare5(String spare5) {
        this.spare5 = spare5;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SubscribersUncomes)) {
            return false;
        }
        return id != null && id.equals(((SubscribersUncomes) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SubscribersUncomes{" +
            "id=" + getId() +
            ", subscriberMsisdn='" + getSubscriberMsisdn() + "'" +
            ", processDate='" + getProcessDate() + "'" +
            ", uncome=" + getUncome() +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", spare4='" + getSpare4() + "'" +
            ", spare5='" + getSpare5() + "'" +
            "}";
    }
}
