package sn.free.mfsreward.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import sn.free.mfsreward.domain.enumeration.TypeFrequence;

import sn.free.mfsreward.domain.enumeration.Statut;

/**
 * A Promotion.
 */
@Entity
@Table(name = "promotion")
public class Promotion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @NotNull
    @Column(name = "critere_eligilibite", nullable = false)
    private String critereEligilibite;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "frequence", nullable = false)
    private TypeFrequence frequence;

    @Column(name = "frequence_details")
    private Integer frequenceDetails;

    @Enumerated(EnumType.STRING)
    @Column(name = "statut")
    private Statut statut;

    @OneToMany(mappedBy = "promotion")
    private Set<Reward> rewards = new HashSet<>();

    @OneToMany(mappedBy = "promotion")
    private Set<PromoExecutionPlan> promoExecutionPlans = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("promotions")
    private Campagne campagne;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Promotion nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCritereEligilibite() {
        return critereEligilibite;
    }

    public Promotion critereEligilibite(String critereEligilibite) {
        this.critereEligilibite = critereEligilibite;
        return this;
    }

    public void setCritereEligilibite(String critereEligilibite) {
        this.critereEligilibite = critereEligilibite;
    }

    public TypeFrequence getFrequence() {
        return frequence;
    }

    public Promotion frequence(TypeFrequence frequence) {
        this.frequence = frequence;
        return this;
    }

    public void setFrequence(TypeFrequence frequence) {
        this.frequence = frequence;
    }

    public Integer getFrequenceDetails() {
        return frequenceDetails;
    }

    public Promotion frequenceDetails(Integer frequenceDetails) {
        this.frequenceDetails = frequenceDetails;
        return this;
    }

    public void setFrequenceDetails(Integer frequenceDetails) {
        this.frequenceDetails = frequenceDetails;
    }

    public Statut getStatut() {
        return statut;
    }

    public Promotion statut(Statut statut) {
        this.statut = statut;
        return this;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Set<Reward> getRewards() {
        return rewards;
    }

    public Promotion rewards(Set<Reward> rewards) {
        this.rewards = rewards;
        return this;
    }

    public Promotion addReward(Reward reward) {
        this.rewards.add(reward);
        reward.setPromotion(this);
        return this;
    }

    public Promotion removeReward(Reward reward) {
        this.rewards.remove(reward);
        reward.setPromotion(null);
        return this;
    }

    public void setRewards(Set<Reward> rewards) {
        this.rewards = rewards;
    }

    public Set<PromoExecutionPlan> getPromoExecutionPlans() {
        return promoExecutionPlans;
    }

    public Promotion promoExecutionPlans(Set<PromoExecutionPlan> promoExecutionPlans) {
        this.promoExecutionPlans = promoExecutionPlans;
        return this;
    }

    public Promotion addPromoExecutionPlan(PromoExecutionPlan promoExecutionPlan) {
        this.promoExecutionPlans.add(promoExecutionPlan);
        promoExecutionPlan.setPromotion(this);
        return this;
    }

    public Promotion removePromoExecutionPlan(PromoExecutionPlan promoExecutionPlan) {
        this.promoExecutionPlans.remove(promoExecutionPlan);
        promoExecutionPlan.setPromotion(null);
        return this;
    }

    public void setPromoExecutionPlans(Set<PromoExecutionPlan> promoExecutionPlans) {
        this.promoExecutionPlans = promoExecutionPlans;
    }

    public Campagne getCampagne() {
        return campagne;
    }

    public Promotion campagne(Campagne campagne) {
        this.campagne = campagne;
        return this;
    }

    public void setCampagne(Campagne campagne) {
        this.campagne = campagne;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Promotion)) {
            return false;
        }
        return id != null && id.equals(((Promotion) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Promotion{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", critereEligilibite='" + getCritereEligilibite() + "'" +
            ", frequence='" + getFrequence() + "'" +
            ", frequenceDetails=" + getFrequenceDetails() +
            ", statut='" + getStatut() + "'" +
            "}";
    }
}
