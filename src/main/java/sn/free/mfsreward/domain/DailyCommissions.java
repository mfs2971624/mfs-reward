package sn.free.mfsreward.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A DailyCommissions.
 */
@Entity
@Table(name = "daily_commissions")
public class DailyCommissions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "agent_msisdn")
    private String agentMsisdn;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "last_transaction_id")
    private String lastTransactionId;

    @Column(name = "total_cashin_transactions")
    private Double totalCashinTransactions;

    @Column(name = "total_cashout_transactions")
    private Double totalCashoutTransactions;

    @Column(name = "total_incentive_commissions")
    private Double totalIncentiveCommissions;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @Column(name = "spare_4")
    private String spare4;

    @Column(name = "spare_5")
    private String spare5;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentMsisdn() {
        return agentMsisdn;
    }

    public DailyCommissions agentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
        return this;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public DailyCommissions lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastTransactionId() {
        return lastTransactionId;
    }

    public DailyCommissions lastTransactionId(String lastTransactionId) {
        this.lastTransactionId = lastTransactionId;
        return this;
    }

    public void setLastTransactionId(String lastTransactionId) {
        this.lastTransactionId = lastTransactionId;
    }

    public Double getTotalCashinTransactions() {
        return totalCashinTransactions;
    }

    public DailyCommissions totalCashinTransactions(Double totalCashinTransactions) {
        this.totalCashinTransactions = totalCashinTransactions;
        return this;
    }

    public void setTotalCashinTransactions(Double totalCashinTransactions) {
        this.totalCashinTransactions = totalCashinTransactions;
    }

    public Double getTotalCashoutTransactions() {
        return totalCashoutTransactions;
    }

    public DailyCommissions totalCashoutTransactions(Double totalCashoutTransactions) {
        this.totalCashoutTransactions = totalCashoutTransactions;
        return this;
    }

    public void setTotalCashoutTransactions(Double totalCashoutTransactions) {
        this.totalCashoutTransactions = totalCashoutTransactions;
    }

    public Double getTotalIncentiveCommissions() {
        return totalIncentiveCommissions;
    }

    public DailyCommissions totalIncentiveCommissions(Double totalIncentiveCommissions) {
        this.totalIncentiveCommissions = totalIncentiveCommissions;
        return this;
    }

    public void setTotalIncentiveCommissions(Double totalIncentiveCommissions) {
        this.totalIncentiveCommissions = totalIncentiveCommissions;
    }

    public String getSpare1() {
        return spare1;
    }

    public DailyCommissions spare1(String spare1) {
        this.spare1 = spare1;
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public DailyCommissions spare2(String spare2) {
        this.spare2 = spare2;
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public DailyCommissions spare3(String spare3) {
        this.spare3 = spare3;
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public DailyCommissions spare4(String spare4) {
        this.spare4 = spare4;
        return this;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

    public String getSpare5() {
        return spare5;
    }

    public DailyCommissions spare5(String spare5) {
        this.spare5 = spare5;
        return this;
    }

    public void setSpare5(String spare5) {
        this.spare5 = spare5;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DailyCommissions)) {
            return false;
        }
        return id != null && id.equals(((DailyCommissions) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DailyCommissions{" +
            "id=" + getId() +
            ", agentMsisdn='" + getAgentMsisdn() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastTransactionId='" + getLastTransactionId() + "'" +
            ", totalCashinTransactions=" + getTotalCashinTransactions() +
            ", totalCashoutTransactions=" + getTotalCashoutTransactions() +
            ", totalIncentiveCommissions=" + getTotalIncentiveCommissions() +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", spare4='" + getSpare4() + "'" +
            ", spare5='" + getSpare5() + "'" +
            "}";
    }
}
