package sn.free.mfsreward.domain;

public class MyTreshold {
    private double somme;
    private int count;
    private boolean sommeAtteint;
    private boolean countAtteint;

    public MyTreshold() {
        this.count = 0;
        this.somme = 0D;
        this.countAtteint = false;
        this.sommeAtteint = false;
    }

    public double getSomme() {
        return somme;
    }

    public void setSomme(double somme) {
        this.somme = somme;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isSommeAtteint() {
        return sommeAtteint;
    }

    public void setSommeAtteint(boolean sommeAtteint) {
        this.sommeAtteint = sommeAtteint;
    }

    public boolean isCountAtteint() {
        return countAtteint;
    }

    public void setCountAtteint(boolean countAtteint) {
        this.countAtteint = countAtteint;
    }
}
