package sn.free.mfsreward.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

import sn.free.mfsreward.domain.enumeration.StatutTransactionReward;

/**
 * A TransactionReward.
 */
@Entity
@Table(name = "transaction_reward")
public class TransactionReward implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "reward_status")
    private StatutTransactionReward rewardStatus;

    @Column(name = "reward_message")
    private String rewardMessage;

    @Column(name = "reward_value")
    private Double rewardValue;

    @Column(name = "reward_date_time")
    private Instant rewardDateTime;

    @ManyToOne
    @JsonIgnoreProperties("transactionRewards")
    private Reward reward;

    @ManyToOne
    @JsonIgnoreProperties("transactionRewards")
    private Transaction transaction;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StatutTransactionReward getRewardStatus() {
        return rewardStatus;
    }

    public TransactionReward rewardStatus(StatutTransactionReward rewardStatus) {
        this.rewardStatus = rewardStatus;
        return this;
    }

    public void setRewardStatus(StatutTransactionReward rewardStatus) {
        this.rewardStatus = rewardStatus;
    }

    public String getRewardMessage() {
        return rewardMessage;
    }

    public TransactionReward rewardMessage(String rewardMessage) {
        this.rewardMessage = rewardMessage;
        return this;
    }

    public void setRewardMessage(String rewardMessage) {
        this.rewardMessage = rewardMessage;
    }

    public Double getRewardValue() {
        return rewardValue;
    }

    public TransactionReward rewardValue(Double rewardValue) {
        this.rewardValue = rewardValue;
        return this;
    }

    public void setRewardValue(Double rewardValue) {
        this.rewardValue = rewardValue;
    }

    public Instant getRewardDateTime() {
        return rewardDateTime;
    }

    public TransactionReward rewardDateTime(Instant rewardDateTime) {
        this.rewardDateTime = rewardDateTime;
        return this;
    }

    public void setRewardDateTime(Instant rewardDateTime) {
        this.rewardDateTime = rewardDateTime;
    }

    public Reward getReward() {
        return reward;
    }

    public TransactionReward reward(Reward reward) {
        this.reward = reward;
        return this;
    }

    public void setReward(Reward reward) {
        this.reward = reward;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public TransactionReward transaction(Transaction transaction) {
        this.transaction = transaction;
        return this;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactionReward)) {
            return false;
        }
        return id != null && id.equals(((TransactionReward) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TransactionReward{" +
            "id=" + getId() +
            ", rewardStatus='" + getRewardStatus() + "'" +
            ", rewardMessage='" + getRewardMessage() + "'" +
            ", rewardValue=" + getRewardValue() +
            ", rewardDateTime='" + getRewardDateTime() + "'" +
            "}";
    }
}
