package sn.free.mfsreward.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A DailyCommissionTransactions.
 */
@Entity
@Table(name = "daily_commission_transactions")
public class DailyCommissionTransactions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "agent_msisdn")
    private String agentMsisdn;

    @Column(name = "process_date")
    private Instant processDate;

    @Column(name = "total_transactions_volume")
    private Double totalTransactionsVolume;

    @Column(name = "total_commissions")
    private Double totalCommissions;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @Column(name = "spare_4")
    private String spare4;

    @Column(name = "spare_5")
    private String spare5;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentMsisdn() {
        return agentMsisdn;
    }

    public DailyCommissionTransactions agentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
        return this;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public Instant getProcessDate() {
        return processDate;
    }

    public DailyCommissionTransactions processDate(Instant processDate) {
        this.processDate = processDate;
        return this;
    }

    public void setProcessDate(Instant processDate) {
        this.processDate = processDate;
    }

    public Double getTotalTransactionsVolume() {
        return totalTransactionsVolume;
    }

    public DailyCommissionTransactions totalTransactionsVolume(Double totalTransactionsVolume) {
        this.totalTransactionsVolume = totalTransactionsVolume;
        return this;
    }

    public void setTotalTransactionsVolume(Double totalTransactionsVolume) {
        this.totalTransactionsVolume = totalTransactionsVolume;
    }

    public Double getTotalCommissions() {
        return totalCommissions;
    }

    public DailyCommissionTransactions totalCommissions(Double totalCommissions) {
        this.totalCommissions = totalCommissions;
        return this;
    }

    public void setTotalCommissions(Double totalCommissions) {
        this.totalCommissions = totalCommissions;
    }

    public String getSpare1() {
        return spare1;
    }

    public DailyCommissionTransactions spare1(String spare1) {
        this.spare1 = spare1;
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public DailyCommissionTransactions spare2(String spare2) {
        this.spare2 = spare2;
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public DailyCommissionTransactions spare3(String spare3) {
        this.spare3 = spare3;
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public DailyCommissionTransactions spare4(String spare4) {
        this.spare4 = spare4;
        return this;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

    public String getSpare5() {
        return spare5;
    }

    public DailyCommissionTransactions spare5(String spare5) {
        this.spare5 = spare5;
        return this;
    }

    public void setSpare5(String spare5) {
        this.spare5 = spare5;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DailyCommissionTransactions)) {
            return false;
        }
        return id != null && id.equals(((DailyCommissionTransactions) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DailyCommissionTransactions{" +
            "id=" + getId() +
            ", agentMsisdn='" + getAgentMsisdn() + "'" +
            ", processDate='" + getProcessDate() + "'" +
            ", totalTransactionsVolume=" + getTotalTransactionsVolume() +
            ", totalCommissions=" + getTotalCommissions() +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", spare4='" + getSpare4() + "'" +
            ", spare5='" + getSpare5() + "'" +
            "}";
    }
}
