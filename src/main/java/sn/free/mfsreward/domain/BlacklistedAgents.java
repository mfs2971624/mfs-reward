package sn.free.mfsreward.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * not an ignored comment
 */
@Entity
@Table(name = "blacklisted_agents")
public class BlacklistedAgents implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "agent_msisdn")
    private String agentMsisdn;

    @Column(name = "black_listed_date")
    private ZonedDateTime blackListedDate;

    @Column(name = "black_listed_by")
    private String blackListedBy;

    @Column(name = "agent_type")
    private String agentType;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @Column(name = "spare_4")
    private String spare4;

    @Column(name = "spare_5")
    private String spare5;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentMsisdn() {
        return agentMsisdn;
    }

    public BlacklistedAgents agentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
        return this;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public ZonedDateTime getBlackListedDate() {
        return blackListedDate;
    }

    public BlacklistedAgents blackListedDate(ZonedDateTime blackListedDate) {
        this.blackListedDate = blackListedDate;
        return this;
    }

    public void setBlackListedDate(ZonedDateTime blackListedDate) {
        this.blackListedDate = blackListedDate;
    }

    public String getBlackListedBy() {
        return blackListedBy;
    }

    public BlacklistedAgents blackListedBy(String blackListedBy) {
        this.blackListedBy = blackListedBy;
        return this;
    }

    public void setBlackListedBy(String blackListedBy) {
        this.blackListedBy = blackListedBy;
    }

    public String getAgentType() {
        return agentType;
    }

    public BlacklistedAgents agentType(String agentType) {
        this.agentType = agentType;
        return this;
    }

    public void setAgentType(String agentType) {
        this.agentType = agentType;
    }

    public String getSpare1() {
        return spare1;
    }

    public BlacklistedAgents spare1(String spare1) {
        this.spare1 = spare1;
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public BlacklistedAgents spare2(String spare2) {
        this.spare2 = spare2;
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public BlacklistedAgents spare3(String spare3) {
        this.spare3 = spare3;
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public BlacklistedAgents spare4(String spare4) {
        this.spare4 = spare4;
        return this;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

    public String getSpare5() {
        return spare5;
    }

    public BlacklistedAgents spare5(String spare5) {
        this.spare5 = spare5;
        return this;
    }

    public void setSpare5(String spare5) {
        this.spare5 = spare5;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BlacklistedAgents)) {
            return false;
        }
        return id != null && id.equals(((BlacklistedAgents) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BlacklistedAgents{" +
            "id=" + getId() +
            ", agentMsisdn='" + getAgentMsisdn() + "'" +
            ", blackListedDate='" + getBlackListedDate() + "'" +
            ", blackListedBy='" + getBlackListedBy() + "'" +
            ", agentType='" + getAgentType() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", spare4='" + getSpare4() + "'" +
            ", spare5='" + getSpare5() + "'" +
            "}";
    }
}
