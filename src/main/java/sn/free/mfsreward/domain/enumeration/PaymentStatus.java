package sn.free.mfsreward.domain.enumeration;

/**
 * The PaymentStatus enumeration.
 */
public enum PaymentStatus {
    PENDING, FAILED, SUCCESS
}
