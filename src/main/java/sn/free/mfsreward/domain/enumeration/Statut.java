package sn.free.mfsreward.domain.enumeration;

/**
 * The Statut enumeration.
 */
public enum Statut {
    ACTIVE, DISABLED
}
