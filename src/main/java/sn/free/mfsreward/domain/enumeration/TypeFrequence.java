package sn.free.mfsreward.domain.enumeration;

/**
 * The TypeFrequence enumeration.
 */
public enum TypeFrequence {
    Daily, Weekly, Monthly
}
