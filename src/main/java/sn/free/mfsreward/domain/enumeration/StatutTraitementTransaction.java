package sn.free.mfsreward.domain.enumeration;

/**
 * The StatutTraitementTransaction enumeration.
 */
public enum StatutTraitementTransaction {
    INITIATED, PROCESSED, REWARDED, FAILED
}
