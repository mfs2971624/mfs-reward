package sn.free.mfsreward.domain.enumeration;

/**
 * The ProcessingStatus enumeration.
 */
public enum ProcessingStatus {
    INITIATED, VALIDATED, PROCESSING, PROCESSED
}
