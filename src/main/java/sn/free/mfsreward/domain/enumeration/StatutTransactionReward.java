package sn.free.mfsreward.domain.enumeration;

/**
 * The StatutTransactionReward enumeration.
 */
public enum StatutTransactionReward {
    GRANTED, NOTGRANTED, THREESHOLD, FAILED
}
