package sn.free.mfsreward.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import sn.free.mfsreward.domain.enumeration.StatutTraitementTransaction;

/**
 * A Transaction.
 */
@Entity
@Table(name = "jhi_transaction")
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "request_id")
    private String requestId;

    @Column(name = "trx_id_mobiquity")
    private String trxIdMobiquity;

    @Column(name = "trx_type")
    private String trxType;

    @Column(name = "trx_partner_type")
    private String trxPartnerType;

    @Column(name = "trx_partner_id")
    private String trxPartnerId;

    @Column(name = "trx_customer_msisdn")
    private String trxCustomerMSISDN;

    @Column(name = "trx_customer_account_number")
    private String trxCustomerAccountNumber;

    @Column(name = "trx_amount")
    private String trxAmount;

    @Column(name = "trx_channel")
    private String trxChannel;

    @Column(name = "trx_date")
    private String trxDate;

    @Column(name = "eligible_promos")
    private String eligiblePromos;

    @Enumerated(EnumType.STRING)
    @Column(name = "traitement_status")
    private StatutTraitementTransaction traitementStatus;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @OneToMany(mappedBy = "transaction")
    private Set<TransactionReward> transactionRewards = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequestId() {
        return requestId;
    }

    public Transaction requestId(String requestId) {
        this.requestId = requestId;
        return this;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTrxIdMobiquity() {
        return trxIdMobiquity;
    }

    public Transaction trxIdMobiquity(String trxIdMobiquity) {
        this.trxIdMobiquity = trxIdMobiquity;
        return this;
    }

    public void setTrxIdMobiquity(String trxIdMobiquity) {
        this.trxIdMobiquity = trxIdMobiquity;
    }

    public String getTrxType() {
        return trxType;
    }

    public Transaction trxType(String trxType) {
        this.trxType = trxType;
        return this;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public String getTrxPartnerType() {
        return trxPartnerType;
    }

    public Transaction trxPartnerType(String trxPartnerType) {
        this.trxPartnerType = trxPartnerType;
        return this;
    }

    public void setTrxPartnerType(String trxPartnerType) {
        this.trxPartnerType = trxPartnerType;
    }

    public String getTrxPartnerId() {
        return trxPartnerId;
    }

    public Transaction trxPartnerId(String trxPartnerId) {
        this.trxPartnerId = trxPartnerId;
        return this;
    }

    public void setTrxPartnerId(String trxPartnerId) {
        this.trxPartnerId = trxPartnerId;
    }

    public String getTrxCustomerMSISDN() {
        return trxCustomerMSISDN;
    }

    public Transaction trxCustomerMSISDN(String trxCustomerMSISDN) {
        this.trxCustomerMSISDN = trxCustomerMSISDN;
        return this;
    }

    public void setTrxCustomerMSISDN(String trxCustomerMSISDN) {
        this.trxCustomerMSISDN = trxCustomerMSISDN;
    }

    public String getTrxCustomerAccountNumber() {
        return trxCustomerAccountNumber;
    }

    public Transaction trxCustomerAccountNumber(String trxCustomerAccountNumber) {
        this.trxCustomerAccountNumber = trxCustomerAccountNumber;
        return this;
    }

    public void setTrxCustomerAccountNumber(String trxCustomerAccountNumber) {
        this.trxCustomerAccountNumber = trxCustomerAccountNumber;
    }

    public String getTrxAmount() {
        return trxAmount;
    }

    public Transaction trxAmount(String trxAmount) {
        this.trxAmount = trxAmount;
        return this;
    }

    public void setTrxAmount(String trxAmount) {
        this.trxAmount = trxAmount;
    }

    public String getTrxChannel() {
        return trxChannel;
    }

    public Transaction trxChannel(String trxChannel) {
        this.trxChannel = trxChannel;
        return this;
    }

    public void setTrxChannel(String trxChannel) {
        this.trxChannel = trxChannel;
    }

    public String getTrxDate() {
        return trxDate;
    }

    public Transaction trxDate(String trxDate) {
        this.trxDate = trxDate;
        return this;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }

    public String getEligiblePromos() {
        return eligiblePromos;
    }

    public Transaction eligiblePromos(String eligiblePromos) {
        this.eligiblePromos = eligiblePromos;
        return this;
    }

    public void setEligiblePromos(String eligiblePromos) {
        this.eligiblePromos = eligiblePromos;
    }

    public StatutTraitementTransaction getTraitementStatus() {
        return traitementStatus;
    }

    public Transaction traitementStatus(StatutTraitementTransaction traitementStatus) {
        this.traitementStatus = traitementStatus;
        return this;
    }

    public void setTraitementStatus(StatutTraitementTransaction traitementStatus) {
        this.traitementStatus = traitementStatus;
    }

    public String getSpare1() {
        return spare1;
    }

    public Transaction spare1(String spare1) {
        this.spare1 = spare1;
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public Transaction spare2(String spare2) {
        this.spare2 = spare2;
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public Set<TransactionReward> getTransactionRewards() {
        return transactionRewards;
    }

    public Transaction transactionRewards(Set<TransactionReward> transactionRewards) {
        this.transactionRewards = transactionRewards;
        return this;
    }

    public Transaction addTransactionReward(TransactionReward transactionReward) {
        this.transactionRewards.add(transactionReward);
        transactionReward.setTransaction(this);
        return this;
    }

    public Transaction removeTransactionReward(TransactionReward transactionReward) {
        this.transactionRewards.remove(transactionReward);
        transactionReward.setTransaction(null);
        return this;
    }

    public void setTransactionRewards(Set<TransactionReward> transactionRewards) {
        this.transactionRewards = transactionRewards;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transaction)) {
            return false;
        }
        return id != null && id.equals(((Transaction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Transaction{" +
            "id=" + getId() +
            ", requestId='" + getRequestId() + "'" +
            ", trxIdMobiquity='" + getTrxIdMobiquity() + "'" +
            ", trxType='" + getTrxType() + "'" +
            ", trxPartnerType='" + getTrxPartnerType() + "'" +
            ", trxPartnerId='" + getTrxPartnerId() + "'" +
            ", trxCustomerMSISDN='" + getTrxCustomerMSISDN() + "'" +
            ", trxCustomerAccountNumber='" + getTrxCustomerAccountNumber() + "'" +
            ", trxAmount='" + getTrxAmount() + "'" +
            ", trxChannel='" + getTrxChannel() + "'" +
            ", trxDate='" + getTrxDate() + "'" +
            ", eligiblePromos='" + getEligiblePromos() + "'" +
            ", traitementStatus='" + getTraitementStatus() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            "}";
    }
}
