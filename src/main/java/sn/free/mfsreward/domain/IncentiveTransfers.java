package sn.free.mfsreward.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

import sn.free.mfsreward.domain.enumeration.PaymentStatus;

/**
 * A IncentiveTransfers.
 */
@Entity
@Table(name = "incentive_transfers")
public class IncentiveTransfers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "agent_msisdn")
    private String agentMsisdn;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "payment_date")
    private Instant paymentDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;

    @Column(name = "mobiquity_response")
    private String mobiquityResponse;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @Column(name = "spare_4")
    private String spare4;

    @Column(name = "spare_5")
    private String spare5;

    @ManyToOne
    @JsonIgnoreProperties(value = "incentiveTransfers", allowSetters = true)
    private CommissionDisbursement commissionDisbursement;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentMsisdn() {
        return agentMsisdn;
    }

    public IncentiveTransfers agentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
        return this;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public Double getAmount() {
        return amount;
    }

    public IncentiveTransfers amount(Double amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Instant getPaymentDate() {
        return paymentDate;
    }

    public IncentiveTransfers paymentDate(Instant paymentDate) {
        this.paymentDate = paymentDate;
        return this;
    }

    public void setPaymentDate(Instant paymentDate) {
        this.paymentDate = paymentDate;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public IncentiveTransfers paymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
        return this;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getMobiquityResponse() {
        return mobiquityResponse;
    }

    public IncentiveTransfers mobiquityResponse(String mobiquityResponse) {
        this.mobiquityResponse = mobiquityResponse;
        return this;
    }

    public void setMobiquityResponse(String mobiquityResponse) {
        this.mobiquityResponse = mobiquityResponse;
    }

    public String getSpare1() {
        return spare1;
    }

    public IncentiveTransfers spare1(String spare1) {
        this.spare1 = spare1;
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public IncentiveTransfers spare2(String spare2) {
        this.spare2 = spare2;
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public IncentiveTransfers spare3(String spare3) {
        this.spare3 = spare3;
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public IncentiveTransfers spare4(String spare4) {
        this.spare4 = spare4;
        return this;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

    public String getSpare5() {
        return spare5;
    }

    public IncentiveTransfers spare5(String spare5) {
        this.spare5 = spare5;
        return this;
    }

    public void setSpare5(String spare5) {
        this.spare5 = spare5;
    }

    public CommissionDisbursement getCommissionDisbursement() {
        return commissionDisbursement;
    }

    public IncentiveTransfers commissionDisbursement(CommissionDisbursement commissionDisbursement) {
        this.commissionDisbursement = commissionDisbursement;
        return this;
    }

    public void setCommissionDisbursement(CommissionDisbursement commissionDisbursement) {
        this.commissionDisbursement = commissionDisbursement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IncentiveTransfers)) {
            return false;
        }
        return id != null && id.equals(((IncentiveTransfers) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "IncentiveTransfers{" +
            "id=" + getId() +
            ", agentMsisdn='" + getAgentMsisdn() + "'" +
            ", amount=" + getAmount() +
            ", paymentDate='" + getPaymentDate() + "'" +
            ", paymentStatus='" + getPaymentStatus() + "'" +
            ", mobiquityResponse='" + getMobiquityResponse() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", spare4='" + getSpare4() + "'" +
            ", spare5='" + getSpare5() + "'" +
            "}";
    }
}
