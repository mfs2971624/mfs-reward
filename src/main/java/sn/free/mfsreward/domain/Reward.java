package sn.free.mfsreward.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Reward.
 */
@Entity
@Table(name = "reward")
public class Reward implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "reward_type", nullable = false)
    private String rewardType;

    @NotNull
    @Column(name = "taux", nullable = false)
    private Double taux;

    @NotNull
    @Column(name = "reward_notif_message", nullable = false)
    private String rewardNotifMessage;

    @Column(name = "count_treshold")
    private Integer countTreshold;

    @Column(name = "value_treshold")
    private Double valueTreshold;

    @OneToMany(mappedBy = "reward")
    private Set<TransactionReward> transactionRewards = new HashSet<>();

    @ManyToOne
    private Promotion promotion;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Reward name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRewardType() {
        return rewardType;
    }

    public Reward rewardType(String rewardType) {
        this.rewardType = rewardType;
        return this;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public Double getTaux() {
        return taux;
    }

    public Reward taux(Double taux) {
        this.taux = taux;
        return this;
    }

    public void setTaux(Double taux) {
        this.taux = taux;
    }

    public String getRewardNotifMessage() {
        return rewardNotifMessage;
    }

    public Reward rewardNotifMessage(String rewardNotifMessage) {
        this.rewardNotifMessage = rewardNotifMessage;
        return this;
    }

    public void setRewardNotifMessage(String rewardNotifMessage) {
        this.rewardNotifMessage = rewardNotifMessage;
    }

    public Integer getCountTreshold() {
        return countTreshold;
    }

    public Reward countTreshold(Integer countTreshold) {
        this.countTreshold = countTreshold;
        return this;
    }

    public void setCountTreshold(Integer countTreshold) {
        this.countTreshold = countTreshold;
    }

    public Double getValueTreshold() {
        return valueTreshold;
    }

    public Reward valueTreshold(Double valueTreshold) {
        this.valueTreshold = valueTreshold;
        return this;
    }

    public void setValueTreshold(Double valueTreshold) {
        this.valueTreshold = valueTreshold;
    }

    public Set<TransactionReward> getTransactionRewards() {
        return transactionRewards;
    }

    public Reward transactionRewards(Set<TransactionReward> transactionRewards) {
        this.transactionRewards = transactionRewards;
        return this;
    }

    public Reward addTransactionReward(TransactionReward transactionReward) {
        this.transactionRewards.add(transactionReward);
        transactionReward.setReward(this);
        return this;
    }

    public Reward removeTransactionReward(TransactionReward transactionReward) {
        this.transactionRewards.remove(transactionReward);
        transactionReward.setReward(null);
        return this;
    }

    public void setTransactionRewards(Set<TransactionReward> transactionRewards) {
        this.transactionRewards = transactionRewards;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public Reward promotion(Promotion promotion) {
        this.promotion = promotion;
        return this;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Reward)) {
            return false;
        }
        return id != null && id.equals(((Reward) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Reward{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", rewardType='" + getRewardType() + "'" +
            ", taux=" + getTaux() +
            ", rewardNotifMessage='" + getRewardNotifMessage() + "'" +
            ", countTreshold=" + getCountTreshold() +
            ", valueTreshold=" + getValueTreshold() +
            "}";
    }
}
