package sn.free.mfsreward.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import sn.free.mfsreward.domain.enumeration.ProcessingStatus;

/**
 * A CommissionDisbursement.
 */
@Entity
@Table(name = "commission_disbursement")
public class CommissionDisbursement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "start_date")
    private Instant startDate;

    @Column(name = "end_date")
    private Instant endDate;

    @Column(name = "validated")
    private Boolean validated;

    @Column(name = "validation_date")
    private ZonedDateTime validationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "processing_status")
    private ProcessingStatus processingStatus;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @Column(name = "spare_4")
    private String spare4;

    @Column(name = "spare_5")
    private String spare5;

    @OneToMany(mappedBy = "commissionDisbursement")
    private Set<IncentiveTransfers> incentiveTransfers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CommissionDisbursement name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public CommissionDisbursement startDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public CommissionDisbursement endDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Boolean isValidated() {
        return validated;
    }

    public CommissionDisbursement validated(Boolean validated) {
        this.validated = validated;
        return this;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public ZonedDateTime getValidationDate() {
        return validationDate;
    }

    public CommissionDisbursement validationDate(ZonedDateTime validationDate) {
        this.validationDate = validationDate;
        return this;
    }

    public void setValidationDate(ZonedDateTime validationDate) {
        this.validationDate = validationDate;
    }

    public ProcessingStatus getProcessingStatus() {
        return processingStatus;
    }

    public CommissionDisbursement processingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
        return this;
    }

    public void setProcessingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
    }

    public String getSpare1() {
        return spare1;
    }

    public CommissionDisbursement spare1(String spare1) {
        this.spare1 = spare1;
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public CommissionDisbursement spare2(String spare2) {
        this.spare2 = spare2;
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public CommissionDisbursement spare3(String spare3) {
        this.spare3 = spare3;
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public CommissionDisbursement spare4(String spare4) {
        this.spare4 = spare4;
        return this;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

    public String getSpare5() {
        return spare5;
    }

    public CommissionDisbursement spare5(String spare5) {
        this.spare5 = spare5;
        return this;
    }

    public void setSpare5(String spare5) {
        this.spare5 = spare5;
    }

    public Set<IncentiveTransfers> getIncentiveTransfers() {
        return incentiveTransfers;
    }

    public CommissionDisbursement incentiveTransfers(Set<IncentiveTransfers> incentiveTransfers) {
        this.incentiveTransfers = incentiveTransfers;
        return this;
    }

    public CommissionDisbursement addIncentiveTransfers(IncentiveTransfers incentiveTransfers) {
        this.incentiveTransfers.add(incentiveTransfers);
        incentiveTransfers.setCommissionDisbursement(this);
        return this;
    }

    public CommissionDisbursement removeIncentiveTransfers(IncentiveTransfers incentiveTransfers) {
        this.incentiveTransfers.remove(incentiveTransfers);
        incentiveTransfers.setCommissionDisbursement(null);
        return this;
    }

    public void setIncentiveTransfers(Set<IncentiveTransfers> incentiveTransfers) {
        this.incentiveTransfers = incentiveTransfers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommissionDisbursement)) {
            return false;
        }
        return id != null && id.equals(((CommissionDisbursement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissionDisbursement{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", validated='" + isValidated() + "'" +
            ", validationDate='" + getValidationDate() + "'" +
            ", processingStatus='" + getProcessingStatus() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", spare4='" + getSpare4() + "'" +
            ", spare5='" + getSpare5() + "'" +
            "}";
    }
}
