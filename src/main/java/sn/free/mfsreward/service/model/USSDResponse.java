package sn.free.mfsreward.service.model;

import lombok.Data;
import lombok.Getter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @project: offrecom
 * @author: psow on 28/09/2020
 */


@Data
@XmlRootElement(name = "doc")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
public class USSDResponse {

    @XmlElement(name = "param")
    private Param[] params;

    public Param[] getParams(){
        return params;
    }
}
