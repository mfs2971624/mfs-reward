package sn.free.mfsreward.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * @project: offrecom
 * @author: psow on 28/09/2020
 */


@Data
@XmlAccessorType(XmlAccessType.FIELD)
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Param {

    @XmlAttribute
    private String name;

    @XmlAttribute
    private String value;

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
