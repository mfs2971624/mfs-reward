package sn.free.mfsreward.service.dto;
import java.io.Serializable;
import java.util.Objects;
import sn.free.mfsreward.domain.enumeration.StatutTraitementTransaction;

/**
 * A DTO for the {@link sn.free.mfsreward.domain.Transaction} entity.
 */
public class TransactionDTO implements Serializable {

    private Long id;

    private String requestId;

    private String trxIdMobiquity;

    private String trxType;

    private String trxPartnerType;

    private String trxPartnerId;

    private String trxCustomerMSISDN;

    private String trxCustomerAccountNumber;

    private String trxAmount;

    private String trxChannel;

    private String trxDate;

    private String eligiblePromos;

    private StatutTraitementTransaction traitementStatus;

    private String spare1;

    private String spare2;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTrxIdMobiquity() {
        return trxIdMobiquity;
    }

    public void setTrxIdMobiquity(String trxIdMobiquity) {
        this.trxIdMobiquity = trxIdMobiquity;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public String getTrxPartnerType() {
        return trxPartnerType;
    }

    public void setTrxPartnerType(String trxPartnerType) {
        this.trxPartnerType = trxPartnerType;
    }

    public String getTrxPartnerId() {
        return trxPartnerId;
    }

    public void setTrxPartnerId(String trxPartnerId) {
        this.trxPartnerId = trxPartnerId;
    }

    public String getTrxCustomerMSISDN() {
        return trxCustomerMSISDN;
    }

    public void setTrxCustomerMSISDN(String trxCustomerMSISDN) {
        this.trxCustomerMSISDN = trxCustomerMSISDN;
    }

    public String getTrxCustomerAccountNumber() {
        return trxCustomerAccountNumber;
    }

    public void setTrxCustomerAccountNumber(String trxCustomerAccountNumber) {
        this.trxCustomerAccountNumber = trxCustomerAccountNumber;
    }

    public String getTrxAmount() {
        return trxAmount;
    }

    public void setTrxAmount(String trxAmount) {
        this.trxAmount = trxAmount;
    }

    public String getTrxChannel() {
        return trxChannel;
    }

    public void setTrxChannel(String trxChannel) {
        this.trxChannel = trxChannel;
    }

    public String getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }

    public String getEligiblePromos() {
        return eligiblePromos;
    }

    public void setEligiblePromos(String eligiblePromos) {
        this.eligiblePromos = eligiblePromos;
    }

    public StatutTraitementTransaction getTraitementStatus() {
        return traitementStatus;
    }

    public void setTraitementStatus(StatutTraitementTransaction traitementStatus) {
        this.traitementStatus = traitementStatus;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TransactionDTO transactionDTO = (TransactionDTO) o;
        if (transactionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), transactionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TransactionDTO{" +
            "id=" + getId() +
            ", requestId='" + getRequestId() + "'" +
            ", trxIdMobiquity='" + getTrxIdMobiquity() + "'" +
            ", trxType='" + getTrxType() + "'" +
            ", trxPartnerType='" + getTrxPartnerType() + "'" +
            ", trxPartnerId='" + getTrxPartnerId() + "'" +
            ", trxCustomerMSISDN='" + getTrxCustomerMSISDN() + "'" +
            ", trxCustomerAccountNumber='" + getTrxCustomerAccountNumber() + "'" +
            ", trxAmount='" + getTrxAmount() + "'" +
            ", trxChannel='" + getTrxChannel() + "'" +
            ", trxDate='" + getTrxDate() + "'" +
            ", eligiblePromos='" + getEligiblePromos() + "'" +
            ", traitementStatus='" + getTraitementStatus() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            "}";
    }
}
