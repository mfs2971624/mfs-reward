package sn.free.mfsreward.service.dto;

import java.time.Instant;
import java.io.Serializable;

/**
 * A DTO for the {@link sn.free.mfsreward.domain.DailyCommissionTransactions} entity.
 */
public class DailyCommissionTransactionsDTO implements Serializable {
    
    private Long id;

    private String agentMsisdn;

    private Instant processDate;

    private Double totalTransactionsVolume;

    private Double totalCommissions;

    private String spare1;

    private String spare2;

    private String spare3;

    private String spare4;

    private String spare5;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentMsisdn() {
        return agentMsisdn;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public Instant getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Instant processDate) {
        this.processDate = processDate;
    }

    public Double getTotalTransactionsVolume() {
        return totalTransactionsVolume;
    }

    public void setTotalTransactionsVolume(Double totalTransactionsVolume) {
        this.totalTransactionsVolume = totalTransactionsVolume;
    }

    public Double getTotalCommissions() {
        return totalCommissions;
    }

    public void setTotalCommissions(Double totalCommissions) {
        this.totalCommissions = totalCommissions;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

    public String getSpare5() {
        return spare5;
    }

    public void setSpare5(String spare5) {
        this.spare5 = spare5;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DailyCommissionTransactionsDTO)) {
            return false;
        }

        return id != null && id.equals(((DailyCommissionTransactionsDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DailyCommissionTransactionsDTO{" +
            "id=" + getId() +
            ", agentMsisdn='" + getAgentMsisdn() + "'" +
            ", processDate='" + getProcessDate() + "'" +
            ", totalTransactionsVolume=" + getTotalTransactionsVolume() +
            ", totalCommissions=" + getTotalCommissions() +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", spare4='" + getSpare4() + "'" +
            ", spare5='" + getSpare5() + "'" +
            "}";
    }
}
