package sn.free.mfsreward.service.dto;

import java.time.Instant;
import java.io.Serializable;
import sn.free.mfsreward.domain.enumeration.PaymentStatus;

/**
 * A DTO for the {@link sn.free.mfsreward.domain.IncentiveTransfers} entity.
 */
public class IncentiveTransfersDTO implements Serializable {
    
    private Long id;

    private String agentMsisdn;

    private Double amount;

    private Instant paymentDate;

    private PaymentStatus paymentStatus;

    private String mobiquityResponse;

    private String spare1;

    private String spare2;

    private String spare3;

    private String spare4;

    private String spare5;


    private Long commissionDisbursementId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentMsisdn() {
        return agentMsisdn;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Instant getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Instant paymentDate) {
        this.paymentDate = paymentDate;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getMobiquityResponse() {
        return mobiquityResponse;
    }

    public void setMobiquityResponse(String mobiquityResponse) {
        this.mobiquityResponse = mobiquityResponse;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

    public String getSpare5() {
        return spare5;
    }

    public void setSpare5(String spare5) {
        this.spare5 = spare5;
    }

    public Long getCommissionDisbursementId() {
        return commissionDisbursementId;
    }

    public void setCommissionDisbursementId(Long commissionDisbursementId) {
        this.commissionDisbursementId = commissionDisbursementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IncentiveTransfersDTO)) {
            return false;
        }

        return id != null && id.equals(((IncentiveTransfersDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "IncentiveTransfersDTO{" +
            "id=" + getId() +
            ", agentMsisdn='" + getAgentMsisdn() + "'" +
            ", amount=" + getAmount() +
            ", paymentDate='" + getPaymentDate() + "'" +
            ", paymentStatus='" + getPaymentStatus() + "'" +
            ", mobiquityResponse='" + getMobiquityResponse() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", spare4='" + getSpare4() + "'" +
            ", spare5='" + getSpare5() + "'" +
            ", commissionDisbursementId=" + getCommissionDisbursementId() +
            "}";
    }
}
