package sn.free.mfsreward.service.dto;

import sn.free.mfsreward.domain.enumeration.Statut;

import java.io.Serializable;
import java.util.Objects;

public class PartnerDTO implements Serializable {
    private Long id;

    private String name;

    private String callbackUrl;

    private Statut status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public Statut getStatus() {
        return status;
    }

    public void setStatus(Statut status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignDTO{" +
            "id=" + getId() +
            ", nom='" + getName() + "'" +
            ", callbackUrl='" + getCallbackUrl() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
