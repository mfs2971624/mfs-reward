package sn.free.mfsreward.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import sn.free.mfsreward.domain.enumeration.TypeFrequence;
import sn.free.mfsreward.domain.enumeration.Statut;

/**
 * A DTO for the {@link sn.free.mfsreward.domain.Promotion} entity.
 */
public class PromotionDTO implements Serializable {

    private Long id;

    @NotNull
    private String nom;

    @NotNull
    private String critereEligilibite;

    @NotNull
    private TypeFrequence frequence;

    private Integer frequenceDetails;

    private Statut statut;


    private Long campagneId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCritereEligilibite() {
        return critereEligilibite;
    }

    public void setCritereEligilibite(String critereEligilibite) {
        this.critereEligilibite = critereEligilibite;
    }

    public TypeFrequence getFrequence() {
        return frequence;
    }

    public void setFrequence(TypeFrequence frequence) {
        this.frequence = frequence;
    }

    public Integer getFrequenceDetails() {
        return frequenceDetails;
    }

    public void setFrequenceDetails(Integer frequenceDetails) {
        this.frequenceDetails = frequenceDetails;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Long getCampagneId() {
        return campagneId;
    }

    public void setCampagneId(Long campagneId) {
        this.campagneId = campagneId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PromotionDTO promotionDTO = (PromotionDTO) o;
        if (promotionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), promotionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PromotionDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", critereEligilibite='" + getCritereEligilibite() + "'" +
            ", frequence='" + getFrequence() + "'" +
            ", frequenceDetails=" + getFrequenceDetails() +
            ", statut='" + getStatut() + "'" +
            ", campagne=" + getCampagneId() +
            "}";
    }
}
