package sn.free.mfsreward.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link sn.free.mfsreward.domain.SubscriberUncomes} entity. This class is used
 * in {@link sn.free.mfsreward.web.rest.SubscriberUncomesResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /subscriber-uncomes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SubscriberUncomesCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter processDate;

    private StringFilter subscriberMsisdn;

    private DoubleFilter uncome;

    private StringFilter spare1;

    private StringFilter spare2;

    private StringFilter spare3;

    private StringFilter spare4;

    public SubscriberUncomesCriteria() {
    }

    public SubscriberUncomesCriteria(SubscriberUncomesCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.processDate = other.processDate == null ? null : other.processDate.copy();
        this.subscriberMsisdn = other.subscriberMsisdn == null ? null : other.subscriberMsisdn.copy();
        this.uncome = other.uncome == null ? null : other.uncome.copy();
        this.spare1 = other.spare1 == null ? null : other.spare1.copy();
        this.spare2 = other.spare2 == null ? null : other.spare2.copy();
        this.spare3 = other.spare3 == null ? null : other.spare3.copy();
        this.spare4 = other.spare4 == null ? null : other.spare4.copy();
    }

    @Override
    public SubscriberUncomesCriteria copy() {
        return new SubscriberUncomesCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getProcessDate() {
        return processDate;
    }

    public void setProcessDate(InstantFilter processDate) {
        this.processDate = processDate;
    }

    public StringFilter getSubscriberMsisdn() {
        return subscriberMsisdn;
    }

    public void setSubscriberMsisdn(StringFilter subscriberMsisdn) {
        this.subscriberMsisdn = subscriberMsisdn;
    }

    public DoubleFilter getUncome() {
        return uncome;
    }

    public void setUncome(DoubleFilter uncome) {
        this.uncome = uncome;
    }

    public StringFilter getSpare1() {
        return spare1;
    }

    public void setSpare1(StringFilter spare1) {
        this.spare1 = spare1;
    }

    public StringFilter getSpare2() {
        return spare2;
    }

    public void setSpare2(StringFilter spare2) {
        this.spare2 = spare2;
    }

    public StringFilter getSpare3() {
        return spare3;
    }

    public void setSpare3(StringFilter spare3) {
        this.spare3 = spare3;
    }

    public StringFilter getSpare4() {
        return spare4;
    }

    public void setSpare4(StringFilter spare4) {
        this.spare4 = spare4;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SubscriberUncomesCriteria that = (SubscriberUncomesCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(processDate, that.processDate) &&
            Objects.equals(subscriberMsisdn, that.subscriberMsisdn) &&
            Objects.equals(uncome, that.uncome) &&
            Objects.equals(spare1, that.spare1) &&
            Objects.equals(spare2, that.spare2) &&
            Objects.equals(spare3, that.spare3) &&
            Objects.equals(spare4, that.spare4);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        processDate,
        subscriberMsisdn,
        uncome,
        spare1,
        spare2,
        spare3,
        spare4
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SubscriberUncomesCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (processDate != null ? "processDate=" + processDate + ", " : "") +
                (subscriberMsisdn != null ? "subscriberMsisdn=" + subscriberMsisdn + ", " : "") +
                (uncome != null ? "uncome=" + uncome + ", " : "") +
                (spare1 != null ? "spare1=" + spare1 + ", " : "") +
                (spare2 != null ? "spare2=" + spare2 + ", " : "") +
                (spare3 != null ? "spare3=" + spare3 + ", " : "") +
                (spare4 != null ? "spare4=" + spare4 + ", " : "") +
            "}";
    }

}
