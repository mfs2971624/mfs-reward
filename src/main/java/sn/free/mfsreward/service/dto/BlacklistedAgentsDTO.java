package sn.free.mfsreward.service.dto;

import io.swagger.annotations.ApiModel;
import java.time.ZonedDateTime;
import java.io.Serializable;

/**
 * A DTO for the {@link sn.free.mfsreward.domain.BlacklistedAgents} entity.
 */
@ApiModel(description = "not an ignored comment")
public class BlacklistedAgentsDTO implements Serializable {
    
    private Long id;

    private String agentMsisdn;

    private ZonedDateTime blackListedDate;

    private String blackListedBy;

    private String agentType;

    private String spare1;

    private String spare2;

    private String spare3;

    private String spare4;

    private String spare5;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentMsisdn() {
        return agentMsisdn;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public ZonedDateTime getBlackListedDate() {
        return blackListedDate;
    }

    public void setBlackListedDate(ZonedDateTime blackListedDate) {
        this.blackListedDate = blackListedDate;
    }

    public String getBlackListedBy() {
        return blackListedBy;
    }

    public void setBlackListedBy(String blackListedBy) {
        this.blackListedBy = blackListedBy;
    }

    public String getAgentType() {
        return agentType;
    }

    public void setAgentType(String agentType) {
        this.agentType = agentType;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

    public String getSpare5() {
        return spare5;
    }

    public void setSpare5(String spare5) {
        this.spare5 = spare5;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BlacklistedAgentsDTO)) {
            return false;
        }

        return id != null && id.equals(((BlacklistedAgentsDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BlacklistedAgentsDTO{" +
            "id=" + getId() +
            ", agentMsisdn='" + getAgentMsisdn() + "'" +
            ", blackListedDate='" + getBlackListedDate() + "'" +
            ", blackListedBy='" + getBlackListedBy() + "'" +
            ", agentType='" + getAgentType() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", spare4='" + getSpare4() + "'" +
            ", spare5='" + getSpare5() + "'" +
            "}";
    }
}
