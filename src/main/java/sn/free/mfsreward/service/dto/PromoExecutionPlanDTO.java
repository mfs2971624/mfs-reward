package sn.free.mfsreward.service.dto;
import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link sn.free.mfsreward.domain.PromoExecutionPlan} entity.
 */
public class PromoExecutionPlanDTO implements Serializable {

    private Long id;

    private Instant dateDebut;

    private Instant dateFin;


    private Long promotionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Instant dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Instant getDateFin() {
        return dateFin;
    }

    public void setDateFin(Instant dateFin) {
        this.dateFin = dateFin;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PromoExecutionPlanDTO promoExecutionPlanDTO = (PromoExecutionPlanDTO) o;
        if (promoExecutionPlanDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), promoExecutionPlanDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PromoExecutionPlanDTO{" +
            "id=" + getId() +
            ", dateDebut='" + getDateDebut() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", promotion=" + getPromotionId() +
            "}";
    }
}
