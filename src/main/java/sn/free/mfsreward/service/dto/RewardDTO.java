package sn.free.mfsreward.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link sn.free.mfsreward.domain.Reward} entity.
 */
public class RewardDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String rewardType;

    @NotNull
    private Double taux;

    @NotNull
    private String rewardNotifMessage;

    private Integer countTreshold;

    private Double valueTreshold;

    @NotNull
    private Long promotionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public Double getTaux() {
        return taux;
    }

    public void setTaux(Double taux) {
        this.taux = taux;
    }

    public String getRewardNotifMessage() {
        return rewardNotifMessage;
    }

    public void setRewardNotifMessage(String rewardNotifMessage) {
        this.rewardNotifMessage = rewardNotifMessage;
    }

    public Integer getCountTreshold() {
        return countTreshold;
    }

    public void setCountTreshold(Integer countTreshold) {
        this.countTreshold = countTreshold;
    }

    public Double getValueTreshold() {
        return valueTreshold;
    }

    public void setValueTreshold(Double valueTreshold) {
        this.valueTreshold = valueTreshold;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RewardDTO)) {
            return false;
        }

        return id != null && id.equals(((RewardDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RewardDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", rewardType='" + getRewardType() + "'" +
            ", taux=" + getTaux() +
            ", rewardNotifMessage='" + getRewardNotifMessage() + "'" +
            ", countTreshold=" + getCountTreshold() +
            ", valueTreshold=" + getValueTreshold() +
            ", promotionId=" + getPromotionId() +
            "}";
    }
}
