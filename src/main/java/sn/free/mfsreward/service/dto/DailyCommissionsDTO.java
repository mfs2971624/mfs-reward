package sn.free.mfsreward.service.dto;

import java.time.Instant;
import java.io.Serializable;

/**
 * A DTO for the {@link sn.free.mfsreward.domain.DailyCommissions} entity.
 */
public class DailyCommissionsDTO implements Serializable {
    
    private Long id;

    private String agentMsisdn;

    private Instant lastModifiedDate;

    private String lastTransactionId;

    private Double totalCashinTransactions;

    private Double totalCashoutTransactions;

    private Double totalIncentiveCommissions;

    private String spare1;

    private String spare2;

    private String spare3;

    private String spare4;

    private String spare5;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentMsisdn() {
        return agentMsisdn;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastTransactionId() {
        return lastTransactionId;
    }

    public void setLastTransactionId(String lastTransactionId) {
        this.lastTransactionId = lastTransactionId;
    }

    public Double getTotalCashinTransactions() {
        return totalCashinTransactions;
    }

    public void setTotalCashinTransactions(Double totalCashinTransactions) {
        this.totalCashinTransactions = totalCashinTransactions;
    }

    public Double getTotalCashoutTransactions() {
        return totalCashoutTransactions;
    }

    public void setTotalCashoutTransactions(Double totalCashoutTransactions) {
        this.totalCashoutTransactions = totalCashoutTransactions;
    }

    public Double getTotalIncentiveCommissions() {
        return totalIncentiveCommissions;
    }

    public void setTotalIncentiveCommissions(Double totalIncentiveCommissions) {
        this.totalIncentiveCommissions = totalIncentiveCommissions;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public String getSpare4() {
        return spare4;
    }

    public void setSpare4(String spare4) {
        this.spare4 = spare4;
    }

    public String getSpare5() {
        return spare5;
    }

    public void setSpare5(String spare5) {
        this.spare5 = spare5;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DailyCommissionsDTO)) {
            return false;
        }

        return id != null && id.equals(((DailyCommissionsDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DailyCommissionsDTO{" +
            "id=" + getId() +
            ", agentMsisdn='" + getAgentMsisdn() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastTransactionId='" + getLastTransactionId() + "'" +
            ", totalCashinTransactions=" + getTotalCashinTransactions() +
            ", totalCashoutTransactions=" + getTotalCashoutTransactions() +
            ", totalIncentiveCommissions=" + getTotalIncentiveCommissions() +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", spare4='" + getSpare4() + "'" +
            ", spare5='" + getSpare5() + "'" +
            "}";
    }
}
