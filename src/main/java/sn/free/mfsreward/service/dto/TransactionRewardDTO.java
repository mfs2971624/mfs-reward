package sn.free.mfsreward.service.dto;
import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;
import sn.free.mfsreward.domain.enumeration.StatutTransactionReward;

/**
 * A DTO for the {@link sn.free.mfsreward.domain.TransactionReward} entity.
 */
public class TransactionRewardDTO implements Serializable {

    private Long id;

    private StatutTransactionReward rewardStatus;

    private String rewardMessage;

    private Double rewardValue;

    private Instant rewardDateTime;


    private Long rewardId;

    private Long transactionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StatutTransactionReward getRewardStatus() {
        return rewardStatus;
    }

    public void setRewardStatus(StatutTransactionReward rewardStatus) {
        this.rewardStatus = rewardStatus;
    }

    public String getRewardMessage() {
        return rewardMessage;
    }

    public void setRewardMessage(String rewardMessage) {
        this.rewardMessage = rewardMessage;
    }

    public Double getRewardValue() {
        return rewardValue;
    }

    public void setRewardValue(Double rewardValue) {
        this.rewardValue = rewardValue;
    }

    public Instant getRewardDateTime() {
        return rewardDateTime;
    }

    public void setRewardDateTime(Instant rewardDateTime) {
        this.rewardDateTime = rewardDateTime;
    }

    public Long getRewardId() {
        return rewardId;
    }

    public void setRewardId(Long rewardId) {
        this.rewardId = rewardId;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TransactionRewardDTO transactionRewardDTO = (TransactionRewardDTO) o;
        if (transactionRewardDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), transactionRewardDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TransactionRewardDTO{" +
            "id=" + getId() +
            ", rewardStatus='" + getRewardStatus() + "'" +
            ", rewardMessage='" + getRewardMessage() + "'" +
            ", rewardValue=" + getRewardValue() +
            ", rewardDateTime='" + getRewardDateTime() + "'" +
            ", reward=" + getRewardId() +
            ", transaction=" + getTransactionId() +
            "}";
    }
}
