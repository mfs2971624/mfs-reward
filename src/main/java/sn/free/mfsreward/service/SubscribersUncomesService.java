package sn.free.mfsreward.service;

import sn.free.mfsreward.domain.SubscribersUncomes;
import sn.free.mfsreward.service.dto.SubscribersUncomesDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfsreward.domain.SubscribersUncomes}.
 */
public interface SubscribersUncomesService {

    /**
     * Save a subscribersUncomes.
     *
     * @param subscribersUncomesDTO the entity to save.
     * @return the persisted entity.
     */
    SubscribersUncomesDTO save(SubscribersUncomesDTO subscribersUncomesDTO);

    /**
     * Get all the subscribersUncomes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SubscribersUncomesDTO> findAll(Pageable pageable);


    /**
     * Get the "id" subscribersUncomes.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SubscribersUncomesDTO> findOne(Long id);

    /**
     * Delete the "id" subscribersUncomes.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Optional<SubscribersUncomesDTO> findFirstBySubscriberMsisdn(String subscriberMsisdn);
}
