package sn.free.mfsreward.service;

import sn.free.mfsreward.service.dto.DailyCommissionTransactionsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfsreward.domain.DailyCommissionTransactions}.
 */
public interface DailyCommissionTransactionsService {

    /**
     * Save a dailyCommissionTransactions.
     *
     * @param dailyCommissionTransactionsDTO the entity to save.
     * @return the persisted entity.
     */
    DailyCommissionTransactionsDTO save(DailyCommissionTransactionsDTO dailyCommissionTransactionsDTO);

    /**
     * Get all the dailyCommissionTransactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DailyCommissionTransactionsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" dailyCommissionTransactions.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DailyCommissionTransactionsDTO> findOne(Long id);

    /**
     * Delete the "id" dailyCommissionTransactions.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
