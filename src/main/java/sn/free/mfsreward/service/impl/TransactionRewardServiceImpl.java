package sn.free.mfsreward.service.impl;

import sn.free.mfsreward.domain.Reward;
import sn.free.mfsreward.domain.Transaction;
import sn.free.mfsreward.domain.enumeration.StatutTransactionReward;
import sn.free.mfsreward.repository.RewardRepository;
import sn.free.mfsreward.repository.TransactionRepository;
import sn.free.mfsreward.service.TransactionRewardService;
import sn.free.mfsreward.domain.TransactionReward;
import sn.free.mfsreward.repository.TransactionRewardRepository;
import sn.free.mfsreward.service.dto.TransactionRewardDTO;
import sn.free.mfsreward.service.mapper.TransactionRewardMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link TransactionReward}.
 */
@Service
@Transactional
public class TransactionRewardServiceImpl implements TransactionRewardService {

    private final Logger log = LoggerFactory.getLogger(TransactionRewardServiceImpl.class);

    private final TransactionRewardRepository transactionRewardRepository;

    private final TransactionRewardMapper transactionRewardMapper;

    private final TransactionRepository transactionRepository;

    private final RewardRepository rewardRepository;



    public TransactionRewardServiceImpl(TransactionRewardRepository transactionRewardRepository, TransactionRewardMapper transactionRewardMapper, TransactionRepository transactionRepository, RewardRepository rewardRepository) {
        this.transactionRewardRepository = transactionRewardRepository;
        this.transactionRewardMapper = transactionRewardMapper;
        this.transactionRepository = transactionRepository;
        this.rewardRepository = rewardRepository;
    }

    /**
     * Save a transactionReward.
     *
     * @param transactionRewardDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TransactionRewardDTO save(TransactionRewardDTO transactionRewardDTO) {
        log.debug("Request to save TransactionReward : {}", transactionRewardDTO);
        TransactionReward transactionReward = transactionRewardMapper.toEntity(transactionRewardDTO);

        // Transaction transaction = transactionRepository.findById(transactionRewardDTO.getTransactionId()).get();
        // transactionReward.setTransaction(transaction);

        // Reward reward = rewardRepository.findById(transactionRewardDTO.getRewardId()).get();
        // transactionReward.setReward(reward);

        transactionReward = transactionRewardRepository.save(transactionReward);
        return transactionRewardMapper.toDto(transactionReward);
    }

    /**
     * Get all the transactionRewards.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TransactionRewardDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TransactionRewards");
        return transactionRewardRepository.findAllByOrderByRewardDateTimeDesc(pageable)
            .map(transactionRewardMapper::toDto);
    }


    /**
     * Get one transactionReward by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TransactionRewardDTO> findOne(Long id) {
        log.debug("Request to get TransactionReward : {}", id);
        return transactionRewardRepository.findById(id)
            .map(transactionRewardMapper::toDto);
    }

    /**
     * Delete the transactionReward by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TransactionReward : {}", id);
        transactionRewardRepository.deleteById(id);
    }

    /**
     * Get all the transactionRewards.
     *
     * @param id the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<TransactionRewardDTO> findAllById(Long id) {
        log.debug("Request to get all TransactionRewards by id");
        return transactionRewardRepository.findByTransactionId(id)
            .stream()
            .map(transactionRewardMapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public Double getTotalRewardedValueForTheDay(String msisdn, String trxPartnerId) {
        Instant startDate = LocalDateTime.now()
            .toLocalDate()
            .atTime(LocalTime.MIN)
            .toInstant(ZoneOffset.UTC);

        Instant endDate = LocalDateTime.now()
            .toLocalDate()
            .atTime(LocalTime.MAX)
            .toInstant(ZoneOffset.UTC);

        return transactionRewardRepository.findByTransaction_TrxCustomerMSISDNAndTransactionTrxPartnerIdAndRewardStatusAndRewardDateTimeIsBetween(msisdn, trxPartnerId, StatutTransactionReward.GRANTED, startDate, endDate)
            .stream()
            .mapToDouble(TransactionReward::getRewardValue)
            .sum();
    }

    @Override
    public int getTotalRewardedTimesForTheDay(String msisdn, String trxPartnerId) {
        Instant startDate = LocalDateTime.now()
            .toLocalDate()
            .atTime(LocalTime.MIN)
            .toInstant(ZoneOffset.UTC);

        Instant endDate = LocalDateTime.now()
            .toLocalDate()
            .atTime(LocalTime.MAX)
            .toInstant(ZoneOffset.UTC);

        return transactionRewardRepository.findByTransaction_TrxCustomerMSISDNAndTransactionTrxPartnerIdAndRewardStatusAndRewardDateTimeIsBetween(msisdn, trxPartnerId, StatutTransactionReward.GRANTED, startDate, endDate).size();
    }
}
