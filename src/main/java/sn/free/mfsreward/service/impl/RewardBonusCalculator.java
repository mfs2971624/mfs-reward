package sn.free.mfsreward.service.impl;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import sn.free.mfsreward.config.ApplicationProperties;
import sn.free.mfsreward.domain.enumeration.StatutTransactionReward;
import sn.free.mfsreward.service.RewardCalculator;
import sn.free.mfsreward.service.dto.RewardDTO;
import sn.free.mfsreward.service.dto.TransactionDTO;
import sn.free.mfsreward.service.dto.TransactionRewardDTO;
import sn.free.mfsreward.service.model.Param;
import sn.free.mfsreward.service.model.USSDResponse;

import javax.net.ssl.SSLContext;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Date;

@Service
@Transactional
public class RewardBonusCalculator implements RewardCalculator {
    private final Logger log = LoggerFactory.getLogger(RewardBonusCalculator.class);
    private ApplicationProperties applicationProperties;
    private final Environment environment;

    @Value("${application.talendHost}")
    private String talendHost;

    public RewardBonusCalculator(ApplicationProperties applicationProperties, Environment environment) {
        this.applicationProperties = applicationProperties;
        this.environment = environment;
    }

    @Override
    public TransactionRewardDTO setReward(TransactionRewardDTO transactionRewardDTO, TransactionDTO transactionDTO) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
            SSLContext sslContext;

            sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
            CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);

            restTemplate = new RestTemplate(requestFactory);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        String uri = talendHost + "/mfs/givebonuscreditmfs";
        log.info("Talend Give bonus url {}", uri);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_XML_VALUE);

        transactionDTO.setSpare2((new java.text.SimpleDateFormat("yyyyMMddHHmmss")).format(new Date()) + (int)(Math.random()*1000));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(uri)
            .queryParam("transactionId", transactionDTO.getSpare2())
            .queryParam("msisdn", transactionDTO.getTrxCustomerMSISDN())
            .queryParam("bonus", transactionRewardDTO.getRewardValue())
            .queryParam("transactionType", transactionDTO.getTrxType()) //TODO update this
            .queryParam("bonusLimit", transactionDTO.getSpare1() == null || transactionDTO.getSpare1().isEmpty() ? "0" : transactionDTO.getSpare1()) //TODO update this
            .build();
        log.info(builder.toString());
        HttpEntity<String> requestEntity = new HttpEntity<>(headers);



        try {
            ResponseEntity<USSDResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), USSDResponse.class);

            if (responseEntity.getStatusCode().is2xxSuccessful() && responseEntity.getBody() != null) {
                final USSDResponse body = responseEntity.getBody();
                final Param resultCode = Arrays.stream(body.getParams())
                    .filter(p -> p.getName().equals("resultCode"))
                    .findFirst()
                    .orElse(null);

                final Param resultMessage = Arrays.stream(body.getParams())
                    .filter(p -> p.getName().equals("resultMessage"))
                    .findFirst()
                    .orElse(null);

                if (resultCode != null && !resultCode.getValue().equals("1")) {
                    transactionRewardDTO.setRewardMessage(resultMessage.getValue());
                    transactionRewardDTO.setRewardStatus(StatutTransactionReward.NOTGRANTED);
                    return transactionRewardDTO;
                } else {
                    assert resultMessage != null;
                    transactionRewardDTO.setRewardMessage(resultMessage.getValue());
                    transactionRewardDTO.setRewardStatus(StatutTransactionReward.GRANTED);
                    return transactionRewardDTO;
                }

            } else {
                transactionRewardDTO.setRewardMessage("Error calling GiveBonus Service: " + responseEntity.getStatusCode().value()
                    + " , could not grant Binus of  " + transactionRewardDTO.getRewardValue() + " to MFS subscriber " + transactionDTO.getTrxCustomerMSISDN());
                transactionRewardDTO.setRewardStatus(StatutTransactionReward.FAILED);
                return transactionRewardDTO;
            }
        } catch (Exception e) {
            transactionRewardDTO.setRewardMessage("Internal Error: " + e.getMessage());
            transactionRewardDTO.setRewardStatus(StatutTransactionReward.FAILED);
            return transactionRewardDTO;
        }

    }

    @Override
    public TransactionRewardDTO getReward(TransactionDTO transactionDTO, RewardDTO rewardDTO) {
        TransactionRewardDTO transactionRewardDTO = new TransactionRewardDTO();
        transactionRewardDTO.setTransactionId(transactionDTO.getId());
        transactionRewardDTO.setRewardId(rewardDTO.getId());
        Double bonusAmount = this.getBonusAmount(transactionDTO,rewardDTO);
        transactionRewardDTO.setRewardValue(bonusAmount);
        return transactionRewardDTO;
    }

    @Override
    public TransactionRewardDTO calculReward(TransactionDTO transactionDTO, RewardDTO rewardDTO) {
        TransactionRewardDTO transactionRewardDTO = this.getReward(transactionDTO, rewardDTO);
        transactionRewardDTO = this.setReward(transactionRewardDTO, transactionDTO);
        return transactionRewardDTO;
    }

    private Double getBonusAmount(TransactionDTO transactionDTO, RewardDTO rewardDTO) {
//        if(Double.valueOf(transactionDTO.getSpare1()) > Double.valueOf(transactionDTO.getSpare2())){
//            return Double.valueOf(transactionDTO.getSpare2());
//        }
//        return Double.valueOf(transactionDTO.getSpare1()) ;
        return (rewardDTO.getTaux() * Double.parseDouble(transactionDTO.getTrxAmount()))/100;
    }

    private Double getBonusLimit(TransactionDTO transactionDTO) {
        return Double.valueOf(transactionDTO.getSpare2());
    }

}
