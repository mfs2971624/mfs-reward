package sn.free.mfsreward.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.mfsreward.domain.Partner;
import sn.free.mfsreward.repository.PartnerRepository;
import sn.free.mfsreward.service.PartnerService;
import sn.free.mfsreward.service.dto.PartnerDTO;
import sn.free.mfsreward.service.mapper.PartnerMapper;

import java.util.Optional;

@Service
@Transactional
public class PartnerServiceImpl implements PartnerService {
    private final Logger log = LoggerFactory.getLogger(PartnerServiceImpl.class);

    private final PartnerRepository partnerRepository;

    private final PartnerMapper partnerMapper;

    public PartnerServiceImpl(PartnerRepository partnerRepository, PartnerMapper partnerMapper) {
        this.partnerRepository = partnerRepository;
        this.partnerMapper = partnerMapper;
    }

    /**
     * Save a partner.
     *
     * @param partnerDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PartnerDTO save(PartnerDTO partnerDTO) {
        log.debug("Request to save partner : {}", partnerDTO);
        Partner partner = partnerMapper.toEntity(partnerDTO);
        partner = partnerRepository.save(partner);
        return partnerMapper.toDto(partner);
    }

    /**
     * Get all the partners.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PartnerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all partners");
        return partnerRepository.findAll(pageable)
            .map(partnerMapper::toDto);
    }


    /**
     * Get one partner by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PartnerDTO> findOne(Long id) {
        log.debug("Request to get partner : {}", id);
        return partnerRepository.findById(id)
            .map(partnerMapper::toDto);
    }

    /**
     * Delete the partner by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete partner : {}", id);
        partnerRepository.deleteById(id);
    }
}
