package sn.free.mfsreward.service.impl;

import sn.free.mfsreward.service.CommissionDisbursementService;
import sn.free.mfsreward.domain.CommissionDisbursement;
import sn.free.mfsreward.repository.CommissionDisbursementRepository;
import sn.free.mfsreward.service.dto.CommissionDisbursementDTO;
import sn.free.mfsreward.service.mapper.CommissionDisbursementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CommissionDisbursement}.
 */
@Service
@Transactional
public class CommissionDisbursementServiceImpl implements CommissionDisbursementService {

    private final Logger log = LoggerFactory.getLogger(CommissionDisbursementServiceImpl.class);

    private final CommissionDisbursementRepository commissionDisbursementRepository;

    private final CommissionDisbursementMapper commissionDisbursementMapper;

    public CommissionDisbursementServiceImpl(CommissionDisbursementRepository commissionDisbursementRepository, CommissionDisbursementMapper commissionDisbursementMapper) {
        this.commissionDisbursementRepository = commissionDisbursementRepository;
        this.commissionDisbursementMapper = commissionDisbursementMapper;
    }

    @Override
    public CommissionDisbursementDTO save(CommissionDisbursementDTO commissionDisbursementDTO) {
        log.debug("Request to save CommissionDisbursement : {}", commissionDisbursementDTO);
        CommissionDisbursement commissionDisbursement = commissionDisbursementMapper.toEntity(commissionDisbursementDTO);
        commissionDisbursement = commissionDisbursementRepository.save(commissionDisbursement);
        return commissionDisbursementMapper.toDto(commissionDisbursement);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CommissionDisbursementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CommissionDisbursements");
        return commissionDisbursementRepository.findAll(pageable)
            .map(commissionDisbursementMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CommissionDisbursementDTO> findOne(Long id) {
        log.debug("Request to get CommissionDisbursement : {}", id);
        return commissionDisbursementRepository.findById(id)
            .map(commissionDisbursementMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CommissionDisbursement : {}", id);
        commissionDisbursementRepository.deleteById(id);
    }
}
