package sn.free.mfsreward.service.impl;

import sn.free.mfsreward.service.IncentiveTransfersService;
import sn.free.mfsreward.domain.IncentiveTransfers;
import sn.free.mfsreward.repository.IncentiveTransfersRepository;
import sn.free.mfsreward.service.dto.IncentiveTransfersDTO;
import sn.free.mfsreward.service.mapper.IncentiveTransfersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link IncentiveTransfers}.
 */
@Service
@Transactional
public class IncentiveTransfersServiceImpl implements IncentiveTransfersService {

    private final Logger log = LoggerFactory.getLogger(IncentiveTransfersServiceImpl.class);

    private final IncentiveTransfersRepository incentiveTransfersRepository;

    private final IncentiveTransfersMapper incentiveTransfersMapper;

    public IncentiveTransfersServiceImpl(IncentiveTransfersRepository incentiveTransfersRepository, IncentiveTransfersMapper incentiveTransfersMapper) {
        this.incentiveTransfersRepository = incentiveTransfersRepository;
        this.incentiveTransfersMapper = incentiveTransfersMapper;
    }

    @Override
    public IncentiveTransfersDTO save(IncentiveTransfersDTO incentiveTransfersDTO) {
        log.debug("Request to save IncentiveTransfers : {}", incentiveTransfersDTO);
        IncentiveTransfers incentiveTransfers = incentiveTransfersMapper.toEntity(incentiveTransfersDTO);
        incentiveTransfers = incentiveTransfersRepository.save(incentiveTransfers);
        return incentiveTransfersMapper.toDto(incentiveTransfers);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<IncentiveTransfersDTO> findAll(Pageable pageable) {
        log.debug("Request to get all IncentiveTransfers");
        return incentiveTransfersRepository.findAll(pageable)
            .map(incentiveTransfersMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<IncentiveTransfersDTO> findOne(Long id) {
        log.debug("Request to get IncentiveTransfers : {}", id);
        return incentiveTransfersRepository.findById(id)
            .map(incentiveTransfersMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete IncentiveTransfers : {}", id);
        incentiveTransfersRepository.deleteById(id);
    }
}
