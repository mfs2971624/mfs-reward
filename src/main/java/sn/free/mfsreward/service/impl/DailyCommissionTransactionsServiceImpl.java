package sn.free.mfsreward.service.impl;

import sn.free.mfsreward.service.DailyCommissionTransactionsService;
import sn.free.mfsreward.domain.DailyCommissionTransactions;
import sn.free.mfsreward.repository.DailyCommissionTransactionsRepository;
import sn.free.mfsreward.service.dto.DailyCommissionTransactionsDTO;
import sn.free.mfsreward.service.mapper.DailyCommissionTransactionsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link DailyCommissionTransactions}.
 */
@Service
@Transactional
public class DailyCommissionTransactionsServiceImpl implements DailyCommissionTransactionsService {

    private final Logger log = LoggerFactory.getLogger(DailyCommissionTransactionsServiceImpl.class);

    private final DailyCommissionTransactionsRepository dailyCommissionTransactionsRepository;

    private final DailyCommissionTransactionsMapper dailyCommissionTransactionsMapper;

    public DailyCommissionTransactionsServiceImpl(DailyCommissionTransactionsRepository dailyCommissionTransactionsRepository, DailyCommissionTransactionsMapper dailyCommissionTransactionsMapper) {
        this.dailyCommissionTransactionsRepository = dailyCommissionTransactionsRepository;
        this.dailyCommissionTransactionsMapper = dailyCommissionTransactionsMapper;
    }

    @Override
    public DailyCommissionTransactionsDTO save(DailyCommissionTransactionsDTO dailyCommissionTransactionsDTO) {
        log.debug("Request to save DailyCommissionTransactions : {}", dailyCommissionTransactionsDTO);
        DailyCommissionTransactions dailyCommissionTransactions = dailyCommissionTransactionsMapper.toEntity(dailyCommissionTransactionsDTO);
        dailyCommissionTransactions = dailyCommissionTransactionsRepository.save(dailyCommissionTransactions);
        return dailyCommissionTransactionsMapper.toDto(dailyCommissionTransactions);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DailyCommissionTransactionsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DailyCommissionTransactions");
        return dailyCommissionTransactionsRepository.findAll(pageable)
            .map(dailyCommissionTransactionsMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DailyCommissionTransactionsDTO> findOne(Long id) {
        log.debug("Request to get DailyCommissionTransactions : {}", id);
        return dailyCommissionTransactionsRepository.findById(id)
            .map(dailyCommissionTransactionsMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DailyCommissionTransactions : {}", id);
        dailyCommissionTransactionsRepository.deleteById(id);
    }
}
