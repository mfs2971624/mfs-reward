package sn.free.mfsreward.service.impl;

import sn.free.mfsreward.service.BlacklistedAgentsService;
import sn.free.mfsreward.domain.BlacklistedAgents;
import sn.free.mfsreward.repository.BlacklistedAgentsRepository;
import sn.free.mfsreward.service.dto.BlacklistedAgentsDTO;
import sn.free.mfsreward.service.mapper.BlacklistedAgentsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BlacklistedAgents}.
 */
@Service
@Transactional
public class BlacklistedAgentsServiceImpl implements BlacklistedAgentsService {

    private final Logger log = LoggerFactory.getLogger(BlacklistedAgentsServiceImpl.class);

    private final BlacklistedAgentsRepository blacklistedAgentsRepository;

    private final BlacklistedAgentsMapper blacklistedAgentsMapper;

    public BlacklistedAgentsServiceImpl(BlacklistedAgentsRepository blacklistedAgentsRepository, BlacklistedAgentsMapper blacklistedAgentsMapper) {
        this.blacklistedAgentsRepository = blacklistedAgentsRepository;
        this.blacklistedAgentsMapper = blacklistedAgentsMapper;
    }

    @Override
    public BlacklistedAgentsDTO save(BlacklistedAgentsDTO blacklistedAgentsDTO) {
        log.debug("Request to save BlacklistedAgents : {}", blacklistedAgentsDTO);
        BlacklistedAgents blacklistedAgents = blacklistedAgentsMapper.toEntity(blacklistedAgentsDTO);
        blacklistedAgents = blacklistedAgentsRepository.save(blacklistedAgents);
        return blacklistedAgentsMapper.toDto(blacklistedAgents);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BlacklistedAgentsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BlacklistedAgents");
        return blacklistedAgentsRepository.findAll(pageable)
            .map(blacklistedAgentsMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<BlacklistedAgentsDTO> findOne(Long id) {
        log.debug("Request to get BlacklistedAgents : {}", id);
        return blacklistedAgentsRepository.findById(id)
            .map(blacklistedAgentsMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete BlacklistedAgents : {}", id);
        blacklistedAgentsRepository.deleteById(id);
    }

    @Override
    public Optional<BlacklistedAgentsDTO> findFirstByAgentMsisdn(String agentMsisdn) {
        log.debug("Request to check if present on blacklisted table with agentMsisdn : {}", agentMsisdn);
        return blacklistedAgentsRepository.findFirstByAgentMsisdn(agentMsisdn)
            .map(blacklistedAgentsMapper::toDto);
    }
}
