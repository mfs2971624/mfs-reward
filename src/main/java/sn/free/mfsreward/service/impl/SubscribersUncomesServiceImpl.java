package sn.free.mfsreward.service.impl;

import sn.free.mfsreward.service.SubscribersUncomesService;
import sn.free.mfsreward.domain.SubscribersUncomes;
import sn.free.mfsreward.repository.SubscribersUncomesRepository;
import sn.free.mfsreward.service.dto.SubscribersUncomesDTO;
import sn.free.mfsreward.service.mapper.SubscribersUncomesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link SubscribersUncomes}.
 */
@Service
@Transactional
public class SubscribersUncomesServiceImpl implements SubscribersUncomesService {

    private final Logger log = LoggerFactory.getLogger(SubscribersUncomesServiceImpl.class);

    private final SubscribersUncomesRepository subscribersUncomesRepository;

    private final SubscribersUncomesMapper subscribersUncomesMapper;

    public SubscribersUncomesServiceImpl(SubscribersUncomesRepository subscribersUncomesRepository, SubscribersUncomesMapper subscribersUncomesMapper) {
        this.subscribersUncomesRepository = subscribersUncomesRepository;
        this.subscribersUncomesMapper = subscribersUncomesMapper;
    }

    @Override
    public SubscribersUncomesDTO save(SubscribersUncomesDTO subscribersUncomesDTO) {
        log.debug("Request to save SubscribersUncomes : {}", subscribersUncomesDTO);
        SubscribersUncomes subscribersUncomes = subscribersUncomesMapper.toEntity(subscribersUncomesDTO);
        subscribersUncomes = subscribersUncomesRepository.save(subscribersUncomes);
        return subscribersUncomesMapper.toDto(subscribersUncomes);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SubscribersUncomesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubscribersUncomes");
        return subscribersUncomesRepository.findAll(pageable)
            .map(subscribersUncomesMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<SubscribersUncomesDTO> findOne(Long id) {
        log.debug("Request to get SubscribersUncomes : {}", id);
        return subscribersUncomesRepository.findById(id)
            .map(subscribersUncomesMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete SubscribersUncomes : {}", id);
        subscribersUncomesRepository.deleteById(id);
    }

    @Override
    public Optional<SubscribersUncomesDTO> findFirstBySubscriberMsisdn(String subscriberMsisdn) {
        log.debug("Request to get SubscribersUncomes of subscriberMsisdn : {}", subscriberMsisdn);
        return subscribersUncomesRepository.findFirstBySubscriberMsisdn(subscriberMsisdn)
            .map(subscribersUncomesMapper::toDto);
    }

}
