package sn.free.mfsreward.service.impl;

import sn.free.mfsreward.domain.Campagne;
import sn.free.mfsreward.domain.Promotion;
import sn.free.mfsreward.repository.PromotionRepository;
import sn.free.mfsreward.service.RewardService;
import sn.free.mfsreward.domain.Reward;
import sn.free.mfsreward.repository.RewardRepository;
import sn.free.mfsreward.service.dto.PromotionDTO;
import sn.free.mfsreward.service.dto.RewardDTO;
import sn.free.mfsreward.service.mapper.PromotionMapper;
import sn.free.mfsreward.service.mapper.RewardMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing {@link Reward}.
 */
@Service
@Transactional
public class RewardServiceImpl implements RewardService {

    private final Logger log = LoggerFactory.getLogger(RewardServiceImpl.class);

    private final RewardRepository rewardRepository;

    private final RewardMapper rewardMapper;

    private final PromotionMapper promotionMapper;

    private final PromotionRepository promotionRepository;

    public RewardServiceImpl(RewardRepository rewardRepository, RewardMapper rewardMapper, PromotionMapper promotionMapper, PromotionRepository promotionRepository) {
        this.rewardRepository = rewardRepository;
        this.rewardMapper = rewardMapper;
        this.promotionMapper = promotionMapper;
        this.promotionRepository = promotionRepository;
    }

    /**
     * Save a reward.
     *
     * @param rewardDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RewardDTO save(RewardDTO rewardDTO) {
        log.debug("Request to save Reward : {}", rewardDTO);
        Reward reward = rewardMapper.toEntity(rewardDTO);

        Promotion promotion = promotionRepository.findById(rewardDTO.getPromotionId()).get();
        reward.setPromotion(promotion);

        reward = rewardRepository.save(reward);
        return rewardMapper.toDto(reward);
    }

    /**
     * Get all the rewards.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RewardDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Rewards");
        return rewardRepository.findAll(pageable)
            .map(rewardMapper::toDto);
    }

    public List<RewardDTO> findAllByPromotionIn(List<PromotionDTO> promotionDTOList){
        log.debug("Request to get Rewards of promotions in a list : {}", promotionDTOList);
        List<Promotion> promotionList = promotionMapper.toEntity(promotionDTOList);
        List<RewardDTO> rewardDTOList = rewardMapper.toDto(
            rewardRepository.findAllByPromotionIn(promotionList)
        );
        log.debug("Rewards matching  : {}", rewardDTOList);
        return rewardDTOList;
    }

    /**
     * Get one reward by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public RewardDTO findOne(Long id) {
        log.debug("Request to get Reward : {}", id);

        Reward reward = rewardRepository.findById(id).get();
        Promotion promotion = promotionRepository.findById(reward.getPromotion().getId()).get();

        RewardDTO rewardDTO = rewardMapper.toDto(reward);
        rewardDTO.setPromotionId(promotion.getId());

        return rewardDTO;

        // return rewardRepository.findById(id).map(rewardMapper::toDto);
    }

    /**
     * Delete the reward by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Reward : {}", id);
        rewardRepository.deleteById(id);
    }
}
