package sn.free.mfsreward.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import liquibase.util.StringUtils;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.MediaType;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import sn.free.mfsreward.config.ApplicationProperties;
import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.domain.enumeration.Statut;
import sn.free.mfsreward.domain.enumeration.StatutTraitementTransaction;
import sn.free.mfsreward.domain.enumeration.StatutTransactionReward;
import sn.free.mfsreward.repository.*;
import sn.free.mfsreward.service.*;
import sn.free.mfsreward.service.dto.*;
import sn.free.mfsreward.service.mapper.PartnerMapper;
import sn.free.mfsreward.service.mapper.PromotionMapper;
import sn.free.mfsreward.service.mapper.TransactionRewardMapper;
import sn.free.mfsreward.service.model.Param;
import sn.free.mfsreward.service.model.USSDResponse;

import javax.net.ssl.*;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class EligibilityServiceImpl implements EligibilityService {

    private final Logger log = LoggerFactory.getLogger(EligibilityServiceImpl.class);
    private final PromoExecutionPlanRepository promoExecutionPlanRepository;
    private final TransactionService transactionService;
    private final PromotionMapper promotionMapper;
    private final RewardService rewardService;
    private final TransactionRewardService transactionRewardService;
    private final DailyCommissionsService dailyCommissionsService;
    private final BlacklistedAgentsService blacklistedAgentsService;
    private final SubscribersUncomesService subscribersUncomesService;
    private final ApplicationProperties applicationProperties;
    private final Environment environment;
    private final TransactionRepository transactionRepository;
    private final TransactionRewardRepository transactionRewardRepository;
    private final RewardRepository rewardRepository;
    private final TransactionRewardMapper transactionRewardMapper;
    private final PartnerRepository partnerRepository;
    private final PartnerMapper partnerMapper;

    private final List<String> trxWhichRequireNotification = new ArrayList<>();


    @Value("${application.rewards.smsSender}")
    private String smsSender;
    @Value("${application.rewards.CASHINTYPESMS}")
    private String CASHINTYPESMS;
    @Value("${application.rewards.CASHOUTTYPESMS}")
    private String CASHOUTTYPESMS;
    @Value("${application.rewards.MERCANTPAYMENTTYPESMS}")
    private String MERCANTPAYMENTTYPESMS;
    @Value("${application.rewards.BUYAIRTIMETYPESMS}")
    private String BUYAIRTIMETYPESMS;
    @Value("${application.rewards.BUNDLETYPESMS}")
    private String BUNDLETYPESMS;
    @Value("${application.rewards.BILLPAYMENTTYPESMS}")
    private String BILLPAYMENTTYPESMS;
    @Value("${application.talendHost}")
    private String talendHost;
    @Value("${application.sendNotificationEndpoint}")
    private String sendNotificationEndpoint;
    @Value("${application.getUserDetailsEndpoint}")
    private String getUserDetailsEndpoint;
    @Value("${application.mfsLoyaltyPointsHost}")
    private String mfsLoyaltyPointsHost;
    @Value("${application.getClientTotalPointsEndpoint}")
    private String getClientTotalPointsEndpoint;

    @Value("${application.getUserBalanceURL}")
    private String getUserBalanceUrl;

    public EligibilityServiceImpl(PromoExecutionPlanRepository promoExecutionPlanRepository, TransactionService transactionService, PromotionMapper promotionMapper, RewardService rewardService, TransactionRewardService transactionRewardService, DailyCommissionsService dailyCommissionsService, BlacklistedAgentsService blacklistedAgentsService, SubscribersUncomesService subscribersUncomesService,
                                  ApplicationProperties applicationProperties, Environment environment, TransactionRepository transactionRepository1, TransactionRewardRepository transactionRewardRepository, RewardRepository rewardRepository, PartnerMapper partnerMapper, PartnerRepository partnerRepository, TransactionRewardMapper transactionRewardMapper) {
        this.promoExecutionPlanRepository = promoExecutionPlanRepository;
        this.transactionService = transactionService;
        this.promotionMapper = promotionMapper;
        this.rewardService = rewardService;
        this.transactionRewardService = transactionRewardService;
        this.dailyCommissionsService = dailyCommissionsService;
        this.blacklistedAgentsService = blacklistedAgentsService;
        this.subscribersUncomesService = subscribersUncomesService;
        this.applicationProperties = applicationProperties;
        this.environment = environment;
        this.transactionRepository = transactionRepository1;
        this.transactionRewardRepository = transactionRewardRepository;
        this.rewardRepository = rewardRepository;
        this.transactionRewardMapper = transactionRewardMapper;
        this.partnerRepository = partnerRepository;
        this.partnerMapper = partnerMapper;

        this.trxWhichRequireNotification.add("AIRTIME");
        this.trxWhichRequireNotification.add("BILL_PAYMENT");
        this.trxWhichRequireNotification.add("BUNDLE");
        this.trxWhichRequireNotification.add("CASHIN");
        this.trxWhichRequireNotification.add("CASHOUT");
        this.trxWhichRequireNotification.add("MERCHANT_PAYMENT");
        this.trxWhichRequireNotification.add("P2P");
        this.trxWhichRequireNotification.add("RECHARGE");

    }


    @Async
    public void notifyTransaction(TransactionDTO transactionDTO1) {

        //save before anything
        TransactionDTO transactionDTO = this.transactionService.save(transactionDTO1);

        //first verify if user is subs else save and return
        if(!this.isMFSSubs(transactionDTO)) return ;

        //processing to get reward to give to customer

        //eligibility sort with eligibility sur les promos
        List<PromotionDTO> promotions = this.getEligiblePromos(transactionDTO);
        log.debug("Promos auxquels la transaction est eligible: {}", promotions.toString() );

        //get list rewards to give
        List<TransactionRewardDTO> transactionRewardDTOList = new ArrayList<>();
        RewardCalculatorFactory rewardCalculatorFactory = new RewardCalculatorFactory(rewardService, transactionService, dailyCommissionsService, blacklistedAgentsService, subscribersUncomesService,
            environment, applicationProperties, transactionRewardService);
        List<RewardDTO> rewardDTOList = rewardService.findAllByPromotionIn(promotions);

        log.debug("Rewards auxquels la transaction est eligible: ");

        //calculation rewards and position fidelity points and make transaction processed
        rewardDTOList.forEach(
            rewardDTO -> {
                log.debug("Reward : {}",  rewardDTO.toString());
                RewardCalculator rewardCalculator = rewardCalculatorFactory.getRewardCalculator(rewardDTO.getRewardType());
                TransactionRewardDTO transactionRewardDTO = rewardCalculator. calculReward(transactionDTO, rewardDTO);
                if((transactionRewardDTO.getRewardStatus() == StatutTransactionReward.THREESHOLD) || (transactionRewardDTO.getRewardStatus() == StatutTransactionReward.FAILED) ){
                    this.transactionRewardService.save(transactionRewardDTO);
                }

                //transactionRewardService.save(transactionRewardDTO);
                transactionRewardDTOList.add(transactionRewardDTO);
            });
        log.debug("Transaction rewards processed : {}", transactionRewardDTOList );
        transactionDTO.setTraitementStatus(StatutTraitementTransaction.PROCESSED);

        //rewarding sms
        transactionRewardDTOList.stream().filter(
            transactionRewardDTO -> transactionRewardDTO.getRewardStatus() == StatutTransactionReward.GRANTED
        ).forEach(
            transactionRewardDTO -> {
                RewardDTO rewardDTO = rewardService.findOne(transactionRewardDTO.getRewardId());

                //save database
                TransactionReward transactionReward = transactionRewardMapper.toEntity(transactionRewardDTO);

                transactionReward.setRewardDateTime(new Date().toInstant());

                Transaction transaction = transactionRepository.findById(transactionDTO.getId()).get();
                transactionReward.setTransaction(transaction);

                Reward reward = rewardRepository.findById(rewardDTO.getId()).get();
                transactionReward.setReward(reward);

                transactionRewardRepository.save(transactionReward);

                String messageToSend = this.constructSms(rewardDTO, transactionDTO, transactionRewardDTO);

                if(rewardDTO.getRewardType().equalsIgnoreCase("Incentive"))
                    return;


                if (this.sendSms(messageToSend, this.smsSender, transactionDTO.getTrxCustomerMSISDN())) {
                    transactionDTO.setTraitementStatus(StatutTraitementTransaction.REWARDED);
                } else {
                    transactionDTO.setTraitementStatus(StatutTraitementTransaction.FAILED);
                    transactionDTO.setSpare1("Error during a sms sending of one transaction reward among transactionRewards of the transaction");
                }

                log.debug("####################### TRANS _ {}", transactionDTO.getTrxIdMobiquity());

            }
        );

        //update transaction processing status
        this.transactionService.save(transactionDTO);

        /*if(trxWhichRequireNotification.contains(transactionDTO.getTrxType())){
            sendNotificationToPartners(transactionDTO);
        }*/
    }


    //used to get the list of promos on which the transaction is eligible
    //cad promo active campaign active
    @Override
    public List<PromotionDTO> getEligiblePromos(TransactionDTO transactionDto) {
        List<PromotionDTO> promotions = new ArrayList<>();
        log.debug("Checking Eligible Promos");
        final DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("yyyyMMddHHmmss")
            .withZone(ZoneId.systemDefault());
        Instant dateTransaction;
        try{
            dateTransaction = Instant.from(formatter.parse(transactionDto.getTrxDate()));
        }
        catch (Exception e){
            //blocking
            log.debug("Incorrect transaction date");
            transactionDto.setTraitementStatus(StatutTraitementTransaction.FAILED);
            transactionDto.setEligiblePromos("notEligibleToAnyPromo");
            transactionDto.setSpare1("Incorrect transaction date");
            transactionService.save(transactionDto);
            return promotions;
        }
        promotions = this.getActivePromo(dateTransaction);
        if(promotions.isEmpty()){
            log.debug("Not Eligible To Any Promo");
            transactionDto.setEligiblePromos("notEligibleToAnyPromo");
            transactionService.save(transactionDto);
        }

        return promotions.stream().filter(promotion -> isEligiblePromo(promotion, transactionDto)).collect(Collectors.toList());
    }


    //used to get current promos that are running active and their campaign is active
    private List<PromotionDTO> getActivePromo(Instant dateTransaction){
        Collection<PromoExecutionPlan> promoExecutionPlans = promoExecutionPlanRepository.findAllByDateDebutIsLessThanEqualAndDateFinIsGreaterThanEqual(dateTransaction, dateTransaction);
        log.debug("PROMOTION EXECUTION PLAN: {}", promoExecutionPlans.toString());
        List<PromoExecutionPlan> promoExecutionPlans1 = promoExecutionPlans.stream().filter(promoExecutionPlan -> {
            try {
                return promoExecutionPlan.getPromotion().getStatut().equals(Statut.ACTIVE);
            }
            catch (NullPointerException ignored){}
            return false;
        }).collect(Collectors.toList());
        List<Promotion> promotions= promoExecutionPlans1.stream().map(PromoExecutionPlan::getPromotion).collect(Collectors.toList());
        //remove distinct promos cuz two or more promoExecutionPlans can be related to the same promotion
        return promotionMapper.toDto(promotions.stream().distinct().filter(promotion -> {
            try {
                return promotion.getCampagne().getStatut().equals(Statut.ACTIVE);
            }
            catch(NullPointerException ignored){}
            return false;
        }).collect(Collectors.toList()));
    }


    //used to evaluate transaction against eligibility criteria of promo
    private boolean isEligiblePromo(PromotionDTO promotionDTO, TransactionDTO transactionDto){
        boolean eligible = false;
        String criteria = promotionDTO.getCritereEligilibite();
        criteria = criteria
            .replaceAll("requestId","\""+transactionDto.getRequestId()+"\"")
            .replaceAll("trxIdMobiquity","\""+transactionDto.getTrxIdMobiquity()+"\"")
            .replaceAll("trxType","\""+transactionDto.getTrxType()+"\"")
            .replaceAll("trxPartnerType","\""+transactionDto.getTrxPartnerType()+"\"")
            .replaceAll("trxPartnerId","\""+transactionDto.getTrxPartnerId()+"\"")
            .replaceAll("trxCustomerMSISDN","\""+transactionDto.getTrxCustomerMSISDN()+"\"")
            .replaceAll("trxCustomerAccountNumber","\""+transactionDto.getTrxCustomerMSISDN()+"\"")
            .replaceAll("trxAmount",transactionDto.getTrxAmount())
            .replaceAll("trxChannel","\""+transactionDto.getTrxChannel()+"\"");
        log.debug(" PROMOTION : {}", promotionDTO);
        log.debug("ELIGIBILITY CRITERIA : {}", criteria);
        try {
            ScriptEngineManager sem = new ScriptEngineManager();
            ScriptEngine se = sem.getEngineByName("JavaScript");
            eligible = (boolean) se.eval(criteria);
            if(eligible){
                transactionDto.setEligiblePromos(transactionDto.getEligiblePromos() + promotionDTO.getId()+":eligible;");
            }else {
                transactionDto.setEligiblePromos(transactionDto.getEligiblePromos() + promotionDTO.getId()+":notEligible;");
            }
        } catch (ScriptException e) {
            System.out.println("Invalid Expression");
            e.printStackTrace();
            transactionDto.setEligiblePromos(transactionDto.getEligiblePromos() + promotionDTO.getId()+":criteriaError;");
        }
        finally {
            this.transactionService.save(transactionDto);
        }
        return eligible;
    }


    private boolean sendSms(String messageToSend, String sender, String receiverMsisdn){
        //ignoring ssl certificate
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        RestTemplate restTemplate = setCertificateToRestTemplateObj(new RestTemplate());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject sendSmsRequest = new JSONObject();

        try {
            sendSmsRequest.put("messageToSend", messageToSend);
            sendSmsRequest.put("receiverMSISDN", receiverMsisdn);
            sendSmsRequest.put("sender", sender);
            sendSmsRequest.put("type", 0);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

        HttpEntity<String> request = new HttpEntity<>(sendSmsRequest.toString(), headers);
        String sendSmsResultStr;
        try{
            sendSmsResultStr = restTemplate.postForObject(this.talendHost + this.sendNotificationEndpoint, request, String.class);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        System.out.println("RESPONSE BODY :" + sendSmsResultStr);
        return sendSmsResultStr.equalsIgnoreCase("0");
    }


    private String constructSms(RewardDTO rewardDTO, TransactionDTO transactionDTO, TransactionRewardDTO transactionRewardDTO){
        String transactionTypeSms;
        switch (transactionDTO.getTrxType()){
            case "CASHIN" :
                transactionTypeSms = this.CASHINTYPESMS;
                break;
            case "CASHOUT" :
                transactionTypeSms = this.CASHOUTTYPESMS;
                break;
            case "MERCHANT_PAYMENT" :
                transactionTypeSms = this.MERCANTPAYMENTTYPESMS;
                break;
            case "BILL_PAYMENT" :
                transactionTypeSms = this.BILLPAYMENTTYPESMS;
                break;
            case "AIRTIME" :
                transactionTypeSms = this.BUYAIRTIMETYPESMS;
                break;
            case "BUNDLE" :
                transactionTypeSms = this.BUNDLETYPESMS;
                break;
            default:
                transactionTypeSms = transactionDTO.getTrxType();

        }
        String model = rewardDTO.getRewardNotifMessage();
        if(rewardDTO.getRewardType().equals("Fidelity"))
            model.replaceAll("totalPointsClient", getCustomerFidelityPoints(transactionDTO.getTrxCustomerMSISDN()));
        return model.replaceAll("requestId",transactionDTO.getRequestId())
            .replaceAll("nbPoints", String.valueOf(transactionRewardDTO.getRewardValue().intValue()))
            .replaceAll("nbBonus", String.valueOf(transactionRewardDTO.getRewardValue().intValue()))
            .replaceAll("trxIdMobiquity",transactionDTO.getTrxIdMobiquity())
            .replaceAll("trxType",transactionTypeSms)
            .replaceAll("trxPartnerType",transactionDTO.getTrxPartnerType())
            .replaceAll("trxPartnerId",transactionDTO.getTrxPartnerId())
            .replaceAll("trxCustomerMSISDN",transactionDTO.getTrxCustomerMSISDN())
            .replaceAll("trxCustomerAccountNumber",transactionDTO.getTrxCustomerMSISDN())
            .replaceAll("trxAmount",transactionDTO.getTrxAmount())
            .replaceAll("trxChannel",transactionDTO.getTrxChannel());
    }


    private String getCustomerFidelityPoints(String customerMsisdn){
        RestTemplate restTemplate = setCertificateToRestTemplateObj(new RestTemplate());

        ResponseEntity<String> getFidelityPointsStr;
        try {
            getFidelityPointsStr = restTemplate.getForEntity(this.mfsLoyaltyPointsHost + this.getClientTotalPointsEndpoint + customerMsisdn, String.class);
        }
        catch (Exception e){
            e.printStackTrace();
            return "XXX";
        }
        String getFidelityPointsJson = getFidelityPointsStr.getBody();
        System.out.println("RESPONSE BODY :" + getFidelityPointsJson);
        int currentBalance;
        JSONObject jsonObjectResponse = new JSONObject();
        try {
            jsonObjectResponse = new JSONObject(getFidelityPointsJson);
        } catch (JSONException e) {
            return "XXX";
        }
        try {
            currentBalance = jsonObjectResponse.getInt("currentBalance");
        } catch (JSONException e) {
            return "XXX";
        }
        return String.valueOf(currentBalance);
    }


    private boolean isMFSSubs(TransactionDTO transactionDTO) {
        //ignoring ssl certificate
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        RestTemplate rs = setCertificateToRestTemplateObj(new RestTemplate(new BufferingClientHttpRequestFactory(new
            SimpleClientHttpRequestFactory())));

        String uri = UriComponentsBuilder.fromHttpUrl(this.talendHost+this.getUserDetailsEndpoint)
            .queryParam("MSISDN", transactionDTO.getTrxCustomerMSISDN()).toUriString();

        final ResponseEntity<USSDResponse> resp = rs.getForEntity(uri, USSDResponse.class);
        if (resp.getStatusCode().is2xxSuccessful() && resp.getBody() != null) {
            final USSDResponse body = resp.getBody();
            final Param detailUserType = Arrays.stream(body.getParams())
                .filter(p -> p.getName().equals("detailUserType"))
                .findFirst()
                .orElse(null);
            final Param txnStatus = Arrays.stream(body.getParams())
                .filter(p -> p.getName().equals("txnStatus"))
                .findFirst()
                .orElse(null);

            if (txnStatus != null && !txnStatus.getValue().equals("200")) {
                log.debug("Request failed: {}", txnStatus);
                transactionDTO.setSpare1("GetUserDetails failed : "+txnStatus);
                transactionDTO.setTraitementStatus(StatutTraitementTransaction.FAILED);
                transactionService.save(transactionDTO);
                return false;
            }
            if(detailUserType != null && !StringUtils.isEmpty(detailUserType.getValue())){
                if(!detailUserType.getValue().equals("SUBSCRIBER")){
                    transactionDTO.setSpare1("User is : "+detailUserType.getValue());
                    transactionDTO.setTraitementStatus(StatutTraitementTransaction.FAILED);
                    transactionService.save(transactionDTO);
                }
                return detailUserType.getValue().equals("SUBSCRIBER");
            }

        } else {
            transactionDTO.setSpare1("GetUserDetails failed Http Error Code: " +resp.getStatusCode());
            transactionDTO.setTraitementStatus(StatutTraitementTransaction.FAILED);
            transactionService.save(transactionDTO);
            return false;
        }
        return false;
    }


    private double getFees(String sender, String receiver, String transactionAmount){
        RestTemplate restTemplate = setCertificateToRestTemplateObj(new RestTemplate());

        String uri = talendHost + "/mfs/quotation";
        log.info("Talend GET QUOTATION url {}", uri);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_XML_VALUE);

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(uri)
            .queryParam("transactionAmount", transactionAmount)
            .queryParam("language", "2")
            .queryParam("requestedServiceCode", "CASHOUT")
            .queryParam("receiverIdValue", receiver)
            .queryParam("senderIdValue", sender)
            .build();
        log.info(builder.toString());

        try {
            ResponseEntity<USSDResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), USSDResponse.class);
            log.info("Get quotation");
            if (responseEntity.getStatusCode().is2xxSuccessful() && responseEntity.getBody() != null) {
                final USSDResponse body = responseEntity.getBody();
                final Param resultCode = Arrays.stream(body.getParams())
                    .filter(p -> p.getName().equals("resultCode"))
                    .findFirst()
                    .orElse(null);
                final Param fees = Arrays.stream(body.getParams())
                    .filter(p -> p.getName().equals("freeAmount"))
                    .findFirst()
                    .orElse(null);
                final Param resultMessage = Arrays.stream(body.getParams())
                    .filter(p -> p.getName().equals("resultMessage"))
                    .findFirst()
                    .orElse(null);
                if (resultCode != null && resultCode.getValue().equals("1")) {
                    assert fees != null;
                    log.info("Fees :" + fees.getValue());
                    return Double.parseDouble(fees.getValue());
                } else {
                    return 0.0;
                }

            } else {
                return 0.0;
            }
        }
        catch (Exception e) {
            return 0.0;
        }
    }


    private void sendNotificationToPartners(@NotNull TransactionDTO transactionDTO){

        RestTemplate restTemplate = setCertificateToRestTemplateObj(new RestTemplate());
        ResponseEntity<USSDResponse> partnerBalanceParam = null;
        ResponseEntity<USSDResponse> customerBalanceParam = null;
        boolean sendIsReceiver = Objects.equals(transactionDTO.getTrxPartnerId(), transactionDTO.getTrxCustomerMSISDN());

        try {
            if (!sendIsReceiver) {
                partnerBalanceParam = restTemplate.getForEntity(getUserBalanceUrl + transactionDTO.getTrxPartnerId(), USSDResponse.class);
            }
            customerBalanceParam = restTemplate.getForEntity(getUserBalanceUrl + transactionDTO.getTrxCustomerMSISDN(), USSDResponse.class);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        List<PartnerDTO> partners = partnerMapper.toDto(partnerRepository.findAllByStatusIs(Statut.ACTIVE));
        log.debug("PARTNERS LIST: {}", partners.toString());

        if(partners.size() != 0){
            for (PartnerDTO partnerDTO : partners){
                log.debug("PARTNER: {}", partnerDTO);
                try {
                    assert partnerBalanceParam != null;
                    assert customerBalanceParam != null;

                    JSONObject trxData = buildPayloadToSendToPartners(transactionDTO, partnerDTO, partnerBalanceParam, customerBalanceParam);

                    postPayloadToPartnerCallbackUrl(partnerDTO.getCallbackUrl(), trxData, restTemplate);
                }
                catch (JSONException | URISyntaxException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

    }


    @NotNull
    private JSONObject buildPayloadToSendToPartners(@NotNull TransactionDTO transactionDTO, @NotNull PartnerDTO partnerDTO, ResponseEntity<USSDResponse> partnerBalanceParam, @NotNull ResponseEntity<USSDResponse> customerBalanceParam) throws JSONException {

        log.debug("BUILDING PAYLOAD TO SEND TO PARTNER " +partnerDTO.getName()+ "...");

        JSONObject trxData = new JSONObject();

        final Param partnerBalance = partnerBalanceParam != null ? Arrays.stream(Objects.requireNonNull(partnerBalanceParam.getBody()).getParams())
            .filter(p -> p.getName().equals("balance"))
            .findFirst()
            .orElse(null) : null;

        final Param customerBalance = Arrays.stream(Objects.requireNonNull(customerBalanceParam.getBody()).getParams())
            .filter(p -> p.getName().equals("balance"))
            .findFirst()
            .orElse(null);

        trxData.put("trxId", transactionDTO.getTrxIdMobiquity());
        trxData.put("trxType", transactionDTO.getTrxType());
        trxData.put("trxAmount", transactionDTO.getTrxAmount());
        trxData.put("trxDate", transactionDTO.getTrxDate());
        trxData.put("partner", partnerDTO.getName());

        if("CASHIN".equals(transactionDTO.getTrxType()) || "CASHOUT".equals(transactionDTO.getTrxType()) || "P2P".equals(transactionDTO.getTrxType())){
            trxData.put("trxSenderMSISDN", transactionDTO.getTrxPartnerId());
            trxData.put("senderBalance", partnerBalance != null ? partnerBalance.getValue() : 0);

            trxData.put("trxReceiverMSISDN", transactionDTO.getTrxCustomerMSISDN());
            trxData.put("receiverBalance", customerBalance != null ? customerBalance.getValue() : 0);
        }
        else if("RECHARGE".equals(transactionDTO.getTrxType()) || "BUNDLE".equals(transactionDTO.getTrxType()) || "AIRTIME".equals(transactionDTO.getTrxType())) {
            trxData.put("trxSenderMSISDN", transactionDTO.getTrxCustomerMSISDN());
            trxData.put("senderBalance", customerBalance != null ? customerBalance.getValue() : 0);

            trxData.put("trxReceiverMSISDN", transactionDTO.getTrxPartnerId());
            trxData.put("receiverBalance", partnerBalance != null ? partnerBalance.getValue() : 0);
        }
        else {
            trxData.put("trxPartnerId", transactionDTO.getTrxPartnerId());
            trxData.put("trxCustomerMSISDN", transactionDTO.getTrxCustomerMSISDN());
            trxData.put("customerBalance", customerBalance != null ? customerBalance.getValue() : 0);
        }

        log.debug("PAYLOAD BUILD: " +trxData);

        return trxData;
    }


    private void postPayloadToPartnerCallbackUrl(String uri, @NotNull JSONObject payload, @NotNull RestTemplate restTemplate) throws URISyntaxException, JSONException, IOException {
        log.debug("SENDING NOTIFICATION TO PARTNER '" + payload.get("partner") + "' IN PROGRESS...");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        ObjectMapper objectMapper = new ObjectMapper();

        HttpEntity<String> request = new HttpEntity<>(payload.toString(), headers);

        ResponseEntity<String> responseEntityStr = restTemplate.postForEntity(new URI(uri), request, String.class);
        JsonNode root = objectMapper.readTree(responseEntityStr.getBody());

        if(!root.get("code").asText().equals("200")) {
            log.debug("FAILED TO SEND TO PARTNER" + payload.get("partner") + " : {}", root);
            return;
        }

        log.debug("SUCCESSFULLY SEND TO PARTNER '" + payload.get("partner") + "' : {}", root);
    }


    private RestTemplate setCertificateToRestTemplateObj(RestTemplate restTemplate){
        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
            SSLContext sslContext;

            sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
            CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);

            restTemplate = new RestTemplate(requestFactory);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return restTemplate;
    }

}
