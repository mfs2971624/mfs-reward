package sn.free.mfsreward.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sn.free.mfsreward.domain.SubscribersUncomes;
import sn.free.mfsreward.domain.enumeration.StatutTransactionReward;
import sn.free.mfsreward.service.*;
import sn.free.mfsreward.service.dto.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;

//import sun.rmi.transport.tcp.TCPTransport;

@Service
public class RewardIncentiveCalculator implements RewardCalculator {

    @Value("${application.rewards.fidelity.fidelityPoints.CASHIN}")
    private double cashinFidelityPoints = 10.0;
    @Value("${application.rewards.fidelity.fidelityPoints.CASHOUT}")
    private double cashoutFidelityPoints = 30.0;
    @Value("${application.rewards.fidelity.fidelityPoints.BILLPAYMENT}")
    private double billPaymentFidelityPoints = 50.0;
    @Value("${application.rewards.fidelity.fidelityPoints.MERCANTPAYMENT}")
    private double mercalPaymentFidelityPoints = 20.0;
    @Value("${application.rewards.fidelity.fidelityPoints.AIRTIME}")
    private double airTimeFidelityPoints = 20.0;
    @Value("${application.rewards.fidelity.fidelityPoints.BUNDLE}")
    private double bundleFidelityPoints = 20.0;
    @Value("${application.mfsLoyaltyPointsHost}")
    private String mfsLoyaltyPointsEndpoint = "https://192.168.41.45:9092/api/mfs-loyalty-points";

    private final TransactionService transactionService;
    private final DailyCommissionsService dailyCommissionsService;
    private final BlacklistedAgentsService blacklistedAgentsService;
    private final SubscribersUncomesService subscribersUncomesService;

    public RewardIncentiveCalculator(TransactionService transactionService, DailyCommissionsService dailyCommissionsService, BlacklistedAgentsService blacklistedAgentsService, SubscribersUncomesService subscribersUncomesService) {
        this.transactionService = transactionService;
        this.dailyCommissionsService = dailyCommissionsService;
        this.blacklistedAgentsService = blacklistedAgentsService;
        this.subscribersUncomesService = subscribersUncomesService;
    }

    private double getIncentiveCommissions(TransactionDTO transactionDTO) throws ParseException {

        //assure agentMSisdn is present on database blacklisted which is considered as a whitelist
        if(!this.blacklistedAgentsService.findFirstByAgentMsisdn(transactionDTO.getTrxPartnerId()).isPresent())
            return 0.0;

        //getDailyCommissions of the day or create it

        /*Calendar todayAtMidnight = new GregorianCalendar();
        todayAtMidnight.set(Calendar.HOUR_OF_DAY, 0);
        todayAtMidnight.set(Calendar.MINUTE, 0);
        todayAtMidnight.set(Calendar.SECOND, 0);
        todayAtMidnight.set(Calendar.MILLISECOND, 0); */

        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        Date transactionDateReal = format.parse(transactionDTO.getTrxDate());
        Date transactionDate = (Date) transactionDateReal.clone();
        transactionDate.setHours(0);
        transactionDate.setMinutes(0);
        transactionDate.setSeconds(0);

        System.out.println("TYRANSACTION DATE TAKEN AT MINUIT : " + transactionDate.toString());
//        LocalDateTime now = LocalDateTime.now(); // current date and time
//        LocalDateTime midnight = now.toLocalDate().atStartOfDay();

        DailyCommissionsDTO dailyCommissionsDTO = new DailyCommissionsDTO();
        Optional<DailyCommissionsDTO> dailyCommissionsOfCurrentAgent = this.dailyCommissionsService.findFirstByAgentMsisdnAndLastModifiedDateAfter(transactionDTO.getTrxPartnerId(), transactionDate.toInstant());
        if(!dailyCommissionsOfCurrentAgent.isPresent()) {
            dailyCommissionsDTO = new DailyCommissionsDTO();
            dailyCommissionsDTO.setAgentMsisdn(transactionDTO.getTrxPartnerId());
            dailyCommissionsDTO.setTotalIncentiveCommissions(0.0);
            dailyCommissionsDTO.setTotalCashinTransactions(0.0);
            dailyCommissionsDTO.setTotalCashoutTransactions(0.0);
        }else{
            dailyCommissionsDTO = dailyCommissionsOfCurrentAgent.get();
        }


        //calculate new cashin and cashout volume
        if (transactionDTO.getTrxType().equalsIgnoreCase("CASHIN")) {
            dailyCommissionsDTO.setTotalCashinTransactions(dailyCommissionsDTO.getTotalCashinTransactions() + Double.valueOf(transactionDTO.getTrxAmount()));
        } else {
            dailyCommissionsDTO.setTotalCashoutTransactions(dailyCommissionsDTO.getTotalCashoutTransactions() + Double.valueOf(transactionDTO.getTrxAmount()));
        }
        double totalVolumeTransactions = dailyCommissionsDTO.getTotalCashinTransactions() + dailyCommissionsDTO.getTotalCashoutTransactions();

        //assure income generated is greater than 500 else return 0
        Optional<SubscribersUncomesDTO> subscribersUncomes = this.subscribersUncomesService.findFirstBySubscriberMsisdn(transactionDTO.getTrxCustomerMSISDN().substring(3));
        if(!subscribersUncomes.isPresent()){
            dailyCommissionsDTO.setLastTransactionId(String.valueOf(transactionDTO.getId()));
            dailyCommissionsDTO.setLastModifiedDate(transactionDateReal.toInstant());
            this.dailyCommissionsService.save(dailyCommissionsDTO);
            return dailyCommissionsDTO.getTotalIncentiveCommissions();
        }
        if(subscribersUncomes.get().getUncome() < 500){
            dailyCommissionsDTO.setLastTransactionId(String.valueOf(transactionDTO.getId()));
            dailyCommissionsDTO.setLastModifiedDate(transactionDateReal.toInstant());
            this.dailyCommissionsService.save(dailyCommissionsDTO);
            return dailyCommissionsDTO.getTotalIncentiveCommissions();
        }

        //Commissions calculation for eligible subs and agents
        if(totalVolumeTransactions >= 100000 && totalVolumeTransactions < 200000){
            dailyCommissionsDTO.setTotalIncentiveCommissions(1000.0);
        }
        else if(totalVolumeTransactions >= 200000 && totalVolumeTransactions < 300000){
            dailyCommissionsDTO.setTotalIncentiveCommissions(1500.0);
        }
        else if(totalVolumeTransactions >= 300000 && totalVolumeTransactions < 400000){
            dailyCommissionsDTO.setTotalIncentiveCommissions(2000.0);
        }
        else if(totalVolumeTransactions >= 400000){
            dailyCommissionsDTO.setTotalIncentiveCommissions(2500.0);
        }
        dailyCommissionsDTO.setLastTransactionId(String.valueOf(transactionDTO.getId()));
        dailyCommissionsDTO.setLastModifiedDate(transactionDateReal.toInstant());
        this.dailyCommissionsService.save(dailyCommissionsDTO);

        return dailyCommissionsDTO.getTotalIncentiveCommissions();
    }

    @Override
    public TransactionRewardDTO getReward(TransactionDTO transactionDTO, RewardDTO rewardDTO) {
        TransactionRewardDTO transactionRewardDTO = new TransactionRewardDTO();
        transactionRewardDTO.setTransactionId(transactionDTO.getId());
        transactionRewardDTO.setRewardId(rewardDTO.getId());
        return transactionRewardDTO;
    }

    @Override
    public TransactionRewardDTO setReward(TransactionRewardDTO transactionRewardDTO, TransactionDTO transactionDTO) {
        double incentiveCommissions = 0.0;
        try{
            incentiveCommissions = this.getIncentiveCommissions(transactionDTO);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        transactionRewardDTO.setRewardValue(incentiveCommissions);
        transactionRewardDTO.setRewardStatus(StatutTransactionReward.GRANTED);
        return transactionRewardDTO;
    }

    @Override
    public TransactionRewardDTO calculReward(TransactionDTO transactionDTO, RewardDTO rewardDTO) {
        TransactionRewardDTO transactionRewardDTO = this.getReward(transactionDTO,rewardDTO);
        transactionRewardDTO = this.setReward(transactionRewardDTO, transactionDTO);
        return transactionRewardDTO;
    }
}
