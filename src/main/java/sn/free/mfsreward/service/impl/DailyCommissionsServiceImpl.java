package sn.free.mfsreward.service.impl;

import sn.free.mfsreward.service.DailyCommissionsService;
import sn.free.mfsreward.domain.DailyCommissions;
import sn.free.mfsreward.repository.DailyCommissionsRepository;
import sn.free.mfsreward.service.dto.DailyCommissionsDTO;
import sn.free.mfsreward.service.mapper.DailyCommissionsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DailyCommissions}.
 */
@Service
@Transactional
public class DailyCommissionsServiceImpl implements DailyCommissionsService {

    private final Logger log = LoggerFactory.getLogger(DailyCommissionsServiceImpl.class);

    private final DailyCommissionsRepository dailyCommissionsRepository;

    private final DailyCommissionsMapper dailyCommissionsMapper;

    public DailyCommissionsServiceImpl(DailyCommissionsRepository dailyCommissionsRepository, DailyCommissionsMapper dailyCommissionsMapper) {
        this.dailyCommissionsRepository = dailyCommissionsRepository;
        this.dailyCommissionsMapper = dailyCommissionsMapper;
    }

    @Override
    public DailyCommissionsDTO save(DailyCommissionsDTO dailyCommissionsDTO) {
        log.debug("Request to save DailyCommissions : {}", dailyCommissionsDTO);
        DailyCommissions dailyCommissions = dailyCommissionsMapper.toEntity(dailyCommissionsDTO);
        dailyCommissions = dailyCommissionsRepository.save(dailyCommissions);
        return dailyCommissionsMapper.toDto(dailyCommissions);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DailyCommissionsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DailyCommissions");
        return dailyCommissionsRepository.findAll(pageable)
            .map(dailyCommissionsMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DailyCommissionsDTO> findOne(Long id) {
        log.debug("Request to get DailyCommissions : {}", id);
        return dailyCommissionsRepository.findById(id)
            .map(dailyCommissionsMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DailyCommissions : {}", id);
        dailyCommissionsRepository.deleteById(id);
    }

    @Override
    public Optional<DailyCommissionsDTO> findFirstByAgentMsisdnAndLastModifiedDateAfter(String agentMsisdn, Instant startDate) {
        log.debug("Request to get DailyCommissions of an agent the current day with agentMsisdn : {} and startDate : {}", agentMsisdn, startDate.toString());
        return dailyCommissionsRepository.findFirstByAgentMsisdnAndLastModifiedDateAfter(agentMsisdn, startDate)
            .map(dailyCommissionsMapper::toDto);
    }

    @Override
    public Page<DailyCommissionsDTO> findByDate(Pageable pageable, Instant today) {
        return dailyCommissionsRepository.findByLastModifiedDate(pageable, today);
    }
}
