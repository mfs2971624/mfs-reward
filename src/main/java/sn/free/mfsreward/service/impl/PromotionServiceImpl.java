package sn.free.mfsreward.service.impl;

import sn.free.mfsreward.domain.Campagne;
import sn.free.mfsreward.repository.CampagneRepository;
import sn.free.mfsreward.service.CampagneService;
import sn.free.mfsreward.service.PromoExecutionPlanService;
import sn.free.mfsreward.service.PromotionService;
import sn.free.mfsreward.domain.Promotion;
import sn.free.mfsreward.repository.PromotionRepository;
import sn.free.mfsreward.service.dto.PromoExecutionPlanDTO;
import sn.free.mfsreward.service.dto.PromotionDTO;
import sn.free.mfsreward.service.mapper.CampagneMapper;
import sn.free.mfsreward.service.mapper.PromotionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.time.temporal.TemporalAdjusters.nextOrSame;

/**
 * Service Implementation for managing {@link Promotion}.
 */
@Service
@Transactional
public class PromotionServiceImpl implements PromotionService {

    private final Logger log = LoggerFactory.getLogger(PromotionServiceImpl.class);

    private final PromotionRepository promotionRepository;
    private final PromoExecutionPlanService promoExecutionPlanService;

    private final CampagneService campagneService;
    private final CampagneMapper campagneMapper;
    private final PromotionMapper promotionMapper;

    private final CampagneRepository campagneRepository;

    public PromotionServiceImpl(PromotionRepository promotionRepository, PromoExecutionPlanService promoExecutionPlanService, PromotionMapper promotionMapper, CampagneService campagneService, CampagneMapper campagneMapper, CampagneRepository campagneRepository) {
        this.promotionRepository = promotionRepository;
        this.promotionMapper = promotionMapper;
        this.promoExecutionPlanService = promoExecutionPlanService;
        this.campagneService = campagneService;
        this.campagneMapper = campagneMapper;
        this.campagneRepository = campagneRepository;
    }

    /**
     * Save a promotion.
     *
     * @param promotionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PromotionDTO save(PromotionDTO promotionDTO) {
        log.debug("Request to save Promotion : {}", promotionDTO);
        Promotion promotion = promotionMapper.toEntity(promotionDTO);

        Campagne campagne = campagneRepository.findById(promotionDTO.getCampagneId()).get();
        promotion.setCampagne(campagne);
        promotion = promotionRepository.save(promotion);


        Instant dateDebutCampagne =  campagneService.findOne(promotionDTO.getCampagneId()).map(campagneMapper::toEntity).get().getDateDebut();
        Instant datefinCampagne = campagneService.findOne(promotionDTO.getCampagneId()).map(campagneMapper::toEntity).get().getDateFin();
        //Instant datefinCampagne = promotion.getCampagne().getDateFin();
        log.debug("debut: "+dateDebutCampagne);
        log.debug("fin: "+datefinCampagne);
        String periode = promotion.getFrequence().name();
        Integer temps = promotion.getFrequenceDetails();


        if ( periode.equals("Daily") ){
          List<LocalDate> dailydates = getDatesBetween(dateDebutCampagne.atZone(ZoneId.systemDefault()).toLocalDate(),datefinCampagne.atZone(ZoneId.systemDefault()).toLocalDate().plusDays(1));

            savePromoExec(dailydates,promotion);

        }

       if ( periode.equals("Weekly") ) {
           List<LocalDate> weeklydates = getDatesBetween2(dateDebutCampagne.atZone(ZoneId.systemDefault()).toLocalDate().with(nextOrSame(DayOfWeek.of(temps))),datefinCampagne.atZone(ZoneId.systemDefault()).toLocalDate().plusDays(1));

           savePromoExec(weeklydates,promotion);

       }

       if ( periode.equals("Monthly") ){
           List<LocalDate> monthlydates = getDatesBetween2(dateDebutCampagne.atZone(ZoneId.systemDefault()).toLocalDate().withDayOfMonth(temps),datefinCampagne.atZone(ZoneId.systemDefault()).toLocalDate().plusDays(1));

           savePromoExec(monthlydates,promotion);

       }

        return promotionMapper.toDto(promotion);
    }

    /**
     * Get all the promotions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PromotionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Promotions");
        return promotionRepository.findAll(pageable)
            .map(promotionMapper::toDto);
    }


    /**
     * Get one promotion by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public PromotionDTO findOne(Long id) {
        log.debug("Request to get Promotion : {}", id);

        Promotion promotion = promotionRepository.findById(id).get();
        Campagne campagne = campagneRepository.findById(promotion.getCampagne().getId()).get();

        PromotionDTO promotionDTO = promotionMapper.toDto(promotion);
        promotionDTO.setCampagneId(campagne.getId());

        return promotionDTO;
    }

    /**
     * Delete the promotion by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Promotion : {}", id);
        promotionRepository.deleteById(id);
    }

    public  List<LocalDate> getDatesBetween(
        LocalDate startDate, LocalDate endDate) {

        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        return IntStream.iterate(0, i -> i + 1)
            .limit(numOfDaysBetween)
            .mapToObj(i -> {
                return startDate.plusDays(i);
            })
            .collect(Collectors.toList());
    }

    public  List<LocalDate> getDatesBetween2(
        LocalDate startDate, LocalDate endDate) {

        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        return IntStream.iterate(0, i -> i + 7)
            .limit(numOfDaysBetween)
            .mapToObj(i -> {
                return startDate.plusDays(i);
            })
            .collect(Collectors.toList());
    }

    public  List<LocalDate> getDatesBetween3(
        LocalDate startDate, LocalDate endDate) {

        long numOfDaysBetween = ChronoUnit.MONTHS.between(startDate, endDate);
        return IntStream.iterate(0, i -> i + 1)
            .limit(numOfDaysBetween)
            .mapToObj(i -> {
                return startDate.plusMonths(i);
            })
            .collect(Collectors.toList());
    }

    private void savePromoExec(List<LocalDate> myDates,Promotion promotion){
        PromoExecutionPlanDTO promodto = new PromoExecutionPlanDTO();

        forEach:
        for (LocalDate mydate : myDates) {
            promodto.setDateDebut(mydate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            promodto.setDateFin(mydate.atTime(23,59,59).toInstant(ZoneOffset.UTC));
            promodto.setPromotionId(promotion.getId());
            PromoExecutionPlanDTO save = promoExecutionPlanService.save(promodto);
        }
    }

}
