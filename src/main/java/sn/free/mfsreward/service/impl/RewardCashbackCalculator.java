package sn.free.mfsreward.service.impl;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import sn.free.mfsreward.domain.enumeration.StatutTransactionReward;
import sn.free.mfsreward.service.RewardCalculator;
import sn.free.mfsreward.service.TransactionRewardService;
import sn.free.mfsreward.service.dto.RewardDTO;
import sn.free.mfsreward.service.dto.TransactionDTO;
import sn.free.mfsreward.service.dto.TransactionRewardDTO;
import sn.free.mfsreward.service.model.Param;
import sn.free.mfsreward.service.model.USSDResponse;

import javax.net.ssl.SSLContext;
import java.security.cert.X509Certificate;
import java.util.Arrays;

@Service
@Transactional
public class RewardCashbackCalculator implements RewardCalculator {

    @Value("${application.cashingEndpoint}")
    private String cashingEndpoint;

    @Value("${application.cashBackAccountMsisdn}")
    private String cashBackAccountMsisdn;

    @Value("${application.cashBackAccountPin}")
    private String cashBackAccountPin;

    private final Logger log = LoggerFactory.getLogger(RewardCashbackCalculator.class);

    private final TransactionRewardService transactionRewardService;

    public RewardCashbackCalculator(TransactionRewardService transactionRewardService) {
        this.transactionRewardService = transactionRewardService;
    }

    @Override
    public TransactionRewardDTO getReward(TransactionDTO transactionDTO, RewardDTO rewardDTO) {
        TransactionRewardDTO transactionRewardDTO = new TransactionRewardDTO();
        transactionRewardDTO.setTransactionId(transactionDTO.getId());
        transactionRewardDTO.setRewardId(rewardDTO.getId());

        Double totalRewardedValueForTheDay = this.transactionRewardService.getTotalRewardedValueForTheDay(transactionDTO.getTrxCustomerMSISDN(), transactionDTO.getTrxPartnerId());
        int rewardedTimes = this.transactionRewardService.getTotalRewardedTimesForTheDay(transactionDTO.getTrxCustomerMSISDN(), transactionDTO.getTrxPartnerId());
        double cashbackAmount = this.getCashbackAmount(transactionDTO, rewardDTO.getTaux(), totalRewardedValueForTheDay, rewardDTO.getValueTreshold());


        if(rewardDTO.getValueTreshold() != null){
            boolean valueThreeShouldered =  totalRewardedValueForTheDay >= rewardDTO.getValueTreshold();
            if(!valueThreeShouldered){
                if(cashbackAmount > rewardDTO.getValueTreshold()){
                    cashbackAmount = rewardDTO.getValueTreshold();
                }
            }else {
                transactionRewardDTO.setRewardMessage("Threshold value reached");
                transactionRewardDTO.setRewardStatus(StatutTransactionReward.THREESHOLD);
                return transactionRewardDTO;
            }
        }

        if(rewardDTO.getCountTreshold() != null){
            boolean countThreeShouldered = rewardedTimes < rewardDTO.getCountTreshold();

            if(!countThreeShouldered){
                transactionRewardDTO.setRewardMessage("Threshold count reached");
                transactionRewardDTO.setRewardStatus(StatutTransactionReward.THREESHOLD);
                return transactionRewardDTO;
            }
        }

        transactionRewardDTO.setRewardValue(cashbackAmount);
        return transactionRewardDTO;
    }


    @Override
    public TransactionRewardDTO setReward(TransactionRewardDTO transactionRewardDTO, TransactionDTO transactionDTO) {
        RestTemplate restTemplate = new RestTemplate();


        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
            SSLContext sslContext = null;

            sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
            CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);

            restTemplate = new RestTemplate(requestFactory);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        log.info("Talend Cashin url {}", cashingEndpoint);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_XML_VALUE);


        UriComponents builder = UriComponentsBuilder.fromHttpUrl(cashingEndpoint)
            .queryParam("bearerCode", "USSD") //TODO check this
            .queryParam("language", "en") //TODO check this
            .queryParam("initiator", "transactor") //TODO check this
            .queryParam("transactorIdType", "mobileNumber")
            .queryParam("receiverIdType", "mobileNumber") //TODO check this
            .queryParam("remarks", "MFS_CASHBACK @" + transactionDTO.getTrxIdMobiquity())
            .queryParam("receiverIdValue", transactionDTO.getTrxCustomerMSISDN()) //TODO check this
            .queryParam("transactionAmount", transactionRewardDTO.getRewardValue())
            .queryParam("transactorIdValue", cashBackAccountMsisdn)
            .queryParam("transactorPIN", cashBackAccountPin) //TODO update this
            .build();
        log.info(builder.toString());

        try {
            ResponseEntity<USSDResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), USSDResponse.class);

            if (responseEntity.getStatusCode().is2xxSuccessful() && responseEntity.getBody() != null) {
                final USSDResponse body = responseEntity.getBody();
                final Param resultCode = getParam(body, "resultCode");
                final Param resultMessage = getParam(body, "resultMessage");
                if(resultCode == null){
                    transactionRewardDTO.setRewardMessage("Error calling Cashin Service: " + responseEntity.getStatusCode().value()
                        + " , could not grant Cashback of  " + transactionRewardDTO.getRewardValue() + " to MFS subscriber " + transactionDTO.getTrxCustomerMSISDN());
                    transactionRewardDTO.setRewardStatus(StatutTransactionReward.FAILED);
                    return transactionRewardDTO;
                }
                else {
                    if(!resultCode.getValue().equals("1")){
                        transactionRewardDTO.setRewardMessage("ResultCode: "+ resultCode.getValue() +", resultMessage: "+resultMessage.getValue());
                        transactionRewardDTO.setRewardStatus(StatutTransactionReward.NOTGRANTED);
                        return transactionRewardDTO;
                    }else{
                        transactionRewardDTO.setRewardMessage("ResultCode: "+ resultCode.getValue() +", resultMessage: "+resultMessage.getValue());
                        transactionRewardDTO.setRewardStatus(StatutTransactionReward.GRANTED);
                        return transactionRewardDTO;
                    }
                }
            } else {
                transactionRewardDTO.setRewardMessage("Error calling Cashin Service: " + responseEntity.getStatusCode().value()
                    + " , could not grant Cashback of  " + transactionRewardDTO.getRewardValue() + " to MFS subscriber " + transactionDTO.getTrxCustomerMSISDN());
                transactionRewardDTO.setRewardStatus(StatutTransactionReward.FAILED);
                return transactionRewardDTO;
            }
        } catch (Exception e) {
            transactionRewardDTO.setRewardMessage("Internal Error: " + e.getMessage());
            transactionRewardDTO.setRewardStatus(StatutTransactionReward.FAILED);
            return transactionRewardDTO;
        }

    }

    private Param getParam(USSDResponse body, String resultCode) {
        return Arrays.stream(body.getParams())
            .filter(p -> p.getName().equals(resultCode))
            .findFirst()
            .orElse(null);
    }


    @Override
    public TransactionRewardDTO calculReward(TransactionDTO transactionDTO, RewardDTO rewardDTO) {
        TransactionRewardDTO transactionRewardDTO = this.getReward(transactionDTO, rewardDTO);
        if(transactionRewardDTO.getRewardStatus() != StatutTransactionReward.THREESHOLD && transactionRewardDTO.getRewardValue() != null && transactionRewardDTO.getRewardValue() != 0.0){
            transactionRewardDTO = this.setReward(transactionRewardDTO, transactionDTO);
        }
        return transactionRewardDTO;
    }

    private Double getCashbackAmount(TransactionDTO transactionDTO, Double taux, Double rewardedValueOfTheDay, Double valueThreshold) {
        double cashbackAmount = ((Double.parseDouble(transactionDTO.getTrxAmount())) * taux) / 100;
        cashbackAmount = Math.min(cashbackAmount , valueThreshold - rewardedValueOfTheDay);
        log.info("TrxAmount: {}, CashbackRate: {}, CashbackAmount: {}", transactionDTO.getTrxAmount(), taux, cashbackAmount);
        return cashbackAmount;
    }

}
