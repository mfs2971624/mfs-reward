package sn.free.mfsreward.service.impl;

import sn.free.mfsreward.domain.Promotion;
import sn.free.mfsreward.repository.PromotionRepository;
import sn.free.mfsreward.service.PromoExecutionPlanService;
import sn.free.mfsreward.domain.PromoExecutionPlan;
import sn.free.mfsreward.repository.PromoExecutionPlanRepository;
import sn.free.mfsreward.service.dto.PromoExecutionPlanDTO;
import sn.free.mfsreward.service.mapper.PromoExecutionPlanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PromoExecutionPlan}.
 */
@Service
@Transactional
public class PromoExecutionPlanServiceImpl implements PromoExecutionPlanService {

    private final Logger log = LoggerFactory.getLogger(PromoExecutionPlanServiceImpl.class);

    private final PromoExecutionPlanRepository promoExecutionPlanRepository;

    private final PromoExecutionPlanMapper promoExecutionPlanMapper;

    private final PromotionRepository promotionRepository;

    public PromoExecutionPlanServiceImpl(PromoExecutionPlanRepository promoExecutionPlanRepository, PromoExecutionPlanMapper promoExecutionPlanMapper, PromotionRepository promotionRepository) {
        this.promoExecutionPlanRepository = promoExecutionPlanRepository;
        this.promoExecutionPlanMapper = promoExecutionPlanMapper;
        this.promotionRepository = promotionRepository;
    }

    /**
     * Save a promoExecutionPlan.
     *
     * @param promoExecutionPlanDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PromoExecutionPlanDTO save(PromoExecutionPlanDTO promoExecutionPlanDTO) {
        log.debug("Request to save PromoExecutionPlan : {}", promoExecutionPlanDTO);
        PromoExecutionPlan promoExecutionPlan = promoExecutionPlanMapper.toEntity(promoExecutionPlanDTO);

        Promotion promotion = promotionRepository.findById(promoExecutionPlanDTO.getPromotionId()).get();
        promoExecutionPlan.setPromotion(promotion);

        promoExecutionPlan = promoExecutionPlanRepository.save(promoExecutionPlan);
        return promoExecutionPlanMapper.toDto(promoExecutionPlan);
    }

    /**
     * Get all the promoExecutionPlans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PromoExecutionPlanDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PromoExecutionPlans");
        return promoExecutionPlanRepository.findAll(pageable)
            .map(promoExecutionPlanMapper::toDto);
    }


    /**
     * Get one promoExecutionPlan by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public PromoExecutionPlanDTO findOne(Long id) {
        log.debug("Request to get PromoExecutionPlan : {}", id);

        PromoExecutionPlan promoExecutionPlan = promoExecutionPlanRepository.findById(id).get();
        Promotion promotion = promotionRepository.findById(promoExecutionPlan.getPromotion().getId()).get();

        PromoExecutionPlanDTO promoExecutionPlanDTO = promoExecutionPlanMapper.toDto(promoExecutionPlan);
        promoExecutionPlanDTO.setPromotionId(promotion.getId());

        return promoExecutionPlanDTO;
    }

    /**
     * Delete the promoExecutionPlan by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PromoExecutionPlan : {}", id);
        promoExecutionPlanRepository.deleteById(id);
    }

    /**
     * Get all the promoExecutionPlans by promotionId.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PromoExecutionPlanDTO> findByPromotionId(Pageable pageable, Long id) {
        log.debug("Request to get all PromoExecutionPlans by promotionId");
        return promoExecutionPlanRepository.findByPromotionId(pageable,id)
        .map(promoExecutionPlanMapper::toDto);
    }
}
