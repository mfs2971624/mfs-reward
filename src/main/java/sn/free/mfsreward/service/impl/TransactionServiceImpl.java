package sn.free.mfsreward.service.impl;

import sn.free.mfsreward.domain.Promotion;
import sn.free.mfsreward.service.EligibilityService;
import sn.free.mfsreward.service.TransactionService;
import sn.free.mfsreward.domain.Transaction;
import sn.free.mfsreward.repository.TransactionRepository;
import sn.free.mfsreward.service.dto.PromotionDTO;
import sn.free.mfsreward.service.dto.TransactionDTO;
import sn.free.mfsreward.service.mapper.TransactionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//import sun.rmi.runtime.Log;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Transaction}.
 */
@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

    private final Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);

    private final TransactionRepository transactionRepository;

    private final TransactionMapper transactionMapper;

    public TransactionServiceImpl(TransactionRepository transactionRepository, TransactionMapper transactionMapper) {
        this.transactionRepository = transactionRepository;
        this.transactionMapper = transactionMapper;
    }

    /**
     * Save a transaction.
     *
     * @param transactionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TransactionDTO save(TransactionDTO transactionDTO) {
        log.debug("Request to save Transaction : {}", transactionDTO);
        Transaction transaction = transactionMapper.toEntity(transactionDTO);
        transaction = transactionRepository.save(transaction);
        return transactionMapper.toDto(transaction);
    }



    /**
     * Get all the transactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TransactionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Transactions");
        return transactionRepository.findAllByOrderByTrxDateDesc(pageable)
            .map(transactionMapper::toDto);
    }


    /**
     * Get one transaction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TransactionDTO> findOne(Long id) {
        log.debug("Request to get Transaction : {}", id);
        return transactionRepository.findById(id)
            .map(transactionMapper::toDto);
    }

    /**
     * Delete the transaction by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Transaction : {}", id);
        transactionRepository.deleteById(id);
    }

    /**
     * Get all the transactions by msisdn.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TransactionDTO> findTransactionByMsisdn(Pageable pageable,String msisdn) {
        log.debug("Request to get all Transactions by msisdn");
        return transactionRepository.findByTrxCustomerMSISDN(pageable,msisdn)
            .map(transactionMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public List<Transaction> findTrxByTypeAndAmount(String type, String amount) {
        log.debug("Request to get all Transactions by msisdn");
        return transactionRepository.findByTrxTypeAndTrxAmountGreaterThanEqual(type, amount);
    }

    @Override
    public double getSumTransactionsTypeDayAgent(String agentMsisdn, String startDate) {
        log.debug("Request to get sum of all Transactions by agentMsisdn : {} from {}", agentMsisdn, startDate);
        return transactionRepository.getSumTransactionsTypeDayAgent(agentMsisdn, startDate);
    }
}
