package sn.free.mfsreward.service.impl;

import com.google.common.annotations.Beta;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import sn.free.mfsreward.domain.enumeration.StatutTransactionReward;
import sn.free.mfsreward.service.RewardCalculator;
import sn.free.mfsreward.service.RewardCalculatorFactory;
import sn.free.mfsreward.service.RewardService;
import sn.free.mfsreward.service.dto.PromotionDTO;
import sn.free.mfsreward.service.dto.RewardDTO;
import sn.free.mfsreward.service.dto.TransactionDTO;
import sn.free.mfsreward.service.dto.TransactionRewardDTO;
//import sun.rmi.transport.tcp.TCPTransport;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class RewardFidelityCalculator implements RewardCalculator {

    private final RewardService rewardService;

    @Value("${application.rewards.fidelity.fidelityPoints.CASHIN}")
    private double cashinFidelityPoints = 10.0;
    @Value("${application.rewards.fidelity.fidelityPoints.CASHOUT}")
    private double cashoutFidelityPoints = 30.0;
    @Value("${application.rewards.fidelity.fidelityPoints.BILLPAYMENT}")
    private double billPaymentFidelityPoints = 50.0;
    @Value("${application.rewards.fidelity.fidelityPoints.MERCANTPAYMENT}")
    private double mercalPaymentFidelityPoints = 20.0;
    @Value("${application.rewards.fidelity.fidelityPoints.AIRTIME}")
    private double airTimeFidelityPoints = 20.0;
    @Value("${application.rewards.fidelity.fidelityPoints.BUNDLE}")
    private double bundleFidelityPoints = 20.0;
    @Value("${application.mfsLoyaltyPointsHost}")
    private String mfsLoyaltyPointsEndpoint = "https://192.168.41.45:9092/api/mfs-loyalty-points";

//    public RewardFidelityCalculator(RewardService rewardService, double cashinFidelityPoints, double cashoutFidelityPoints,
//                                    double billPaymentFidelityPoints, double mercalPaymentFidelityPoints, double airTimeFidelityPoints,
//                                    double bundleFidelityPoints, String mfsLoyaltyPointsEndpoint) {
//        this.rewardService = rewardService;
//        this.cashinFidelityPoints = cashinFidelityPoints;
//        this.cashoutFidelityPoints = cashoutFidelityPoints;
//        this.billPaymentFidelityPoints = billPaymentFidelityPoints;
//        this.mercalPaymentFidelityPoints = mercalPaymentFidelityPoints;
//        this.airTimeFidelityPoints = airTimeFidelityPoints;
//        this.bundleFidelityPoints = bundleFidelityPoints;
//        this.mfsLoyaltyPointsEndpoint = mfsLoyaltyPointsEndpoint;
//    }

    public RewardFidelityCalculator(RewardService rewardService) {
        this.rewardService = rewardService;
    }

    private double getNbFidelityPoints(TransactionDTO transactionDTO){
        //logique nombre de points selon type de transaction
        double fidelityPoints = 0.0;
        switch (transactionDTO.getTrxType()){
            case "CASHIN" :
                fidelityPoints = cashinFidelityPoints;
                break;
            case "CASHOUT" :
                fidelityPoints = cashoutFidelityPoints;
                break;
            case "MERCHANT_PAYMENT" :
                fidelityPoints = mercalPaymentFidelityPoints;
                break;
            case "BILL_PAYMENT" :
                fidelityPoints = billPaymentFidelityPoints;
                break;
            case "AIRTIME" :
                fidelityPoints = airTimeFidelityPoints;
                break;
            case "BUNDLE" :
                fidelityPoints = bundleFidelityPoints;
                break;
            default:
                fidelityPoints = 0.0;

        }
        return fidelityPoints;
    }

    @Override
    public TransactionRewardDTO getReward(TransactionDTO transactionDTO, RewardDTO rewardDTO) {
        TransactionRewardDTO transactionRewardDTO = new TransactionRewardDTO();
        transactionRewardDTO.setTransactionId(transactionDTO.getId());
        transactionRewardDTO.setRewardId(rewardDTO.getId());
        double fidelityPoints = this.getNbFidelityPoints(transactionDTO);
        transactionRewardDTO.setRewardValue(fidelityPoints);
        return transactionRewardDTO;
    }

    @Override
    public TransactionRewardDTO setReward(TransactionRewardDTO transactionRewardDTO, TransactionDTO transactionDTO) {
        //ignoring ssl certificate
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        //appel api mfs loyalty points and set the following
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        //headers.set("Authorization", mfsLoyaltyPointsBearer);
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject manageLoyaltyPointRequest = new JSONObject();
        try {
            manageLoyaltyPointRequest.put("customerMsisdn", transactionDTO.getTrxCustomerMSISDN());
            manageLoyaltyPointRequest.put("points", transactionRewardDTO.getRewardValue().intValue());
            manageLoyaltyPointRequest.put("trxId", transactionDTO.getTrxIdMobiquity());
            manageLoyaltyPointRequest.put("type", "ADD");
        } catch (JSONException e) {
            //blocking
            transactionRewardDTO.setRewardStatus(StatutTransactionReward.FAILED);
            transactionRewardDTO.setRewardMessage("Internal Error");
            return transactionRewardDTO;
        }
        System.out.println("REQUEST BODY :" + manageLoyaltyPointRequest.toString());
        HttpEntity<String> request = new HttpEntity<String>(manageLoyaltyPointRequest.toString(), headers);
        String addLoyaltyResultAsJsonStr="";
        try {
            addLoyaltyResultAsJsonStr = restTemplate.postForObject(mfsLoyaltyPointsEndpoint, request, String.class);
        }
        catch (Exception e){
            //blocking
            transactionRewardDTO.setRewardStatus(StatutTransactionReward.FAILED);
            transactionRewardDTO.setRewardMessage("Error calling MfsLoyaltyPoints system");
            return transactionRewardDTO;
        }
        System.out.println("RESPONSE BODY :" + addLoyaltyResultAsJsonStr);
        JSONObject jsonObjectResponse = new JSONObject();
        int pointsEarned = 0;
        try {
            jsonObjectResponse = new JSONObject(addLoyaltyResultAsJsonStr);
        } catch (JSONException e) {
            //blocking
            transactionRewardDTO.setRewardStatus(StatutTransactionReward.FAILED);
            transactionRewardDTO.setRewardMessage("Error MfsLoyaltyPoints response body");
            return transactionRewardDTO;
        }
        try {
            pointsEarned = jsonObjectResponse.getInt("pointsEarned");
        } catch (JSONException e) {
            //blocking
            transactionRewardDTO.setRewardMessage("Internal Error");
            transactionRewardDTO.setRewardStatus(StatutTransactionReward.FAILED);
            return transactionRewardDTO;
        }

        if(pointsEarned == transactionRewardDTO.getRewardValue().intValue()){
            transactionRewardDTO.setRewardStatus(StatutTransactionReward.GRANTED);
        }
        else{
            //not blocking
            transactionRewardDTO.setRewardMessage("Points not positionned by MfsLoyaltyPoints system");
            transactionRewardDTO.setRewardStatus(StatutTransactionReward.NOTGRANTED);
        }

        transactionRewardDTO.setRewardDateTime(Instant.now());
        return transactionRewardDTO;
        //ManageLoyaltyPointRequest manageLoyaltyPointRequest = new ManageLoyaltyPointRequest(transactionDTO.getTrxCustomerMSISDN(),transactionRewardDTO.getRewardValue().intValue(), transactionDTO.getTrxIdMobiquity(),"ADD");
        //HttpEntity<ManageLoyaltyPointRequest> requestHttpEntity = new HttpEntity<>(manageLoyaltyPointRequest, headers);
        //ManageLoyaltyPointResponse manageLoyaltyPointResponse = restTemplate.postForObject(this.mfsLoyaltyPointsEndpoint, requestHttpEntity, ManageLoyaltyPointResponse.class);

    }

    @Override
    public TransactionRewardDTO calculReward(TransactionDTO transactionDTO, RewardDTO rewardDTO) {
        TransactionRewardDTO transactionRewardDTO = this.getReward(transactionDTO,rewardDTO);
        transactionRewardDTO = this.setReward(transactionRewardDTO, transactionDTO);
        return transactionRewardDTO;
    }

    //    class ManageLoyaltyPointResponse{
//        Long id;
//        String customerMsisdn;
//        String trxId;
//        int pointsEarned;
//        int pointsUsed;
//        boolean isReset;
//        int previousBalance;
//        int currentBalance;
//
//        public ManageLoyaltyPointResponse(Long id, String customerMsisdn, String trxId, int pointsEarned, int pointsUsed, boolean isReset, int previousBalance, int currentBalance) {
//            this.id = id;
//            this.customerMsisdn = customerMsisdn;
//            this.trxId = trxId;
//            this.pointsEarned = pointsEarned;
//            this.pointsUsed = pointsUsed;
//            this.isReset = isReset;
//            this.previousBalance = previousBalance;
//            this.currentBalance = currentBalance;
//        }
//    }
//
//    class ManageLoyaltyPointRequest{
//        String customerMsisdn;
//        int points;
//        String trxId;
//        String type;
//
//        public ManageLoyaltyPointRequest(String customerMsisdn, int points, String trxId, String type) {
//            this.customerMsisdn = customerMsisdn;
//            this.points = points;
//            this.trxId = trxId;
//            this.type = type;
//        }
//    }

}
