package sn.free.mfsreward.service;

import sn.free.mfsreward.service.dto.PromotionDTO;
import sn.free.mfsreward.service.dto.RewardDTO;
import sn.free.mfsreward.service.dto.TransactionDTO;
import sn.free.mfsreward.service.dto.TransactionRewardDTO;

import java.util.List;

public interface RewardCalculator {

    TransactionRewardDTO setReward(TransactionRewardDTO transactionRewardDTO, TransactionDTO transactionDTO);

    TransactionRewardDTO getReward(TransactionDTO transactionDTO, RewardDTO rewardDTO);

    TransactionRewardDTO calculReward(TransactionDTO transactionDTO, RewardDTO rewardDTO);


}
