package sn.free.mfsreward.service.client;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "serviceCode",
    "transactionAmount",
    "initiator",
    "currency",
    "bearerCode",
    "language",
    "remarks",
    "transactionMode",
    "externalReferenceId",
    "receiver",
    "sender"
})
public class FreeP2P {
    @JsonProperty("serviceCode")
    private String serviceCode;
    @JsonProperty("transactionAmount")
    private String transactionAmount;
    @JsonProperty("initiator")
    private String initiator;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("bearerCode")
    private String bearerCode;
    @JsonProperty("language")
    private String language;
    @JsonProperty("remarks")
    private String remarks;
    @JsonProperty("transactionMode")
    private String transactionMode;
    @JsonProperty("externalReferenceId")
    private String externalReferenceId;
    @JsonProperty("receiver")
    private Receiver receiver;
    @JsonProperty("sender")
    private Sender sender;
    @JsonIgnore
    private final Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("serviceCode")
    public String getServiceCode() {
        return serviceCode;
    }

    @JsonProperty("serviceCode")
    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    @JsonProperty("transactionAmount")
    public String getTransactionAmount() {
        return transactionAmount;
    }

    @JsonProperty("transactionAmount")
    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    @JsonProperty("initiator")
    public String getInitiator() {
        return initiator;
    }

    @JsonProperty("initiator")
    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("bearerCode")
    public String getBearerCode() {
        return bearerCode;
    }

    @JsonProperty("bearerCode")
    public void setBearerCode(String bearerCode) {
        this.bearerCode = bearerCode;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("remarks")
    public String getRemarks() {
        return remarks;
    }

    @JsonProperty("remarks")
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @JsonProperty("transactionMode")
    public String getTransactionMode() {
        return transactionMode;
    }

    @JsonProperty("transactionMode")
    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    @JsonProperty("externalReferenceId")
    public String getExternalReferenceId() {
        return externalReferenceId;
    }

    @JsonProperty("externalReferenceId")
    public void setExternalReferenceId(String externalReferenceId) {
        this.externalReferenceId = externalReferenceId;
    }

    @JsonProperty("receiver")
    public Receiver getReceiver() {
        return receiver;
    }

    @JsonProperty("receiver")
    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    @JsonProperty("sender")
    public Sender getSender() {
        return sender;
    }

    @JsonProperty("sender")
    public void setSender(Sender sender) {
        this.sender = sender;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
