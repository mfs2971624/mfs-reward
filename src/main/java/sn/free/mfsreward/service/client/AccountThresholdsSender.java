package sn.free.mfsreward.service.client;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "payeeMonthlyInsLimit",
    "payeeMonthlyCustLimit",
    "payerMonthlyCustLimit",
    "payerDailyInsLimit",
    "payerDailyCustLimit",
    "payerMonthlyInsLimit",
    "payeeDailyInsLimit",
    "payeeDailyCustLimit"
})
@Generated("jsonschema2pojo")
public class AccountThresholdsSender {

    @JsonProperty("payeeMonthlyInsLimit")
    private String payeeMonthlyInsLimit;
    @JsonProperty("payeeMonthlyCustLimit")
    private String payeeMonthlyCustLimit;
    @JsonProperty("payerMonthlyCustLimit")
    private String payerMonthlyCustLimit;
    @JsonProperty("payerDailyInsLimit")
    private String payerDailyInsLimit;
    @JsonProperty("payerDailyCustLimit")
    private String payerDailyCustLimit;
    @JsonProperty("payerMonthlyInsLimit")
    private String payerMonthlyInsLimit;
    @JsonProperty("payeeDailyInsLimit")
    private String payeeDailyInsLimit;
    @JsonProperty("payeeDailyCustLimit")
    private String payeeDailyCustLimit;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("payeeMonthlyInsLimit")
    public String getPayeeMonthlyInsLimit() {
        return payeeMonthlyInsLimit;
    }

    @JsonProperty("payeeMonthlyInsLimit")
    public void setPayeeMonthlyInsLimit(String payeeMonthlyInsLimit) {
        this.payeeMonthlyInsLimit = payeeMonthlyInsLimit;
    }

    @JsonProperty("payeeMonthlyCustLimit")
    public String getPayeeMonthlyCustLimit() {
        return payeeMonthlyCustLimit;
    }

    @JsonProperty("payeeMonthlyCustLimit")
    public void setPayeeMonthlyCustLimit(String payeeMonthlyCustLimit) {
        this.payeeMonthlyCustLimit = payeeMonthlyCustLimit;
    }

    @JsonProperty("payerMonthlyCustLimit")
    public String getPayerMonthlyCustLimit() {
        return payerMonthlyCustLimit;
    }

    @JsonProperty("payerMonthlyCustLimit")
    public void setPayerMonthlyCustLimit(String payerMonthlyCustLimit) {
        this.payerMonthlyCustLimit = payerMonthlyCustLimit;
    }

    @JsonProperty("payerDailyInsLimit")
    public String getPayerDailyInsLimit() {
        return payerDailyInsLimit;
    }

    @JsonProperty("payerDailyInsLimit")
    public void setPayerDailyInsLimit(String payerDailyInsLimit) {
        this.payerDailyInsLimit = payerDailyInsLimit;
    }

    @JsonProperty("payerDailyCustLimit")
    public String getPayerDailyCustLimit() {
        return payerDailyCustLimit;
    }

    @JsonProperty("payerDailyCustLimit")
    public void setPayerDailyCustLimit(String payerDailyCustLimit) {
        this.payerDailyCustLimit = payerDailyCustLimit;
    }

    @JsonProperty("payerMonthlyInsLimit")
    public String getPayerMonthlyInsLimit() {
        return payerMonthlyInsLimit;
    }

    @JsonProperty("payerMonthlyInsLimit")
    public void setPayerMonthlyInsLimit(String payerMonthlyInsLimit) {
        this.payerMonthlyInsLimit = payerMonthlyInsLimit;
    }

    @JsonProperty("payeeDailyInsLimit")
    public String getPayeeDailyInsLimit() {
        return payeeDailyInsLimit;
    }

    @JsonProperty("payeeDailyInsLimit")
    public void setPayeeDailyInsLimit(String payeeDailyInsLimit) {
        this.payeeDailyInsLimit = payeeDailyInsLimit;
    }

    @JsonProperty("payeeDailyCustLimit")
    public String getPayeeDailyCustLimit() {
        return payeeDailyCustLimit;
    }

    @JsonProperty("payeeDailyCustLimit")
    public void setPayeeDailyCustLimit(String payeeDailyCustLimit) {
        this.payeeDailyCustLimit = payeeDailyCustLimit;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
