package sn.free.mfsreward.service.client;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "serviceRequestId",
    "mfsTenantId",
    "language",
    "serviceFlow",
    "errors",
    "message",
    "transactionId",
    "accountThresholdsSender",
    "originalServiceRequestId",
    "txnStatus",
    "remarks",
    "status"
})
@Generated("jsonschema2pojo")
public class FreeP2PResponse {

    @JsonProperty("serviceRequestId")
    private String serviceRequestId;
    @JsonProperty("mfsTenantId")
    private String mfsTenantId;
    @JsonProperty("language")
    private String language;
    @JsonProperty("serviceFlow")
    private String serviceFlow;
    @JsonProperty("errors")
    private List<FreeP2PError> errors = null;
    @JsonProperty("message")
    private String message;
    @JsonProperty("transactionId")
    private String transactionId;
    @JsonProperty("accountThresholdsSender")
    private AccountThresholdsSender accountThresholdsSender;
    @JsonProperty("originalServiceRequestId")
    private String originalServiceRequestId;
    @JsonProperty("txnStatus")
    private String txnStatus;
    @JsonProperty("remarks")
    private String remarks;
    @JsonProperty("status")
    private String status;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("serviceRequestId")
    public String getServiceRequestId() {
        return serviceRequestId;
    }

    @JsonProperty("serviceRequestId")
    public void setServiceRequestId(String serviceRequestId) {
        this.serviceRequestId = serviceRequestId;
    }

    @JsonProperty("mfsTenantId")
    public String getMfsTenantId() {
        return mfsTenantId;
    }

    @JsonProperty("mfsTenantId")
    public void setMfsTenantId(String mfsTenantId) {
        this.mfsTenantId = mfsTenantId;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("serviceFlow")
    public String getServiceFlow() {
        return serviceFlow;
    }

    @JsonProperty("serviceFlow")
    public void setServiceFlow(String serviceFlow) {
        this.serviceFlow = serviceFlow;
    }

    @JsonProperty("errors")
    public List<FreeP2PError> getFreeP2PErrors() {
        return errors;
    }

    @JsonProperty("errors")
    public void setFreeP2PErrors(List<FreeP2PError> errors) {
        this.errors = errors;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("transactionId")
    public String getTransactionId() {
        return transactionId;
    }

    @JsonProperty("transactionId")
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @JsonProperty("accountThresholdsSender")
    public AccountThresholdsSender getAccountThresholdsSender() {
        return accountThresholdsSender;
    }

    @JsonProperty("accountThresholdsSender")
    public void setAccountThresholdsSender(AccountThresholdsSender accountThresholdsSender) {
        this.accountThresholdsSender = accountThresholdsSender;
    }

    @JsonProperty("originalServiceRequestId")
    public String getOriginalServiceRequestId() {
        return originalServiceRequestId;
    }

    @JsonProperty("originalServiceRequestId")
    public void setOriginalServiceRequestId(String originalServiceRequestId) {
        this.originalServiceRequestId = originalServiceRequestId;
    }

    @JsonProperty("txnStatus")
    public String getTxnStatus() {
        return txnStatus;
    }

    @JsonProperty("txnStatus")
    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    @JsonProperty("remarks")
    public String getRemarks() {
        return remarks;
    }

    @JsonProperty("remarks")
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
