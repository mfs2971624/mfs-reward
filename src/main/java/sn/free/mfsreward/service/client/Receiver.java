package sn.free.mfsreward.service.client;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "idType",
    "productId",
    "idValue",
    "identificationNo",
    "userRole",
    "mpin"
})
public class Receiver {
    @JsonProperty("idType")
    private String idType;
    @JsonProperty("productId")
    private String productId;
    @JsonProperty("idValue")
    private String idValue;
    @JsonProperty("identificationNo")
    private String identificationNo;
    @JsonProperty("userRole")
    private String userRole;
    @JsonProperty("mpin")
    private String mpin;
    @JsonIgnore
    private final Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("idType")
    public String getIdType() {
        return idType;
    }

    @JsonProperty("idType")
    public void setIdType(String idType) {
        this.idType = idType;
    }

    @JsonProperty("productId")
    public String getProductId() {
        return productId;
    }

    @JsonProperty("productId")
    public void setProductId(String productId) {
        this.productId = productId;
    }

    @JsonProperty("idValue")
    public String getIdValue() {
        return idValue;
    }

    @JsonProperty("idValue")
    public void setIdValue(String idValue) {
        this.idValue = idValue;
    }

    @JsonProperty("identificationNo")
    public String getIdentificationNo() {
        return identificationNo;
    }

    @JsonProperty("identificationNo")
    public void setIdentificationNo(String identificationNo) {
        this.identificationNo = identificationNo;
    }

    @JsonProperty("userRole")
    public String getUserRole() {
        return userRole;
    }

    @JsonProperty("userRole")
    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    @JsonProperty("mpin")
    public String getMpin() {
        return mpin;
    }

    @JsonProperty("mpin")
    public void setMpin(String mpin) {
        this.mpin = mpin;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
