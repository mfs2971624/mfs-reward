package sn.free.mfsreward.service;

import sn.free.mfsreward.service.dto.IncentiveTransfersDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfsreward.domain.IncentiveTransfers}.
 */
public interface IncentiveTransfersService {

    /**
     * Save a incentiveTransfers.
     *
     * @param incentiveTransfersDTO the entity to save.
     * @return the persisted entity.
     */
    IncentiveTransfersDTO save(IncentiveTransfersDTO incentiveTransfersDTO);

    /**
     * Get all the incentiveTransfers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<IncentiveTransfersDTO> findAll(Pageable pageable);


    /**
     * Get the "id" incentiveTransfers.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<IncentiveTransfersDTO> findOne(Long id);

    /**
     * Delete the "id" incentiveTransfers.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
