package sn.free.mfsreward.service;

import sn.free.mfsreward.service.dto.PromoExecutionPlanDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link sn.free.mfsreward.domain.PromoExecutionPlan}.
 */
public interface PromoExecutionPlanService {

    /**
     * Save a promoExecutionPlan.
     *
     * @param promoExecutionPlanDTO the entity to save.
     * @return the persisted entity.
     */
    PromoExecutionPlanDTO save(PromoExecutionPlanDTO promoExecutionPlanDTO);

    /**
     * Get all the promoExecutionPlans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PromoExecutionPlanDTO> findAll(Pageable pageable);


    /**
     * Get the "id" promoExecutionPlan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    PromoExecutionPlanDTO findOne(Long id);

    /**
     * Delete the "id" promoExecutionPlan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Page<PromoExecutionPlanDTO> findByPromotionId(Pageable pageable, Long id);

}
