package sn.free.mfsreward.service.mapper;


import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.BlacklistedAgentsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BlacklistedAgents} and its DTO {@link BlacklistedAgentsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BlacklistedAgentsMapper extends EntityMapper<BlacklistedAgentsDTO, BlacklistedAgents> {



    default BlacklistedAgents fromId(Long id) {
        if (id == null) {
            return null;
        }
        BlacklistedAgents blacklistedAgents = new BlacklistedAgents();
        blacklistedAgents.setId(id);
        return blacklistedAgents;
    }
}
