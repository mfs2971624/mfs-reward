package sn.free.mfsreward.service.mapper;


import org.mapstruct.Mapper;
import sn.free.mfsreward.domain.SubscribersUncomes;
import sn.free.mfsreward.service.dto.SubscribersUncomesDTO;

/**
 * Mapper for the entity {@link SubscribersUncomes} and its DTO {@link SubscribersUncomesDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SubscriberUncomesMapper extends EntityMapper<SubscribersUncomesDTO, SubscribersUncomes> {

}
