package sn.free.mfsreward.service.mapper;


import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.SubscribersUncomesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SubscribersUncomes} and its DTO {@link SubscribersUncomesDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SubscribersUncomesMapper extends EntityMapper<SubscribersUncomesDTO, SubscribersUncomes> {



    default SubscribersUncomes fromId(Long id) {
        if (id == null) {
            return null;
        }
        SubscribersUncomes subscribersUncomes = new SubscribersUncomes();
        subscribersUncomes.setId(id);
        return subscribersUncomes;
    }
}
