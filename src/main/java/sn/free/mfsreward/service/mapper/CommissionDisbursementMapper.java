package sn.free.mfsreward.service.mapper;


import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.CommissionDisbursementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CommissionDisbursement} and its DTO {@link CommissionDisbursementDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CommissionDisbursementMapper extends EntityMapper<CommissionDisbursementDTO, CommissionDisbursement> {


    @Mapping(target = "incentiveTransfers", ignore = true)
    @Mapping(target = "removeIncentiveTransfers", ignore = true)
    CommissionDisbursement toEntity(CommissionDisbursementDTO commissionDisbursementDTO);

    default CommissionDisbursement fromId(Long id) {
        if (id == null) {
            return null;
        }
        CommissionDisbursement commissionDisbursement = new CommissionDisbursement();
        commissionDisbursement.setId(id);
        return commissionDisbursement;
    }
}
