package sn.free.mfsreward.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sn.free.mfsreward.domain.Partner;
import sn.free.mfsreward.service.dto.PartnerDTO;

/**
 * Mapper for the entity {@link Partner} and its DTO {@link PartnerDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PartnerMapper extends EntityMapper<PartnerDTO, Partner> {


    @Mapping(target = "partner", ignore = true)
    @Mapping(target = "removePartner", ignore = true)
    //Partner toEntity(PartnerDTO partnerDTO);

    default Partner fromId(Long id) {
        if (id == null) {
            return null;
        }
        Partner partner = new Partner();
        partner.setId(id);
        return partner;
    }
}
