package sn.free.mfsreward.service.mapper;

import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.TransactionRewardDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TransactionReward} and its DTO {@link TransactionRewardDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TransactionRewardMapper extends EntityMapper<TransactionRewardDTO, TransactionReward> {

}
