package sn.free.mfsreward.service.mapper;


import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.IncentiveTransfersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link IncentiveTransfers} and its DTO {@link IncentiveTransfersDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface IncentiveTransfersMapper extends EntityMapper<IncentiveTransfersDTO, IncentiveTransfers> {


}
