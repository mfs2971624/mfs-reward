package sn.free.mfsreward.service.mapper;


import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.DailyCommissionTransactionsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DailyCommissionTransactions} and its DTO {@link DailyCommissionTransactionsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DailyCommissionTransactionsMapper extends EntityMapper<DailyCommissionTransactionsDTO, DailyCommissionTransactions> {



    default DailyCommissionTransactions fromId(Long id) {
        if (id == null) {
            return null;
        }
        DailyCommissionTransactions dailyCommissionTransactions = new DailyCommissionTransactions();
        dailyCommissionTransactions.setId(id);
        return dailyCommissionTransactions;
    }
}
