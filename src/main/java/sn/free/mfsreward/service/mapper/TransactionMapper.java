package sn.free.mfsreward.service.mapper;

import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.TransactionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Transaction} and its DTO {@link TransactionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TransactionMapper extends EntityMapper<TransactionDTO, Transaction> {


    @Mapping(target = "transactionRewards", ignore = true)
    @Mapping(target = "removeTransactionReward", ignore = true)
    Transaction toEntity(TransactionDTO transactionDTO);

    default Transaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        Transaction transaction = new Transaction();
        transaction.setId(id);
        return transaction;
    }
}
