package sn.free.mfsreward.service.mapper;

import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.PromoExecutionPlanDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PromoExecutionPlan} and its DTO {@link PromoExecutionPlanDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PromoExecutionPlanMapper extends EntityMapper<PromoExecutionPlanDTO, PromoExecutionPlan> {


}
