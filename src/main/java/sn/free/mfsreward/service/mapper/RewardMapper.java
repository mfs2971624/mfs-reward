package sn.free.mfsreward.service.mapper;


import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.RewardDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Reward} and its DTO {@link RewardDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RewardMapper extends EntityMapper<RewardDTO, Reward> {

}
