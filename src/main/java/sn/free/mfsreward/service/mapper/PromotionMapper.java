package sn.free.mfsreward.service.mapper;

import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.PromotionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Promotion} and its DTO {@link PromotionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PromotionMapper extends EntityMapper<PromotionDTO, Promotion> {
//
//    @Mapping(source = "campagne.id", target = "campagneId")
//    PromotionDTO toDto(Promotion promotion);
//
//    @Mapping(target = "rewards", ignore = true)
//    @Mapping(target = "removeReward", ignore = true)
//    @Mapping(target = "promoExecutionPlans", ignore = true)
//    @Mapping(target = "removePromoExecutionPlan", ignore = true)
//    @Mapping(source = "campagneId", target = "campagne")
//    Promotion toEntity(PromotionDTO promotionDTO);
//
//    default Promotion fromId(Long id) {
//        if (id == null) {
//            return null;
//        }
//        Promotion promotion = new Promotion();
//        promotion.setId(id);
//        return promotion;
//    }

    PromotionDTO toDto(Promotion promotion);
}
