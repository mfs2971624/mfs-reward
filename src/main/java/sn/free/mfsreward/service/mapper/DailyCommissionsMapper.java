package sn.free.mfsreward.service.mapper;


import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.DailyCommissionsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DailyCommissions} and its DTO {@link DailyCommissionsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DailyCommissionsMapper extends EntityMapper<DailyCommissionsDTO, DailyCommissions> {



    default DailyCommissions fromId(Long id) {
        if (id == null) {
            return null;
        }
        DailyCommissions dailyCommissions = new DailyCommissions();
        dailyCommissions.setId(id);
        return dailyCommissions;
    }
}
