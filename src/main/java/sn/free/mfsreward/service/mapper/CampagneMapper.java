package sn.free.mfsreward.service.mapper;

import sn.free.mfsreward.domain.*;
import sn.free.mfsreward.service.dto.CampagneDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Campagne} and its DTO {@link CampagneDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CampagneMapper extends EntityMapper<CampagneDTO, Campagne> {


    @Mapping(target = "promotions", ignore = true)
    @Mapping(target = "removePromotion", ignore = true)
    Campagne toEntity(CampagneDTO campagneDTO);

    default Campagne fromId(Long id) {
        if (id == null) {
            return null;
        }
        Campagne campagne = new Campagne();
        campagne.setId(id);
        return campagne;
    }
}
