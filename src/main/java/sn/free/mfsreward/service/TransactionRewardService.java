package sn.free.mfsreward.service;

import sn.free.mfsreward.service.dto.TransactionRewardDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfsreward.domain.TransactionReward}.
 */
public interface TransactionRewardService {

    /**
     * Save a transactionReward.
     *
     * @param transactionRewardDTO the entity to save.
     * @return the persisted entity.
     */
    TransactionRewardDTO save(TransactionRewardDTO transactionRewardDTO);

    /**
     * Get all the transactionRewards.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TransactionRewardDTO> findAll(Pageable pageable);


    /**
     * Get the "id" transactionReward.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TransactionRewardDTO> findOne(Long id);

    /**
     * Delete the "id" transactionReward.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get all the transactionRewards.
     *
     * @param id the pagination information.
     * @return the list of entities.
     */
    List<TransactionRewardDTO> findAllById(Long id);

    Double getTotalRewardedValueForTheDay(String msisdn, String trxPartnerId);

    int getTotalRewardedTimesForTheDay(String msisdn, String trxPartnerId);

}
