package sn.free.mfsreward.service;

import sn.free.mfsreward.service.dto.CommissionDisbursementDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfsreward.domain.CommissionDisbursement}.
 */
public interface CommissionDisbursementService {

    /**
     * Save a commissionDisbursement.
     *
     * @param commissionDisbursementDTO the entity to save.
     * @return the persisted entity.
     */
    CommissionDisbursementDTO save(CommissionDisbursementDTO commissionDisbursementDTO);

    /**
     * Get all the commissionDisbursements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CommissionDisbursementDTO> findAll(Pageable pageable);


    /**
     * Get the "id" commissionDisbursement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CommissionDisbursementDTO> findOne(Long id);

    /**
     * Delete the "id" commissionDisbursement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
