package sn.free.mfsreward.service;

import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import sn.free.mfsreward.domain.Transaction;
import sn.free.mfsreward.service.dto.TransactionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfsreward.domain.Transaction}.
 */
public interface TransactionService {

    /**
     * Save a transaction.
     *
     * @param transactionDTO the entity to save.
     * @return the persisted entity.
     */
    TransactionDTO save(TransactionDTO transactionDTO);

    /**
     * Get all the transactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TransactionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" transaction.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TransactionDTO> findOne(Long id);

    /**
     * Delete the "id" transaction.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);


    /**
     * Get all the transactions by msisdn.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TransactionDTO> findTransactionByMsisdn(Pageable pageable, String msisdn);


    List<Transaction> findTrxByTypeAndAmount(String type, String amount);

    double getSumTransactionsTypeDayAgent(String agentMsisdn, String startDate);

}
