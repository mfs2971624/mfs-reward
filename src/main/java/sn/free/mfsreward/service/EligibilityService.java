package sn.free.mfsreward.service;

import org.springframework.scheduling.annotation.Async;
import sn.free.mfsreward.service.dto.PromotionDTO;
import sn.free.mfsreward.service.dto.TransactionDTO;

import java.util.List;

public interface EligibilityService {

    List<PromotionDTO> getEligiblePromos(TransactionDTO transactionDTO);

    @Async
    void notifyTransaction(TransactionDTO transactionDTO);

}
