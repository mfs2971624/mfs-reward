package sn.free.mfsreward.service;


import org.springframework.core.env.Environment;
import sn.free.mfsreward.config.ApplicationProperties;
import sn.free.mfsreward.service.impl.RewardBonusCalculator;
import sn.free.mfsreward.service.impl.RewardCashbackCalculator;
import sn.free.mfsreward.service.impl.RewardFidelityCalculator;
import sn.free.mfsreward.service.impl.RewardIncentiveCalculator;


public class RewardCalculatorFactory {

    private final RewardService rewardService;
    private final TransactionService transactionService;
    private final DailyCommissionsService dailyCommissionsService;
    private final BlacklistedAgentsService blacklistedAgentsService;
    private final SubscribersUncomesService subscribersUncomesService;
    private final ApplicationProperties applicationProperties;
    private final Environment environment;
    private final TransactionRewardService transactionRewardService;

    public RewardCalculatorFactory(RewardService rewardService, TransactionService transactionService, DailyCommissionsService dailyCommissionsService, BlacklistedAgentsService blacklistedAgentsService, SubscribersUncomesService subscribersUncomesService,
                                   Environment environment, ApplicationProperties applicationProperties, TransactionRewardService transactionRewardService) {
        this.rewardService = rewardService;
        this.transactionService = transactionService;
        this.dailyCommissionsService = dailyCommissionsService;
        this.blacklistedAgentsService = blacklistedAgentsService;
        this.subscribersUncomesService = subscribersUncomesService;
        this.applicationProperties = applicationProperties;
        this.environment = environment;
        this.transactionRewardService = transactionRewardService;
    }

    public RewardCalculator getRewardCalculator(String rewardType){
        if(rewardType==null){
            return null;
        }
        if(rewardType.equalsIgnoreCase("Fidelity")){
            return new RewardFidelityCalculator(rewardService);
        }
        else if (rewardType.equalsIgnoreCase("Cashback")){
            return new RewardCashbackCalculator(transactionRewardService);
        }
        else if (rewardType.equalsIgnoreCase("Bonus")){
            return new RewardBonusCalculator(applicationProperties, environment);
        }
        else if (rewardType.equalsIgnoreCase("Incentive")){
            return new RewardIncentiveCalculator(transactionService, dailyCommissionsService, blacklistedAgentsService, subscribersUncomesService);
        }
        return null;
    }
}
