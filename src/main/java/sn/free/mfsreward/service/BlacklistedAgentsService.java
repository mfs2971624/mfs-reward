package sn.free.mfsreward.service;

import sn.free.mfsreward.domain.BlacklistedAgents;
import sn.free.mfsreward.service.dto.BlacklistedAgentsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfsreward.domain.BlacklistedAgents}.
 */
public interface BlacklistedAgentsService {

    /**
     * Save a blacklistedAgents.
     *
     * @param blacklistedAgentsDTO the entity to save.
     * @return the persisted entity.
     */
    BlacklistedAgentsDTO save(BlacklistedAgentsDTO blacklistedAgentsDTO);

    /**
     * Get all the blacklistedAgents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BlacklistedAgentsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" blacklistedAgents.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BlacklistedAgentsDTO> findOne(Long id);

    /**
     * Delete the "id" blacklistedAgents.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Optional<BlacklistedAgentsDTO> findFirstByAgentMsisdn(String agentMsisdn);
}
