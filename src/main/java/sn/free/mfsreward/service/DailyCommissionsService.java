package sn.free.mfsreward.service;

import sn.free.mfsreward.service.dto.DailyCommissionsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfsreward.domain.DailyCommissions}.
 */
public interface DailyCommissionsService {

    /**
     * Save a dailyCommissions.
     *
     * @param dailyCommissionsDTO the entity to save.
     * @return the persisted entity.
     */
    DailyCommissionsDTO save(DailyCommissionsDTO dailyCommissionsDTO);

    /**
     * Get all the dailyCommissions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DailyCommissionsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" dailyCommissions.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DailyCommissionsDTO> findOne(Long id);

    /**
     * Delete the "id" dailyCommissions.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Optional<DailyCommissionsDTO> findFirstByAgentMsisdnAndLastModifiedDateAfter(String agentMsisdn, Instant startDate);

    /**
     * Get all the dailyCommissions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DailyCommissionsDTO> findByDate(Pageable pageable, Instant today);
}
