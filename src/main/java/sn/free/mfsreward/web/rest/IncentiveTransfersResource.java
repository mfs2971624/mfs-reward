package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.service.IncentiveTransfersService;
import sn.free.mfsreward.web.rest.errors.BadRequestAlertException;
import sn.free.mfsreward.service.dto.IncentiveTransfersDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfsreward.domain.IncentiveTransfers}.
 */
@RestController
@RequestMapping("/api")
public class IncentiveTransfersResource {

    private final Logger log = LoggerFactory.getLogger(IncentiveTransfersResource.class);

    private static final String ENTITY_NAME = "incentiveTransfers";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IncentiveTransfersService incentiveTransfersService;

    public IncentiveTransfersResource(IncentiveTransfersService incentiveTransfersService) {
        this.incentiveTransfersService = incentiveTransfersService;
    }

    /**
     * {@code POST  /incentive-transfers} : Create a new incentiveTransfers.
     *
     * @param incentiveTransfersDTO the incentiveTransfersDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new incentiveTransfersDTO, or with status {@code 400 (Bad Request)} if the incentiveTransfers has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/incentive-transfers")
    public ResponseEntity<IncentiveTransfersDTO> createIncentiveTransfers(@RequestBody IncentiveTransfersDTO incentiveTransfersDTO) throws URISyntaxException {
        log.debug("REST request to save IncentiveTransfers : {}", incentiveTransfersDTO);
        if (incentiveTransfersDTO.getId() != null) {
            throw new BadRequestAlertException("A new incentiveTransfers cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IncentiveTransfersDTO result = incentiveTransfersService.save(incentiveTransfersDTO);
        return ResponseEntity.created(new URI("/api/incentive-transfers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /incentive-transfers} : Updates an existing incentiveTransfers.
     *
     * @param incentiveTransfersDTO the incentiveTransfersDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated incentiveTransfersDTO,
     * or with status {@code 400 (Bad Request)} if the incentiveTransfersDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the incentiveTransfersDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/incentive-transfers")
    public ResponseEntity<IncentiveTransfersDTO> updateIncentiveTransfers(@RequestBody IncentiveTransfersDTO incentiveTransfersDTO) throws URISyntaxException {
        log.debug("REST request to update IncentiveTransfers : {}", incentiveTransfersDTO);
        if (incentiveTransfersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IncentiveTransfersDTO result = incentiveTransfersService.save(incentiveTransfersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, incentiveTransfersDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /incentive-transfers} : get all the incentiveTransfers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of incentiveTransfers in body.
     */
    @GetMapping("/incentive-transfers")
    public ResponseEntity<List<IncentiveTransfersDTO>> getAllIncentiveTransfers(Pageable pageable) {
        log.debug("REST request to get a page of IncentiveTransfers");
        Page<IncentiveTransfersDTO> page = incentiveTransfersService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /incentive-transfers/:id} : get the "id" incentiveTransfers.
     *
     * @param id the id of the incentiveTransfersDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the incentiveTransfersDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/incentive-transfers/{id}")
    public ResponseEntity<IncentiveTransfersDTO> getIncentiveTransfers(@PathVariable Long id) {
        log.debug("REST request to get IncentiveTransfers : {}", id);
        Optional<IncentiveTransfersDTO> incentiveTransfersDTO = incentiveTransfersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(incentiveTransfersDTO);
    }

    /**
     * {@code DELETE  /incentive-transfers/:id} : delete the "id" incentiveTransfers.
     *
     * @param id the id of the incentiveTransfersDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/incentive-transfers/{id}")
    public ResponseEntity<Void> deleteIncentiveTransfers(@PathVariable Long id) {
        log.debug("REST request to delete IncentiveTransfers : {}", id);
        incentiveTransfersService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
