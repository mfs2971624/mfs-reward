package sn.free.mfsreward.web.rest;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.mfsreward.service.PromoExecutionPlanService;
import sn.free.mfsreward.web.rest.errors.BadRequestAlertException;
import sn.free.mfsreward.service.dto.PromoExecutionPlanDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfsreward.domain.PromoExecutionPlan}.
 */
@RestController
@RequestMapping("/api")
public class PromoExecutionPlanResource {

    private final Logger log = LoggerFactory.getLogger(PromoExecutionPlanResource.class);

    private static final String ENTITY_NAME = "promoExecutionPlan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PromoExecutionPlanService promoExecutionPlanService;

    public PromoExecutionPlanResource(PromoExecutionPlanService promoExecutionPlanService) {
        this.promoExecutionPlanService = promoExecutionPlanService;
    }

    /**
     * {@code POST  /promo-execution-plans} : Create a new promoExecutionPlan.
     *
     * @param promoExecutionPlanDTO the promoExecutionPlanDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new promoExecutionPlanDTO, or with status {@code 400 (Bad Request)} if the promoExecutionPlan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/promo-execution-plans")
    public ResponseEntity<PromoExecutionPlanDTO> createPromoExecutionPlan(@RequestBody PromoExecutionPlanDTO promoExecutionPlanDTO) throws URISyntaxException {
        log.debug("REST request to save PromoExecutionPlan : {}", promoExecutionPlanDTO);
        if (promoExecutionPlanDTO.getId() != null) {
            throw new BadRequestAlertException("A new promoExecutionPlan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PromoExecutionPlanDTO result = promoExecutionPlanService.save(promoExecutionPlanDTO);
        return ResponseEntity.created(new URI("/api/promo-execution-plans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /promo-execution-plans} : Updates an existing promoExecutionPlan.
     *
     * @param promoExecutionPlanDTO the promoExecutionPlanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated promoExecutionPlanDTO,
     * or with status {@code 400 (Bad Request)} if the promoExecutionPlanDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the promoExecutionPlanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/promo-execution-plans")
    public ResponseEntity<PromoExecutionPlanDTO> updatePromoExecutionPlan(@RequestBody PromoExecutionPlanDTO promoExecutionPlanDTO) throws URISyntaxException {
        log.debug("REST request to update PromoExecutionPlan : {}", promoExecutionPlanDTO);
        if (promoExecutionPlanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PromoExecutionPlanDTO result = promoExecutionPlanService.save(promoExecutionPlanDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, promoExecutionPlanDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /promo-execution-plans} : get all the promoExecutionPlans.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of promoExecutionPlans in body.
     */
    @GetMapping("/promo-execution-plans")
    public ResponseEntity<List<PromoExecutionPlanDTO>> getAllPromoExecutionPlans(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of PromoExecutionPlans");
        Page<PromoExecutionPlanDTO> page = promoExecutionPlanService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /promo-execution-plans/:id} : get the "id" promoExecutionPlan.
     *
     * @param id the id of the promoExecutionPlanDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the promoExecutionPlanDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/promo-execution-plans/{id}")
    public ResponseEntity<PromoExecutionPlanDTO> getPromoExecutionPlan(@PathVariable Long id) {
        log.debug("REST request to get PromoExecutionPlan : {}", id);
        PromoExecutionPlanDTO promoExecutionPlanDTO = promoExecutionPlanService.findOne(id);
        return ResponseEntity.ok(promoExecutionPlanDTO);
    }

    /**
     * {@code DELETE  /promo-execution-plans/:id} : delete the "id" promoExecutionPlan.
     *
     * @param id the id of the promoExecutionPlanDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/promo-execution-plans/{id}")
    public ResponseEntity<Void> deletePromoExecutionPlan(@PathVariable Long id) {
        log.debug("REST request to delete PromoExecutionPlan : {}", id);
        promoExecutionPlanService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /promo-execution-plans} : get all the promoExecutionPlans by promotionId.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of promoExecutionPlans in body.
     */
    @GetMapping("/promo-execution-plans/byPromoId/{id}")
    public ResponseEntity<List<PromoExecutionPlanDTO>> getPromoExecutionPlansByPromotionId(Pageable pageable, @PathVariable Long id,@RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of PromoExecutionPlans by proromotion id",id);
        Page<PromoExecutionPlanDTO> page = promoExecutionPlanService.findByPromotionId(pageable,id);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
