package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.service.SubscribersUncomesService;
import sn.free.mfsreward.web.rest.errors.BadRequestAlertException;
import sn.free.mfsreward.service.dto.SubscribersUncomesDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfsreward.domain.SubscribersUncomes}.
 */
@RestController
@RequestMapping("/api")
public class SubscribersUncomesResource {

    private final Logger log = LoggerFactory.getLogger(SubscribersUncomesResource.class);

    private static final String ENTITY_NAME = "subscribersUncomes";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SubscribersUncomesService subscribersUncomesService;

    public SubscribersUncomesResource(SubscribersUncomesService subscribersUncomesService) {
        this.subscribersUncomesService = subscribersUncomesService;
    }

    /**
     * {@code POST  /subscribers-uncomes} : Create a new subscribersUncomes.
     *
     * @param subscribersUncomesDTO the subscribersUncomesDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new subscribersUncomesDTO, or with status {@code 400 (Bad Request)} if the subscribersUncomes has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/subscribers-uncomes")
    public ResponseEntity<SubscribersUncomesDTO> createSubscribersUncomes(@RequestBody SubscribersUncomesDTO subscribersUncomesDTO) throws URISyntaxException {
        log.debug("REST request to save SubscribersUncomes : {}", subscribersUncomesDTO);
        if (subscribersUncomesDTO.getId() != null) {
            throw new BadRequestAlertException("A new subscribersUncomes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubscribersUncomesDTO result = subscribersUncomesService.save(subscribersUncomesDTO);
        return ResponseEntity.created(new URI("/api/subscribers-uncomes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /subscribers-uncomes} : Updates an existing subscribersUncomes.
     *
     * @param subscribersUncomesDTO the subscribersUncomesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated subscribersUncomesDTO,
     * or with status {@code 400 (Bad Request)} if the subscribersUncomesDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the subscribersUncomesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/subscribers-uncomes")
    public ResponseEntity<SubscribersUncomesDTO> updateSubscribersUncomes(@RequestBody SubscribersUncomesDTO subscribersUncomesDTO) throws URISyntaxException {
        log.debug("REST request to update SubscribersUncomes : {}", subscribersUncomesDTO);
        if (subscribersUncomesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SubscribersUncomesDTO result = subscribersUncomesService.save(subscribersUncomesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, subscribersUncomesDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /subscribers-uncomes} : get all the subscribersUncomes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subscribersUncomes in body.
     */
    @GetMapping("/subscribers-uncomes")
    public ResponseEntity<List<SubscribersUncomesDTO>> getAllSubscribersUncomes(Pageable pageable) {
        log.debug("REST request to get a page of SubscribersUncomes");
        Page<SubscribersUncomesDTO> page = subscribersUncomesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /subscribers-uncomes/:id} : get the "id" subscribersUncomes.
     *
     * @param id the id of the subscribersUncomesDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the subscribersUncomesDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/subscribers-uncomes/{id}")
    public ResponseEntity<SubscribersUncomesDTO> getSubscribersUncomes(@PathVariable Long id) {
        log.debug("REST request to get SubscribersUncomes : {}", id);
        Optional<SubscribersUncomesDTO> subscribersUncomesDTO = subscribersUncomesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(subscribersUncomesDTO);
    }

    /**
     * {@code DELETE  /subscribers-uncomes/:id} : delete the "id" subscribersUncomes.
     *
     * @param id the id of the subscribersUncomesDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/subscribers-uncomes/{id}")
    public ResponseEntity<Void> deleteSubscribersUncomes(@PathVariable Long id) {
        log.debug("REST request to delete SubscribersUncomes : {}", id);
        subscribersUncomesService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
