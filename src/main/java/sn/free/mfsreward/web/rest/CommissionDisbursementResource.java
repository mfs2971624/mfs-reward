package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.service.CommissionDisbursementService;
import sn.free.mfsreward.web.rest.errors.BadRequestAlertException;
import sn.free.mfsreward.service.dto.CommissionDisbursementDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfsreward.domain.CommissionDisbursement}.
 */
@RestController
@RequestMapping("/api")
public class CommissionDisbursementResource {

    private final Logger log = LoggerFactory.getLogger(CommissionDisbursementResource.class);

    private static final String ENTITY_NAME = "commissionDisbursement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommissionDisbursementService commissionDisbursementService;

    public CommissionDisbursementResource(CommissionDisbursementService commissionDisbursementService) {
        this.commissionDisbursementService = commissionDisbursementService;
    }

    /**
     * {@code POST  /commission-disbursements} : Create a new commissionDisbursement.
     *
     * @param commissionDisbursementDTO the commissionDisbursementDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new commissionDisbursementDTO, or with status {@code 400 (Bad Request)} if the commissionDisbursement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/commission-disbursements")
    public ResponseEntity<CommissionDisbursementDTO> createCommissionDisbursement(@RequestBody CommissionDisbursementDTO commissionDisbursementDTO) throws URISyntaxException {
        log.debug("REST request to save CommissionDisbursement : {}", commissionDisbursementDTO);
        if (commissionDisbursementDTO.getId() != null) {
            throw new BadRequestAlertException("A new commissionDisbursement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommissionDisbursementDTO result = commissionDisbursementService.save(commissionDisbursementDTO);
        return ResponseEntity.created(new URI("/api/commission-disbursements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /commission-disbursements} : Updates an existing commissionDisbursement.
     *
     * @param commissionDisbursementDTO the commissionDisbursementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commissionDisbursementDTO,
     * or with status {@code 400 (Bad Request)} if the commissionDisbursementDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the commissionDisbursementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/commission-disbursements")
    public ResponseEntity<CommissionDisbursementDTO> updateCommissionDisbursement(@RequestBody CommissionDisbursementDTO commissionDisbursementDTO) throws URISyntaxException {
        log.debug("REST request to update CommissionDisbursement : {}", commissionDisbursementDTO);
        if (commissionDisbursementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CommissionDisbursementDTO result = commissionDisbursementService.save(commissionDisbursementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissionDisbursementDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /commission-disbursements} : get all the commissionDisbursements.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of commissionDisbursements in body.
     */
    @GetMapping("/commission-disbursements")
    public ResponseEntity<List<CommissionDisbursementDTO>> getAllCommissionDisbursements(Pageable pageable) {
        log.debug("REST request to get a page of CommissionDisbursements");
        Page<CommissionDisbursementDTO> page = commissionDisbursementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /commission-disbursements/:id} : get the "id" commissionDisbursement.
     *
     * @param id the id of the commissionDisbursementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the commissionDisbursementDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/commission-disbursements/{id}")
    public ResponseEntity<CommissionDisbursementDTO> getCommissionDisbursement(@PathVariable Long id) {
        log.debug("REST request to get CommissionDisbursement : {}", id);
        Optional<CommissionDisbursementDTO> commissionDisbursementDTO = commissionDisbursementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(commissionDisbursementDTO);
    }

    /**
     * {@code DELETE  /commission-disbursements/:id} : delete the "id" commissionDisbursement.
     *
     * @param id the id of the commissionDisbursementDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/commission-disbursements/{id}")
    public ResponseEntity<Void> deleteCommissionDisbursement(@PathVariable Long id) {
        log.debug("REST request to delete CommissionDisbursement : {}", id);
        commissionDisbursementService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
