/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.free.mfsreward.web.rest.vm;
