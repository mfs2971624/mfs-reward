package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.service.BlacklistedAgentsService;
import sn.free.mfsreward.web.rest.errors.BadRequestAlertException;
import sn.free.mfsreward.service.dto.BlacklistedAgentsDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfsreward.domain.BlacklistedAgents}.
 */
@RestController
@RequestMapping("/api")
public class BlacklistedAgentsResource {

    private final Logger log = LoggerFactory.getLogger(BlacklistedAgentsResource.class);

    private static final String ENTITY_NAME = "blacklistedAgents";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BlacklistedAgentsService blacklistedAgentsService;

    public BlacklistedAgentsResource(BlacklistedAgentsService blacklistedAgentsService) {
        this.blacklistedAgentsService = blacklistedAgentsService;
    }

    /**
     * {@code POST  /blacklisted-agents} : Create a new blacklistedAgents.
     *
     * @param blacklistedAgentsDTO the blacklistedAgentsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new blacklistedAgentsDTO, or with status {@code 400 (Bad Request)} if the blacklistedAgents has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/blacklisted-agents")
    public ResponseEntity<BlacklistedAgentsDTO> createBlacklistedAgents(@RequestBody BlacklistedAgentsDTO blacklistedAgentsDTO) throws URISyntaxException {
        log.debug("REST request to save BlacklistedAgents : {}", blacklistedAgentsDTO);
        if (blacklistedAgentsDTO.getId() != null) {
            throw new BadRequestAlertException("A new blacklistedAgents cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BlacklistedAgentsDTO result = blacklistedAgentsService.save(blacklistedAgentsDTO);
        return ResponseEntity.created(new URI("/api/blacklisted-agents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /blacklisted-agents} : Updates an existing blacklistedAgents.
     *
     * @param blacklistedAgentsDTO the blacklistedAgentsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated blacklistedAgentsDTO,
     * or with status {@code 400 (Bad Request)} if the blacklistedAgentsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the blacklistedAgentsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/blacklisted-agents")
    public ResponseEntity<BlacklistedAgentsDTO> updateBlacklistedAgents(@RequestBody BlacklistedAgentsDTO blacklistedAgentsDTO) throws URISyntaxException {
        log.debug("REST request to update BlacklistedAgents : {}", blacklistedAgentsDTO);
        if (blacklistedAgentsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BlacklistedAgentsDTO result = blacklistedAgentsService.save(blacklistedAgentsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, blacklistedAgentsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /blacklisted-agents} : get all the blacklistedAgents.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of blacklistedAgents in body.
     */
    @GetMapping("/blacklisted-agents")
    public ResponseEntity<List<BlacklistedAgentsDTO>> getAllBlacklistedAgents(Pageable pageable) {
        log.debug("REST request to get a page of BlacklistedAgents");
        Page<BlacklistedAgentsDTO> page = blacklistedAgentsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /blacklisted-agents/:id} : get the "id" blacklistedAgents.
     *
     * @param id the id of the blacklistedAgentsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the blacklistedAgentsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/blacklisted-agents/{id}")
    public ResponseEntity<BlacklistedAgentsDTO> getBlacklistedAgents(@PathVariable Long id) {
        log.debug("REST request to get BlacklistedAgents : {}", id);
        Optional<BlacklistedAgentsDTO> blacklistedAgentsDTO = blacklistedAgentsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(blacklistedAgentsDTO);
    }

    /**
     * {@code DELETE  /blacklisted-agents/:id} : delete the "id" blacklistedAgents.
     *
     * @param id the id of the blacklistedAgentsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/blacklisted-agents/{id}")
    public ResponseEntity<Void> deleteBlacklistedAgents(@PathVariable Long id) {
        log.debug("REST request to delete BlacklistedAgents : {}", id);
        blacklistedAgentsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
