package sn.free.mfsreward.web.rest;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.mfsreward.service.TransactionRewardService;
import sn.free.mfsreward.web.rest.errors.BadRequestAlertException;
import sn.free.mfsreward.service.dto.TransactionRewardDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfsreward.domain.TransactionReward}.
 */
@RestController
@RequestMapping("/api")
public class TransactionRewardResource {

    private final Logger log = LoggerFactory.getLogger(TransactionRewardResource.class);

    private static final String ENTITY_NAME = "transactionReward";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TransactionRewardService transactionRewardService;

    public TransactionRewardResource(TransactionRewardService transactionRewardService) {
        this.transactionRewardService = transactionRewardService;
    }

    /**
     * {@code POST  /transaction-rewards} : Create a new transactionReward.
     *
     * @param transactionRewardDTO the transactionRewardDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transactionRewardDTO, or with status {@code 400 (Bad Request)} if the transactionReward has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/transaction-rewards")
    public ResponseEntity<TransactionRewardDTO> createTransactionReward(@RequestBody TransactionRewardDTO transactionRewardDTO) throws URISyntaxException {
        log.debug("REST request to save TransactionReward : {}", transactionRewardDTO);
        if (transactionRewardDTO.getId() != null) {
            throw new BadRequestAlertException("A new transactionReward cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TransactionRewardDTO result = transactionRewardService.save(transactionRewardDTO);
        return ResponseEntity.created(new URI("/api/transaction-rewards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /transaction-rewards} : Updates an existing transactionReward.
     *
     * @param transactionRewardDTO the transactionRewardDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transactionRewardDTO,
     * or with status {@code 400 (Bad Request)} if the transactionRewardDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the transactionRewardDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/transaction-rewards")
    public ResponseEntity<TransactionRewardDTO> updateTransactionReward(@RequestBody TransactionRewardDTO transactionRewardDTO) throws URISyntaxException {
        log.debug("REST request to update TransactionReward : {}", transactionRewardDTO);
        if (transactionRewardDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TransactionRewardDTO result = transactionRewardService.save(transactionRewardDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, transactionRewardDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /transaction-rewards} : get all the transactionRewards.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactionRewards in body.
     */
    @GetMapping("/transaction-rewards")
    public ResponseEntity<List<TransactionRewardDTO>> getAllTransactionRewards(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of TransactionRewards");
        Page<TransactionRewardDTO> page = transactionRewardService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /transaction-rewards/:id} : get the "id" transactionReward.
     *
     * @param id the id of the transactionRewardDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transactionRewardDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/transaction-rewards/{id}")
    public ResponseEntity<TransactionRewardDTO> getTransactionReward(@PathVariable Long id) {
        log.debug("REST request to get TransactionReward : {}", id);
        Optional<TransactionRewardDTO> transactionRewardDTO = transactionRewardService.findOne(id);
        return ResponseUtil.wrapOrNotFound(transactionRewardDTO);
    }

    /**
     * {@code DELETE  /transaction-rewards/:id} : delete the "id" transactionReward.
     *
     * @param id the id of the transactionRewardDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/transaction-rewards/{id}")
    public ResponseEntity<Void> deleteTransactionReward(@PathVariable Long id) {
        log.debug("REST request to delete TransactionReward : {}", id);
        transactionRewardService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/transaction-rewardsById/{id}")
    public ResponseEntity<List<TransactionRewardDTO>> getAllTransactionRewardsbyId(@PathVariable Long id) {
        log.debug("REST request to get a page of TransactionRewards");
       // List<TransactionRewardDTO> transactionRewardDTOs = transactionRewardService.findAllById(id);
      //  HttpHeaders headers = PaginationUtil. .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(),transactionRewardDTOs);
        return ResponseEntity.ok(this.transactionRewardService.findAllById(id));
     //   transactionRewardDTOs;
    }
}
