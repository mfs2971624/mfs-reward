package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.service.DailyCommissionsService;
import sn.free.mfsreward.web.rest.errors.BadRequestAlertException;
import sn.free.mfsreward.service.dto.DailyCommissionsDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfsreward.domain.DailyCommissions}.
 */
@RestController
@RequestMapping("/api")
public class DailyCommissionsResource {

    private final Logger log = LoggerFactory.getLogger(DailyCommissionsResource.class);

    private static final String ENTITY_NAME = "dailyCommissions";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DailyCommissionsService dailyCommissionsService;

    public DailyCommissionsResource(DailyCommissionsService dailyCommissionsService) {
        this.dailyCommissionsService = dailyCommissionsService;
    }

    /**
     * {@code POST  /daily-commissions} : Create a new dailyCommissions.
     *
     * @param dailyCommissionsDTO the dailyCommissionsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dailyCommissionsDTO, or with status {@code 400 (Bad Request)} if the dailyCommissions has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/daily-commissions")
    public ResponseEntity<DailyCommissionsDTO> createDailyCommissions(@RequestBody DailyCommissionsDTO dailyCommissionsDTO) throws URISyntaxException {
        log.debug("REST request to save DailyCommissions : {}", dailyCommissionsDTO);
        if (dailyCommissionsDTO.getId() != null) {
            throw new BadRequestAlertException("A new dailyCommissions cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DailyCommissionsDTO result = dailyCommissionsService.save(dailyCommissionsDTO);
        return ResponseEntity.created(new URI("/api/daily-commissions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /daily-commissions} : Updates an existing dailyCommissions.
     *
     * @param dailyCommissionsDTO the dailyCommissionsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dailyCommissionsDTO,
     * or with status {@code 400 (Bad Request)} if the dailyCommissionsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dailyCommissionsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/daily-commissions")
    public ResponseEntity<DailyCommissionsDTO> updateDailyCommissions(@RequestBody DailyCommissionsDTO dailyCommissionsDTO) throws URISyntaxException {
        log.debug("REST request to update DailyCommissions : {}", dailyCommissionsDTO);
        if (dailyCommissionsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DailyCommissionsDTO result = dailyCommissionsService.save(dailyCommissionsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dailyCommissionsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /daily-commissions} : get all the dailyCommissions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dailyCommissions in body.
     */
    @GetMapping("/daily-commissions")
    public ResponseEntity<List<DailyCommissionsDTO>> getAllDailyCommissions(Pageable pageable) {
        log.debug("REST request to get a page of DailyCommissions");
        Page<DailyCommissionsDTO> page = dailyCommissionsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /daily-commissions/:id} : get the "id" dailyCommissions.
     *
     * @param id the id of the dailyCommissionsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dailyCommissionsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/daily-commissions/{id}")
    public ResponseEntity<DailyCommissionsDTO> getDailyCommissions(@PathVariable Long id) {
        log.debug("REST request to get DailyCommissions : {}", id);
        Optional<DailyCommissionsDTO> dailyCommissionsDTO = dailyCommissionsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dailyCommissionsDTO);
    }

    /**
     * {@code DELETE  /daily-commissions/:id} : delete the "id" dailyCommissions.
     *
     * @param id the id of the dailyCommissionsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/daily-commissions/{id}")
    public ResponseEntity<Void> deleteDailyCommissions(@PathVariable Long id) {
        log.debug("REST request to delete DailyCommissions : {}", id);
        dailyCommissionsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /daily-commissions} : get all the dailyCommissions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dailyCommissions in body.
     */
    @GetMapping("/daily-commissions/today/{today}")
    public ResponseEntity<List<DailyCommissionsDTO>> getDailyCommissionsByToday(Pageable pageable, @PathVariable String today) {
        // get format date : yyyy-MM-dd
        //Instant checkFormat =  Instant.parse(today + "T00:00:37.596775900Z");
       // log.info("checkFormat = " + checkFormat + "\n");
      //  log.debug("REST request to get a page of DailyCommissions");
      // DateTimeFormatter formatter =  DateTimeFormatter.ofPattern("yyyy-MM-dd");
      // Instant todayConverted = null;
    //   todayConverted = LocalDateTime.parse(today, formatter).toInstant(ZoneOffset.UTC);
     //   log.info("todayConverted = " + todayConverted + "\n");
        log.info("today = " + Instant.now() + "\n");
        List<DailyCommissionsDTO> commissionsByToday = new ArrayList<>();
        Page<DailyCommissionsDTO> page = dailyCommissionsService.findAll(pageable);
        page.getContent().forEach(s -> {
            if(s.getLastModifiedDate().toString().startsWith(today)){
                commissionsByToday.add(s);
            }
        });
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(commissionsByToday);
    }
}
