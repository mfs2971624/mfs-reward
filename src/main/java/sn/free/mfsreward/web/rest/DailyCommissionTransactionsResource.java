package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.service.DailyCommissionTransactionsService;
import sn.free.mfsreward.web.rest.errors.BadRequestAlertException;
import sn.free.mfsreward.service.dto.DailyCommissionTransactionsDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfsreward.domain.DailyCommissionTransactions}.
 */
@RestController
@RequestMapping("/api")
public class DailyCommissionTransactionsResource {

    private final Logger log = LoggerFactory.getLogger(DailyCommissionTransactionsResource.class);

    private static final String ENTITY_NAME = "dailyCommissionTransactions";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DailyCommissionTransactionsService dailyCommissionTransactionsService;

    public DailyCommissionTransactionsResource(DailyCommissionTransactionsService dailyCommissionTransactionsService) {
        this.dailyCommissionTransactionsService = dailyCommissionTransactionsService;
    }

    /**
     * {@code POST  /daily-commission-transactions} : Create a new dailyCommissionTransactions.
     *
     * @param dailyCommissionTransactionsDTO the dailyCommissionTransactionsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dailyCommissionTransactionsDTO, or with status {@code 400 (Bad Request)} if the dailyCommissionTransactions has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/daily-commission-transactions")
    public ResponseEntity<DailyCommissionTransactionsDTO> createDailyCommissionTransactions(@RequestBody DailyCommissionTransactionsDTO dailyCommissionTransactionsDTO) throws URISyntaxException {
        log.debug("REST request to save DailyCommissionTransactions : {}", dailyCommissionTransactionsDTO);
        if (dailyCommissionTransactionsDTO.getId() != null) {
            throw new BadRequestAlertException("A new dailyCommissionTransactions cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DailyCommissionTransactionsDTO result = dailyCommissionTransactionsService.save(dailyCommissionTransactionsDTO);
        return ResponseEntity.created(new URI("/api/daily-commission-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /daily-commission-transactions} : Updates an existing dailyCommissionTransactions.
     *
     * @param dailyCommissionTransactionsDTO the dailyCommissionTransactionsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dailyCommissionTransactionsDTO,
     * or with status {@code 400 (Bad Request)} if the dailyCommissionTransactionsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dailyCommissionTransactionsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/daily-commission-transactions")
    public ResponseEntity<DailyCommissionTransactionsDTO> updateDailyCommissionTransactions(@RequestBody DailyCommissionTransactionsDTO dailyCommissionTransactionsDTO) throws URISyntaxException {
        log.debug("REST request to update DailyCommissionTransactions : {}", dailyCommissionTransactionsDTO);
        if (dailyCommissionTransactionsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DailyCommissionTransactionsDTO result = dailyCommissionTransactionsService.save(dailyCommissionTransactionsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dailyCommissionTransactionsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /daily-commission-transactions} : get all the dailyCommissionTransactions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dailyCommissionTransactions in body.
     */
    @GetMapping("/daily-commission-transactions")
    public ResponseEntity<List<DailyCommissionTransactionsDTO>> getAllDailyCommissionTransactions(Pageable pageable) {
        log.debug("REST request to get a page of DailyCommissionTransactions");
        Page<DailyCommissionTransactionsDTO> page = dailyCommissionTransactionsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /daily-commission-transactions/:id} : get the "id" dailyCommissionTransactions.
     *
     * @param id the id of the dailyCommissionTransactionsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dailyCommissionTransactionsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/daily-commission-transactions/{id}")
    public ResponseEntity<DailyCommissionTransactionsDTO> getDailyCommissionTransactions(@PathVariable Long id) {
        log.debug("REST request to get DailyCommissionTransactions : {}", id);
        Optional<DailyCommissionTransactionsDTO> dailyCommissionTransactionsDTO = dailyCommissionTransactionsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dailyCommissionTransactionsDTO);
    }

    /**
     * {@code DELETE  /daily-commission-transactions/:id} : delete the "id" dailyCommissionTransactions.
     *
     * @param id the id of the dailyCommissionTransactionsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/daily-commission-transactions/{id}")
    public ResponseEntity<Void> deleteDailyCommissionTransactions(@PathVariable Long id) {
        log.debug("REST request to delete DailyCommissionTransactions : {}", id);
        dailyCommissionTransactionsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
