import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MfsrewardTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { SubscribersUncomesDeleteDialogComponent } from 'app/entities/subscribers-uncomes/subscribers-uncomes-delete-dialog.component';
import { SubscribersUncomesService } from 'app/entities/subscribers-uncomes/subscribers-uncomes.service';

describe('Component Tests', () => {
  describe('SubscribersUncomes Management Delete Component', () => {
    let comp: SubscribersUncomesDeleteDialogComponent;
    let fixture: ComponentFixture<SubscribersUncomesDeleteDialogComponent>;
    let service: SubscribersUncomesService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [SubscribersUncomesDeleteDialogComponent]
      })
        .overrideTemplate(SubscribersUncomesDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SubscribersUncomesDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SubscribersUncomesService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
