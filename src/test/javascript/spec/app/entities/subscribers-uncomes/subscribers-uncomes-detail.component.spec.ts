import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { SubscribersUncomesDetailComponent } from 'app/entities/subscribers-uncomes/subscribers-uncomes-detail.component';
import { SubscribersUncomes } from 'app/shared/model/subscribers-uncomes.model';

describe('Component Tests', () => {
  describe('SubscribersUncomes Management Detail Component', () => {
    let comp: SubscribersUncomesDetailComponent;
    let fixture: ComponentFixture<SubscribersUncomesDetailComponent>;
    const route = ({ data: of({ subscribersUncomes: new SubscribersUncomes(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [SubscribersUncomesDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SubscribersUncomesDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SubscribersUncomesDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load subscribersUncomes on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.subscribersUncomes).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
