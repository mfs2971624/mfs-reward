import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { SubscribersUncomesService } from 'app/entities/subscribers-uncomes/subscribers-uncomes.service';
import { ISubscribersUncomes, SubscribersUncomes } from 'app/shared/model/subscribers-uncomes.model';

describe('Service Tests', () => {
  describe('SubscribersUncomes Service', () => {
    let injector: TestBed;
    let service: SubscribersUncomesService;
    let httpMock: HttpTestingController;
    let elemDefault: ISubscribersUncomes;
    let expectedResult: ISubscribersUncomes | ISubscribersUncomes[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(SubscribersUncomesService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new SubscribersUncomes(0, 'AAAAAAA', currentDate, 0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            processDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a SubscribersUncomes', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            processDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            processDate: currentDate
          },
          returnedFromService
        );

        service.create(new SubscribersUncomes()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a SubscribersUncomes', () => {
        const returnedFromService = Object.assign(
          {
            subscriberMsisdn: 'BBBBBB',
            processDate: currentDate.format(DATE_TIME_FORMAT),
            uncome: 1,
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            processDate: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of SubscribersUncomes', () => {
        const returnedFromService = Object.assign(
          {
            subscriberMsisdn: 'BBBBBB',
            processDate: currentDate.format(DATE_TIME_FORMAT),
            uncome: 1,
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            processDate: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a SubscribersUncomes', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
