import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { SubscribersUncomesUpdateComponent } from 'app/entities/subscribers-uncomes/subscribers-uncomes-update.component';
import { SubscribersUncomesService } from 'app/entities/subscribers-uncomes/subscribers-uncomes.service';
import { SubscribersUncomes } from 'app/shared/model/subscribers-uncomes.model';

describe('Component Tests', () => {
  describe('SubscribersUncomes Management Update Component', () => {
    let comp: SubscribersUncomesUpdateComponent;
    let fixture: ComponentFixture<SubscribersUncomesUpdateComponent>;
    let service: SubscribersUncomesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [SubscribersUncomesUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SubscribersUncomesUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SubscribersUncomesUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SubscribersUncomesService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SubscribersUncomes(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SubscribersUncomes();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
