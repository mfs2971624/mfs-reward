/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { TransactionRewardDetailComponent } from 'app/entities/transaction-reward/transaction-reward-detail.component';
import { TransactionReward } from 'app/shared/model/transaction-reward.model';

describe('Component Tests', () => {
  describe('TransactionReward Management Detail Component', () => {
    let comp: TransactionRewardDetailComponent;
    let fixture: ComponentFixture<TransactionRewardDetailComponent>;
    const route = ({ data: of({ transactionReward: new TransactionReward(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [TransactionRewardDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TransactionRewardDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TransactionRewardDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.transactionReward).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
