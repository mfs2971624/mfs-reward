/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MfsrewardTestModule } from '../../../test.module';
import { TransactionRewardDeleteDialogComponent } from 'app/entities/transaction-reward/transaction-reward-delete-dialog.component';
import { TransactionRewardService } from 'app/entities/transaction-reward/transaction-reward.service';

describe('Component Tests', () => {
  describe('TransactionReward Management Delete Component', () => {
    let comp: TransactionRewardDeleteDialogComponent;
    let fixture: ComponentFixture<TransactionRewardDeleteDialogComponent>;
    let service: TransactionRewardService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [TransactionRewardDeleteDialogComponent]
      })
        .overrideTemplate(TransactionRewardDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TransactionRewardDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TransactionRewardService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
