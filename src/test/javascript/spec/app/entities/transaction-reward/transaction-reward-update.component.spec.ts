/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { TransactionRewardUpdateComponent } from 'app/entities/transaction-reward/transaction-reward-update.component';
import { TransactionRewardService } from 'app/entities/transaction-reward/transaction-reward.service';
import { TransactionReward } from 'app/shared/model/transaction-reward.model';

describe('Component Tests', () => {
  describe('TransactionReward Management Update Component', () => {
    let comp: TransactionRewardUpdateComponent;
    let fixture: ComponentFixture<TransactionRewardUpdateComponent>;
    let service: TransactionRewardService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [TransactionRewardUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TransactionRewardUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TransactionRewardUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TransactionRewardService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TransactionReward(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TransactionReward();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
