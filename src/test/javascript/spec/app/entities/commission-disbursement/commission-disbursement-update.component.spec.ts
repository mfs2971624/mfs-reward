import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { CommissionDisbursementUpdateComponent } from 'app/entities/commission-disbursement/commission-disbursement-update.component';
import { CommissionDisbursementService } from 'app/entities/commission-disbursement/commission-disbursement.service';
import { CommissionDisbursement } from 'app/shared/model/commission-disbursement.model';

describe('Component Tests', () => {
  describe('CommissionDisbursement Management Update Component', () => {
    let comp: CommissionDisbursementUpdateComponent;
    let fixture: ComponentFixture<CommissionDisbursementUpdateComponent>;
    let service: CommissionDisbursementService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [CommissionDisbursementUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CommissionDisbursementUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CommissionDisbursementUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CommissionDisbursementService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CommissionDisbursement(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CommissionDisbursement();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
