import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { CommissionDisbursementDetailComponent } from 'app/entities/commission-disbursement/commission-disbursement-detail.component';
import { CommissionDisbursement } from 'app/shared/model/commission-disbursement.model';

describe('Component Tests', () => {
  describe('CommissionDisbursement Management Detail Component', () => {
    let comp: CommissionDisbursementDetailComponent;
    let fixture: ComponentFixture<CommissionDisbursementDetailComponent>;
    const route = ({ data: of({ commissionDisbursement: new CommissionDisbursement(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [CommissionDisbursementDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CommissionDisbursementDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CommissionDisbursementDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load commissionDisbursement on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.commissionDisbursement).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
