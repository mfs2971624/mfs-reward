import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { BlacklistedAgentsService } from 'app/entities/blacklisted-agents/blacklisted-agents.service';
import { IBlacklistedAgents, BlacklistedAgents } from 'app/shared/model/blacklisted-agents.model';

describe('Service Tests', () => {
  describe('BlacklistedAgents Service', () => {
    let injector: TestBed;
    let service: BlacklistedAgentsService;
    let httpMock: HttpTestingController;
    let elemDefault: IBlacklistedAgents;
    let expectedResult: IBlacklistedAgents | IBlacklistedAgents[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(BlacklistedAgentsService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new BlacklistedAgents(
        0,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            blackListedDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a BlacklistedAgents', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            blackListedDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            blackListedDate: currentDate
          },
          returnedFromService
        );

        service.create(new BlacklistedAgents()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a BlacklistedAgents', () => {
        const returnedFromService = Object.assign(
          {
            agentMsisdn: 'BBBBBB',
            blackListedDate: currentDate.format(DATE_TIME_FORMAT),
            blackListedBy: 'BBBBBB',
            agentType: 'BBBBBB',
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            blackListedDate: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of BlacklistedAgents', () => {
        const returnedFromService = Object.assign(
          {
            agentMsisdn: 'BBBBBB',
            blackListedDate: currentDate.format(DATE_TIME_FORMAT),
            blackListedBy: 'BBBBBB',
            agentType: 'BBBBBB',
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            blackListedDate: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BlacklistedAgents', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
