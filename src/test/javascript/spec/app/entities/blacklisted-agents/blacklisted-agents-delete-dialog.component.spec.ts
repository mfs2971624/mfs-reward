import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MfsrewardTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { BlacklistedAgentsDeleteDialogComponent } from 'app/entities/blacklisted-agents/blacklisted-agents-delete-dialog.component';
import { BlacklistedAgentsService } from 'app/entities/blacklisted-agents/blacklisted-agents.service';

describe('Component Tests', () => {
  describe('BlacklistedAgents Management Delete Component', () => {
    let comp: BlacklistedAgentsDeleteDialogComponent;
    let fixture: ComponentFixture<BlacklistedAgentsDeleteDialogComponent>;
    let service: BlacklistedAgentsService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [BlacklistedAgentsDeleteDialogComponent]
      })
        .overrideTemplate(BlacklistedAgentsDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BlacklistedAgentsDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BlacklistedAgentsService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
