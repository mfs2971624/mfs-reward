import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

import { MfsrewardTestModule } from '../../../test.module';
import { BlacklistedAgentsComponent } from 'app/entities/blacklisted-agents/blacklisted-agents.component';
import { BlacklistedAgentsService } from 'app/entities/blacklisted-agents/blacklisted-agents.service';
import { BlacklistedAgents } from 'app/shared/model/blacklisted-agents.model';

describe('Component Tests', () => {
  describe('BlacklistedAgents Management Component', () => {
    let comp: BlacklistedAgentsComponent;
    let fixture: ComponentFixture<BlacklistedAgentsComponent>;
    let service: BlacklistedAgentsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [BlacklistedAgentsComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: of({
                defaultSort: 'id,asc'
              }),
              queryParamMap: of(
                convertToParamMap({
                  page: '1',
                  size: '1',
                  sort: 'id,desc'
                })
              )
            }
          }
        ]
      })
        .overrideTemplate(BlacklistedAgentsComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BlacklistedAgentsComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BlacklistedAgentsService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BlacklistedAgents(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.blacklistedAgents && comp.blacklistedAgents[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BlacklistedAgents(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.blacklistedAgents && comp.blacklistedAgents[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
