import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { BlacklistedAgentsUpdateComponent } from 'app/entities/blacklisted-agents/blacklisted-agents-update.component';
import { BlacklistedAgentsService } from 'app/entities/blacklisted-agents/blacklisted-agents.service';
import { BlacklistedAgents } from 'app/shared/model/blacklisted-agents.model';

describe('Component Tests', () => {
  describe('BlacklistedAgents Management Update Component', () => {
    let comp: BlacklistedAgentsUpdateComponent;
    let fixture: ComponentFixture<BlacklistedAgentsUpdateComponent>;
    let service: BlacklistedAgentsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [BlacklistedAgentsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BlacklistedAgentsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BlacklistedAgentsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BlacklistedAgentsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BlacklistedAgents(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BlacklistedAgents();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
