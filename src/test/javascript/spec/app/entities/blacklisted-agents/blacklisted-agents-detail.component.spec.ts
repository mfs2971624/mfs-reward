import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { BlacklistedAgentsDetailComponent } from 'app/entities/blacklisted-agents/blacklisted-agents-detail.component';
import { BlacklistedAgents } from 'app/shared/model/blacklisted-agents.model';

describe('Component Tests', () => {
  describe('BlacklistedAgents Management Detail Component', () => {
    let comp: BlacklistedAgentsDetailComponent;
    let fixture: ComponentFixture<BlacklistedAgentsDetailComponent>;
    const route = ({ data: of({ blacklistedAgents: new BlacklistedAgents(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [BlacklistedAgentsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BlacklistedAgentsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BlacklistedAgentsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load blacklistedAgents on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.blacklistedAgents).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
