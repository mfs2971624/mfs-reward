import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { DailyCommissionTransactionsService } from 'app/entities/daily-commission-transactions/daily-commission-transactions.service';
import { IDailyCommissionTransactions, DailyCommissionTransactions } from 'app/shared/model/daily-commission-transactions.model';

describe('Service Tests', () => {
  describe('DailyCommissionTransactions Service', () => {
    let injector: TestBed;
    let service: DailyCommissionTransactionsService;
    let httpMock: HttpTestingController;
    let elemDefault: IDailyCommissionTransactions;
    let expectedResult: IDailyCommissionTransactions | IDailyCommissionTransactions[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(DailyCommissionTransactionsService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new DailyCommissionTransactions(0, 'AAAAAAA', currentDate, 0, 0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            processDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DailyCommissionTransactions', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            processDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            processDate: currentDate
          },
          returnedFromService
        );

        service.create(new DailyCommissionTransactions()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DailyCommissionTransactions', () => {
        const returnedFromService = Object.assign(
          {
            agentMsisdn: 'BBBBBB',
            processDate: currentDate.format(DATE_TIME_FORMAT),
            totalTransactionsVolume: 1,
            totalCommissions: 1,
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            processDate: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DailyCommissionTransactions', () => {
        const returnedFromService = Object.assign(
          {
            agentMsisdn: 'BBBBBB',
            processDate: currentDate.format(DATE_TIME_FORMAT),
            totalTransactionsVolume: 1,
            totalCommissions: 1,
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            processDate: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DailyCommissionTransactions', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
