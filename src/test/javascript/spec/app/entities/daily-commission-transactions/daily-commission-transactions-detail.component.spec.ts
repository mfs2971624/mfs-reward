import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { DailyCommissionTransactionsDetailComponent } from 'app/entities/daily-commission-transactions/daily-commission-transactions-detail.component';
import { DailyCommissionTransactions } from 'app/shared/model/daily-commission-transactions.model';

describe('Component Tests', () => {
  describe('DailyCommissionTransactions Management Detail Component', () => {
    let comp: DailyCommissionTransactionsDetailComponent;
    let fixture: ComponentFixture<DailyCommissionTransactionsDetailComponent>;
    const route = ({ data: of({ dailyCommissionTransactions: new DailyCommissionTransactions(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [DailyCommissionTransactionsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DailyCommissionTransactionsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DailyCommissionTransactionsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load dailyCommissionTransactions on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.dailyCommissionTransactions).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
