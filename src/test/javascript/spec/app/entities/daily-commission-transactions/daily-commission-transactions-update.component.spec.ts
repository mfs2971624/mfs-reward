import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { DailyCommissionTransactionsUpdateComponent } from 'app/entities/daily-commission-transactions/daily-commission-transactions-update.component';
import { DailyCommissionTransactionsService } from 'app/entities/daily-commission-transactions/daily-commission-transactions.service';
import { DailyCommissionTransactions } from 'app/shared/model/daily-commission-transactions.model';

describe('Component Tests', () => {
  describe('DailyCommissionTransactions Management Update Component', () => {
    let comp: DailyCommissionTransactionsUpdateComponent;
    let fixture: ComponentFixture<DailyCommissionTransactionsUpdateComponent>;
    let service: DailyCommissionTransactionsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [DailyCommissionTransactionsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DailyCommissionTransactionsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DailyCommissionTransactionsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DailyCommissionTransactionsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DailyCommissionTransactions(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DailyCommissionTransactions();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
