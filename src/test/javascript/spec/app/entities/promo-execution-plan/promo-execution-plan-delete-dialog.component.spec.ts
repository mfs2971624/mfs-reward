/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MfsrewardTestModule } from '../../../test.module';
import { PromoExecutionPlanDeleteDialogComponent } from 'app/entities/promo-execution-plan/promo-execution-plan-delete-dialog.component';
import { PromoExecutionPlanService } from 'app/entities/promo-execution-plan/promo-execution-plan.service';

describe('Component Tests', () => {
  describe('PromoExecutionPlan Management Delete Component', () => {
    let comp: PromoExecutionPlanDeleteDialogComponent;
    let fixture: ComponentFixture<PromoExecutionPlanDeleteDialogComponent>;
    let service: PromoExecutionPlanService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [PromoExecutionPlanDeleteDialogComponent]
      })
        .overrideTemplate(PromoExecutionPlanDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PromoExecutionPlanDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PromoExecutionPlanService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
