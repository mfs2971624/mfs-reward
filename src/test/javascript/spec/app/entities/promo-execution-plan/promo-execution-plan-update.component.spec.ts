/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { PromoExecutionPlanUpdateComponent } from 'app/entities/promo-execution-plan/promo-execution-plan-update.component';
import { PromoExecutionPlanService } from 'app/entities/promo-execution-plan/promo-execution-plan.service';
import { PromoExecutionPlan } from 'app/shared/model/promo-execution-plan.model';

describe('Component Tests', () => {
  describe('PromoExecutionPlan Management Update Component', () => {
    let comp: PromoExecutionPlanUpdateComponent;
    let fixture: ComponentFixture<PromoExecutionPlanUpdateComponent>;
    let service: PromoExecutionPlanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [PromoExecutionPlanUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PromoExecutionPlanUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PromoExecutionPlanUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PromoExecutionPlanService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new PromoExecutionPlan(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new PromoExecutionPlan();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
