/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { PromoExecutionPlanDetailComponent } from 'app/entities/promo-execution-plan/promo-execution-plan-detail.component';
import { PromoExecutionPlan } from 'app/shared/model/promo-execution-plan.model';

describe('Component Tests', () => {
  describe('PromoExecutionPlan Management Detail Component', () => {
    let comp: PromoExecutionPlanDetailComponent;
    let fixture: ComponentFixture<PromoExecutionPlanDetailComponent>;
    const route = ({ data: of({ promoExecutionPlan: new PromoExecutionPlan(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [PromoExecutionPlanDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PromoExecutionPlanDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PromoExecutionPlanDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.promoExecutionPlan).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
