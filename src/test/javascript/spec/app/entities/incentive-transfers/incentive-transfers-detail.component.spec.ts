import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { IncentiveTransfersDetailComponent } from 'app/entities/incentive-transfers/incentive-transfers-detail.component';
import { IncentiveTransfers } from 'app/shared/model/incentive-transfers.model';

describe('Component Tests', () => {
  describe('IncentiveTransfers Management Detail Component', () => {
    let comp: IncentiveTransfersDetailComponent;
    let fixture: ComponentFixture<IncentiveTransfersDetailComponent>;
    const route = ({ data: of({ incentiveTransfers: new IncentiveTransfers(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [IncentiveTransfersDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(IncentiveTransfersDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(IncentiveTransfersDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load incentiveTransfers on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.incentiveTransfers).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
