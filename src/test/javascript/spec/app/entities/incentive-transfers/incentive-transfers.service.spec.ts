import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IncentiveTransfersService } from 'app/entities/incentive-transfers/incentive-transfers.service';
import { IIncentiveTransfers, IncentiveTransfers } from 'app/shared/model/incentive-transfers.model';
import { PaymentStatus } from 'app/shared/model/enumerations/payment-status.model';

describe('Service Tests', () => {
  describe('IncentiveTransfers Service', () => {
    let injector: TestBed;
    let service: IncentiveTransfersService;
    let httpMock: HttpTestingController;
    let elemDefault: IIncentiveTransfers;
    let expectedResult: IIncentiveTransfers | IIncentiveTransfers[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(IncentiveTransfersService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new IncentiveTransfers(
        0,
        'AAAAAAA',
        0,
        currentDate,
        PaymentStatus.PENDING,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            paymentDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a IncentiveTransfers', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            paymentDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            paymentDate: currentDate
          },
          returnedFromService
        );

        service.create(new IncentiveTransfers()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a IncentiveTransfers', () => {
        const returnedFromService = Object.assign(
          {
            agentMsisdn: 'BBBBBB',
            amount: 1,
            paymentDate: currentDate.format(DATE_TIME_FORMAT),
            paymentStatus: 'BBBBBB',
            mobiquityResponse: 'BBBBBB',
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            paymentDate: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of IncentiveTransfers', () => {
        const returnedFromService = Object.assign(
          {
            agentMsisdn: 'BBBBBB',
            amount: 1,
            paymentDate: currentDate.format(DATE_TIME_FORMAT),
            paymentStatus: 'BBBBBB',
            mobiquityResponse: 'BBBBBB',
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            paymentDate: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a IncentiveTransfers', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
