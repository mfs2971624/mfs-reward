import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { IncentiveTransfersUpdateComponent } from 'app/entities/incentive-transfers/incentive-transfers-update.component';
import { IncentiveTransfersService } from 'app/entities/incentive-transfers/incentive-transfers.service';
import { IncentiveTransfers } from 'app/shared/model/incentive-transfers.model';

describe('Component Tests', () => {
  describe('IncentiveTransfers Management Update Component', () => {
    let comp: IncentiveTransfersUpdateComponent;
    let fixture: ComponentFixture<IncentiveTransfersUpdateComponent>;
    let service: IncentiveTransfersService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [IncentiveTransfersUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(IncentiveTransfersUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(IncentiveTransfersUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(IncentiveTransfersService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new IncentiveTransfers(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new IncentiveTransfers();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
