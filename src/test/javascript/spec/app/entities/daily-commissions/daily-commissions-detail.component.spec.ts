import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { DailyCommissionsDetailComponent } from 'app/entities/daily-commissions/daily-commissions-detail.component';
import { DailyCommissions } from 'app/shared/model/daily-commissions.model';

describe('Component Tests', () => {
  describe('DailyCommissions Management Detail Component', () => {
    let comp: DailyCommissionsDetailComponent;
    let fixture: ComponentFixture<DailyCommissionsDetailComponent>;
    const route = ({ data: of({ dailyCommissions: new DailyCommissions(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [DailyCommissionsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DailyCommissionsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DailyCommissionsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load dailyCommissions on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.dailyCommissions).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
