import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { DailyCommissionsService } from 'app/entities/daily-commissions/daily-commissions.service';
import { IDailyCommissions, DailyCommissions } from 'app/shared/model/daily-commissions.model';

describe('Service Tests', () => {
  describe('DailyCommissions Service', () => {
    let injector: TestBed;
    let service: DailyCommissionsService;
    let httpMock: HttpTestingController;
    let elemDefault: IDailyCommissions;
    let expectedResult: IDailyCommissions | IDailyCommissions[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(DailyCommissionsService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new DailyCommissions(
        0,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        0,
        0,
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            lastModifiedDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DailyCommissions', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            lastModifiedDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            lastModifiedDate: currentDate
          },
          returnedFromService
        );

        service.create(new DailyCommissions()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DailyCommissions', () => {
        const returnedFromService = Object.assign(
          {
            agentMsisdn: 'BBBBBB',
            lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
            lastTransactionId: 'BBBBBB',
            totalCashinTransactions: 1,
            totalCashoutTransactions: 1,
            totalIncentiveCommissions: 1,
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            lastModifiedDate: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DailyCommissions', () => {
        const returnedFromService = Object.assign(
          {
            agentMsisdn: 'BBBBBB',
            lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
            lastTransactionId: 'BBBBBB',
            totalCashinTransactions: 1,
            totalCashoutTransactions: 1,
            totalIncentiveCommissions: 1,
            spare1: 'BBBBBB',
            spare2: 'BBBBBB',
            spare3: 'BBBBBB',
            spare4: 'BBBBBB',
            spare5: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            lastModifiedDate: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DailyCommissions', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
