import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MfsrewardTestModule } from '../../../test.module';
import { DailyCommissionsUpdateComponent } from 'app/entities/daily-commissions/daily-commissions-update.component';
import { DailyCommissionsService } from 'app/entities/daily-commissions/daily-commissions.service';
import { DailyCommissions } from 'app/shared/model/daily-commissions.model';

describe('Component Tests', () => {
  describe('DailyCommissions Management Update Component', () => {
    let comp: DailyCommissionsUpdateComponent;
    let fixture: ComponentFixture<DailyCommissionsUpdateComponent>;
    let service: DailyCommissionsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsrewardTestModule],
        declarations: [DailyCommissionsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DailyCommissionsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DailyCommissionsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DailyCommissionsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DailyCommissions(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DailyCommissions();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
