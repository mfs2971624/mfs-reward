import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RewardService } from 'app/entities/reward/reward.service';
import { IReward, Reward } from 'app/shared/model/reward.model';

describe('Service Tests', () => {
  describe('Reward Service', () => {
    let injector: TestBed;
    let service: RewardService;
    let httpMock: HttpTestingController;
    let elemDefault: IReward;
    let expectedResult: IReward | IReward[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(RewardService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Reward(0, 'AAAAAAA', 'AAAAAAA', 0, 'AAAAAAA', 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Reward', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Reward()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Reward', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            rewardType: 'BBBBBB',
            taux: 1,
            rewardNotifMessage: 'BBBBBB',
            countTreshold: 1,
            valueTreshold: 1
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Reward', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            rewardType: 'BBBBBB',
            taux: 1,
            rewardNotifMessage: 'BBBBBB',
            countTreshold: 1,
            valueTreshold: 1
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Reward', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
