package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.MfsrewardApp;
import sn.free.mfsreward.domain.DailyCommissions;
import sn.free.mfsreward.repository.DailyCommissionsRepository;
import sn.free.mfsreward.service.DailyCommissionsService;
import sn.free.mfsreward.service.dto.DailyCommissionsDTO;
import sn.free.mfsreward.service.mapper.DailyCommissionsMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DailyCommissionsResource} REST controller.
 */
@SpringBootTest(classes = MfsrewardApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class DailyCommissionsResourceIT {

    private static final String DEFAULT_AGENT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_MSISDN = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_TRANSACTION_ID = "AAAAAAAAAA";
    private static final String UPDATED_LAST_TRANSACTION_ID = "BBBBBBBBBB";

    private static final Double DEFAULT_TOTAL_CASHIN_TRANSACTIONS = 1D;
    private static final Double UPDATED_TOTAL_CASHIN_TRANSACTIONS = 2D;

    private static final Double DEFAULT_TOTAL_CASHOUT_TRANSACTIONS = 1D;
    private static final Double UPDATED_TOTAL_CASHOUT_TRANSACTIONS = 2D;

    private static final Double DEFAULT_TOTAL_INCENTIVE_COMMISSIONS = 1D;
    private static final Double UPDATED_TOTAL_INCENTIVE_COMMISSIONS = 2D;

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_4 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_4 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_5 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_5 = "BBBBBBBBBB";

    @Autowired
    private DailyCommissionsRepository dailyCommissionsRepository;

    @Autowired
    private DailyCommissionsMapper dailyCommissionsMapper;

    @Autowired
    private DailyCommissionsService dailyCommissionsService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDailyCommissionsMockMvc;

    private DailyCommissions dailyCommissions;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DailyCommissions createEntity(EntityManager em) {
        DailyCommissions dailyCommissions = new DailyCommissions()
            .agentMsisdn(DEFAULT_AGENT_MSISDN)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastTransactionId(DEFAULT_LAST_TRANSACTION_ID)
            .totalCashinTransactions(DEFAULT_TOTAL_CASHIN_TRANSACTIONS)
            .totalCashoutTransactions(DEFAULT_TOTAL_CASHOUT_TRANSACTIONS)
            .totalIncentiveCommissions(DEFAULT_TOTAL_INCENTIVE_COMMISSIONS)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3)
            .spare4(DEFAULT_SPARE_4)
            .spare5(DEFAULT_SPARE_5);
        return dailyCommissions;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DailyCommissions createUpdatedEntity(EntityManager em) {
        DailyCommissions dailyCommissions = new DailyCommissions()
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastTransactionId(UPDATED_LAST_TRANSACTION_ID)
            .totalCashinTransactions(UPDATED_TOTAL_CASHIN_TRANSACTIONS)
            .totalCashoutTransactions(UPDATED_TOTAL_CASHOUT_TRANSACTIONS)
            .totalIncentiveCommissions(UPDATED_TOTAL_INCENTIVE_COMMISSIONS)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        return dailyCommissions;
    }

    @BeforeEach
    public void initTest() {
        dailyCommissions = createEntity(em);
    }

    @Test
    @Transactional
    public void createDailyCommissions() throws Exception {
        int databaseSizeBeforeCreate = dailyCommissionsRepository.findAll().size();
        // Create the DailyCommissions
        DailyCommissionsDTO dailyCommissionsDTO = dailyCommissionsMapper.toDto(dailyCommissions);
        restDailyCommissionsMockMvc.perform(post("/api/daily-commissions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dailyCommissionsDTO)))
            .andExpect(status().isCreated());

        // Validate the DailyCommissions in the database
        List<DailyCommissions> dailyCommissionsList = dailyCommissionsRepository.findAll();
        assertThat(dailyCommissionsList).hasSize(databaseSizeBeforeCreate + 1);
        DailyCommissions testDailyCommissions = dailyCommissionsList.get(dailyCommissionsList.size() - 1);
        assertThat(testDailyCommissions.getAgentMsisdn()).isEqualTo(DEFAULT_AGENT_MSISDN);
        assertThat(testDailyCommissions.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testDailyCommissions.getLastTransactionId()).isEqualTo(DEFAULT_LAST_TRANSACTION_ID);
        assertThat(testDailyCommissions.getTotalCashinTransactions()).isEqualTo(DEFAULT_TOTAL_CASHIN_TRANSACTIONS);
        assertThat(testDailyCommissions.getTotalCashoutTransactions()).isEqualTo(DEFAULT_TOTAL_CASHOUT_TRANSACTIONS);
        assertThat(testDailyCommissions.getTotalIncentiveCommissions()).isEqualTo(DEFAULT_TOTAL_INCENTIVE_COMMISSIONS);
        assertThat(testDailyCommissions.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testDailyCommissions.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testDailyCommissions.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
        assertThat(testDailyCommissions.getSpare4()).isEqualTo(DEFAULT_SPARE_4);
        assertThat(testDailyCommissions.getSpare5()).isEqualTo(DEFAULT_SPARE_5);
    }

    @Test
    @Transactional
    public void createDailyCommissionsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dailyCommissionsRepository.findAll().size();

        // Create the DailyCommissions with an existing ID
        dailyCommissions.setId(1L);
        DailyCommissionsDTO dailyCommissionsDTO = dailyCommissionsMapper.toDto(dailyCommissions);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDailyCommissionsMockMvc.perform(post("/api/daily-commissions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dailyCommissionsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DailyCommissions in the database
        List<DailyCommissions> dailyCommissionsList = dailyCommissionsRepository.findAll();
        assertThat(dailyCommissionsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDailyCommissions() throws Exception {
        // Initialize the database
        dailyCommissionsRepository.saveAndFlush(dailyCommissions);

        // Get all the dailyCommissionsList
        restDailyCommissionsMockMvc.perform(get("/api/daily-commissions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dailyCommissions.getId().intValue())))
            .andExpect(jsonPath("$.[*].agentMsisdn").value(hasItem(DEFAULT_AGENT_MSISDN)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastTransactionId").value(hasItem(DEFAULT_LAST_TRANSACTION_ID)))
            .andExpect(jsonPath("$.[*].totalCashinTransactions").value(hasItem(DEFAULT_TOTAL_CASHIN_TRANSACTIONS.doubleValue())))
            .andExpect(jsonPath("$.[*].totalCashoutTransactions").value(hasItem(DEFAULT_TOTAL_CASHOUT_TRANSACTIONS.doubleValue())))
            .andExpect(jsonPath("$.[*].totalIncentiveCommissions").value(hasItem(DEFAULT_TOTAL_INCENTIVE_COMMISSIONS.doubleValue())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)))
            .andExpect(jsonPath("$.[*].spare4").value(hasItem(DEFAULT_SPARE_4)))
            .andExpect(jsonPath("$.[*].spare5").value(hasItem(DEFAULT_SPARE_5)));
    }
    
    @Test
    @Transactional
    public void getDailyCommissions() throws Exception {
        // Initialize the database
        dailyCommissionsRepository.saveAndFlush(dailyCommissions);

        // Get the dailyCommissions
        restDailyCommissionsMockMvc.perform(get("/api/daily-commissions/{id}", dailyCommissions.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dailyCommissions.getId().intValue()))
            .andExpect(jsonPath("$.agentMsisdn").value(DEFAULT_AGENT_MSISDN))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastTransactionId").value(DEFAULT_LAST_TRANSACTION_ID))
            .andExpect(jsonPath("$.totalCashinTransactions").value(DEFAULT_TOTAL_CASHIN_TRANSACTIONS.doubleValue()))
            .andExpect(jsonPath("$.totalCashoutTransactions").value(DEFAULT_TOTAL_CASHOUT_TRANSACTIONS.doubleValue()))
            .andExpect(jsonPath("$.totalIncentiveCommissions").value(DEFAULT_TOTAL_INCENTIVE_COMMISSIONS.doubleValue()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3))
            .andExpect(jsonPath("$.spare4").value(DEFAULT_SPARE_4))
            .andExpect(jsonPath("$.spare5").value(DEFAULT_SPARE_5));
    }
    @Test
    @Transactional
    public void getNonExistingDailyCommissions() throws Exception {
        // Get the dailyCommissions
        restDailyCommissionsMockMvc.perform(get("/api/daily-commissions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDailyCommissions() throws Exception {
        // Initialize the database
        dailyCommissionsRepository.saveAndFlush(dailyCommissions);

        int databaseSizeBeforeUpdate = dailyCommissionsRepository.findAll().size();

        // Update the dailyCommissions
        DailyCommissions updatedDailyCommissions = dailyCommissionsRepository.findById(dailyCommissions.getId()).get();
        // Disconnect from session so that the updates on updatedDailyCommissions are not directly saved in db
        em.detach(updatedDailyCommissions);
        updatedDailyCommissions
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastTransactionId(UPDATED_LAST_TRANSACTION_ID)
            .totalCashinTransactions(UPDATED_TOTAL_CASHIN_TRANSACTIONS)
            .totalCashoutTransactions(UPDATED_TOTAL_CASHOUT_TRANSACTIONS)
            .totalIncentiveCommissions(UPDATED_TOTAL_INCENTIVE_COMMISSIONS)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        DailyCommissionsDTO dailyCommissionsDTO = dailyCommissionsMapper.toDto(updatedDailyCommissions);

        restDailyCommissionsMockMvc.perform(put("/api/daily-commissions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dailyCommissionsDTO)))
            .andExpect(status().isOk());

        // Validate the DailyCommissions in the database
        List<DailyCommissions> dailyCommissionsList = dailyCommissionsRepository.findAll();
        assertThat(dailyCommissionsList).hasSize(databaseSizeBeforeUpdate);
        DailyCommissions testDailyCommissions = dailyCommissionsList.get(dailyCommissionsList.size() - 1);
        assertThat(testDailyCommissions.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testDailyCommissions.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testDailyCommissions.getLastTransactionId()).isEqualTo(UPDATED_LAST_TRANSACTION_ID);
        assertThat(testDailyCommissions.getTotalCashinTransactions()).isEqualTo(UPDATED_TOTAL_CASHIN_TRANSACTIONS);
        assertThat(testDailyCommissions.getTotalCashoutTransactions()).isEqualTo(UPDATED_TOTAL_CASHOUT_TRANSACTIONS);
        assertThat(testDailyCommissions.getTotalIncentiveCommissions()).isEqualTo(UPDATED_TOTAL_INCENTIVE_COMMISSIONS);
        assertThat(testDailyCommissions.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testDailyCommissions.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testDailyCommissions.getSpare3()).isEqualTo(UPDATED_SPARE_3);
        assertThat(testDailyCommissions.getSpare4()).isEqualTo(UPDATED_SPARE_4);
        assertThat(testDailyCommissions.getSpare5()).isEqualTo(UPDATED_SPARE_5);
    }

    @Test
    @Transactional
    public void updateNonExistingDailyCommissions() throws Exception {
        int databaseSizeBeforeUpdate = dailyCommissionsRepository.findAll().size();

        // Create the DailyCommissions
        DailyCommissionsDTO dailyCommissionsDTO = dailyCommissionsMapper.toDto(dailyCommissions);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDailyCommissionsMockMvc.perform(put("/api/daily-commissions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dailyCommissionsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DailyCommissions in the database
        List<DailyCommissions> dailyCommissionsList = dailyCommissionsRepository.findAll();
        assertThat(dailyCommissionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDailyCommissions() throws Exception {
        // Initialize the database
        dailyCommissionsRepository.saveAndFlush(dailyCommissions);

        int databaseSizeBeforeDelete = dailyCommissionsRepository.findAll().size();

        // Delete the dailyCommissions
        restDailyCommissionsMockMvc.perform(delete("/api/daily-commissions/{id}", dailyCommissions.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DailyCommissions> dailyCommissionsList = dailyCommissionsRepository.findAll();
        assertThat(dailyCommissionsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
