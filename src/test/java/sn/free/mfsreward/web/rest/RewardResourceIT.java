package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.MfsrewardApp;
import sn.free.mfsreward.domain.Reward;
import sn.free.mfsreward.repository.RewardRepository;
import sn.free.mfsreward.service.RewardService;
import sn.free.mfsreward.service.dto.RewardDTO;
import sn.free.mfsreward.service.mapper.RewardMapper;
import sn.free.mfsreward.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.free.mfsreward.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link RewardResource} REST controller.
 */
@SpringBootTest(classes = MfsrewardApp.class)
public class RewardResourceIT {
//
//    private static final String DEFAULT_NAME = "AAAAAAAAAA";
//    private static final String UPDATED_NAME = "BBBBBBBBBB";
//
//    private static final String DEFAULT_REWARD_TYPE = "AAAAAAAAAA";
//    private static final String UPDATED_REWARD_TYPE = "BBBBBBBBBB";
//
//    private static final Double DEFAULT_TAUX = 1D;
//    private static final Double UPDATED_TAUX = 2D;
//
//    private static final String DEFAULT_REWARD_NOTIF_MESSAGE = "AAAAAAAAAA";
//    private static final String UPDATED_REWARD_NOTIF_MESSAGE = "BBBBBBBBBB";
//
//    @Autowired
//    private RewardRepository rewardRepository;
//
//    @Autowired
//    private RewardMapper rewardMapper;
//
//    @Autowired
//    private RewardService rewardService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restRewardMockMvc;
//
//    private Reward reward;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final RewardResource rewardResource = new RewardResource(rewardService);
//        this.restRewardMockMvc = MockMvcBuilders.standaloneSetup(rewardResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Reward createEntity(EntityManager em) {
//        Reward reward = new Reward()
//            .name(DEFAULT_NAME)
//            .rewardType(DEFAULT_REWARD_TYPE)
//            .taux(DEFAULT_TAUX)
//            .rewardNotifMessage(DEFAULT_REWARD_NOTIF_MESSAGE);
//        return reward;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Reward createUpdatedEntity(EntityManager em) {
//        Reward reward = new Reward()
//            .name(UPDATED_NAME)
//            .rewardType(UPDATED_REWARD_TYPE)
//            .taux(UPDATED_TAUX)
//            .rewardNotifMessage(UPDATED_REWARD_NOTIF_MESSAGE);
//        return reward;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        reward = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createReward() throws Exception {
//        int databaseSizeBeforeCreate = rewardRepository.findAll().size();
//
//        // Create the Reward
//        RewardDTO rewardDTO = rewardMapper.toDto(reward);
//        restRewardMockMvc.perform(post("/api/rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(rewardDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the Reward in the database
//        List<Reward> rewardList = rewardRepository.findAll();
//        assertThat(rewardList).hasSize(databaseSizeBeforeCreate + 1);
//        Reward testReward = rewardList.get(rewardList.size() - 1);
//        assertThat(testReward.getName()).isEqualTo(DEFAULT_NAME);
//        assertThat(testReward.getRewardType()).isEqualTo(DEFAULT_REWARD_TYPE);
//        assertThat(testReward.getTaux()).isEqualTo(DEFAULT_TAUX);
//        assertThat(testReward.getRewardNotifMessage()).isEqualTo(DEFAULT_REWARD_NOTIF_MESSAGE);
//    }
//
//    @Test
//    @Transactional
//    public void createRewardWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = rewardRepository.findAll().size();
//
//        // Create the Reward with an existing ID
//        reward.setId(1L);
//        RewardDTO rewardDTO = rewardMapper.toDto(reward);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restRewardMockMvc.perform(post("/api/rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(rewardDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the Reward in the database
//        List<Reward> rewardList = rewardRepository.findAll();
//        assertThat(rewardList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkNameIsRequired() throws Exception {
//        int databaseSizeBeforeTest = rewardRepository.findAll().size();
//        // set the field null
//        reward.setName(null);
//
//        // Create the Reward, which fails.
//        RewardDTO rewardDTO = rewardMapper.toDto(reward);
//
//        restRewardMockMvc.perform(post("/api/rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(rewardDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Reward> rewardList = rewardRepository.findAll();
//        assertThat(rewardList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkRewardTypeIsRequired() throws Exception {
//        int databaseSizeBeforeTest = rewardRepository.findAll().size();
//        // set the field null
//        reward.setRewardType(null);
//
//        // Create the Reward, which fails.
//        RewardDTO rewardDTO = rewardMapper.toDto(reward);
//
//        restRewardMockMvc.perform(post("/api/rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(rewardDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Reward> rewardList = rewardRepository.findAll();
//        assertThat(rewardList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkTauxIsRequired() throws Exception {
//        int databaseSizeBeforeTest = rewardRepository.findAll().size();
//        // set the field null
//        reward.setTaux(null);
//
//        // Create the Reward, which fails.
//        RewardDTO rewardDTO = rewardMapper.toDto(reward);
//
//        restRewardMockMvc.perform(post("/api/rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(rewardDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Reward> rewardList = rewardRepository.findAll();
//        assertThat(rewardList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkRewardNotifMessageIsRequired() throws Exception {
//        int databaseSizeBeforeTest = rewardRepository.findAll().size();
//        // set the field null
//        reward.setRewardNotifMessage(null);
//
//        // Create the Reward, which fails.
//        RewardDTO rewardDTO = rewardMapper.toDto(reward);
//
//        restRewardMockMvc.perform(post("/api/rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(rewardDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Reward> rewardList = rewardRepository.findAll();
//        assertThat(rewardList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllRewards() throws Exception {
//        // Initialize the database
//        rewardRepository.saveAndFlush(reward);
//
//        // Get all the rewardList
//        restRewardMockMvc.perform(get("/api/rewards?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(reward.getId().intValue())))
//            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
//            .andExpect(jsonPath("$.[*].rewardType").value(hasItem(DEFAULT_REWARD_TYPE.toString())))
//            .andExpect(jsonPath("$.[*].taux").value(hasItem(DEFAULT_TAUX.doubleValue())))
//            .andExpect(jsonPath("$.[*].rewardNotifMessage").value(hasItem(DEFAULT_REWARD_NOTIF_MESSAGE.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getReward() throws Exception {
//        // Initialize the database
//        rewardRepository.saveAndFlush(reward);
//
//        // Get the reward
//        restRewardMockMvc.perform(get("/api/rewards/{id}", reward.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(reward.getId().intValue()))
//            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
//            .andExpect(jsonPath("$.rewardType").value(DEFAULT_REWARD_TYPE.toString()))
//            .andExpect(jsonPath("$.taux").value(DEFAULT_TAUX.doubleValue()))
//            .andExpect(jsonPath("$.rewardNotifMessage").value(DEFAULT_REWARD_NOTIF_MESSAGE.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingReward() throws Exception {
//        // Get the reward
//        restRewardMockMvc.perform(get("/api/rewards/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateReward() throws Exception {
//        // Initialize the database
//        rewardRepository.saveAndFlush(reward);
//
//        int databaseSizeBeforeUpdate = rewardRepository.findAll().size();
//
//        // Update the reward
//        Reward updatedReward = rewardRepository.findById(reward.getId()).get();
//        // Disconnect from session so that the updates on updatedReward are not directly saved in db
//        em.detach(updatedReward);
//        updatedReward
//            .name(UPDATED_NAME)
//            .rewardType(UPDATED_REWARD_TYPE)
//            .taux(UPDATED_TAUX)
//            .rewardNotifMessage(UPDATED_REWARD_NOTIF_MESSAGE);
//        RewardDTO rewardDTO = rewardMapper.toDto(updatedReward);
//
//        restRewardMockMvc.perform(put("/api/rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(rewardDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the Reward in the database
//        List<Reward> rewardList = rewardRepository.findAll();
//        assertThat(rewardList).hasSize(databaseSizeBeforeUpdate);
//        Reward testReward = rewardList.get(rewardList.size() - 1);
//        assertThat(testReward.getName()).isEqualTo(UPDATED_NAME);
//        assertThat(testReward.getRewardType()).isEqualTo(UPDATED_REWARD_TYPE);
//        assertThat(testReward.getTaux()).isEqualTo(UPDATED_TAUX);
//        assertThat(testReward.getRewardNotifMessage()).isEqualTo(UPDATED_REWARD_NOTIF_MESSAGE);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingReward() throws Exception {
//        int databaseSizeBeforeUpdate = rewardRepository.findAll().size();
//
//        // Create the Reward
//        RewardDTO rewardDTO = rewardMapper.toDto(reward);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restRewardMockMvc.perform(put("/api/rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(rewardDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the Reward in the database
//        List<Reward> rewardList = rewardRepository.findAll();
//        assertThat(rewardList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteReward() throws Exception {
//        // Initialize the database
//        rewardRepository.saveAndFlush(reward);
//
//        int databaseSizeBeforeDelete = rewardRepository.findAll().size();
//
//        // Delete the reward
//        restRewardMockMvc.perform(delete("/api/rewards/{id}", reward.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<Reward> rewardList = rewardRepository.findAll();
//        assertThat(rewardList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(Reward.class);
//        Reward reward1 = new Reward();
//        reward1.setId(1L);
//        Reward reward2 = new Reward();
//        reward2.setId(reward1.getId());
//        assertThat(reward1).isEqualTo(reward2);
//        reward2.setId(2L);
//        assertThat(reward1).isNotEqualTo(reward2);
//        reward1.setId(null);
//        assertThat(reward1).isNotEqualTo(reward2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(RewardDTO.class);
//        RewardDTO rewardDTO1 = new RewardDTO();
//        rewardDTO1.setId(1L);
//        RewardDTO rewardDTO2 = new RewardDTO();
//        assertThat(rewardDTO1).isNotEqualTo(rewardDTO2);
//        rewardDTO2.setId(rewardDTO1.getId());
//        assertThat(rewardDTO1).isEqualTo(rewardDTO2);
//        rewardDTO2.setId(2L);
//        assertThat(rewardDTO1).isNotEqualTo(rewardDTO2);
//        rewardDTO1.setId(null);
//        assertThat(rewardDTO1).isNotEqualTo(rewardDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(rewardMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(rewardMapper.fromId(null)).isNull();
//    }
}
