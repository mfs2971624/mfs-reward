package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.MfsrewardApp;
import sn.free.mfsreward.domain.PromoExecutionPlan;
import sn.free.mfsreward.repository.PromoExecutionPlanRepository;
import sn.free.mfsreward.service.PromoExecutionPlanService;
import sn.free.mfsreward.service.dto.PromoExecutionPlanDTO;
import sn.free.mfsreward.service.mapper.PromoExecutionPlanMapper;
import sn.free.mfsreward.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static sn.free.mfsreward.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link PromoExecutionPlanResource} REST controller.
 */
@SpringBootTest(classes = MfsrewardApp.class)
public class PromoExecutionPlanResourceIT {
//
//    private static final Instant DEFAULT_DATE_DEBUT = Instant.ofEpochMilli(0L);
//    private static final Instant UPDATED_DATE_DEBUT = Instant.now().truncatedTo(ChronoUnit.MILLIS);
//
//    private static final Instant DEFAULT_DATE_FIN = Instant.ofEpochMilli(0L);
//    private static final Instant UPDATED_DATE_FIN = Instant.now().truncatedTo(ChronoUnit.MILLIS);
//
//    @Autowired
//    private PromoExecutionPlanRepository promoExecutionPlanRepository;
//
//    @Autowired
//    private PromoExecutionPlanMapper promoExecutionPlanMapper;
//
//    @Autowired
//    private PromoExecutionPlanService promoExecutionPlanService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restPromoExecutionPlanMockMvc;
//
//    private PromoExecutionPlan promoExecutionPlan;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final PromoExecutionPlanResource promoExecutionPlanResource = new PromoExecutionPlanResource(promoExecutionPlanService);
//        this.restPromoExecutionPlanMockMvc = MockMvcBuilders.standaloneSetup(promoExecutionPlanResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static PromoExecutionPlan createEntity(EntityManager em) {
//        PromoExecutionPlan promoExecutionPlan = new PromoExecutionPlan()
//            .dateDebut(DEFAULT_DATE_DEBUT)
//            .dateFin(DEFAULT_DATE_FIN);
//        return promoExecutionPlan;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static PromoExecutionPlan createUpdatedEntity(EntityManager em) {
//        PromoExecutionPlan promoExecutionPlan = new PromoExecutionPlan()
//            .dateDebut(UPDATED_DATE_DEBUT)
//            .dateFin(UPDATED_DATE_FIN);
//        return promoExecutionPlan;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        promoExecutionPlan = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createPromoExecutionPlan() throws Exception {
//        int databaseSizeBeforeCreate = promoExecutionPlanRepository.findAll().size();
//
//        // Create the PromoExecutionPlan
//        PromoExecutionPlanDTO promoExecutionPlanDTO = promoExecutionPlanMapper.toDto(promoExecutionPlan);
//        restPromoExecutionPlanMockMvc.perform(post("/api/promo-execution-plans")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(promoExecutionPlanDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the PromoExecutionPlan in the database
//        List<PromoExecutionPlan> promoExecutionPlanList = promoExecutionPlanRepository.findAll();
//        assertThat(promoExecutionPlanList).hasSize(databaseSizeBeforeCreate + 1);
//        PromoExecutionPlan testPromoExecutionPlan = promoExecutionPlanList.get(promoExecutionPlanList.size() - 1);
//        assertThat(testPromoExecutionPlan.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
//        assertThat(testPromoExecutionPlan.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
//    }
//
//    @Test
//    @Transactional
//    public void createPromoExecutionPlanWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = promoExecutionPlanRepository.findAll().size();
//
//        // Create the PromoExecutionPlan with an existing ID
//        promoExecutionPlan.setId(1L);
//        PromoExecutionPlanDTO promoExecutionPlanDTO = promoExecutionPlanMapper.toDto(promoExecutionPlan);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restPromoExecutionPlanMockMvc.perform(post("/api/promo-execution-plans")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(promoExecutionPlanDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the PromoExecutionPlan in the database
//        List<PromoExecutionPlan> promoExecutionPlanList = promoExecutionPlanRepository.findAll();
//        assertThat(promoExecutionPlanList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void getAllPromoExecutionPlans() throws Exception {
//        // Initialize the database
//        promoExecutionPlanRepository.saveAndFlush(promoExecutionPlan);
//
//        // Get all the promoExecutionPlanList
//        restPromoExecutionPlanMockMvc.perform(get("/api/promo-execution-plans?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(promoExecutionPlan.getId().intValue())))
//            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(DEFAULT_DATE_DEBUT.toString())))
//            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getPromoExecutionPlan() throws Exception {
//        // Initialize the database
//        promoExecutionPlanRepository.saveAndFlush(promoExecutionPlan);
//
//        // Get the promoExecutionPlan
//        restPromoExecutionPlanMockMvc.perform(get("/api/promo-execution-plans/{id}", promoExecutionPlan.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(promoExecutionPlan.getId().intValue()))
//            .andExpect(jsonPath("$.dateDebut").value(DEFAULT_DATE_DEBUT.toString()))
//            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingPromoExecutionPlan() throws Exception {
//        // Get the promoExecutionPlan
//        restPromoExecutionPlanMockMvc.perform(get("/api/promo-execution-plans/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updatePromoExecutionPlan() throws Exception {
//        // Initialize the database
//        promoExecutionPlanRepository.saveAndFlush(promoExecutionPlan);
//
//        int databaseSizeBeforeUpdate = promoExecutionPlanRepository.findAll().size();
//
//        // Update the promoExecutionPlan
//        PromoExecutionPlan updatedPromoExecutionPlan = promoExecutionPlanRepository.findById(promoExecutionPlan.getId()).get();
//        // Disconnect from session so that the updates on updatedPromoExecutionPlan are not directly saved in db
//        em.detach(updatedPromoExecutionPlan);
//        updatedPromoExecutionPlan
//            .dateDebut(UPDATED_DATE_DEBUT)
//            .dateFin(UPDATED_DATE_FIN);
//        PromoExecutionPlanDTO promoExecutionPlanDTO = promoExecutionPlanMapper.toDto(updatedPromoExecutionPlan);
//
//        restPromoExecutionPlanMockMvc.perform(put("/api/promo-execution-plans")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(promoExecutionPlanDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the PromoExecutionPlan in the database
//        List<PromoExecutionPlan> promoExecutionPlanList = promoExecutionPlanRepository.findAll();
//        assertThat(promoExecutionPlanList).hasSize(databaseSizeBeforeUpdate);
//        PromoExecutionPlan testPromoExecutionPlan = promoExecutionPlanList.get(promoExecutionPlanList.size() - 1);
//        assertThat(testPromoExecutionPlan.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
//        assertThat(testPromoExecutionPlan.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingPromoExecutionPlan() throws Exception {
//        int databaseSizeBeforeUpdate = promoExecutionPlanRepository.findAll().size();
//
//        // Create the PromoExecutionPlan
//        PromoExecutionPlanDTO promoExecutionPlanDTO = promoExecutionPlanMapper.toDto(promoExecutionPlan);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restPromoExecutionPlanMockMvc.perform(put("/api/promo-execution-plans")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(promoExecutionPlanDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the PromoExecutionPlan in the database
//        List<PromoExecutionPlan> promoExecutionPlanList = promoExecutionPlanRepository.findAll();
//        assertThat(promoExecutionPlanList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deletePromoExecutionPlan() throws Exception {
//        // Initialize the database
//        promoExecutionPlanRepository.saveAndFlush(promoExecutionPlan);
//
//        int databaseSizeBeforeDelete = promoExecutionPlanRepository.findAll().size();
//
//        // Delete the promoExecutionPlan
//        restPromoExecutionPlanMockMvc.perform(delete("/api/promo-execution-plans/{id}", promoExecutionPlan.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<PromoExecutionPlan> promoExecutionPlanList = promoExecutionPlanRepository.findAll();
//        assertThat(promoExecutionPlanList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(PromoExecutionPlan.class);
//        PromoExecutionPlan promoExecutionPlan1 = new PromoExecutionPlan();
//        promoExecutionPlan1.setId(1L);
//        PromoExecutionPlan promoExecutionPlan2 = new PromoExecutionPlan();
//        promoExecutionPlan2.setId(promoExecutionPlan1.getId());
//        assertThat(promoExecutionPlan1).isEqualTo(promoExecutionPlan2);
//        promoExecutionPlan2.setId(2L);
//        assertThat(promoExecutionPlan1).isNotEqualTo(promoExecutionPlan2);
//        promoExecutionPlan1.setId(null);
//        assertThat(promoExecutionPlan1).isNotEqualTo(promoExecutionPlan2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(PromoExecutionPlanDTO.class);
//        PromoExecutionPlanDTO promoExecutionPlanDTO1 = new PromoExecutionPlanDTO();
//        promoExecutionPlanDTO1.setId(1L);
//        PromoExecutionPlanDTO promoExecutionPlanDTO2 = new PromoExecutionPlanDTO();
//        assertThat(promoExecutionPlanDTO1).isNotEqualTo(promoExecutionPlanDTO2);
//        promoExecutionPlanDTO2.setId(promoExecutionPlanDTO1.getId());
//        assertThat(promoExecutionPlanDTO1).isEqualTo(promoExecutionPlanDTO2);
//        promoExecutionPlanDTO2.setId(2L);
//        assertThat(promoExecutionPlanDTO1).isNotEqualTo(promoExecutionPlanDTO2);
//        promoExecutionPlanDTO1.setId(null);
//        assertThat(promoExecutionPlanDTO1).isNotEqualTo(promoExecutionPlanDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(promoExecutionPlanMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(promoExecutionPlanMapper.fromId(null)).isNull();
//    }
}
