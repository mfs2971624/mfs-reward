package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.MfsrewardApp;
import sn.free.mfsreward.domain.Promotion;
import sn.free.mfsreward.repository.PromotionRepository;
import sn.free.mfsreward.service.PromotionService;
import sn.free.mfsreward.service.dto.PromotionDTO;
import sn.free.mfsreward.service.mapper.PromotionMapper;
import sn.free.mfsreward.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.free.mfsreward.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.free.mfsreward.domain.enumeration.TypeFrequence;
import sn.free.mfsreward.domain.enumeration.Statut;
/**
 * Integration tests for the {@Link PromotionResource} REST controller.
 */
@SpringBootTest(classes = MfsrewardApp.class)
public class PromotionResourceIT {

//    private static final String DEFAULT_NOM = "AAAAAAAAAA";
//    private static final String UPDATED_NOM = "BBBBBBBBBB";
//
//    private static final String DEFAULT_CRITERE_ELIGILIBITE = "AAAAAAAAAA";
//    private static final String UPDATED_CRITERE_ELIGILIBITE = "BBBBBBBBBB";
//
//    private static final TypeFrequence DEFAULT_FREQUENCE = TypeFrequence.Daily;
//    private static final TypeFrequence UPDATED_FREQUENCE = TypeFrequence.Weekly;
//
//    private static final Integer DEFAULT_FREQUENCE_DETAILS = 1;
//    private static final Integer UPDATED_FREQUENCE_DETAILS = 2;
//
//    private static final Statut DEFAULT_STATUT = Statut.ACTIVE;
//    private static final Statut UPDATED_STATUT = Statut.DISABLED;
//
//    @Autowired
//    private PromotionRepository promotionRepository;
//
//    @Autowired
//    private PromotionMapper promotionMapper;
//
//    @Autowired
//    private PromotionService promotionService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restPromotionMockMvc;
//
//    private Promotion promotion;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final PromotionResource promotionResource = new PromotionResource(promotionService);
//        this.restPromotionMockMvc = MockMvcBuilders.standaloneSetup(promotionResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Promotion createEntity(EntityManager em) {
//        Promotion promotion = new Promotion()
//            .nom(DEFAULT_NOM)
//            .critereEligilibite(DEFAULT_CRITERE_ELIGILIBITE)
//            .frequence(DEFAULT_FREQUENCE)
//            .frequenceDetails(DEFAULT_FREQUENCE_DETAILS)
//            .statut(DEFAULT_STATUT);
//        return promotion;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Promotion createUpdatedEntity(EntityManager em) {
//        Promotion promotion = new Promotion()
//            .nom(UPDATED_NOM)
//            .critereEligilibite(UPDATED_CRITERE_ELIGILIBITE)
//            .frequence(UPDATED_FREQUENCE)
//            .frequenceDetails(UPDATED_FREQUENCE_DETAILS)
//            .statut(UPDATED_STATUT);
//        return promotion;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        promotion = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createPromotion() throws Exception {
//        int databaseSizeBeforeCreate = promotionRepository.findAll().size();
//
//        // Create the Promotion
//        PromotionDTO promotionDTO = promotionMapper.toDto(promotion);
//        restPromotionMockMvc.perform(post("/api/promotions")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(promotionDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the Promotion in the database
//        List<Promotion> promotionList = promotionRepository.findAll();
//        assertThat(promotionList).hasSize(databaseSizeBeforeCreate + 1);
//        Promotion testPromotion = promotionList.get(promotionList.size() - 1);
//        assertThat(testPromotion.getNom()).isEqualTo(DEFAULT_NOM);
//        assertThat(testPromotion.getCritereEligilibite()).isEqualTo(DEFAULT_CRITERE_ELIGILIBITE);
//        assertThat(testPromotion.getFrequence()).isEqualTo(DEFAULT_FREQUENCE);
//        assertThat(testPromotion.getFrequenceDetails()).isEqualTo(DEFAULT_FREQUENCE_DETAILS);
//        assertThat(testPromotion.getStatut()).isEqualTo(DEFAULT_STATUT);
//    }
//
//    @Test
//    @Transactional
//    public void createPromotionWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = promotionRepository.findAll().size();
//
//        // Create the Promotion with an existing ID
//        promotion.setId(1L);
//        PromotionDTO promotionDTO = promotionMapper.toDto(promotion);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restPromotionMockMvc.perform(post("/api/promotions")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(promotionDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the Promotion in the database
//        List<Promotion> promotionList = promotionRepository.findAll();
//        assertThat(promotionList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkNomIsRequired() throws Exception {
//        int databaseSizeBeforeTest = promotionRepository.findAll().size();
//        // set the field null
//        promotion.setNom(null);
//
//        // Create the Promotion, which fails.
//        PromotionDTO promotionDTO = promotionMapper.toDto(promotion);
//
//        restPromotionMockMvc.perform(post("/api/promotions")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(promotionDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Promotion> promotionList = promotionRepository.findAll();
//        assertThat(promotionList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkCritereEligilibiteIsRequired() throws Exception {
//        int databaseSizeBeforeTest = promotionRepository.findAll().size();
//        // set the field null
//        promotion.setCritereEligilibite(null);
//
//        // Create the Promotion, which fails.
//        PromotionDTO promotionDTO = promotionMapper.toDto(promotion);
//
//        restPromotionMockMvc.perform(post("/api/promotions")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(promotionDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Promotion> promotionList = promotionRepository.findAll();
//        assertThat(promotionList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkFrequenceIsRequired() throws Exception {
//        int databaseSizeBeforeTest = promotionRepository.findAll().size();
//        // set the field null
//        promotion.setFrequence(null);
//
//        // Create the Promotion, which fails.
//        PromotionDTO promotionDTO = promotionMapper.toDto(promotion);
//
//        restPromotionMockMvc.perform(post("/api/promotions")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(promotionDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<Promotion> promotionList = promotionRepository.findAll();
//        assertThat(promotionList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllPromotions() throws Exception {
//        // Initialize the database
//        promotionRepository.saveAndFlush(promotion);
//
//        // Get all the promotionList
//        restPromotionMockMvc.perform(get("/api/promotions?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(promotion.getId().intValue())))
//            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
//            .andExpect(jsonPath("$.[*].critereEligilibite").value(hasItem(DEFAULT_CRITERE_ELIGILIBITE.toString())))
//            .andExpect(jsonPath("$.[*].frequence").value(hasItem(DEFAULT_FREQUENCE.toString())))
//            .andExpect(jsonPath("$.[*].frequenceDetails").value(hasItem(DEFAULT_FREQUENCE_DETAILS)))
//            .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getPromotion() throws Exception {
//        // Initialize the database
//        promotionRepository.saveAndFlush(promotion);
//
//        // Get the promotion
//        restPromotionMockMvc.perform(get("/api/promotions/{id}", promotion.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(promotion.getId().intValue()))
//            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()))
//            .andExpect(jsonPath("$.critereEligilibite").value(DEFAULT_CRITERE_ELIGILIBITE.toString()))
//            .andExpect(jsonPath("$.frequence").value(DEFAULT_FREQUENCE.toString()))
//            .andExpect(jsonPath("$.frequenceDetails").value(DEFAULT_FREQUENCE_DETAILS))
//            .andExpect(jsonPath("$.statut").value(DEFAULT_STATUT.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingPromotion() throws Exception {
//        // Get the promotion
//        restPromotionMockMvc.perform(get("/api/promotions/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updatePromotion() throws Exception {
//        // Initialize the database
//        promotionRepository.saveAndFlush(promotion);
//
//        int databaseSizeBeforeUpdate = promotionRepository.findAll().size();
//
//        // Update the promotion
//        Promotion updatedPromotion = promotionRepository.findById(promotion.getId()).get();
//        // Disconnect from session so that the updates on updatedPromotion are not directly saved in db
//        em.detach(updatedPromotion);
//        updatedPromotion
//            .nom(UPDATED_NOM)
//            .critereEligilibite(UPDATED_CRITERE_ELIGILIBITE)
//            .frequence(UPDATED_FREQUENCE)
//            .frequenceDetails(UPDATED_FREQUENCE_DETAILS)
//            .statut(UPDATED_STATUT);
//        PromotionDTO promotionDTO = promotionMapper.toDto(updatedPromotion);
//
//        restPromotionMockMvc.perform(put("/api/promotions")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(promotionDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the Promotion in the database
//        List<Promotion> promotionList = promotionRepository.findAll();
//        assertThat(promotionList).hasSize(databaseSizeBeforeUpdate);
//        Promotion testPromotion = promotionList.get(promotionList.size() - 1);
//        assertThat(testPromotion.getNom()).isEqualTo(UPDATED_NOM);
//        assertThat(testPromotion.getCritereEligilibite()).isEqualTo(UPDATED_CRITERE_ELIGILIBITE);
//        assertThat(testPromotion.getFrequence()).isEqualTo(UPDATED_FREQUENCE);
//        assertThat(testPromotion.getFrequenceDetails()).isEqualTo(UPDATED_FREQUENCE_DETAILS);
//        assertThat(testPromotion.getStatut()).isEqualTo(UPDATED_STATUT);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingPromotion() throws Exception {
//        int databaseSizeBeforeUpdate = promotionRepository.findAll().size();
//
//        // Create the Promotion
//        PromotionDTO promotionDTO = promotionMapper.toDto(promotion);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restPromotionMockMvc.perform(put("/api/promotions")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(promotionDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the Promotion in the database
//        List<Promotion> promotionList = promotionRepository.findAll();
//        assertThat(promotionList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deletePromotion() throws Exception {
//        // Initialize the database
//        promotionRepository.saveAndFlush(promotion);
//
//        int databaseSizeBeforeDelete = promotionRepository.findAll().size();
//
//        // Delete the promotion
//        restPromotionMockMvc.perform(delete("/api/promotions/{id}", promotion.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<Promotion> promotionList = promotionRepository.findAll();
//        assertThat(promotionList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(Promotion.class);
//        Promotion promotion1 = new Promotion();
//        promotion1.setId(1L);
//        Promotion promotion2 = new Promotion();
//        promotion2.setId(promotion1.getId());
//        assertThat(promotion1).isEqualTo(promotion2);
//        promotion2.setId(2L);
//        assertThat(promotion1).isNotEqualTo(promotion2);
//        promotion1.setId(null);
//        assertThat(promotion1).isNotEqualTo(promotion2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(PromotionDTO.class);
//        PromotionDTO promotionDTO1 = new PromotionDTO();
//        promotionDTO1.setId(1L);
//        PromotionDTO promotionDTO2 = new PromotionDTO();
//        assertThat(promotionDTO1).isNotEqualTo(promotionDTO2);
//        promotionDTO2.setId(promotionDTO1.getId());
//        assertThat(promotionDTO1).isEqualTo(promotionDTO2);
//        promotionDTO2.setId(2L);
//        assertThat(promotionDTO1).isNotEqualTo(promotionDTO2);
//        promotionDTO1.setId(null);
//        assertThat(promotionDTO1).isNotEqualTo(promotionDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(promotionMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(promotionMapper.fromId(null)).isNull();
//    }
}
