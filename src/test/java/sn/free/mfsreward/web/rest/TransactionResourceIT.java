package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.MfsrewardApp;
import sn.free.mfsreward.domain.Transaction;
import sn.free.mfsreward.repository.TransactionRepository;
import sn.free.mfsreward.service.TransactionService;
import sn.free.mfsreward.service.dto.TransactionDTO;
import sn.free.mfsreward.service.mapper.TransactionMapper;
import sn.free.mfsreward.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.free.mfsreward.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.free.mfsreward.domain.enumeration.StatutTraitementTransaction;
/**
 * Integration tests for the {@Link TransactionResource} REST controller.
 */
@SpringBootTest(classes = MfsrewardApp.class)
public class TransactionResourceIT {

    /*private static final String DEFAULT_REQUEST_ID = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_ID = "BBBBBBBBBB";

    private static final String DEFAULT_TRX_ID_MOBIQUITY = "AAAAAAAAAA";
    private static final String UPDATED_TRX_ID_MOBIQUITY = "BBBBBBBBBB";

    private static final String DEFAULT_TRX_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TRX_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_TRX_PARTNER_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TRX_PARTNER_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_TRX_PARTNER_ID = "AAAAAAAAAA";
    private static final String UPDATED_TRX_PARTNER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_TRX_CUSTOMER_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_TRX_CUSTOMER_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_TRX_CUSTOMER_ACCOUNT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_TRX_CUSTOMER_ACCOUNT_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_TRX_AMOUNT = "AAAAAAAAAA";
    private static final String UPDATED_TRX_AMOUNT = "BBBBBBBBBB";

    private static final String DEFAULT_TRX_CHANNEL = "AAAAAAAAAA";
    private static final String UPDATED_TRX_CHANNEL = "BBBBBBBBBB";

    private static final String DEFAULT_TRX_DATE = "AAAAAAAAAA";
    private static final String UPDATED_TRX_DATE = "BBBBBBBBBB";

    private static final String DEFAULT_ELIGIBLE_PROMOS = "AAAAAAAAAA";
    private static final String UPDATED_ELIGIBLE_PROMOS = "BBBBBBBBBB";

    private static final StatutTraitementTransaction DEFAULT_TRAITEMENT_STATUS = StatutTraitementTransaction.INITIATED;
    private static final StatutTraitementTransaction UPDATED_TRAITEMENT_STATUS = StatutTraitementTransaction.PROCESSED;

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionMapper transactionMapper;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTransactionMockMvc;

    private Transaction transaction;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TransactionResource transactionResource = new TransactionResource(transactionService);
        this.restTransactionMockMvc = MockMvcBuilders.standaloneSetup(transactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    *//**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     *//*
    public static Transaction createEntity(EntityManager em) {
        Transaction transaction = new Transaction()
            .requestId(DEFAULT_REQUEST_ID)
            .trxIdMobiquity(DEFAULT_TRX_ID_MOBIQUITY)
            .trxType(DEFAULT_TRX_TYPE)
            .trxPartnerType(DEFAULT_TRX_PARTNER_TYPE)
            .trxPartnerId(DEFAULT_TRX_PARTNER_ID)
            .trxCustomerMSISDN(DEFAULT_TRX_CUSTOMER_MSISDN)
            .trxCustomerAccountNumber(DEFAULT_TRX_CUSTOMER_ACCOUNT_NUMBER)
            .trxAmount(DEFAULT_TRX_AMOUNT)
            .trxChannel(DEFAULT_TRX_CHANNEL)
            .trxDate(DEFAULT_TRX_DATE)
            .eligiblePromos(DEFAULT_ELIGIBLE_PROMOS)
            .traitementStatus(DEFAULT_TRAITEMENT_STATUS)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2);
        return transaction;
    }
    *//**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     *//*
    public static Transaction createUpdatedEntity(EntityManager em) {
        Transaction transaction = new Transaction()
            .requestId(UPDATED_REQUEST_ID)
            .trxIdMobiquity(UPDATED_TRX_ID_MOBIQUITY)
            .trxType(UPDATED_TRX_TYPE)
            .trxPartnerType(UPDATED_TRX_PARTNER_TYPE)
            .trxPartnerId(UPDATED_TRX_PARTNER_ID)
            .trxCustomerMSISDN(UPDATED_TRX_CUSTOMER_MSISDN)
            .trxCustomerAccountNumber(UPDATED_TRX_CUSTOMER_ACCOUNT_NUMBER)
            .trxAmount(UPDATED_TRX_AMOUNT)
            .trxChannel(UPDATED_TRX_CHANNEL)
            .trxDate(UPDATED_TRX_DATE)
            .eligiblePromos(UPDATED_ELIGIBLE_PROMOS)
            .traitementStatus(UPDATED_TRAITEMENT_STATUS)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2);
        return transaction;
    }

    @BeforeEach
    public void initTest() {
        transaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createTransaction() throws Exception {
        int databaseSizeBeforeCreate = transactionRepository.findAll().size();

        // Create the Transaction
        TransactionDTO transactionDTO = transactionMapper.toDto(transaction);
        restTransactionMockMvc.perform(post("/api/transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionDTO)))
            .andExpect(status().isCreated());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeCreate + 1);
        Transaction testTransaction = transactionList.get(transactionList.size() - 1);
        assertThat(testTransaction.getRequestId()).isEqualTo(DEFAULT_REQUEST_ID);
        assertThat(testTransaction.getTrxIdMobiquity()).isEqualTo(DEFAULT_TRX_ID_MOBIQUITY);
        assertThat(testTransaction.getTrxType()).isEqualTo(DEFAULT_TRX_TYPE);
        assertThat(testTransaction.getTrxPartnerType()).isEqualTo(DEFAULT_TRX_PARTNER_TYPE);
        assertThat(testTransaction.getTrxPartnerId()).isEqualTo(DEFAULT_TRX_PARTNER_ID);
        assertThat(testTransaction.getTrxCustomerMSISDN()).isEqualTo(DEFAULT_TRX_CUSTOMER_MSISDN);
        assertThat(testTransaction.getTrxCustomerAccountNumber()).isEqualTo(DEFAULT_TRX_CUSTOMER_ACCOUNT_NUMBER);
        assertThat(testTransaction.getTrxAmount()).isEqualTo(DEFAULT_TRX_AMOUNT);
        assertThat(testTransaction.getTrxChannel()).isEqualTo(DEFAULT_TRX_CHANNEL);
        assertThat(testTransaction.getTrxDate()).isEqualTo(DEFAULT_TRX_DATE);
        assertThat(testTransaction.getEligiblePromos()).isEqualTo(DEFAULT_ELIGIBLE_PROMOS);
        assertThat(testTransaction.getTraitementStatus()).isEqualTo(DEFAULT_TRAITEMENT_STATUS);
        assertThat(testTransaction.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testTransaction.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
    }

    @Test
    @Transactional
    public void createTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = transactionRepository.findAll().size();

        // Create the Transaction with an existing ID
        transaction.setId(1L);
        TransactionDTO transactionDTO = transactionMapper.toDto(transaction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransactionMockMvc.perform(post("/api/transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTransactions() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList
        restTransactionMockMvc.perform(get("/api/transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].requestId").value(hasItem(DEFAULT_REQUEST_ID.toString())))
            .andExpect(jsonPath("$.[*].trxIdMobiquity").value(hasItem(DEFAULT_TRX_ID_MOBIQUITY.toString())))
            .andExpect(jsonPath("$.[*].trxType").value(hasItem(DEFAULT_TRX_TYPE.toString())))
            .andExpect(jsonPath("$.[*].trxPartnerType").value(hasItem(DEFAULT_TRX_PARTNER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].trxPartnerId").value(hasItem(DEFAULT_TRX_PARTNER_ID.toString())))
            .andExpect(jsonPath("$.[*].trxCustomerMSISDN").value(hasItem(DEFAULT_TRX_CUSTOMER_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].trxCustomerAccountNumber").value(hasItem(DEFAULT_TRX_CUSTOMER_ACCOUNT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].trxAmount").value(hasItem(DEFAULT_TRX_AMOUNT.toString())))
            .andExpect(jsonPath("$.[*].trxChannel").value(hasItem(DEFAULT_TRX_CHANNEL.toString())))
            .andExpect(jsonPath("$.[*].trxDate").value(hasItem(DEFAULT_TRX_DATE.toString())))
            .andExpect(jsonPath("$.[*].eligiblePromos").value(hasItem(DEFAULT_ELIGIBLE_PROMOS.toString())))
            .andExpect(jsonPath("$.[*].traitementStatus").value(hasItem(DEFAULT_TRAITEMENT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1.toString())))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2.toString())));
    }

    @Test
    @Transactional
    public void getTransaction() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get the transaction
        restTransactionMockMvc.perform(get("/api/transactions/{id}", transaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(transaction.getId().intValue()))
            .andExpect(jsonPath("$.requestId").value(DEFAULT_REQUEST_ID.toString()))
            .andExpect(jsonPath("$.trxIdMobiquity").value(DEFAULT_TRX_ID_MOBIQUITY.toString()))
            .andExpect(jsonPath("$.trxType").value(DEFAULT_TRX_TYPE.toString()))
            .andExpect(jsonPath("$.trxPartnerType").value(DEFAULT_TRX_PARTNER_TYPE.toString()))
            .andExpect(jsonPath("$.trxPartnerId").value(DEFAULT_TRX_PARTNER_ID.toString()))
            .andExpect(jsonPath("$.trxCustomerMSISDN").value(DEFAULT_TRX_CUSTOMER_MSISDN.toString()))
            .andExpect(jsonPath("$.trxCustomerAccountNumber").value(DEFAULT_TRX_CUSTOMER_ACCOUNT_NUMBER.toString()))
            .andExpect(jsonPath("$.trxAmount").value(DEFAULT_TRX_AMOUNT.toString()))
            .andExpect(jsonPath("$.trxChannel").value(DEFAULT_TRX_CHANNEL.toString()))
            .andExpect(jsonPath("$.trxDate").value(DEFAULT_TRX_DATE.toString()))
            .andExpect(jsonPath("$.eligiblePromos").value(DEFAULT_ELIGIBLE_PROMOS.toString()))
            .andExpect(jsonPath("$.traitementStatus").value(DEFAULT_TRAITEMENT_STATUS.toString()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1.toString()))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTransaction() throws Exception {
        // Get the transaction
        restTransactionMockMvc.perform(get("/api/transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTransaction() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        int databaseSizeBeforeUpdate = transactionRepository.findAll().size();

        // Update the transaction
        Transaction updatedTransaction = transactionRepository.findById(transaction.getId()).get();
        // Disconnect from session so that the updates on updatedTransaction are not directly saved in db
        em.detach(updatedTransaction);
        updatedTransaction
            .requestId(UPDATED_REQUEST_ID)
            .trxIdMobiquity(UPDATED_TRX_ID_MOBIQUITY)
            .trxType(UPDATED_TRX_TYPE)
            .trxPartnerType(UPDATED_TRX_PARTNER_TYPE)
            .trxPartnerId(UPDATED_TRX_PARTNER_ID)
            .trxCustomerMSISDN(UPDATED_TRX_CUSTOMER_MSISDN)
            .trxCustomerAccountNumber(UPDATED_TRX_CUSTOMER_ACCOUNT_NUMBER)
            .trxAmount(UPDATED_TRX_AMOUNT)
            .trxChannel(UPDATED_TRX_CHANNEL)
            .trxDate(UPDATED_TRX_DATE)
            .eligiblePromos(UPDATED_ELIGIBLE_PROMOS)
            .traitementStatus(UPDATED_TRAITEMENT_STATUS)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2);
        TransactionDTO transactionDTO = transactionMapper.toDto(updatedTransaction);

        restTransactionMockMvc.perform(put("/api/transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionDTO)))
            .andExpect(status().isOk());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeUpdate);
        Transaction testTransaction = transactionList.get(transactionList.size() - 1);
        assertThat(testTransaction.getRequestId()).isEqualTo(UPDATED_REQUEST_ID);
        assertThat(testTransaction.getTrxIdMobiquity()).isEqualTo(UPDATED_TRX_ID_MOBIQUITY);
        assertThat(testTransaction.getTrxType()).isEqualTo(UPDATED_TRX_TYPE);
        assertThat(testTransaction.getTrxPartnerType()).isEqualTo(UPDATED_TRX_PARTNER_TYPE);
        assertThat(testTransaction.getTrxPartnerId()).isEqualTo(UPDATED_TRX_PARTNER_ID);
        assertThat(testTransaction.getTrxCustomerMSISDN()).isEqualTo(UPDATED_TRX_CUSTOMER_MSISDN);
        assertThat(testTransaction.getTrxCustomerAccountNumber()).isEqualTo(UPDATED_TRX_CUSTOMER_ACCOUNT_NUMBER);
        assertThat(testTransaction.getTrxAmount()).isEqualTo(UPDATED_TRX_AMOUNT);
        assertThat(testTransaction.getTrxChannel()).isEqualTo(UPDATED_TRX_CHANNEL);
        assertThat(testTransaction.getTrxDate()).isEqualTo(UPDATED_TRX_DATE);
        assertThat(testTransaction.getEligiblePromos()).isEqualTo(UPDATED_ELIGIBLE_PROMOS);
        assertThat(testTransaction.getTraitementStatus()).isEqualTo(UPDATED_TRAITEMENT_STATUS);
        assertThat(testTransaction.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testTransaction.getSpare2()).isEqualTo(UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    public void updateNonExistingTransaction() throws Exception {
        int databaseSizeBeforeUpdate = transactionRepository.findAll().size();

        // Create the Transaction
        TransactionDTO transactionDTO = transactionMapper.toDto(transaction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactionMockMvc.perform(put("/api/transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTransaction() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        int databaseSizeBeforeDelete = transactionRepository.findAll().size();

        // Delete the transaction
        restTransactionMockMvc.perform(delete("/api/transactions/{id}", transaction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Transaction.class);
        Transaction transaction1 = new Transaction();
        transaction1.setId(1L);
        Transaction transaction2 = new Transaction();
        transaction2.setId(transaction1.getId());
        assertThat(transaction1).isEqualTo(transaction2);
        transaction2.setId(2L);
        assertThat(transaction1).isNotEqualTo(transaction2);
        transaction1.setId(null);
        assertThat(transaction1).isNotEqualTo(transaction2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransactionDTO.class);
        TransactionDTO transactionDTO1 = new TransactionDTO();
        transactionDTO1.setId(1L);
        TransactionDTO transactionDTO2 = new TransactionDTO();
        assertThat(transactionDTO1).isNotEqualTo(transactionDTO2);
        transactionDTO2.setId(transactionDTO1.getId());
        assertThat(transactionDTO1).isEqualTo(transactionDTO2);
        transactionDTO2.setId(2L);
        assertThat(transactionDTO1).isNotEqualTo(transactionDTO2);
        transactionDTO1.setId(null);
        assertThat(transactionDTO1).isNotEqualTo(transactionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(transactionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(transactionMapper.fromId(null)).isNull();
    }*/
}
