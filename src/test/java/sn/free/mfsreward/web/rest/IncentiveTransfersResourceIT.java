package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.MfsrewardApp;
import sn.free.mfsreward.domain.IncentiveTransfers;
import sn.free.mfsreward.repository.IncentiveTransfersRepository;
import sn.free.mfsreward.service.IncentiveTransfersService;
import sn.free.mfsreward.service.dto.IncentiveTransfersDTO;
import sn.free.mfsreward.service.mapper.IncentiveTransfersMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.free.mfsreward.domain.enumeration.PaymentStatus;
/**
 * Integration tests for the {@link IncentiveTransfersResource} REST controller.
 */
@SpringBootTest(classes = MfsrewardApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class IncentiveTransfersResourceIT {

    private static final String DEFAULT_AGENT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_MSISDN = "BBBBBBBBBB";

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;

    private static final Instant DEFAULT_PAYMENT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_PAYMENT_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final PaymentStatus DEFAULT_PAYMENT_STATUS = PaymentStatus.PENDING;
    private static final PaymentStatus UPDATED_PAYMENT_STATUS = PaymentStatus.FAILED;

    private static final String DEFAULT_MOBIQUITY_RESPONSE = "AAAAAAAAAA";
    private static final String UPDATED_MOBIQUITY_RESPONSE = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_4 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_4 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_5 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_5 = "BBBBBBBBBB";

    @Autowired
    private IncentiveTransfersRepository incentiveTransfersRepository;

    @Autowired
    private IncentiveTransfersMapper incentiveTransfersMapper;

    @Autowired
    private IncentiveTransfersService incentiveTransfersService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restIncentiveTransfersMockMvc;

    private IncentiveTransfers incentiveTransfers;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IncentiveTransfers createEntity(EntityManager em) {
        IncentiveTransfers incentiveTransfers = new IncentiveTransfers()
            .agentMsisdn(DEFAULT_AGENT_MSISDN)
            .amount(DEFAULT_AMOUNT)
            .paymentDate(DEFAULT_PAYMENT_DATE)
            .paymentStatus(DEFAULT_PAYMENT_STATUS)
            .mobiquityResponse(DEFAULT_MOBIQUITY_RESPONSE)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3)
            .spare4(DEFAULT_SPARE_4)
            .spare5(DEFAULT_SPARE_5);
        return incentiveTransfers;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IncentiveTransfers createUpdatedEntity(EntityManager em) {
        IncentiveTransfers incentiveTransfers = new IncentiveTransfers()
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .amount(UPDATED_AMOUNT)
            .paymentDate(UPDATED_PAYMENT_DATE)
            .paymentStatus(UPDATED_PAYMENT_STATUS)
            .mobiquityResponse(UPDATED_MOBIQUITY_RESPONSE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        return incentiveTransfers;
    }

    @BeforeEach
    public void initTest() {
        incentiveTransfers = createEntity(em);
    }

    @Test
    @Transactional
    public void createIncentiveTransfers() throws Exception {
        int databaseSizeBeforeCreate = incentiveTransfersRepository.findAll().size();
        // Create the IncentiveTransfers
        IncentiveTransfersDTO incentiveTransfersDTO = incentiveTransfersMapper.toDto(incentiveTransfers);
        restIncentiveTransfersMockMvc.perform(post("/api/incentive-transfers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(incentiveTransfersDTO)))
            .andExpect(status().isCreated());

        // Validate the IncentiveTransfers in the database
        List<IncentiveTransfers> incentiveTransfersList = incentiveTransfersRepository.findAll();
        assertThat(incentiveTransfersList).hasSize(databaseSizeBeforeCreate + 1);
        IncentiveTransfers testIncentiveTransfers = incentiveTransfersList.get(incentiveTransfersList.size() - 1);
        assertThat(testIncentiveTransfers.getAgentMsisdn()).isEqualTo(DEFAULT_AGENT_MSISDN);
        assertThat(testIncentiveTransfers.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testIncentiveTransfers.getPaymentDate()).isEqualTo(DEFAULT_PAYMENT_DATE);
        assertThat(testIncentiveTransfers.getPaymentStatus()).isEqualTo(DEFAULT_PAYMENT_STATUS);
        assertThat(testIncentiveTransfers.getMobiquityResponse()).isEqualTo(DEFAULT_MOBIQUITY_RESPONSE);
        assertThat(testIncentiveTransfers.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testIncentiveTransfers.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testIncentiveTransfers.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
        assertThat(testIncentiveTransfers.getSpare4()).isEqualTo(DEFAULT_SPARE_4);
        assertThat(testIncentiveTransfers.getSpare5()).isEqualTo(DEFAULT_SPARE_5);
    }

    @Test
    @Transactional
    public void createIncentiveTransfersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = incentiveTransfersRepository.findAll().size();

        // Create the IncentiveTransfers with an existing ID
        incentiveTransfers.setId(1L);
        IncentiveTransfersDTO incentiveTransfersDTO = incentiveTransfersMapper.toDto(incentiveTransfers);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIncentiveTransfersMockMvc.perform(post("/api/incentive-transfers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(incentiveTransfersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the IncentiveTransfers in the database
        List<IncentiveTransfers> incentiveTransfersList = incentiveTransfersRepository.findAll();
        assertThat(incentiveTransfersList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllIncentiveTransfers() throws Exception {
        // Initialize the database
        incentiveTransfersRepository.saveAndFlush(incentiveTransfers);

        // Get all the incentiveTransfersList
        restIncentiveTransfersMockMvc.perform(get("/api/incentive-transfers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(incentiveTransfers.getId().intValue())))
            .andExpect(jsonPath("$.[*].agentMsisdn").value(hasItem(DEFAULT_AGENT_MSISDN)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].paymentDate").value(hasItem(DEFAULT_PAYMENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].paymentStatus").value(hasItem(DEFAULT_PAYMENT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].mobiquityResponse").value(hasItem(DEFAULT_MOBIQUITY_RESPONSE)))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)))
            .andExpect(jsonPath("$.[*].spare4").value(hasItem(DEFAULT_SPARE_4)))
            .andExpect(jsonPath("$.[*].spare5").value(hasItem(DEFAULT_SPARE_5)));
    }
    
    @Test
    @Transactional
    public void getIncentiveTransfers() throws Exception {
        // Initialize the database
        incentiveTransfersRepository.saveAndFlush(incentiveTransfers);

        // Get the incentiveTransfers
        restIncentiveTransfersMockMvc.perform(get("/api/incentive-transfers/{id}", incentiveTransfers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(incentiveTransfers.getId().intValue()))
            .andExpect(jsonPath("$.agentMsisdn").value(DEFAULT_AGENT_MSISDN))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.paymentDate").value(DEFAULT_PAYMENT_DATE.toString()))
            .andExpect(jsonPath("$.paymentStatus").value(DEFAULT_PAYMENT_STATUS.toString()))
            .andExpect(jsonPath("$.mobiquityResponse").value(DEFAULT_MOBIQUITY_RESPONSE))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3))
            .andExpect(jsonPath("$.spare4").value(DEFAULT_SPARE_4))
            .andExpect(jsonPath("$.spare5").value(DEFAULT_SPARE_5));
    }
    @Test
    @Transactional
    public void getNonExistingIncentiveTransfers() throws Exception {
        // Get the incentiveTransfers
        restIncentiveTransfersMockMvc.perform(get("/api/incentive-transfers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIncentiveTransfers() throws Exception {
        // Initialize the database
        incentiveTransfersRepository.saveAndFlush(incentiveTransfers);

        int databaseSizeBeforeUpdate = incentiveTransfersRepository.findAll().size();

        // Update the incentiveTransfers
        IncentiveTransfers updatedIncentiveTransfers = incentiveTransfersRepository.findById(incentiveTransfers.getId()).get();
        // Disconnect from session so that the updates on updatedIncentiveTransfers are not directly saved in db
        em.detach(updatedIncentiveTransfers);
        updatedIncentiveTransfers
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .amount(UPDATED_AMOUNT)
            .paymentDate(UPDATED_PAYMENT_DATE)
            .paymentStatus(UPDATED_PAYMENT_STATUS)
            .mobiquityResponse(UPDATED_MOBIQUITY_RESPONSE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        IncentiveTransfersDTO incentiveTransfersDTO = incentiveTransfersMapper.toDto(updatedIncentiveTransfers);

        restIncentiveTransfersMockMvc.perform(put("/api/incentive-transfers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(incentiveTransfersDTO)))
            .andExpect(status().isOk());

        // Validate the IncentiveTransfers in the database
        List<IncentiveTransfers> incentiveTransfersList = incentiveTransfersRepository.findAll();
        assertThat(incentiveTransfersList).hasSize(databaseSizeBeforeUpdate);
        IncentiveTransfers testIncentiveTransfers = incentiveTransfersList.get(incentiveTransfersList.size() - 1);
        assertThat(testIncentiveTransfers.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testIncentiveTransfers.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testIncentiveTransfers.getPaymentDate()).isEqualTo(UPDATED_PAYMENT_DATE);
        assertThat(testIncentiveTransfers.getPaymentStatus()).isEqualTo(UPDATED_PAYMENT_STATUS);
        assertThat(testIncentiveTransfers.getMobiquityResponse()).isEqualTo(UPDATED_MOBIQUITY_RESPONSE);
        assertThat(testIncentiveTransfers.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testIncentiveTransfers.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testIncentiveTransfers.getSpare3()).isEqualTo(UPDATED_SPARE_3);
        assertThat(testIncentiveTransfers.getSpare4()).isEqualTo(UPDATED_SPARE_4);
        assertThat(testIncentiveTransfers.getSpare5()).isEqualTo(UPDATED_SPARE_5);
    }

    @Test
    @Transactional
    public void updateNonExistingIncentiveTransfers() throws Exception {
        int databaseSizeBeforeUpdate = incentiveTransfersRepository.findAll().size();

        // Create the IncentiveTransfers
        IncentiveTransfersDTO incentiveTransfersDTO = incentiveTransfersMapper.toDto(incentiveTransfers);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIncentiveTransfersMockMvc.perform(put("/api/incentive-transfers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(incentiveTransfersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the IncentiveTransfers in the database
        List<IncentiveTransfers> incentiveTransfersList = incentiveTransfersRepository.findAll();
        assertThat(incentiveTransfersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIncentiveTransfers() throws Exception {
        // Initialize the database
        incentiveTransfersRepository.saveAndFlush(incentiveTransfers);

        int databaseSizeBeforeDelete = incentiveTransfersRepository.findAll().size();

        // Delete the incentiveTransfers
        restIncentiveTransfersMockMvc.perform(delete("/api/incentive-transfers/{id}", incentiveTransfers.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IncentiveTransfers> incentiveTransfersList = incentiveTransfersRepository.findAll();
        assertThat(incentiveTransfersList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
