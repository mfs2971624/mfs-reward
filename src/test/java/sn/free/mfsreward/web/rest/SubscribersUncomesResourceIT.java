package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.MfsrewardApp;
import sn.free.mfsreward.domain.SubscribersUncomes;
import sn.free.mfsreward.repository.SubscribersUncomesRepository;
import sn.free.mfsreward.service.SubscribersUncomesService;
import sn.free.mfsreward.service.dto.SubscribersUncomesDTO;
import sn.free.mfsreward.service.mapper.SubscribersUncomesMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SubscribersUncomesResource} REST controller.
 */
@SpringBootTest(classes = MfsrewardApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class SubscribersUncomesResourceIT {

    private static final String DEFAULT_SUBSCRIBER_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_SUBSCRIBER_MSISDN = "BBBBBBBBBB";

    private static final Instant DEFAULT_PROCESS_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_PROCESS_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_UNCOME = 1D;
    private static final Double UPDATED_UNCOME = 2D;

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_4 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_4 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_5 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_5 = "BBBBBBBBBB";

    @Autowired
    private SubscribersUncomesRepository subscribersUncomesRepository;

    @Autowired
    private SubscribersUncomesMapper subscribersUncomesMapper;

    @Autowired
    private SubscribersUncomesService subscribersUncomesService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSubscribersUncomesMockMvc;

    private SubscribersUncomes subscribersUncomes;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubscribersUncomes createEntity(EntityManager em) {
        SubscribersUncomes subscribersUncomes = new SubscribersUncomes()
            .subscriberMsisdn(DEFAULT_SUBSCRIBER_MSISDN)
            .processDate(DEFAULT_PROCESS_DATE)
            .uncome(DEFAULT_UNCOME)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3)
            .spare4(DEFAULT_SPARE_4)
            .spare5(DEFAULT_SPARE_5);
        return subscribersUncomes;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubscribersUncomes createUpdatedEntity(EntityManager em) {
        SubscribersUncomes subscribersUncomes = new SubscribersUncomes()
            .subscriberMsisdn(UPDATED_SUBSCRIBER_MSISDN)
            .processDate(UPDATED_PROCESS_DATE)
            .uncome(UPDATED_UNCOME)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        return subscribersUncomes;
    }

    @BeforeEach
    public void initTest() {
        subscribersUncomes = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubscribersUncomes() throws Exception {
        int databaseSizeBeforeCreate = subscribersUncomesRepository.findAll().size();
        // Create the SubscribersUncomes
        SubscribersUncomesDTO subscribersUncomesDTO = subscribersUncomesMapper.toDto(subscribersUncomes);
        restSubscribersUncomesMockMvc.perform(post("/api/subscribers-uncomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subscribersUncomesDTO)))
            .andExpect(status().isCreated());

        // Validate the SubscribersUncomes in the database
        List<SubscribersUncomes> subscribersUncomesList = subscribersUncomesRepository.findAll();
        assertThat(subscribersUncomesList).hasSize(databaseSizeBeforeCreate + 1);
        SubscribersUncomes testSubscribersUncomes = subscribersUncomesList.get(subscribersUncomesList.size() - 1);
        assertThat(testSubscribersUncomes.getSubscriberMsisdn()).isEqualTo(DEFAULT_SUBSCRIBER_MSISDN);
        assertThat(testSubscribersUncomes.getProcessDate()).isEqualTo(DEFAULT_PROCESS_DATE);
        assertThat(testSubscribersUncomes.getUncome()).isEqualTo(DEFAULT_UNCOME);
        assertThat(testSubscribersUncomes.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testSubscribersUncomes.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testSubscribersUncomes.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
        assertThat(testSubscribersUncomes.getSpare4()).isEqualTo(DEFAULT_SPARE_4);
        assertThat(testSubscribersUncomes.getSpare5()).isEqualTo(DEFAULT_SPARE_5);
    }

    @Test
    @Transactional
    public void createSubscribersUncomesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subscribersUncomesRepository.findAll().size();

        // Create the SubscribersUncomes with an existing ID
        subscribersUncomes.setId(1L);
        SubscribersUncomesDTO subscribersUncomesDTO = subscribersUncomesMapper.toDto(subscribersUncomes);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubscribersUncomesMockMvc.perform(post("/api/subscribers-uncomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subscribersUncomesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubscribersUncomes in the database
        List<SubscribersUncomes> subscribersUncomesList = subscribersUncomesRepository.findAll();
        assertThat(subscribersUncomesList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSubscribersUncomes() throws Exception {
        // Initialize the database
        subscribersUncomesRepository.saveAndFlush(subscribersUncomes);

        // Get all the subscribersUncomesList
        restSubscribersUncomesMockMvc.perform(get("/api/subscribers-uncomes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subscribersUncomes.getId().intValue())))
            .andExpect(jsonPath("$.[*].subscriberMsisdn").value(hasItem(DEFAULT_SUBSCRIBER_MSISDN)))
            .andExpect(jsonPath("$.[*].processDate").value(hasItem(DEFAULT_PROCESS_DATE.toString())))
            .andExpect(jsonPath("$.[*].uncome").value(hasItem(DEFAULT_UNCOME.doubleValue())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)))
            .andExpect(jsonPath("$.[*].spare4").value(hasItem(DEFAULT_SPARE_4)))
            .andExpect(jsonPath("$.[*].spare5").value(hasItem(DEFAULT_SPARE_5)));
    }
    
    @Test
    @Transactional
    public void getSubscribersUncomes() throws Exception {
        // Initialize the database
        subscribersUncomesRepository.saveAndFlush(subscribersUncomes);

        // Get the subscribersUncomes
        restSubscribersUncomesMockMvc.perform(get("/api/subscribers-uncomes/{id}", subscribersUncomes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(subscribersUncomes.getId().intValue()))
            .andExpect(jsonPath("$.subscriberMsisdn").value(DEFAULT_SUBSCRIBER_MSISDN))
            .andExpect(jsonPath("$.processDate").value(DEFAULT_PROCESS_DATE.toString()))
            .andExpect(jsonPath("$.uncome").value(DEFAULT_UNCOME.doubleValue()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3))
            .andExpect(jsonPath("$.spare4").value(DEFAULT_SPARE_4))
            .andExpect(jsonPath("$.spare5").value(DEFAULT_SPARE_5));
    }
    @Test
    @Transactional
    public void getNonExistingSubscribersUncomes() throws Exception {
        // Get the subscribersUncomes
        restSubscribersUncomesMockMvc.perform(get("/api/subscribers-uncomes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubscribersUncomes() throws Exception {
        // Initialize the database
        subscribersUncomesRepository.saveAndFlush(subscribersUncomes);

        int databaseSizeBeforeUpdate = subscribersUncomesRepository.findAll().size();

        // Update the subscribersUncomes
        SubscribersUncomes updatedSubscribersUncomes = subscribersUncomesRepository.findById(subscribersUncomes.getId()).get();
        // Disconnect from session so that the updates on updatedSubscribersUncomes are not directly saved in db
        em.detach(updatedSubscribersUncomes);
        updatedSubscribersUncomes
            .subscriberMsisdn(UPDATED_SUBSCRIBER_MSISDN)
            .processDate(UPDATED_PROCESS_DATE)
            .uncome(UPDATED_UNCOME)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        SubscribersUncomesDTO subscribersUncomesDTO = subscribersUncomesMapper.toDto(updatedSubscribersUncomes);

        restSubscribersUncomesMockMvc.perform(put("/api/subscribers-uncomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subscribersUncomesDTO)))
            .andExpect(status().isOk());

        // Validate the SubscribersUncomes in the database
        List<SubscribersUncomes> subscribersUncomesList = subscribersUncomesRepository.findAll();
        assertThat(subscribersUncomesList).hasSize(databaseSizeBeforeUpdate);
        SubscribersUncomes testSubscribersUncomes = subscribersUncomesList.get(subscribersUncomesList.size() - 1);
        assertThat(testSubscribersUncomes.getSubscriberMsisdn()).isEqualTo(UPDATED_SUBSCRIBER_MSISDN);
        assertThat(testSubscribersUncomes.getProcessDate()).isEqualTo(UPDATED_PROCESS_DATE);
        assertThat(testSubscribersUncomes.getUncome()).isEqualTo(UPDATED_UNCOME);
        assertThat(testSubscribersUncomes.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testSubscribersUncomes.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testSubscribersUncomes.getSpare3()).isEqualTo(UPDATED_SPARE_3);
        assertThat(testSubscribersUncomes.getSpare4()).isEqualTo(UPDATED_SPARE_4);
        assertThat(testSubscribersUncomes.getSpare5()).isEqualTo(UPDATED_SPARE_5);
    }

    @Test
    @Transactional
    public void updateNonExistingSubscribersUncomes() throws Exception {
        int databaseSizeBeforeUpdate = subscribersUncomesRepository.findAll().size();

        // Create the SubscribersUncomes
        SubscribersUncomesDTO subscribersUncomesDTO = subscribersUncomesMapper.toDto(subscribersUncomes);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubscribersUncomesMockMvc.perform(put("/api/subscribers-uncomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subscribersUncomesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubscribersUncomes in the database
        List<SubscribersUncomes> subscribersUncomesList = subscribersUncomesRepository.findAll();
        assertThat(subscribersUncomesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSubscribersUncomes() throws Exception {
        // Initialize the database
        subscribersUncomesRepository.saveAndFlush(subscribersUncomes);

        int databaseSizeBeforeDelete = subscribersUncomesRepository.findAll().size();

        // Delete the subscribersUncomes
        restSubscribersUncomesMockMvc.perform(delete("/api/subscribers-uncomes/{id}", subscribersUncomes.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SubscribersUncomes> subscribersUncomesList = subscribersUncomesRepository.findAll();
        assertThat(subscribersUncomesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
