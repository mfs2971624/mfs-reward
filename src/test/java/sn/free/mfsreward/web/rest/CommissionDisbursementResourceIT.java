package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.MfsrewardApp;
import sn.free.mfsreward.domain.CommissionDisbursement;
import sn.free.mfsreward.repository.CommissionDisbursementRepository;
import sn.free.mfsreward.service.CommissionDisbursementService;
import sn.free.mfsreward.service.dto.CommissionDisbursementDTO;
import sn.free.mfsreward.service.mapper.CommissionDisbursementMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static sn.free.mfsreward.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.free.mfsreward.domain.enumeration.ProcessingStatus;
/**
 * Integration tests for the {@link CommissionDisbursementResource} REST controller.
 */
@SpringBootTest(classes = MfsrewardApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CommissionDisbursementResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_START_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_VALIDATED = false;
    private static final Boolean UPDATED_VALIDATED = true;

    private static final ZonedDateTime DEFAULT_VALIDATION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VALIDATION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ProcessingStatus DEFAULT_PROCESSING_STATUS = ProcessingStatus.INITIATED;
    private static final ProcessingStatus UPDATED_PROCESSING_STATUS = ProcessingStatus.VALIDATED;

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_4 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_4 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_5 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_5 = "BBBBBBBBBB";

    @Autowired
    private CommissionDisbursementRepository commissionDisbursementRepository;

    @Autowired
    private CommissionDisbursementMapper commissionDisbursementMapper;

    @Autowired
    private CommissionDisbursementService commissionDisbursementService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCommissionDisbursementMockMvc;

    private CommissionDisbursement commissionDisbursement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommissionDisbursement createEntity(EntityManager em) {
        CommissionDisbursement commissionDisbursement = new CommissionDisbursement()
            .name(DEFAULT_NAME)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .validated(DEFAULT_VALIDATED)
            .validationDate(DEFAULT_VALIDATION_DATE)
            .processingStatus(DEFAULT_PROCESSING_STATUS)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3)
            .spare4(DEFAULT_SPARE_4)
            .spare5(DEFAULT_SPARE_5);
        return commissionDisbursement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommissionDisbursement createUpdatedEntity(EntityManager em) {
        CommissionDisbursement commissionDisbursement = new CommissionDisbursement()
            .name(UPDATED_NAME)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .validated(UPDATED_VALIDATED)
            .validationDate(UPDATED_VALIDATION_DATE)
            .processingStatus(UPDATED_PROCESSING_STATUS)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        return commissionDisbursement;
    }

    @BeforeEach
    public void initTest() {
        commissionDisbursement = createEntity(em);
    }

    @Test
    @Transactional
    public void createCommissionDisbursement() throws Exception {
        int databaseSizeBeforeCreate = commissionDisbursementRepository.findAll().size();
        // Create the CommissionDisbursement
        CommissionDisbursementDTO commissionDisbursementDTO = commissionDisbursementMapper.toDto(commissionDisbursement);
        restCommissionDisbursementMockMvc.perform(post("/api/commission-disbursements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(commissionDisbursementDTO)))
            .andExpect(status().isCreated());

        // Validate the CommissionDisbursement in the database
        List<CommissionDisbursement> commissionDisbursementList = commissionDisbursementRepository.findAll();
        assertThat(commissionDisbursementList).hasSize(databaseSizeBeforeCreate + 1);
        CommissionDisbursement testCommissionDisbursement = commissionDisbursementList.get(commissionDisbursementList.size() - 1);
        assertThat(testCommissionDisbursement.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCommissionDisbursement.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testCommissionDisbursement.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testCommissionDisbursement.isValidated()).isEqualTo(DEFAULT_VALIDATED);
        assertThat(testCommissionDisbursement.getValidationDate()).isEqualTo(DEFAULT_VALIDATION_DATE);
        assertThat(testCommissionDisbursement.getProcessingStatus()).isEqualTo(DEFAULT_PROCESSING_STATUS);
        assertThat(testCommissionDisbursement.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testCommissionDisbursement.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testCommissionDisbursement.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
        assertThat(testCommissionDisbursement.getSpare4()).isEqualTo(DEFAULT_SPARE_4);
        assertThat(testCommissionDisbursement.getSpare5()).isEqualTo(DEFAULT_SPARE_5);
    }

    @Test
    @Transactional
    public void createCommissionDisbursementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = commissionDisbursementRepository.findAll().size();

        // Create the CommissionDisbursement with an existing ID
        commissionDisbursement.setId(1L);
        CommissionDisbursementDTO commissionDisbursementDTO = commissionDisbursementMapper.toDto(commissionDisbursement);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommissionDisbursementMockMvc.perform(post("/api/commission-disbursements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(commissionDisbursementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CommissionDisbursement in the database
        List<CommissionDisbursement> commissionDisbursementList = commissionDisbursementRepository.findAll();
        assertThat(commissionDisbursementList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCommissionDisbursements() throws Exception {
        // Initialize the database
        commissionDisbursementRepository.saveAndFlush(commissionDisbursement);

        // Get all the commissionDisbursementList
        restCommissionDisbursementMockMvc.perform(get("/api/commission-disbursements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commissionDisbursement.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].validated").value(hasItem(DEFAULT_VALIDATED.booleanValue())))
            .andExpect(jsonPath("$.[*].validationDate").value(hasItem(sameInstant(DEFAULT_VALIDATION_DATE))))
            .andExpect(jsonPath("$.[*].processingStatus").value(hasItem(DEFAULT_PROCESSING_STATUS.toString())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)))
            .andExpect(jsonPath("$.[*].spare4").value(hasItem(DEFAULT_SPARE_4)))
            .andExpect(jsonPath("$.[*].spare5").value(hasItem(DEFAULT_SPARE_5)));
    }
    
    @Test
    @Transactional
    public void getCommissionDisbursement() throws Exception {
        // Initialize the database
        commissionDisbursementRepository.saveAndFlush(commissionDisbursement);

        // Get the commissionDisbursement
        restCommissionDisbursementMockMvc.perform(get("/api/commission-disbursements/{id}", commissionDisbursement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(commissionDisbursement.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.validated").value(DEFAULT_VALIDATED.booleanValue()))
            .andExpect(jsonPath("$.validationDate").value(sameInstant(DEFAULT_VALIDATION_DATE)))
            .andExpect(jsonPath("$.processingStatus").value(DEFAULT_PROCESSING_STATUS.toString()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3))
            .andExpect(jsonPath("$.spare4").value(DEFAULT_SPARE_4))
            .andExpect(jsonPath("$.spare5").value(DEFAULT_SPARE_5));
    }
    @Test
    @Transactional
    public void getNonExistingCommissionDisbursement() throws Exception {
        // Get the commissionDisbursement
        restCommissionDisbursementMockMvc.perform(get("/api/commission-disbursements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCommissionDisbursement() throws Exception {
        // Initialize the database
        commissionDisbursementRepository.saveAndFlush(commissionDisbursement);

        int databaseSizeBeforeUpdate = commissionDisbursementRepository.findAll().size();

        // Update the commissionDisbursement
        CommissionDisbursement updatedCommissionDisbursement = commissionDisbursementRepository.findById(commissionDisbursement.getId()).get();
        // Disconnect from session so that the updates on updatedCommissionDisbursement are not directly saved in db
        em.detach(updatedCommissionDisbursement);
        updatedCommissionDisbursement
            .name(UPDATED_NAME)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .validated(UPDATED_VALIDATED)
            .validationDate(UPDATED_VALIDATION_DATE)
            .processingStatus(UPDATED_PROCESSING_STATUS)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        CommissionDisbursementDTO commissionDisbursementDTO = commissionDisbursementMapper.toDto(updatedCommissionDisbursement);

        restCommissionDisbursementMockMvc.perform(put("/api/commission-disbursements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(commissionDisbursementDTO)))
            .andExpect(status().isOk());

        // Validate the CommissionDisbursement in the database
        List<CommissionDisbursement> commissionDisbursementList = commissionDisbursementRepository.findAll();
        assertThat(commissionDisbursementList).hasSize(databaseSizeBeforeUpdate);
        CommissionDisbursement testCommissionDisbursement = commissionDisbursementList.get(commissionDisbursementList.size() - 1);
        assertThat(testCommissionDisbursement.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCommissionDisbursement.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testCommissionDisbursement.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testCommissionDisbursement.isValidated()).isEqualTo(UPDATED_VALIDATED);
        assertThat(testCommissionDisbursement.getValidationDate()).isEqualTo(UPDATED_VALIDATION_DATE);
        assertThat(testCommissionDisbursement.getProcessingStatus()).isEqualTo(UPDATED_PROCESSING_STATUS);
        assertThat(testCommissionDisbursement.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommissionDisbursement.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommissionDisbursement.getSpare3()).isEqualTo(UPDATED_SPARE_3);
        assertThat(testCommissionDisbursement.getSpare4()).isEqualTo(UPDATED_SPARE_4);
        assertThat(testCommissionDisbursement.getSpare5()).isEqualTo(UPDATED_SPARE_5);
    }

    @Test
    @Transactional
    public void updateNonExistingCommissionDisbursement() throws Exception {
        int databaseSizeBeforeUpdate = commissionDisbursementRepository.findAll().size();

        // Create the CommissionDisbursement
        CommissionDisbursementDTO commissionDisbursementDTO = commissionDisbursementMapper.toDto(commissionDisbursement);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommissionDisbursementMockMvc.perform(put("/api/commission-disbursements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(commissionDisbursementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CommissionDisbursement in the database
        List<CommissionDisbursement> commissionDisbursementList = commissionDisbursementRepository.findAll();
        assertThat(commissionDisbursementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCommissionDisbursement() throws Exception {
        // Initialize the database
        commissionDisbursementRepository.saveAndFlush(commissionDisbursement);

        int databaseSizeBeforeDelete = commissionDisbursementRepository.findAll().size();

        // Delete the commissionDisbursement
        restCommissionDisbursementMockMvc.perform(delete("/api/commission-disbursements/{id}", commissionDisbursement.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CommissionDisbursement> commissionDisbursementList = commissionDisbursementRepository.findAll();
        assertThat(commissionDisbursementList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
