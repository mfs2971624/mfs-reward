package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.MfsrewardApp;
import sn.free.mfsreward.domain.BlacklistedAgents;
import sn.free.mfsreward.repository.BlacklistedAgentsRepository;
import sn.free.mfsreward.service.BlacklistedAgentsService;
import sn.free.mfsreward.service.dto.BlacklistedAgentsDTO;
import sn.free.mfsreward.service.mapper.BlacklistedAgentsMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static sn.free.mfsreward.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BlacklistedAgentsResource} REST controller.
 */
@SpringBootTest(classes = MfsrewardApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BlacklistedAgentsResourceIT {

    private static final String DEFAULT_AGENT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_MSISDN = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_BLACK_LISTED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_BLACK_LISTED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_BLACK_LISTED_BY = "AAAAAAAAAA";
    private static final String UPDATED_BLACK_LISTED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_AGENT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_4 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_4 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_5 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_5 = "BBBBBBBBBB";

    @Autowired
    private BlacklistedAgentsRepository blacklistedAgentsRepository;

    @Autowired
    private BlacklistedAgentsMapper blacklistedAgentsMapper;

    @Autowired
    private BlacklistedAgentsService blacklistedAgentsService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBlacklistedAgentsMockMvc;

    private BlacklistedAgents blacklistedAgents;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BlacklistedAgents createEntity(EntityManager em) {
        BlacklistedAgents blacklistedAgents = new BlacklistedAgents()
            .agentMsisdn(DEFAULT_AGENT_MSISDN)
            .blackListedDate(DEFAULT_BLACK_LISTED_DATE)
            .blackListedBy(DEFAULT_BLACK_LISTED_BY)
            .agentType(DEFAULT_AGENT_TYPE)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3)
            .spare4(DEFAULT_SPARE_4)
            .spare5(DEFAULT_SPARE_5);
        return blacklistedAgents;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BlacklistedAgents createUpdatedEntity(EntityManager em) {
        BlacklistedAgents blacklistedAgents = new BlacklistedAgents()
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .blackListedDate(UPDATED_BLACK_LISTED_DATE)
            .blackListedBy(UPDATED_BLACK_LISTED_BY)
            .agentType(UPDATED_AGENT_TYPE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        return blacklistedAgents;
    }

    @BeforeEach
    public void initTest() {
        blacklistedAgents = createEntity(em);
    }

    @Test
    @Transactional
    public void createBlacklistedAgents() throws Exception {
        int databaseSizeBeforeCreate = blacklistedAgentsRepository.findAll().size();
        // Create the BlacklistedAgents
        BlacklistedAgentsDTO blacklistedAgentsDTO = blacklistedAgentsMapper.toDto(blacklistedAgents);
        restBlacklistedAgentsMockMvc.perform(post("/api/blacklisted-agents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(blacklistedAgentsDTO)))
            .andExpect(status().isCreated());

        // Validate the BlacklistedAgents in the database
        List<BlacklistedAgents> blacklistedAgentsList = blacklistedAgentsRepository.findAll();
        assertThat(blacklistedAgentsList).hasSize(databaseSizeBeforeCreate + 1);
        BlacklistedAgents testBlacklistedAgents = blacklistedAgentsList.get(blacklistedAgentsList.size() - 1);
        assertThat(testBlacklistedAgents.getAgentMsisdn()).isEqualTo(DEFAULT_AGENT_MSISDN);
        assertThat(testBlacklistedAgents.getBlackListedDate()).isEqualTo(DEFAULT_BLACK_LISTED_DATE);
        assertThat(testBlacklistedAgents.getBlackListedBy()).isEqualTo(DEFAULT_BLACK_LISTED_BY);
        assertThat(testBlacklistedAgents.getAgentType()).isEqualTo(DEFAULT_AGENT_TYPE);
        assertThat(testBlacklistedAgents.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testBlacklistedAgents.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testBlacklistedAgents.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
        assertThat(testBlacklistedAgents.getSpare4()).isEqualTo(DEFAULT_SPARE_4);
        assertThat(testBlacklistedAgents.getSpare5()).isEqualTo(DEFAULT_SPARE_5);
    }

    @Test
    @Transactional
    public void createBlacklistedAgentsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = blacklistedAgentsRepository.findAll().size();

        // Create the BlacklistedAgents with an existing ID
        blacklistedAgents.setId(1L);
        BlacklistedAgentsDTO blacklistedAgentsDTO = blacklistedAgentsMapper.toDto(blacklistedAgents);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBlacklistedAgentsMockMvc.perform(post("/api/blacklisted-agents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(blacklistedAgentsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BlacklistedAgents in the database
        List<BlacklistedAgents> blacklistedAgentsList = blacklistedAgentsRepository.findAll();
        assertThat(blacklistedAgentsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBlacklistedAgents() throws Exception {
        // Initialize the database
        blacklistedAgentsRepository.saveAndFlush(blacklistedAgents);

        // Get all the blacklistedAgentsList
        restBlacklistedAgentsMockMvc.perform(get("/api/blacklisted-agents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(blacklistedAgents.getId().intValue())))
            .andExpect(jsonPath("$.[*].agentMsisdn").value(hasItem(DEFAULT_AGENT_MSISDN)))
            .andExpect(jsonPath("$.[*].blackListedDate").value(hasItem(sameInstant(DEFAULT_BLACK_LISTED_DATE))))
            .andExpect(jsonPath("$.[*].blackListedBy").value(hasItem(DEFAULT_BLACK_LISTED_BY)))
            .andExpect(jsonPath("$.[*].agentType").value(hasItem(DEFAULT_AGENT_TYPE)))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)))
            .andExpect(jsonPath("$.[*].spare4").value(hasItem(DEFAULT_SPARE_4)))
            .andExpect(jsonPath("$.[*].spare5").value(hasItem(DEFAULT_SPARE_5)));
    }
    
    @Test
    @Transactional
    public void getBlacklistedAgents() throws Exception {
        // Initialize the database
        blacklistedAgentsRepository.saveAndFlush(blacklistedAgents);

        // Get the blacklistedAgents
        restBlacklistedAgentsMockMvc.perform(get("/api/blacklisted-agents/{id}", blacklistedAgents.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(blacklistedAgents.getId().intValue()))
            .andExpect(jsonPath("$.agentMsisdn").value(DEFAULT_AGENT_MSISDN))
            .andExpect(jsonPath("$.blackListedDate").value(sameInstant(DEFAULT_BLACK_LISTED_DATE)))
            .andExpect(jsonPath("$.blackListedBy").value(DEFAULT_BLACK_LISTED_BY))
            .andExpect(jsonPath("$.agentType").value(DEFAULT_AGENT_TYPE))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3))
            .andExpect(jsonPath("$.spare4").value(DEFAULT_SPARE_4))
            .andExpect(jsonPath("$.spare5").value(DEFAULT_SPARE_5));
    }
    @Test
    @Transactional
    public void getNonExistingBlacklistedAgents() throws Exception {
        // Get the blacklistedAgents
        restBlacklistedAgentsMockMvc.perform(get("/api/blacklisted-agents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBlacklistedAgents() throws Exception {
        // Initialize the database
        blacklistedAgentsRepository.saveAndFlush(blacklistedAgents);

        int databaseSizeBeforeUpdate = blacklistedAgentsRepository.findAll().size();

        // Update the blacklistedAgents
        BlacklistedAgents updatedBlacklistedAgents = blacklistedAgentsRepository.findById(blacklistedAgents.getId()).get();
        // Disconnect from session so that the updates on updatedBlacklistedAgents are not directly saved in db
        em.detach(updatedBlacklistedAgents);
        updatedBlacklistedAgents
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .blackListedDate(UPDATED_BLACK_LISTED_DATE)
            .blackListedBy(UPDATED_BLACK_LISTED_BY)
            .agentType(UPDATED_AGENT_TYPE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        BlacklistedAgentsDTO blacklistedAgentsDTO = blacklistedAgentsMapper.toDto(updatedBlacklistedAgents);

        restBlacklistedAgentsMockMvc.perform(put("/api/blacklisted-agents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(blacklistedAgentsDTO)))
            .andExpect(status().isOk());

        // Validate the BlacklistedAgents in the database
        List<BlacklistedAgents> blacklistedAgentsList = blacklistedAgentsRepository.findAll();
        assertThat(blacklistedAgentsList).hasSize(databaseSizeBeforeUpdate);
        BlacklistedAgents testBlacklistedAgents = blacklistedAgentsList.get(blacklistedAgentsList.size() - 1);
        assertThat(testBlacklistedAgents.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testBlacklistedAgents.getBlackListedDate()).isEqualTo(UPDATED_BLACK_LISTED_DATE);
        assertThat(testBlacklistedAgents.getBlackListedBy()).isEqualTo(UPDATED_BLACK_LISTED_BY);
        assertThat(testBlacklistedAgents.getAgentType()).isEqualTo(UPDATED_AGENT_TYPE);
        assertThat(testBlacklistedAgents.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testBlacklistedAgents.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testBlacklistedAgents.getSpare3()).isEqualTo(UPDATED_SPARE_3);
        assertThat(testBlacklistedAgents.getSpare4()).isEqualTo(UPDATED_SPARE_4);
        assertThat(testBlacklistedAgents.getSpare5()).isEqualTo(UPDATED_SPARE_5);
    }

    @Test
    @Transactional
    public void updateNonExistingBlacklistedAgents() throws Exception {
        int databaseSizeBeforeUpdate = blacklistedAgentsRepository.findAll().size();

        // Create the BlacklistedAgents
        BlacklistedAgentsDTO blacklistedAgentsDTO = blacklistedAgentsMapper.toDto(blacklistedAgents);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBlacklistedAgentsMockMvc.perform(put("/api/blacklisted-agents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(blacklistedAgentsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BlacklistedAgents in the database
        List<BlacklistedAgents> blacklistedAgentsList = blacklistedAgentsRepository.findAll();
        assertThat(blacklistedAgentsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBlacklistedAgents() throws Exception {
        // Initialize the database
        blacklistedAgentsRepository.saveAndFlush(blacklistedAgents);

        int databaseSizeBeforeDelete = blacklistedAgentsRepository.findAll().size();

        // Delete the blacklistedAgents
        restBlacklistedAgentsMockMvc.perform(delete("/api/blacklisted-agents/{id}", blacklistedAgents.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BlacklistedAgents> blacklistedAgentsList = blacklistedAgentsRepository.findAll();
        assertThat(blacklistedAgentsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
