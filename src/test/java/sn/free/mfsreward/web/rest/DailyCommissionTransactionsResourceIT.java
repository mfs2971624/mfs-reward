package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.MfsrewardApp;
import sn.free.mfsreward.domain.DailyCommissionTransactions;
import sn.free.mfsreward.repository.DailyCommissionTransactionsRepository;
import sn.free.mfsreward.service.DailyCommissionTransactionsService;
import sn.free.mfsreward.service.dto.DailyCommissionTransactionsDTO;
import sn.free.mfsreward.service.mapper.DailyCommissionTransactionsMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DailyCommissionTransactionsResource} REST controller.
 */
@SpringBootTest(classes = MfsrewardApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class DailyCommissionTransactionsResourceIT {

    private static final String DEFAULT_AGENT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_MSISDN = "BBBBBBBBBB";

    private static final Instant DEFAULT_PROCESS_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_PROCESS_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_TOTAL_TRANSACTIONS_VOLUME = 1D;
    private static final Double UPDATED_TOTAL_TRANSACTIONS_VOLUME = 2D;

    private static final Double DEFAULT_TOTAL_COMMISSIONS = 1D;
    private static final Double UPDATED_TOTAL_COMMISSIONS = 2D;

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_4 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_4 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_5 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_5 = "BBBBBBBBBB";

    @Autowired
    private DailyCommissionTransactionsRepository dailyCommissionTransactionsRepository;

    @Autowired
    private DailyCommissionTransactionsMapper dailyCommissionTransactionsMapper;

    @Autowired
    private DailyCommissionTransactionsService dailyCommissionTransactionsService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDailyCommissionTransactionsMockMvc;

    private DailyCommissionTransactions dailyCommissionTransactions;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DailyCommissionTransactions createEntity(EntityManager em) {
        DailyCommissionTransactions dailyCommissionTransactions = new DailyCommissionTransactions()
            .agentMsisdn(DEFAULT_AGENT_MSISDN)
            .processDate(DEFAULT_PROCESS_DATE)
            .totalTransactionsVolume(DEFAULT_TOTAL_TRANSACTIONS_VOLUME)
            .totalCommissions(DEFAULT_TOTAL_COMMISSIONS)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3)
            .spare4(DEFAULT_SPARE_4)
            .spare5(DEFAULT_SPARE_5);
        return dailyCommissionTransactions;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DailyCommissionTransactions createUpdatedEntity(EntityManager em) {
        DailyCommissionTransactions dailyCommissionTransactions = new DailyCommissionTransactions()
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .processDate(UPDATED_PROCESS_DATE)
            .totalTransactionsVolume(UPDATED_TOTAL_TRANSACTIONS_VOLUME)
            .totalCommissions(UPDATED_TOTAL_COMMISSIONS)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        return dailyCommissionTransactions;
    }

    @BeforeEach
    public void initTest() {
        dailyCommissionTransactions = createEntity(em);
    }

    @Test
    @Transactional
    public void createDailyCommissionTransactions() throws Exception {
        int databaseSizeBeforeCreate = dailyCommissionTransactionsRepository.findAll().size();
        // Create the DailyCommissionTransactions
        DailyCommissionTransactionsDTO dailyCommissionTransactionsDTO = dailyCommissionTransactionsMapper.toDto(dailyCommissionTransactions);
        restDailyCommissionTransactionsMockMvc.perform(post("/api/daily-commission-transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dailyCommissionTransactionsDTO)))
            .andExpect(status().isCreated());

        // Validate the DailyCommissionTransactions in the database
        List<DailyCommissionTransactions> dailyCommissionTransactionsList = dailyCommissionTransactionsRepository.findAll();
        assertThat(dailyCommissionTransactionsList).hasSize(databaseSizeBeforeCreate + 1);
        DailyCommissionTransactions testDailyCommissionTransactions = dailyCommissionTransactionsList.get(dailyCommissionTransactionsList.size() - 1);
        assertThat(testDailyCommissionTransactions.getAgentMsisdn()).isEqualTo(DEFAULT_AGENT_MSISDN);
        assertThat(testDailyCommissionTransactions.getProcessDate()).isEqualTo(DEFAULT_PROCESS_DATE);
        assertThat(testDailyCommissionTransactions.getTotalTransactionsVolume()).isEqualTo(DEFAULT_TOTAL_TRANSACTIONS_VOLUME);
        assertThat(testDailyCommissionTransactions.getTotalCommissions()).isEqualTo(DEFAULT_TOTAL_COMMISSIONS);
        assertThat(testDailyCommissionTransactions.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testDailyCommissionTransactions.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testDailyCommissionTransactions.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
        assertThat(testDailyCommissionTransactions.getSpare4()).isEqualTo(DEFAULT_SPARE_4);
        assertThat(testDailyCommissionTransactions.getSpare5()).isEqualTo(DEFAULT_SPARE_5);
    }

    @Test
    @Transactional
    public void createDailyCommissionTransactionsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dailyCommissionTransactionsRepository.findAll().size();

        // Create the DailyCommissionTransactions with an existing ID
        dailyCommissionTransactions.setId(1L);
        DailyCommissionTransactionsDTO dailyCommissionTransactionsDTO = dailyCommissionTransactionsMapper.toDto(dailyCommissionTransactions);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDailyCommissionTransactionsMockMvc.perform(post("/api/daily-commission-transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dailyCommissionTransactionsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DailyCommissionTransactions in the database
        List<DailyCommissionTransactions> dailyCommissionTransactionsList = dailyCommissionTransactionsRepository.findAll();
        assertThat(dailyCommissionTransactionsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDailyCommissionTransactions() throws Exception {
        // Initialize the database
        dailyCommissionTransactionsRepository.saveAndFlush(dailyCommissionTransactions);

        // Get all the dailyCommissionTransactionsList
        restDailyCommissionTransactionsMockMvc.perform(get("/api/daily-commission-transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dailyCommissionTransactions.getId().intValue())))
            .andExpect(jsonPath("$.[*].agentMsisdn").value(hasItem(DEFAULT_AGENT_MSISDN)))
            .andExpect(jsonPath("$.[*].processDate").value(hasItem(DEFAULT_PROCESS_DATE.toString())))
            .andExpect(jsonPath("$.[*].totalTransactionsVolume").value(hasItem(DEFAULT_TOTAL_TRANSACTIONS_VOLUME.doubleValue())))
            .andExpect(jsonPath("$.[*].totalCommissions").value(hasItem(DEFAULT_TOTAL_COMMISSIONS.doubleValue())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)))
            .andExpect(jsonPath("$.[*].spare4").value(hasItem(DEFAULT_SPARE_4)))
            .andExpect(jsonPath("$.[*].spare5").value(hasItem(DEFAULT_SPARE_5)));
    }
    
    @Test
    @Transactional
    public void getDailyCommissionTransactions() throws Exception {
        // Initialize the database
        dailyCommissionTransactionsRepository.saveAndFlush(dailyCommissionTransactions);

        // Get the dailyCommissionTransactions
        restDailyCommissionTransactionsMockMvc.perform(get("/api/daily-commission-transactions/{id}", dailyCommissionTransactions.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dailyCommissionTransactions.getId().intValue()))
            .andExpect(jsonPath("$.agentMsisdn").value(DEFAULT_AGENT_MSISDN))
            .andExpect(jsonPath("$.processDate").value(DEFAULT_PROCESS_DATE.toString()))
            .andExpect(jsonPath("$.totalTransactionsVolume").value(DEFAULT_TOTAL_TRANSACTIONS_VOLUME.doubleValue()))
            .andExpect(jsonPath("$.totalCommissions").value(DEFAULT_TOTAL_COMMISSIONS.doubleValue()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3))
            .andExpect(jsonPath("$.spare4").value(DEFAULT_SPARE_4))
            .andExpect(jsonPath("$.spare5").value(DEFAULT_SPARE_5));
    }
    @Test
    @Transactional
    public void getNonExistingDailyCommissionTransactions() throws Exception {
        // Get the dailyCommissionTransactions
        restDailyCommissionTransactionsMockMvc.perform(get("/api/daily-commission-transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDailyCommissionTransactions() throws Exception {
        // Initialize the database
        dailyCommissionTransactionsRepository.saveAndFlush(dailyCommissionTransactions);

        int databaseSizeBeforeUpdate = dailyCommissionTransactionsRepository.findAll().size();

        // Update the dailyCommissionTransactions
        DailyCommissionTransactions updatedDailyCommissionTransactions = dailyCommissionTransactionsRepository.findById(dailyCommissionTransactions.getId()).get();
        // Disconnect from session so that the updates on updatedDailyCommissionTransactions are not directly saved in db
        em.detach(updatedDailyCommissionTransactions);
        updatedDailyCommissionTransactions
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .processDate(UPDATED_PROCESS_DATE)
            .totalTransactionsVolume(UPDATED_TOTAL_TRANSACTIONS_VOLUME)
            .totalCommissions(UPDATED_TOTAL_COMMISSIONS)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3)
            .spare4(UPDATED_SPARE_4)
            .spare5(UPDATED_SPARE_5);
        DailyCommissionTransactionsDTO dailyCommissionTransactionsDTO = dailyCommissionTransactionsMapper.toDto(updatedDailyCommissionTransactions);

        restDailyCommissionTransactionsMockMvc.perform(put("/api/daily-commission-transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dailyCommissionTransactionsDTO)))
            .andExpect(status().isOk());

        // Validate the DailyCommissionTransactions in the database
        List<DailyCommissionTransactions> dailyCommissionTransactionsList = dailyCommissionTransactionsRepository.findAll();
        assertThat(dailyCommissionTransactionsList).hasSize(databaseSizeBeforeUpdate);
        DailyCommissionTransactions testDailyCommissionTransactions = dailyCommissionTransactionsList.get(dailyCommissionTransactionsList.size() - 1);
        assertThat(testDailyCommissionTransactions.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testDailyCommissionTransactions.getProcessDate()).isEqualTo(UPDATED_PROCESS_DATE);
        assertThat(testDailyCommissionTransactions.getTotalTransactionsVolume()).isEqualTo(UPDATED_TOTAL_TRANSACTIONS_VOLUME);
        assertThat(testDailyCommissionTransactions.getTotalCommissions()).isEqualTo(UPDATED_TOTAL_COMMISSIONS);
        assertThat(testDailyCommissionTransactions.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testDailyCommissionTransactions.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testDailyCommissionTransactions.getSpare3()).isEqualTo(UPDATED_SPARE_3);
        assertThat(testDailyCommissionTransactions.getSpare4()).isEqualTo(UPDATED_SPARE_4);
        assertThat(testDailyCommissionTransactions.getSpare5()).isEqualTo(UPDATED_SPARE_5);
    }

    @Test
    @Transactional
    public void updateNonExistingDailyCommissionTransactions() throws Exception {
        int databaseSizeBeforeUpdate = dailyCommissionTransactionsRepository.findAll().size();

        // Create the DailyCommissionTransactions
        DailyCommissionTransactionsDTO dailyCommissionTransactionsDTO = dailyCommissionTransactionsMapper.toDto(dailyCommissionTransactions);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDailyCommissionTransactionsMockMvc.perform(put("/api/daily-commission-transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dailyCommissionTransactionsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DailyCommissionTransactions in the database
        List<DailyCommissionTransactions> dailyCommissionTransactionsList = dailyCommissionTransactionsRepository.findAll();
        assertThat(dailyCommissionTransactionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDailyCommissionTransactions() throws Exception {
        // Initialize the database
        dailyCommissionTransactionsRepository.saveAndFlush(dailyCommissionTransactions);

        int databaseSizeBeforeDelete = dailyCommissionTransactionsRepository.findAll().size();

        // Delete the dailyCommissionTransactions
        restDailyCommissionTransactionsMockMvc.perform(delete("/api/daily-commission-transactions/{id}", dailyCommissionTransactions.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DailyCommissionTransactions> dailyCommissionTransactionsList = dailyCommissionTransactionsRepository.findAll();
        assertThat(dailyCommissionTransactionsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
