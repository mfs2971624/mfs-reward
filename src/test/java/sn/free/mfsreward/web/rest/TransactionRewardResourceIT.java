package sn.free.mfsreward.web.rest;

import sn.free.mfsreward.MfsrewardApp;
import sn.free.mfsreward.domain.TransactionReward;
import sn.free.mfsreward.repository.TransactionRewardRepository;
import sn.free.mfsreward.service.TransactionRewardService;
import sn.free.mfsreward.service.dto.TransactionRewardDTO;
import sn.free.mfsreward.service.mapper.TransactionRewardMapper;
import sn.free.mfsreward.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static sn.free.mfsreward.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.free.mfsreward.domain.enumeration.StatutTransactionReward;
/**
 * Integration tests for the {@Link TransactionRewardResource} REST controller.
 */
@SpringBootTest(classes = MfsrewardApp.class)
public class TransactionRewardResourceIT {
//
//    private static final StatutTransactionReward DEFAULT_REWARD_STATUS = StatutTransactionReward.GRANTED;
//    private static final StatutTransactionReward UPDATED_REWARD_STATUS = StatutTransactionReward.THREESHOLD;
//
//    private static final String DEFAULT_REWARD_MESSAGE = "AAAAAAAAAA";
//    private static final String UPDATED_REWARD_MESSAGE = "BBBBBBBBBB";
//
//    private static final Double DEFAULT_REWARD_VALUE = 1D;
//    private static final Double UPDATED_REWARD_VALUE = 2D;
//
//    private static final Instant DEFAULT_REWARD_DATE_TIME = Instant.ofEpochMilli(0L);
//    private static final Instant UPDATED_REWARD_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);
//
//    @Autowired
//    private TransactionRewardRepository transactionRewardRepository;
//
//    @Autowired
//    private TransactionRewardMapper transactionRewardMapper;
//
//    @Autowired
//    private TransactionRewardService transactionRewardService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restTransactionRewardMockMvc;
//
//    private TransactionReward transactionReward;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final TransactionRewardResource transactionRewardResource = new TransactionRewardResource(transactionRewardService);
//        this.restTransactionRewardMockMvc = MockMvcBuilders.standaloneSetup(transactionRewardResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static TransactionReward createEntity(EntityManager em) {
//        TransactionReward transactionReward = new TransactionReward()
//            .rewardStatus(DEFAULT_REWARD_STATUS)
//            .rewardMessage(DEFAULT_REWARD_MESSAGE)
//            .rewardValue(DEFAULT_REWARD_VALUE)
//            .rewardDateTime(DEFAULT_REWARD_DATE_TIME);
//        return transactionReward;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static TransactionReward createUpdatedEntity(EntityManager em) {
//        TransactionReward transactionReward = new TransactionReward()
//            .rewardStatus(UPDATED_REWARD_STATUS)
//            .rewardMessage(UPDATED_REWARD_MESSAGE)
//            .rewardValue(UPDATED_REWARD_VALUE)
//            .rewardDateTime(UPDATED_REWARD_DATE_TIME);
//        return transactionReward;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        transactionReward = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createTransactionReward() throws Exception {
//        int databaseSizeBeforeCreate = transactionRewardRepository.findAll().size();
//
//        // Create the TransactionReward
//        TransactionRewardDTO transactionRewardDTO = transactionRewardMapper.toDto(transactionReward);
//        restTransactionRewardMockMvc.perform(post("/api/transaction-rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(transactionRewardDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the TransactionReward in the database
//        List<TransactionReward> transactionRewardList = transactionRewardRepository.findAll();
//        assertThat(transactionRewardList).hasSize(databaseSizeBeforeCreate + 1);
//        TransactionReward testTransactionReward = transactionRewardList.get(transactionRewardList.size() - 1);
//        assertThat(testTransactionReward.getRewardStatus()).isEqualTo(DEFAULT_REWARD_STATUS);
//        assertThat(testTransactionReward.getRewardMessage()).isEqualTo(DEFAULT_REWARD_MESSAGE);
//        assertThat(testTransactionReward.getRewardValue()).isEqualTo(DEFAULT_REWARD_VALUE);
//        assertThat(testTransactionReward.getRewardDateTime()).isEqualTo(DEFAULT_REWARD_DATE_TIME);
//    }
//
//    @Test
//    @Transactional
//    public void createTransactionRewardWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = transactionRewardRepository.findAll().size();
//
//        // Create the TransactionReward with an existing ID
//        transactionReward.setId(1L);
//        TransactionRewardDTO transactionRewardDTO = transactionRewardMapper.toDto(transactionReward);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restTransactionRewardMockMvc.perform(post("/api/transaction-rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(transactionRewardDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the TransactionReward in the database
//        List<TransactionReward> transactionRewardList = transactionRewardRepository.findAll();
//        assertThat(transactionRewardList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void getAllTransactionRewards() throws Exception {
//        // Initialize the database
//        transactionRewardRepository.saveAndFlush(transactionReward);
//
//        // Get all the transactionRewardList
//        restTransactionRewardMockMvc.perform(get("/api/transaction-rewards?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(transactionReward.getId().intValue())))
//            .andExpect(jsonPath("$.[*].rewardStatus").value(hasItem(DEFAULT_REWARD_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].rewardMessage").value(hasItem(DEFAULT_REWARD_MESSAGE.toString())))
//            .andExpect(jsonPath("$.[*].rewardValue").value(hasItem(DEFAULT_REWARD_VALUE.doubleValue())))
//            .andExpect(jsonPath("$.[*].rewardDateTime").value(hasItem(DEFAULT_REWARD_DATE_TIME.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getTransactionReward() throws Exception {
//        // Initialize the database
//        transactionRewardRepository.saveAndFlush(transactionReward);
//
//        // Get the transactionReward
//        restTransactionRewardMockMvc.perform(get("/api/transaction-rewards/{id}", transactionReward.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(transactionReward.getId().intValue()))
//            .andExpect(jsonPath("$.rewardStatus").value(DEFAULT_REWARD_STATUS.toString()))
//            .andExpect(jsonPath("$.rewardMessage").value(DEFAULT_REWARD_MESSAGE.toString()))
//            .andExpect(jsonPath("$.rewardValue").value(DEFAULT_REWARD_VALUE.doubleValue()))
//            .andExpect(jsonPath("$.rewardDateTime").value(DEFAULT_REWARD_DATE_TIME.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingTransactionReward() throws Exception {
//        // Get the transactionReward
//        restTransactionRewardMockMvc.perform(get("/api/transaction-rewards/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateTransactionReward() throws Exception {
//        // Initialize the database
//        transactionRewardRepository.saveAndFlush(transactionReward);
//
//        int databaseSizeBeforeUpdate = transactionRewardRepository.findAll().size();
//
//        // Update the transactionReward
//        TransactionReward updatedTransactionReward = transactionRewardRepository.findById(transactionReward.getId()).get();
//        // Disconnect from session so that the updates on updatedTransactionReward are not directly saved in db
//        em.detach(updatedTransactionReward);
//        updatedTransactionReward
//            .rewardStatus(UPDATED_REWARD_STATUS)
//            .rewardMessage(UPDATED_REWARD_MESSAGE)
//            .rewardValue(UPDATED_REWARD_VALUE)
//            .rewardDateTime(UPDATED_REWARD_DATE_TIME);
//        TransactionRewardDTO transactionRewardDTO = transactionRewardMapper.toDto(updatedTransactionReward);
//
//        restTransactionRewardMockMvc.perform(put("/api/transaction-rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(transactionRewardDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the TransactionReward in the database
//        List<TransactionReward> transactionRewardList = transactionRewardRepository.findAll();
//        assertThat(transactionRewardList).hasSize(databaseSizeBeforeUpdate);
//        TransactionReward testTransactionReward = transactionRewardList.get(transactionRewardList.size() - 1);
//        assertThat(testTransactionReward.getRewardStatus()).isEqualTo(UPDATED_REWARD_STATUS);
//        assertThat(testTransactionReward.getRewardMessage()).isEqualTo(UPDATED_REWARD_MESSAGE);
//        assertThat(testTransactionReward.getRewardValue()).isEqualTo(UPDATED_REWARD_VALUE);
//        assertThat(testTransactionReward.getRewardDateTime()).isEqualTo(UPDATED_REWARD_DATE_TIME);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingTransactionReward() throws Exception {
//        int databaseSizeBeforeUpdate = transactionRewardRepository.findAll().size();
//
//        // Create the TransactionReward
//        TransactionRewardDTO transactionRewardDTO = transactionRewardMapper.toDto(transactionReward);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restTransactionRewardMockMvc.perform(put("/api/transaction-rewards")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(transactionRewardDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the TransactionReward in the database
//        List<TransactionReward> transactionRewardList = transactionRewardRepository.findAll();
//        assertThat(transactionRewardList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteTransactionReward() throws Exception {
//        // Initialize the database
//        transactionRewardRepository.saveAndFlush(transactionReward);
//
//        int databaseSizeBeforeDelete = transactionRewardRepository.findAll().size();
//
//        // Delete the transactionReward
//        restTransactionRewardMockMvc.perform(delete("/api/transaction-rewards/{id}", transactionReward.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<TransactionReward> transactionRewardList = transactionRewardRepository.findAll();
//        assertThat(transactionRewardList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(TransactionReward.class);
//        TransactionReward transactionReward1 = new TransactionReward();
//        transactionReward1.setId(1L);
//        TransactionReward transactionReward2 = new TransactionReward();
//        transactionReward2.setId(transactionReward1.getId());
//        assertThat(transactionReward1).isEqualTo(transactionReward2);
//        transactionReward2.setId(2L);
//        assertThat(transactionReward1).isNotEqualTo(transactionReward2);
//        transactionReward1.setId(null);
//        assertThat(transactionReward1).isNotEqualTo(transactionReward2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(TransactionRewardDTO.class);
//        TransactionRewardDTO transactionRewardDTO1 = new TransactionRewardDTO();
//        transactionRewardDTO1.setId(1L);
//        TransactionRewardDTO transactionRewardDTO2 = new TransactionRewardDTO();
//        assertThat(transactionRewardDTO1).isNotEqualTo(transactionRewardDTO2);
//        transactionRewardDTO2.setId(transactionRewardDTO1.getId());
//        assertThat(transactionRewardDTO1).isEqualTo(transactionRewardDTO2);
//        transactionRewardDTO2.setId(2L);
//        assertThat(transactionRewardDTO1).isNotEqualTo(transactionRewardDTO2);
//        transactionRewardDTO1.setId(null);
//        assertThat(transactionRewardDTO1).isNotEqualTo(transactionRewardDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(transactionRewardMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(transactionRewardMapper.fromId(null)).isNull();
//    }
}
