package sn.free.mfsreward.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class CommissionDisbursementTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommissionDisbursement.class);
        CommissionDisbursement commissionDisbursement1 = new CommissionDisbursement();
        commissionDisbursement1.setId(1L);
        CommissionDisbursement commissionDisbursement2 = new CommissionDisbursement();
        commissionDisbursement2.setId(commissionDisbursement1.getId());
        assertThat(commissionDisbursement1).isEqualTo(commissionDisbursement2);
        commissionDisbursement2.setId(2L);
        assertThat(commissionDisbursement1).isNotEqualTo(commissionDisbursement2);
        commissionDisbursement1.setId(null);
        assertThat(commissionDisbursement1).isNotEqualTo(commissionDisbursement2);
    }
}
