package sn.free.mfsreward.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class DailyCommissionsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DailyCommissions.class);
        DailyCommissions dailyCommissions1 = new DailyCommissions();
        dailyCommissions1.setId(1L);
        DailyCommissions dailyCommissions2 = new DailyCommissions();
        dailyCommissions2.setId(dailyCommissions1.getId());
        assertThat(dailyCommissions1).isEqualTo(dailyCommissions2);
        dailyCommissions2.setId(2L);
        assertThat(dailyCommissions1).isNotEqualTo(dailyCommissions2);
        dailyCommissions1.setId(null);
        assertThat(dailyCommissions1).isNotEqualTo(dailyCommissions2);
    }
}
