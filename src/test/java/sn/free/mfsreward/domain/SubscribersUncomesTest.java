package sn.free.mfsreward.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class SubscribersUncomesTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubscribersUncomes.class);
        SubscribersUncomes subscribersUncomes1 = new SubscribersUncomes();
        subscribersUncomes1.setId(1L);
        SubscribersUncomes subscribersUncomes2 = new SubscribersUncomes();
        subscribersUncomes2.setId(subscribersUncomes1.getId());
        assertThat(subscribersUncomes1).isEqualTo(subscribersUncomes2);
        subscribersUncomes2.setId(2L);
        assertThat(subscribersUncomes1).isNotEqualTo(subscribersUncomes2);
        subscribersUncomes1.setId(null);
        assertThat(subscribersUncomes1).isNotEqualTo(subscribersUncomes2);
    }
}
