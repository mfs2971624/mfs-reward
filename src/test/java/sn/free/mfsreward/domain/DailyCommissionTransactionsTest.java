package sn.free.mfsreward.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class DailyCommissionTransactionsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DailyCommissionTransactions.class);
        DailyCommissionTransactions dailyCommissionTransactions1 = new DailyCommissionTransactions();
        dailyCommissionTransactions1.setId(1L);
        DailyCommissionTransactions dailyCommissionTransactions2 = new DailyCommissionTransactions();
        dailyCommissionTransactions2.setId(dailyCommissionTransactions1.getId());
        assertThat(dailyCommissionTransactions1).isEqualTo(dailyCommissionTransactions2);
        dailyCommissionTransactions2.setId(2L);
        assertThat(dailyCommissionTransactions1).isNotEqualTo(dailyCommissionTransactions2);
        dailyCommissionTransactions1.setId(null);
        assertThat(dailyCommissionTransactions1).isNotEqualTo(dailyCommissionTransactions2);
    }
}
