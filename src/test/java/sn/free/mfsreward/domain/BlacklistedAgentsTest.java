package sn.free.mfsreward.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class BlacklistedAgentsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BlacklistedAgents.class);
        BlacklistedAgents blacklistedAgents1 = new BlacklistedAgents();
        blacklistedAgents1.setId(1L);
        BlacklistedAgents blacklistedAgents2 = new BlacklistedAgents();
        blacklistedAgents2.setId(blacklistedAgents1.getId());
        assertThat(blacklistedAgents1).isEqualTo(blacklistedAgents2);
        blacklistedAgents2.setId(2L);
        assertThat(blacklistedAgents1).isNotEqualTo(blacklistedAgents2);
        blacklistedAgents1.setId(null);
        assertThat(blacklistedAgents1).isNotEqualTo(blacklistedAgents2);
    }
}
