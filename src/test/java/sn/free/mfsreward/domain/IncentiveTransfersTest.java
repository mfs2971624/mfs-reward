package sn.free.mfsreward.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class IncentiveTransfersTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IncentiveTransfers.class);
        IncentiveTransfers incentiveTransfers1 = new IncentiveTransfers();
        incentiveTransfers1.setId(1L);
        IncentiveTransfers incentiveTransfers2 = new IncentiveTransfers();
        incentiveTransfers2.setId(incentiveTransfers1.getId());
        assertThat(incentiveTransfers1).isEqualTo(incentiveTransfers2);
        incentiveTransfers2.setId(2L);
        assertThat(incentiveTransfers1).isNotEqualTo(incentiveTransfers2);
        incentiveTransfers1.setId(null);
        assertThat(incentiveTransfers1).isNotEqualTo(incentiveTransfers2);
    }
}
