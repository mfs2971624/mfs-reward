package sn.free.mfsreward.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class BlacklistedAgentsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BlacklistedAgentsDTO.class);
        BlacklistedAgentsDTO blacklistedAgentsDTO1 = new BlacklistedAgentsDTO();
        blacklistedAgentsDTO1.setId(1L);
        BlacklistedAgentsDTO blacklistedAgentsDTO2 = new BlacklistedAgentsDTO();
        assertThat(blacklistedAgentsDTO1).isNotEqualTo(blacklistedAgentsDTO2);
        blacklistedAgentsDTO2.setId(blacklistedAgentsDTO1.getId());
        assertThat(blacklistedAgentsDTO1).isEqualTo(blacklistedAgentsDTO2);
        blacklistedAgentsDTO2.setId(2L);
        assertThat(blacklistedAgentsDTO1).isNotEqualTo(blacklistedAgentsDTO2);
        blacklistedAgentsDTO1.setId(null);
        assertThat(blacklistedAgentsDTO1).isNotEqualTo(blacklistedAgentsDTO2);
    }
}
