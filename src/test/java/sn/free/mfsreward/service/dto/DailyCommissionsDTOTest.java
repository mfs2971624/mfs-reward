package sn.free.mfsreward.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class DailyCommissionsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DailyCommissionsDTO.class);
        DailyCommissionsDTO dailyCommissionsDTO1 = new DailyCommissionsDTO();
        dailyCommissionsDTO1.setId(1L);
        DailyCommissionsDTO dailyCommissionsDTO2 = new DailyCommissionsDTO();
        assertThat(dailyCommissionsDTO1).isNotEqualTo(dailyCommissionsDTO2);
        dailyCommissionsDTO2.setId(dailyCommissionsDTO1.getId());
        assertThat(dailyCommissionsDTO1).isEqualTo(dailyCommissionsDTO2);
        dailyCommissionsDTO2.setId(2L);
        assertThat(dailyCommissionsDTO1).isNotEqualTo(dailyCommissionsDTO2);
        dailyCommissionsDTO1.setId(null);
        assertThat(dailyCommissionsDTO1).isNotEqualTo(dailyCommissionsDTO2);
    }
}
