package sn.free.mfsreward.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class IncentiveTransfersDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(IncentiveTransfersDTO.class);
        IncentiveTransfersDTO incentiveTransfersDTO1 = new IncentiveTransfersDTO();
        incentiveTransfersDTO1.setId(1L);
        IncentiveTransfersDTO incentiveTransfersDTO2 = new IncentiveTransfersDTO();
        assertThat(incentiveTransfersDTO1).isNotEqualTo(incentiveTransfersDTO2);
        incentiveTransfersDTO2.setId(incentiveTransfersDTO1.getId());
        assertThat(incentiveTransfersDTO1).isEqualTo(incentiveTransfersDTO2);
        incentiveTransfersDTO2.setId(2L);
        assertThat(incentiveTransfersDTO1).isNotEqualTo(incentiveTransfersDTO2);
        incentiveTransfersDTO1.setId(null);
        assertThat(incentiveTransfersDTO1).isNotEqualTo(incentiveTransfersDTO2);
    }
}
