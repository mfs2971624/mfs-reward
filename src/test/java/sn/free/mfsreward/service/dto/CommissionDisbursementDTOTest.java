package sn.free.mfsreward.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class CommissionDisbursementDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommissionDisbursementDTO.class);
        CommissionDisbursementDTO commissionDisbursementDTO1 = new CommissionDisbursementDTO();
        commissionDisbursementDTO1.setId(1L);
        CommissionDisbursementDTO commissionDisbursementDTO2 = new CommissionDisbursementDTO();
        assertThat(commissionDisbursementDTO1).isNotEqualTo(commissionDisbursementDTO2);
        commissionDisbursementDTO2.setId(commissionDisbursementDTO1.getId());
        assertThat(commissionDisbursementDTO1).isEqualTo(commissionDisbursementDTO2);
        commissionDisbursementDTO2.setId(2L);
        assertThat(commissionDisbursementDTO1).isNotEqualTo(commissionDisbursementDTO2);
        commissionDisbursementDTO1.setId(null);
        assertThat(commissionDisbursementDTO1).isNotEqualTo(commissionDisbursementDTO2);
    }
}
