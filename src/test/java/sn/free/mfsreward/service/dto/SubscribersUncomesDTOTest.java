package sn.free.mfsreward.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class SubscribersUncomesDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubscribersUncomesDTO.class);
        SubscribersUncomesDTO subscribersUncomesDTO1 = new SubscribersUncomesDTO();
        subscribersUncomesDTO1.setId(1L);
        SubscribersUncomesDTO subscribersUncomesDTO2 = new SubscribersUncomesDTO();
        assertThat(subscribersUncomesDTO1).isNotEqualTo(subscribersUncomesDTO2);
        subscribersUncomesDTO2.setId(subscribersUncomesDTO1.getId());
        assertThat(subscribersUncomesDTO1).isEqualTo(subscribersUncomesDTO2);
        subscribersUncomesDTO2.setId(2L);
        assertThat(subscribersUncomesDTO1).isNotEqualTo(subscribersUncomesDTO2);
        subscribersUncomesDTO1.setId(null);
        assertThat(subscribersUncomesDTO1).isNotEqualTo(subscribersUncomesDTO2);
    }
}
