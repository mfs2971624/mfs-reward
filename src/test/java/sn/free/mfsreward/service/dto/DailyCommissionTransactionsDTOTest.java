package sn.free.mfsreward.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class DailyCommissionTransactionsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DailyCommissionTransactionsDTO.class);
        DailyCommissionTransactionsDTO dailyCommissionTransactionsDTO1 = new DailyCommissionTransactionsDTO();
        dailyCommissionTransactionsDTO1.setId(1L);
        DailyCommissionTransactionsDTO dailyCommissionTransactionsDTO2 = new DailyCommissionTransactionsDTO();
        assertThat(dailyCommissionTransactionsDTO1).isNotEqualTo(dailyCommissionTransactionsDTO2);
        dailyCommissionTransactionsDTO2.setId(dailyCommissionTransactionsDTO1.getId());
        assertThat(dailyCommissionTransactionsDTO1).isEqualTo(dailyCommissionTransactionsDTO2);
        dailyCommissionTransactionsDTO2.setId(2L);
        assertThat(dailyCommissionTransactionsDTO1).isNotEqualTo(dailyCommissionTransactionsDTO2);
        dailyCommissionTransactionsDTO1.setId(null);
        assertThat(dailyCommissionTransactionsDTO1).isNotEqualTo(dailyCommissionTransactionsDTO2);
    }
}
