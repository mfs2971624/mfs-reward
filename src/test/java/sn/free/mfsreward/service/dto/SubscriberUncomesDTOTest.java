package sn.free.mfsreward.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfsreward.web.rest.TestUtil;

public class SubscriberUncomesDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubscriberUncomesDTO.class);
        SubscriberUncomesDTO subscriberUncomesDTO1 = new SubscriberUncomesDTO();
        subscriberUncomesDTO1.setId(1L);
        SubscriberUncomesDTO subscriberUncomesDTO2 = new SubscriberUncomesDTO();
        assertThat(subscriberUncomesDTO1).isNotEqualTo(subscriberUncomesDTO2);
        subscriberUncomesDTO2.setId(subscriberUncomesDTO1.getId());
        assertThat(subscriberUncomesDTO1).isEqualTo(subscriberUncomesDTO2);
        subscriberUncomesDTO2.setId(2L);
        assertThat(subscriberUncomesDTO1).isNotEqualTo(subscriberUncomesDTO2);
        subscriberUncomesDTO1.setId(null);
        assertThat(subscriberUncomesDTO1).isNotEqualTo(subscriberUncomesDTO2);
    }
}
