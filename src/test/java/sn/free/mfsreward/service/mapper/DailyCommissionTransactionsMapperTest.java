package sn.free.mfsreward.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DailyCommissionTransactionsMapperTest {

    private DailyCommissionTransactionsMapper dailyCommissionTransactionsMapper;

    @BeforeEach
    public void setUp() {
        dailyCommissionTransactionsMapper = new DailyCommissionTransactionsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(dailyCommissionTransactionsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(dailyCommissionTransactionsMapper.fromId(null)).isNull();
    }
}
