package sn.free.mfsreward.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CommissionDisbursementMapperTest {

    private CommissionDisbursementMapper commissionDisbursementMapper;

    @BeforeEach
    public void setUp() {
        commissionDisbursementMapper = new CommissionDisbursementMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(commissionDisbursementMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(commissionDisbursementMapper.fromId(null)).isNull();
    }
}
