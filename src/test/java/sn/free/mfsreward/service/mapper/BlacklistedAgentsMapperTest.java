package sn.free.mfsreward.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BlacklistedAgentsMapperTest {

    private BlacklistedAgentsMapper blacklistedAgentsMapper;

    @BeforeEach
    public void setUp() {
        blacklistedAgentsMapper = new BlacklistedAgentsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(blacklistedAgentsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(blacklistedAgentsMapper.fromId(null)).isNull();
    }
}
