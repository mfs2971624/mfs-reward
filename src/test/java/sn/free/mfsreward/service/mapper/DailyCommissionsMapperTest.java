package sn.free.mfsreward.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DailyCommissionsMapperTest {

    private DailyCommissionsMapper dailyCommissionsMapper;

    @BeforeEach
    public void setUp() {
        dailyCommissionsMapper = new DailyCommissionsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(dailyCommissionsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(dailyCommissionsMapper.fromId(null)).isNull();
    }
}
