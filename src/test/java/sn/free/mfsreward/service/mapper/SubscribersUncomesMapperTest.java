package sn.free.mfsreward.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SubscribersUncomesMapperTest {

    private SubscribersUncomesMapper subscribersUncomesMapper;

    @BeforeEach
    public void setUp() {
        subscribersUncomesMapper = new SubscribersUncomesMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(subscribersUncomesMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(subscribersUncomesMapper.fromId(null)).isNull();
    }
}
